/**
 * Webpack Dev Server
 * This file is used to run our local enviroment
 */
// import webpack from 'webpack'
// import WebpackDevServer from 'webpack-dev-server'
// import webpackConfig from './webpack.config.babel.js'
// import path from 'path'
// import UglifyJsPlugin from 'uglifyjs-webpack-plugin'

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const webpackConfig = require('./webpack.config.babel.js');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

/**
 * Always dev enviroment when running webpack dev server
 * There are other ways to do this, so feel free to do
 * whatever you find suites your taste
 */
const env = { dev: process.env.NODE_ENV};

const devServerConfig = {
  open: true,
  // disableHostCheck: true,
  contentBase: path.join(__dirname, '../../app/'),
  historyApiFallback: true, // Need historyApiFallback to be able to refresh on dynamic route
  stats: { colors: true },
  public: 'dev-web.myramed.in'
  // publicPath: '/__build__/'
};

const server = new WebpackDevServer(webpack(webpackConfig(env)), devServerConfig);

server.listen(3939, 'localhost');