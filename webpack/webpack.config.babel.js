const webpack           = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const Autoprefixer      = require('autoprefixer');
const path              = require('path');
const UglifyJsPlugin    = require('uglifyjs-webpack-plugin');
const Polyfill          = require('babel-polyfill')
const WebpackPwaManifest = require('webpack-pwa-manifest')
const SwPrecacheDevWebpackPlugin = require('sw-precache-webpack-dev-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


module.exports = env => {
  const ifProd      = plugin => env.prod ? plugin : undefined;
  const removeEmpty = array  => array.filter( p => !!p);
  const PATHS       = {
    app         : path.join(__dirname, '../app'),
    build       : path.join(__dirname, '../dist'),
    redux       : path.join(__dirname, '../app/redux'),
    utils       : path.join(__dirname, '../app/utils'),
    sharedStyle : path.join(__dirname, '../app/sharedStyle'),
    config      : path.join(__dirname, '../app/config'),
    containers  : path.join(__dirname, '../app/containers'),
    components  : path.join(__dirname, '../app/components'),
    partials    : path.join(__dirname, '../app/Partials'),
    helper      : path.join(__dirname, '../app/helpers'),
  }

  return {
    context: PATHS.app,
    entry: {
      'babel-polyfill': ['babel-polyfill'],
      app: PATHS.app,
      vendor: ['axios', 'react', 'react-dom', 'react-redux', 'react-router', 'redux', 'redux-thunk'],
      // 'service-worker': PATHS.app + '/service-worker.js'
    },

    output: {
      path          :  PATHS.build,
      filename      : '[name].js',
      chunkFilename : '[name].[hash].js',
      publicPath: './'
    },

    module : {
      rules  : [
        {
          test           : /\.jsx$|\.js$/,
          exclude        : /node_modules/,
          loader         : 'babel-loader',
          query          : {
          cacheDirectory : true,
          },
        },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            use: [
                    "file-loader?name=[name].[ext]",
                    // "file-loader?name=[namlse].[ext]&outputPath=sharedImage/&publicPath=sharedImage/",
                    "image-webpack-loader"
                ]
        },
        {
          test   : /\.json$/,
          loader : 'json-loader'
        },
        {
          test           : /\.(scss|css)$/,
          exclude        : [path.resolve(__dirname, "node_modules"), path.resolve(__dirname, "../app/sharedStyle")],
          use            : ExtractTextPlugin.extract('css-loader?localIdentName=[hash:base64:5]&modules&importLoaders=1!postcss-loader!autoprefixer-loader!sass-loader?outputStyle=expanded'),
        },
        {
          test           : /\.(scss|css)$/,
          include        : [path.resolve(__dirname, "node_modules"), path.resolve(__dirname, "../app/sharedStyle")],
          use            : [
                              'style-loader',
                              'css-loader?minimize&-autoprefixer',
                              'postcss-loader',
                              'sass-loader'
                            ]
        }
      ]
    },

    resolve : {
      alias : {
        $APP          : PATHS.app,
        $BUILD        : PATHS.build,
        $UTILS        : PATHS.utils,
        $REDUX        : PATHS.redux,
        $CONFIG       : PATHS.config,
        $SHAREDSTYLES : PATHS.sharedStyle,
        $COMPONENTS   : PATHS.components,
        $CONTAINERS   : PATHS.containers,
        $PARTIALS     : PATHS.partials,
        $HELPERS      : PATHS.helper
      },
      enforceExtension : false,
      extensions       : ['.js', '.jsx', '.css', '.json', '.jpg', '.png'],
      modules          : [path.resolve('.'), 'node_modules']
    },

    plugins: removeEmpty([
      // new BundleAnalyzerPlugin(),
      new ExtractTextPlugin( { filename: '[hash].style.css', allChunks: true }),

      new webpack.optimize.CommonsChunkPlugin({
          name: 'common',
          chunks: ['app', 'vendor'],
          filename: '[name].js',
      }),

      new webpack.optimize.CommonsChunkPlugin({
        children: true, 
        minChunks: 3,
        async: true
      }),

      // new HtmlWebpackPlugin({
      //   template : PATHS.app + '/public/offline.html',
      //   filename : 'offline.html',
      //   // inject   : 'body',
      //   favicon  : PATHS.app + '/favicon.ico',
      // }),

      new HtmlWebpackPlugin({
        template : PATHS.app + '/index.html',
        filename : 'index.html',
        inject   : 'body',
        favicon  : PATHS.app + '/favicon.ico',
      }),

      new WebpackPwaManifest({
        "name": "Myra Medicine",
        "short_name": "Myra",
        "display": "standalone",
        "orientation": "portrait",
        "background_color": "#FFF",
        "theme_color": "#6354D5",
        "start_url": "./page/home",
        "manifest_version": 2,
        "version": "0.1",
        "description": "Myra Medicines - The smartest and fastest way to get your medicines. ",
        "gcm_sender_id": "351753830114",
        "gcm_user_visible_only": true,
        filename: "manifest.json",
        inject: true,
        icons: [
          {
            "src": PATHS.app + "/sharedImage/logo_new.png",
            "sizes": "192x192",
            "type": "image/png"
          }, {
            "src": PATHS.app + "/sharedImage/logo_new.png",
            "sizes": "144x144",
            "type": "image/png"
          },{
            "src": PATHS.app + "/sharedImage/logo_new.png",
            "sizes": "120x120",
            "type": "image/png"
          }
        ]
      }),

      new SwPrecacheDevWebpackPlugin({
        cacheId: 'your-appcache-id',
        staticFileGlobs: [
          PATHS.build + '/*.js',
          PATHS.build + '/offline.html',
          PATHS.build + '/**.css',
        ],
        runtimeCaching: [{
          handler: 'cacheFirst',
          urlPattern: /https?:\/\/fonts.+/
        }],
        filename: '/mysw.js'
      }),

      ifProd(new UglifyJsPlugin({sourceMap: false, extractComments: true, cache: true, })),

      ifProd(new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }))
    ]),

    devtool   : 'inline-source-map'
  };
};