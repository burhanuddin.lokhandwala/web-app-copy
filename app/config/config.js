import React from 'react'
import {isLocalStorageSupported, createCookie, readCookie } from '../helpers/helperFunction'

//****** sentry setting start ****//

const sentryKey = 'be9299467f754acca363b71eb919c47d';
const sentryApp = '1222095';
const sentryURL = 'https://' + sentryKey + '@sentry.io/' + sentryApp ;

const _APP_INFO = {
	name: 'Web App',
	branch: 'Main',
	version: '2.0'
}

//****** sentry setting end ****//

const def_configs = {
	'server': {
		api: 'https://api.myramed.in/',
		serverMode: 'server',
	},

	'test':{
		// api: 'https://dev-r2d2.myramed.in/',
		api: 'https://api.myramed.in/',
		// api: 'https://dev-c3po.myramed.in/',
		serverMode: 'debug'
	},

	'debug':{
		// api: 'https://api.myramed.in/',
		// api: 'https://dev-r2d2.myramed.in/',
		api: 'https://dev-c3po.myramed.in/',
		// api: 'http://192.168.56.153:5000/',
		serverMode: 'devServer'
	},
}

let current_config = def_configs.debug

if(window.location.hostname == "test-web.myramed.in"){
	current_config = def_configs.test;
	Raven.config(sentryURL, {
		release: _APP_INFO.version,
		tags: {
			branch: _APP_INFO.branch,
			github_commit: 'check local error'
		}
	}).install();
}
else if((window.location.hostname == "myra.app") || (window.location.hostname == "web.myramed.in")){
	current_config = def_configs.server;
	Raven.config(sentryURL, {
		release: _APP_INFO.version,
		tags: {
			branch: _APP_INFO.branch,
			github_commit: 'web app error handler'
		}
	}).install();
}

let current_config_mode = {}

export function getConfigMode() {
	let token = ""
	if(isLocalStorageSupported()){
		if(localStorage.user){
			if(JSON.parse(localStorage.user).data){
				localStorage.user = JSON.stringify(JSON.parse(localStorage.user).data)
			}
			token = JSON.parse(localStorage.user).access_token
		}

	}else if(document.cookie){
		let user = readCookie('user')
		token = user ? JSON.parse(user).access_token : ""
	}else{
		alert('Please select Chrome browser')
		return current_config_mode = {
			network:{
				$api: current_config.api,
			},
			serverMode:{
				mode: current_config.serverMode
			},
			message: "Local storage not sporting",
		}

	}
	return  current_config_mode = {
		network:{
			$api: current_config.api,
			token: token,
			// token: '38f91077b44f4f98b07837735b2b8df2',
			user_info: (localStorage && localStorage.user) ? JSON.parse(localStorage.user).data : '',
		},
		serverMode:{
			mode: current_config.serverMode
		},
	}
}
