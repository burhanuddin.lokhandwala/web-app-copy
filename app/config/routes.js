import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Match,
  Redirect,
} from 'react-router-dom'
import history from './history'
import {getConfigMode} from './config'

function asyncComponent(getComponent) {
  return class AsyncComponent extends React.Component {
    static Component = null;
    state = { Component: AsyncComponent.Component };

    componentWillMount() {
      // alert("function call irrespective of path");
      if (!this.state.Component) {
        getComponent().then(Component => {
          AsyncComponent.Component = Component
          this.setState({ Component })
        })
      }
    }
    render() {
      
      const { Component } = this.state
      if (Component) {
        return <Component {...this.props} />
      }
      return null
    }
  }
}

const Cart                        = asyncComponent(() => import('$CONTAINERS/Cart/CartContainer').then(module => module.default))
const Login                       = asyncComponent(() => import('$CONTAINERS/Login/LoginContainer').then(module => module.default))
const MainContainer               = asyncComponent(() => import('$CONTAINERS/Main/MainContainer').then(module => module.default))
const OrderPrescriptionContainer  = asyncComponent(() => import('$CONTAINERS/OrderPrescription/OrderPrescriptionContainer').then(module => module.default))
const AccountContainer            = asyncComponent(() => import('$CONTAINERS/Account/AccountContainer').then(module  => module.default))
const UserAccountEditer           = asyncComponent(() => import('$CONTAINERS/Account/UserAccountEditer').then(module => module.default))
const PreviousOrderContainer      = asyncComponent(() => import('$CONTAINERS/PreviousOrder/PreviousOrderContainer').then(module => module.default))
const ProductCategoriesContainer  = asyncComponent(() => import('$CONTAINERS/OrderPrescription/ProductCategoriesContainer').then(module => module.default))
const AddLocationContainer        = asyncComponent(() => import('$CONTAINERS/AddLocation/AddLocationContainer').then(module => module.default))
const ImageSelectorContainer      = asyncComponent(() => import('$CONTAINERS/ImageSelector/ImageSelectorContainer').then(module => module.default))
const EditLocaintionContainer     = asyncComponent(() => import('$CONTAINERS/AddLocation/EditLocaintionContainer').then(module => module.default))
const PaymentSuccessContainer     = asyncComponent(() => import('$CONTAINERS/PaymentSuccess/PaymentSuccessContainer').then(module => module.default))
const DirectPaymentContainer      = asyncComponent(() => import('$CONTAINERS/PaymentMethod/DirectPaymentContainer').then(module => module.default))
const TrackOrderContainer         = asyncComponent(() => import('$CONTAINERS/TrackOrder/TrackOrderContainer').then(module => module.default))
const SubcategoryContainer        = asyncComponent(() => import('$CONTAINERS/OrderPrescription/SubcategoryContainer').then(module => module.default))
const OrderDetailView             = asyncComponent(() => import('$COMPONENTS/OrderDetailView/OrderDetailView').then(module => module.default))
const SelectLocationContainer     = asyncComponent(() => import('$CONTAINERS/SelectLocation/SelectLocationContainer').then(module => module.default))
const ScheduleOrderContainer      = asyncComponent(() => import('$CONTAINERS/Cart/ScheduleOrderContainer').then(module => module.default))
const RecomendedItemsContainer    = asyncComponent(() => import('$CONTAINERS/RecomendedItems/RecomendedItemsContainer').then(module => module.default))
const RecallOrderContainer        = asyncComponent(() => import('$CONTAINERS/RecallOrder/RecallOrderContainer').then(module => module.default))
const AttachPrescriptionContainer = asyncComponent(() => import('$CONTAINERS/AttachPrescription/AttachPrescriptionContainer').then(module => module.default))
const SearchContainer             = asyncComponent(() => import('$CONTAINERS/Search/SearchContainer').then(module => module.default))
const LocationListContainer       = asyncComponent(() => import('$CONTAINERS/AddLocation/LocationListContainer').then(module => module.default))
const PageContainer               = asyncComponent(() => import('$CONTAINERS/Page/PageContainer').then(module => module.default))
const WalletContainer             = asyncComponent(() => import('$CONTAINERS/Wallet/WalletContainer').then(module => module.default))

const App = (checkAuth) =>
  <Router>
    <div className = "main_centered_container">
      <Route path="/" component={MainContainer} />
      <div className = "full_width" >
        {<Route path="/prescription" component = {OrderPrescriptionContainer} />}
        <Route path="/login" component={Login} />
        <Route path="/cart" component={Cart} />
        <Route path="/accounts" component = {AccountContainer} />
        <Route path="/user" component = {UserAccountEditer} />
        <Route path="/product_categories" component = {ProductCategoriesContainer} />
        <Route path="/previoud_order" component = {PreviousOrderContainer} />
        <Route path="/search_location" component = {AddLocationContainer} />
        <Route path="/image_prescription" component = {ImageSelectorContainer} />
        <Route path="/edit_location" component = {EditLocaintionContainer} />
        <Route path="/select_location" component = {SelectLocationContainer}/>
        <Route path="/schedule_order" component = {ScheduleOrderContainer}/>
        <Route path="/recomended_items_for_you" component = {RecomendedItemsContainer} />
        <Route path="/recall_order" component = {RecallOrderContainer} />
        <Route path="/users/order/:order_id" component = {OrderDetailView} />
        <Route path="/attach/:access_token" component = {AttachPrescriptionContainer}/>
        <Route path="/sub_category/:subcat_name" component = {SubcategoryContainer} />
        <Route path="/users/reorder/:order_id" component = {OrderDetailView} />
        <Route path="/search_medicine" component = {SearchContainer}/>
        <Route path="/payments/:displayId" component = {DirectPaymentContainer} />
        <Route path="/track/:displayId" component = {TrackOrderContainer} />
        <Route path="/user_locations" component = {LocationListContainer} />
        {/*<Route path="/payments/paytm" component = {PaymentSuccessContainer} />*/}
        <Route path="/page/:pageNameOrID" component = {PageContainer} />
        <Route path="/user_wallet" component = {WalletContainer} />
      </div>
    </div>
  </Router>

export default App