import React, { Component } from 'react';
import * as style           from './style.scss'

export default class ChordingPopup extends Component {
	render() {
		return (
			<div className = {style.popover}>
				<div className = {style.header_text}>{this.props.header_text}</div>
				<div className = {style.body_text}>{this.props.bodyText}</div>
				{this.props.actionButton
					? (
					   <div className = {style.action_button_wrap}>
					   		<span onClick = {this.props.hideAlert} className = {`${style.button_padding} left`}>DISMISS</span>
					   		<span onClick = {this.props.actionAlert} className = {`${style.enable_button} ${style.button_padding} right`}>ENABLE LOCATION</span>
					   </div>
					): ""
				}
			</div>
		);
	}
}
