import React, {Component} from 'react'
import * as style  from './style.scss'
import {getFormattedString} from '../../helpers/helperFunction'

export default class SelectBox extends Component {

 	optionHandler = (e) => {
 		// this.setState({currentSelected: e.target.value})
 		this.props.handleChangeOption(e.target.value)
 	}

	render() {
		let optionList = this.props.optionList, options = '';
		if(optionList.length){
			options = optionList.map((value, index) => {
				return (<option  value = {this.props.indexValue ? index : (this.props.keyId ? value[this.props.keyId] : value)} key = {index} >{ this.props.keyVal ? value[ this.props.keyVal] : value}</option>)
			})
		}
		return (
				<div className = {style.select_box_container}>

					<select value = {(this.props.currentSelected ) ? this.props.currentSelected :'0'} onChange = {this.optionHandler} className = {style.select_box}>
						{this.props.noSelect ? null : <option value = "0"> {"--- Select --- "}</option>}
						{(optionList && optionList.length ) ? options : null}
					</select>
				</div>
		)
	}
}

