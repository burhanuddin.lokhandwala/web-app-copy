import React, {Component} from 'react'
import * as style  from './style.scss'
import {cancel, plus, imageLoader} from '$SHAREDSTYLES/icons'

export default class OverLayPopover extends Component{
	constructor(props) {
	  super(props);

	  this.state = {
	  	isImageLoad: false,
	  };
	}

	componentDidMount() {
		if(this.props.imageView ){
			document.body.style.position = "fixed"
			this.findSizeOnLoad();
		}

	}

	componentWillReceiveProps(newProp) {
		if(this.props.imageView ){
			this.findSizeOnLoad();
		}
	}

	findSizeOnLoad = () => {
		this.getImgSize()
		let overlay = document.querySelector("#popup-img-wrp");
		// this.setState({
		// 				isImageLoad: true,
		// 			});
	}

	getImgSize = (imgSrc) => {
		let newImg = new Image(), val = {};
		newImg.src = this.props.showImageList.image_url;
		let that = this
		 newImg.onload = function() {
			let height = newImg.naturalHeight;
			let width = newImg.naturalWidth;
			that.setState({
						imgHeight: height,
						imgWidth: width,
					});
		}

	}

	handleCancelButton = () => {
		document.body.style.position = ""
		this.setState({isImageLoad: false})
		this.props.cancelButton()
	}

	loade = (e) => {
		this.setState({isImageLoad: true})
	}

	render() {
		let buttonElement = '', markerElement = ''
		if(this.props.okButton && this.props.cancelButton){
			buttonElement = (<div className = {style.button_wraper}>
												<div onClick = {this.handleCancelButton} className = {style.half_button}>Cancel</div>
												<div onClick = {this.props.okButton} className = {style.half_button}>OK</div>
											</div>)
		}else if(this.props.okButton || this.props.cancelButton){
			buttonElement = (<div className = {style.button_wraper}>
												<div onClick = {this.props.cancelButton || this.props.okButton} className = {style.full_button}>Done</div>
											</div>)
		}

		if(this.props.showImageList && this.props.showImageList.markers && this.state && !!document.querySelector("#popup-img-wrp") ){
			let imgHeight = this.state.imgHeight,
					imgWidth = this.state.imgWidth,
					width = document.querySelector("#popup-img-wrp").offsetWidth,
					height = document.querySelector("#popup-img-wrp").offsetHeight,
					markers = this.props.showImageList.markers;

			markerElement = markers.map((i, index) => {
				let top = i.position.y,
						left = i.position.x;

				top = (top * height)/imgHeight;
				left = (left * width)/imgWidth;

				let markerStyle = {'top': top > 11 ? top-11 : 2 ,  'left': left > 70 ? left - 45 : 2}
				return <span key = {index} style = {markerStyle} className = {style.transcription_marker}>{i.text}</span>;
			});

		}

		return (
			<div className = {style.overlay_window_container}>
				<div className = {style.overlay_window} onClick = {this.handleCancelButton}   ></div>
				{this.props.imageView
					?(<div id = "popup-img" className = {style.overlay_image_model_window}>
							<div id = "popup-img-wrp" style={{ display: 'inline-block', position: 'relative'}}>
							{this.state.isImageLoad ? <span className = {style.cross_button} onClick = {this.handleCancelButton} >{cancel}</span> : null}
							{markerElement}
							<img onLoad = {this.loade} src = {this.props.showImageList.image_url}/>
	 						{this.state.isImageLoad ? '' : <span className = {style.image_loader}>{imageLoader}</span>}
						</div>
					</div>)
					: null
				}
				{this.props.messageView
					?(<div className = {style.overlay_dialog_window}>
						{this.props.headerText ? <div className = {style.dyalog_header}>{ this.props.headerText} </div> : null}
						<div className = {style.dyalog_body_wraper}>
							{this.props.body}
						</div>
						{buttonElement}
					</div>
						)
					: null
				}

			</div>
		)
	}

}

