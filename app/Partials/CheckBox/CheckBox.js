import React, {Component} from 'react';
import * as style         from './style.scss'

export default class CheckBox extends Component{
	render() {
		return (
			<div className= {style.input_container}>
				<label htmlFor={this.props.id} className={style.checkbox_label}>
					<input className={style.input_checkBox} {...this.props}/>
					<div className={style.square_box}>
						<div className={style.tic}></div>
					</div>
					<span className= {style.label_text}>{this.props.label}</span>
				</label>

			</div>
		);
	}
}
