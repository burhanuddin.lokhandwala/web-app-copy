import React from 'react';
import * as style  from './style.scss'

export default function SmallLoader() {
	return (
		<div className = {style.lds_ring}>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
	)
}