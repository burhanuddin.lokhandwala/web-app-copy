import React, {Component} from 'react'
import * as style  from './style.scss'

export default class SearchBoxList extends Component{
	onListClick = () => {
		this.props.onListClick(this.props.item)
	}

	render() {
		return (
			<li onClick = {this.onListClick} className = {style.search_list}>{this.props.textValue}</li>
		)
	}

}

SearchBoxList