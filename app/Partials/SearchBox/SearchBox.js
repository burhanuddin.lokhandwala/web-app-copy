import React, {Component} from 'react'
import * as style  from './style.scss'
import SearchBoxList from "./SearchBoxList"

export default class SearchBox extends Component{

	hideListHandler = (e) => {
		// console.log('parent', e.target.parentNode, e.target.parentElement)
		// this.setState({isListShow: false})
	}

	// componentWillReceiveProps(newProps){
	// 	if (newProps.resultList && newProps.resultList.length) {
	// 		this.setState({isListShow: true})
	// 	}
	// },

	render() {
		let keyVel = this.props.keyVel
		let searchList = ''
		if(this.props.resultList && this.props.resultList.length){
			searchList = this.props.resultList.map((item, index) => {
				return <SearchBoxList item = {item} key = {index} onListClick = {this.props.onListClick} textValue = {item[keyVel]} />
			})

		}

		return (
			<div ref = "searchBox"  className = {style.search_box_container}>
				<input className = {`${style.searchInput} full_width`} onBlur = {this.hideListHandler} type = "text" placeholder = {this.props.placeholder} onChange = {this.props.inputChangeHandler} />
				{
					this.props.resultList && this.props.resultList.length
					? (<ul className = {style.search_list_container} >
						{searchList}
					</ul>)
					: null
				}
			</div>
		)
	}

}

