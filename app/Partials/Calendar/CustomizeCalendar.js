import React, { Component } from 'react';
// import Calendar from 'rc-calendar'
import Flatpickr from 'react-flatpickr'
import * as style  from './style.scss'

export default class CustomizeCalendar extends Component {

	render() {
		return (
			<div className = {style.calender_container}>
				<Flatpickr
					value={new Date()}
					onOpen = {this.props.onOpenCalender}
					onChange = {this.props.onChangeDate}
					options = {{enable: this.props.enableDate ? this.props.enableDate : [new Date()], wrap: true}}
				>
					<div className="flatpickr">
						<input className = {style.calender_input} type="text" placeholder="" data-input/>
						<span style = {{fontSize: "14px"}} className = "green_text" title="toggle" data-toggle>{this.props.placeholder_text}</span>
					</div>
				</Flatpickr>
			</div>
		);
	}
}
