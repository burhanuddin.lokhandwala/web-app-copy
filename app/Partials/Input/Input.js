import React, {Component} from 'react';
import * as style         from './style.scss'

export default class Input extends Component{

	componentWillReceiveProps(nextProps) {
		if(this.props.value){
			this.setState({hidePlaceholder: false})
		}
	}

	onFocus = (e) => {
		// e.preventDefault();
		console.log(document.body.scrollTop, this.props.data, e.target)
		// debugger
		if(this.props.data && document.body.scrollTop < 240){
			document.body.scrollTop = this.props.data
		}
		this.setState({hidePlaceholder: true})
		navigator.userAgent.indexOf("Android")>0 && document.querySelector("#next_button") ? document.querySelector("#next_button").style.position = "absolute" : ""
		e.target ? e.target.focus() : ""
	}

	onBlur = (e) => {
		// e.preventDefault();
		navigator.userAgent.indexOf("Android")>0 && document.querySelector("#next_button") ? document.querySelector("#next_button").style.position = "fixed" : ""
		if (!this.props.value) {
			this.setState({hidePlaceholder: false})
		}
	}

	render() {
		return (
			<div className= {style.input_container} style = {this.props.wrap_style}>
				<input
					{...this.props}
					onFocus={this.onFocus}
					onBlur={this.onBlur}
					onKeyUp={this.onKeyUp}
					className={`${this.state && this.state.hidePlaceholder ? style.input_field_focused : ""} ${style.input_field}`}
					autoComplete="off"
					/>
				<span onClick = {this.onFocus} className= {(this.props.value || (this.state && this.state.hidePlaceholder)) ? style.lable_up : style.label_Placeholder}>{this.props.label}</span>
			</div>
		);
	}
}
