import React, { Component } from 'react';
import * as style from './style.scss'

class ShowOrderProcess extends Component {
    // static propTypes = {
    //     className: PropTypes.string,
    // };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
            	<div className="field_blank_container">
								<span className="bigHeaderText">What Happens to Your Order?</span>
							</div>
							<div>
								<ul className= {style.order_process}>
									<li>
										<span>1</span>
										<div>Pharmacist reviews & confirms order</div>
									</li>
									<li>
										<span>2</span>
										<div>Order gets packed & sealed </div>
									</li>
									<li>
										<span>3</span>
										<div>Delivery Executive picks up the order</div>
									</li>
									<li>
										<span>4</span>
										<div>You receive medicines in no time at all and get flat 20% off! </div>
									</li>
								</ul>
							</div>
            </div>
        );
    }
}

export default  ShowOrderProcess;
