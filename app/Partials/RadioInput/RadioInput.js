import React, {Component} from 'react';
import * as style         from './style.scss'

export default class RadioInput extends Component{

	render() {
		return (
			<div className= {style.input_container}>
				<label htmlFor = {this.props.id} className  = { style.request_call_label}>
					{this.props.isRequestCall
						? (<span className= { style.checkbox_check_border_r} > <span className = {style.checkbox_check_show_r}> </span> </span>)
						: (<span className= { style.checkbox_check_border } > <span className = {style.checkbox_check_show}> </span> </span>)
					}
					<input checked = {this.props.isRequestCall} onChange = {this.props.setRequestCall} name = "request_call_label" id = "request_call_label" type = "checkbox" />
					<span > {this.props.label}</span>
				</label>
			</div>
		);
	}
}
