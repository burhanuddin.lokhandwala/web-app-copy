import React from 'react';
import * as style  from './style.scss'

export default function Loader() {
	return (
		<div className = {style.loader_container}>
			<div className = {style.loader_overlay} ></div>
			<div className = {style.loader}></div>
		</div>
	)
}
