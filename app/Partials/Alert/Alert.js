import React, { Component } from 'react';
import * as style  from './style.scss'

class Alert extends Component {

    constructor(props) {
      super(props);
      this.state = {
        showQtyPrompt:true,
        otherQtyValue:null
      }
    }

    render() {
        return (
            <div>
                {this.state.showQtyPrompt ?
                    <div className={style.qtyPrompt}>
                        <div className={style.popup}>
                        {this.props.header_text ? <h4>{this.props.header_text}</h4> : ""}
                        {this.props.popup_body ? <div className = {style.popover_body}>{this.props.popup_body}</div> : ""}
                        {/*this.props.showInput ? <h4>Add Quantity</h4> : ''*/}
                        {/*this.props.showInput ? <input type="number" onChange={this.props.inputChange} value={this.props.inputValue} autoFocus/> : ''*/}
                        <div className={style.actionRow}>
                            {this.props.cancelBtn ? <span onClick={this.props.cancelBtn}>Cancel</span> : ""}
                            <span onClick={this.props.actionBtn}>OK</span>
                        </div>
                        </div>
                    </div>
                    :''
                }
            </div>
        );
    }
}

export default Alert;
