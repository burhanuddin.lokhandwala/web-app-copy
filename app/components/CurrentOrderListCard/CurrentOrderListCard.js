import React, {Component}                           from 'react'
import { Link }                                     from 'react-router-dom'
import {differenceBwtTimestamp} from '$HELPERS/helperFunction'
import * as style                                   from './style.scss'
import Clevertap                                    from '$HELPERS/clevertap'
import phonePe                                      from '$HELPERS/phonepeFunction'

export default class CurrentOrderListCard extends Component{
	detailOrderView = () => {
  		this.transitionTo('order-detail');
	}

	handleViewItem = (orderID) => {
		

		this.props.handleViewItem(false)
		this.props.history.push('/track/' + orderID)

	}

	handlePaymentMethod = (orderID, event) => {
		
		event.stopPropagation()
		Clevertap.event('mWeb - payNow', {
			source: 'Home',
			orderId: orderID
		})
		
		// alert("payment false")
		if(this.props.orderItem && this.props.orderItem.juspay_payment_enabled){
			this.payByJuspay()
		}else{
			this.props.history.push('/payments/' + orderID)
		}
		
	}

	handleRorderItems = (e) => {
		e.stopPropagation()
	}

	payByJuspay = () => {
		this.props.payFunction("juspay", this.props.orderItem.display_id).then((res)=> {
			if(res.code == 400 || res.code == 401 || res.code == 403 || res.code == 499){
				this.setState({errorMsg: res.message})
			}
			this.setState({showLoader:false});
			window.open(res.url,"_self");
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
		})
	}

	render () {
		let order = this.props.orderItem

		let extra_info = order.extra_info
		let start_time = new Date().getTime()
		let end_time = order.expected_delivery_timestamp * 1000
		let expected_time = differenceBwtTimestamp(start_time, end_time)
		let remain_time = ''
		let time_type = ''
		let progressBarStatus = ""

		if (extra_info && extra_info.eta) {
			if(extra_info.eta - parseInt(start_time/1000) > 0){
				progressBarStatus = (100 - parseInt((extra_info.eta - parseInt(start_time/1000))/36)) + "%"
			}else{
				progressBarStatus= "100%"
			}
		}else{
			progressBarStatus = "0"
		}

		if(expected_time.hours > 0){
			remain_time = expected_time.hours
			time_type = "hrs"
		}
		else if(expected_time.minutes > 0){
			remain_time = expected_time.minutes
			time_type = "mins"
		}else{
			remain_type = "Arriving Soon"
			time_type = ""
		}

		return (
		<div className = "container">
			<div className = {style.crunt_order_card} onClick = {this.handleViewItem.bind(this, order.display_id)}>
				<div className = {style.inner_box}>
					<div>
						<span >Order <span className = "upper_text"> {order.display_id}</span></span>
						<span className="right green_text">Track</span>
					</div>
					{order.show_delivery_time ? <span className = {style.time_remain}><span >{remain_time}</span>  <span className = {style.sub}>{time_type}</span></span> : ""}
					{<div className="full_width">
											<span className={style.estimated_time} > {extra_info ? extra_info.formatted_eta : "Reviewing Order"}</span>
											<span className={style.small_text}>{extra_info ? extra_info.eta_description : "(Usually takes 10mins)"}</span>
										</div>}
				</div>

				<div className="full_width">
					<div className={`full_width ${style.progracive_bar_wrap}`}>
						<div className={style.progracive_bar}>
							<div className={style.active_progracive_bar} style= {{width: progressBarStatus }}>

							</div>
						</div>
					</div>
				</div>
				<div className = {style.inner_box}>
					{extra_info && extra_info.description ?
						(<div className="full_width">
							<span className={style.small_text}>*{extra_info.description}</span>
						</div>): null}
					<div className = "container">
						{order.is_price_confirmed
							?(
								<span className = {style.payment_info} >
									<span className = {style.rupee}>₹ {order.total_amount}/-</span>
									<span className = {style.saved_text}>(SAVED ₹ {order.total_discount})</span>
								</span>
							)
							: (order.price_status == "payment_enabled") ?<span className = {style.payment_info} >
							{(order.payment_status == "SUCCESS" || order.payment_status == "paid" ) ? <span className = {style.rupee}>₹ {order.total_amount}/-</span> : 
							<span className = {style.rupee}>₹ {order.total_payable_amount}/-</span> }
							<span className = {style.saved_text}>(SAVED ₹ {order.total_discount})</span>
						</span> : <span className = {style.small_text}>Est Amount : ₹{order.total_amount} <span className = {style.saved_text}>(SAVED ₹ {order.total_discount})</span> </span>
						}
						{order.price_status == "payment_enabled"
							?  (order.payment_status == null || order.payment_status == "FAILED" ? 
								<span className = "right">
								<span onClick= {this.handlePaymentMethod.bind(this, order.display_id)} className = "border_button button" style={{background:'rgb(0, 186, 123)',color:'white',border:'none'}}>Pay Now</span>
								</span> : (order.payment_status == "SUCCESS" || order.payment_status == "paid")?  <span className = "right">
								<span  className = "border_button button">PAID</span>
								</span> :  <span className = "right">
								<span  className = "border_button button">PROCESSING</span>
								</span>)
							: (order.price_status == "paid") ? <span className = "right">
								<span  className = "border_button button">PAID</span>
							</span> : null
						}
					</div>

				</div>
			</div>

		</div>
		)
	}

}


