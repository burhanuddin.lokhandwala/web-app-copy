import React, { Component } from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { smallSearchIcon2, backArrow, smallgreyCancel } from "$SHAREDSTYLES/icons";
import * as style from "./style.scss";
import { callAnalytics } from "$HELPERS/analytics";
import { updateImageElement } from "$REDUX/modules/imageSelector";
import { getFormattedString } from "$HELPERS/helperFunction";
import Clevertap from "$HELPERS/clevertap";
import medlifeLogo from "../../sharedImage/medlife-logo.png";
import offer from "../../sharedImage/offers-icon.svg";
import express from "../../sharedImage/express-icon.svg";
import standard from "../../sharedImage/standard.svg";
import GoToPage from "../GoToPage/GoToPage";
import SmallLoader from "$PARTIALS/SmallLoader/SmallLoader";

class HomeNavigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scrolled: props.route_path === "previoud_order" || props.route_path === "page" ? true : false,
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            scrolled: newProps.route_path == "previoud_order" || newProps.route_path === "page" ? true : false,
        });
        newProps.route_path == "previoud_order" || newProps.route_path === "page"
            ? document.removeEventListener("scroll", this.scroll)
            : document.addEventListener("scroll", this.scroll);
        setTimeout(() => {
            const { isAuthed, userActiveLocation } = this.props;
            let mainContainer = document.querySelector(".main_centered_container");
            if (isAuthed && userActiveLocation && userActiveLocation.address) mainContainer.classList.add("address-margin");
            else mainContainer.classList.add("fixed-header-margin");
        }, 100);
    }

    componentDidMount() {
        this.props.route_path == "previoud_order" || this.props.route_path === "page" ? "" : document.addEventListener("scroll", this.scroll);

        setTimeout(() => {
            const { isAuthed, userActiveLocation } = this.props;
            let mainContainer = document.querySelector(".main_centered_container");
            if (isAuthed && userActiveLocation && userActiveLocation.address) mainContainer.classList.add("address-margin");
            else mainContainer.classList.add("fixed-header-margin");
        }, 100);
    }

    componentWillUnmount() {
        document.removeEventListener("scroll", this.scroll);
        let mainContainer = document.querySelector(".main_centered_container");
        mainContainer.classList.remove("fixed-header-margin", "address-margin");
    }

    scroll = () => {
        let isTop = window.scrollY;
        if (isTop > 50 && !this.state.scrolled) {
            this.setState({ scrolled: true });
        } else if (isTop < 50 && this.state.scrolled) {
            this.setState({ scrolled: false });
        }
    };

    // makeTabActive = (e) => {
    // 	// this.setState({activeTab: e.target.getAttribute("data")})
    // 	this.props.changeHomepageTab(e.target.getAttribute("data"))

    //   if(e.target.getAttribute("data") == 'orders'){
    //       Clevertap.event('mWeb - View My Orders')
    //   } else {
    //       Clevertap.event('mWeb - View For You')
    //   }
    // }

    // getImage = e => {
    //     let rotate = "";
    //     if (e.target) {
    //         let file = e.target;
    //         getOrientation(file.files[0], function(orientation) {
    //             rotate = orientation;
    //         });
    //         setTimeout(() => {
    //             this.props.updateImageElement({
    //                 image_value: file,
    //                 image_element: setImgUrlFromFiles(file),
    //                 orientation_flag: rotate,
    //             });
    //             this.props.history.push("/image_prescription");
    //         }, 500);
    //     }
    // };

    // handleImageUpload = e => {
    //     Clevertap.event("mWeb - Quick order from home", {
    //         userId: this.props.userId ? this.props.userId : "",
    //     });

    //     if (this.props.userOnlineStatus == "offline") {
    //         e.preventDefault();
    //         this.props.history.push("/login");
    //     }
    // };

    handleAddressNav = () => {
        Clevertap.event("mWeb - Add New Address Screen", { origin: "Home" });
        // console.log("successful event call")
        this.props.maintainRoutes("home");
    };

    render() {
        let userActiveLocation = this.props.userActiveLocation;
        let dynamicText = this.props.appDynamicText && userActiveLocation ? this.props.appDynamicText.load_properties.value.delivery_prefix : "";
        let markup = (
            <p
                style={{
                    fontSize: "14px",
                    lineHeight: "1",
                    fontWeight: "500",
                    color: "#134f70",
                }}
                className="d-flex align-items-center">
                <img
                    src={offer}
                    style={{
                        height: "16px",
                        width: "16px",
                        marginRight: "5px",
                    }}
                    alt="offer"
                />
                offers
            </p>
        );
        return (
            <div className={`fixed-header ${style.fix_header}`}>
                <div style={{ padding: "10px 15px 0", display: "flex" }}>
                    <img src={medlifeLogo} style={{ maxWidth: "53px", maxHeight: "26px" }} alt="logo" />
                </div>
                {this.props.isAuthed && userActiveLocation && userActiveLocation.address && (
                    <div className="p-t-10 p-l-15 p-r-15 d-flex justify-content-between align-items-center">
                        <img
                            src={userActiveLocation.delivery_type == "express" ? express : standard}
                            className={style.delivery_type}
                            alt="delivery"
                        />
                        <Link to="/user_locations" className={style.addressWrapper} onClick={this.handleAddressNav}>
                            <div className={style.addContainer}>
                                <p>
                                    <strong>
                                        {userActiveLocation && userActiveLocation.label ? userActiveLocation.label.toUpperCase() + ", " : ""}
                                    </strong>
                                    {userActiveLocation.address ? getFormattedString(userActiveLocation.address, 75) : null}
                                </p>
                                <span className={style.down_arrow} />
                            </div>
                        </Link>
                        {GoToPage("offers", markup)}
                    </div>
                )}
                {this.props.route_path !== "search_medicine" ? (
                    <div className="p-15">
                        <div
                            style={{
                                backgroundColor: "#fff",
                                padding: "4px 8px",
                                borderRadius: "4px",
                                color: "#000",
                                fontSize: "14px",
                                height: "36px",
                            }}
                            onClick={() => {
                                this.props.handleShowSearch(true, "Home");
                            }}
                            className="d-flex align-items-center justify-content-center">
                            <span className="d-flex m-r-5">{smallSearchIcon2}</span>
                            <p style={{ lineHeight: "16px" }}>Search for Medicines</p>
                        </div>
                    </div>
                ) : (
                    <div className="p-15 d-flex align-items-center">
                        <div className={style.back_arrow} onClick={this.props.handleShowSearch}>
                            {backArrow}
                        </div>
                        <div style={{ position: "relative", flexGrow: "1" }}>
                            <span className={style.search}>{smallSearchIcon2}</span>
                            <input
                                autoComplete="off"
                                id="searchInput"
                                type="text"
                                value={this.props.searchInput}
                                style={{
                                    backgroundColor: "#fff",
                                    padding: "4px 35px",
                                    borderRadius: "4px",
                                    color: "#000",
                                    fontSize: "14px",
                                    height: "36px",
                                    width: "100%",
                                }}
                                placeholder="Search for medicines or products"
                                onChange={this.props.handleInputChange}
                                autoFocus
                            />
                            <span onClick={this.props.removeInputText} className={style.removeText}>
                                {this.props.isFetching ? <SmallLoader /> : smallgreyCancel}
                            </span>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default connect(
    ({ users, cart }) => ({
        userOnlineStatus: users.userOnlineStatus,
        isAuthed: users.isAuthed,
        items: cart.items,
        userId: users.userInfo.id,
        appDynamicText: cart.appDynamicText,
    }),
    dispatch =>
        bindActionCreators(
            {
                updateImageElement,
            },
            dispatch
        )
)(HomeNavigation);
