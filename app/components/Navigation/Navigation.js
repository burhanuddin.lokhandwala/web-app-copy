import React from "react";
import { Link } from "react-router-dom";
import * as style from "./style.scss";
import { backArrow, cartIconPurpel } from "$SHAREDSTYLES/icons";
import HomeNavigation from "../../components/Navigation/HomeNavigation";

export default function Navigation ({isAuthed, logoutAndUnauth, otp, history, route_path, pathname, isLocationFlag, userName, scrolled, handleShowSearch, changeHomepageTab, homepageTabType, items, categoryName, userActiveLocation, maintainRoutes, cart_charges}) {
	if(route_path.split("/").length > 1){
		let route = route_path.split("/")
		route.pop()
		route_path = route.join("/")
	}
	var navbarInfo = {
		cart: {
			header: <span className={style.cart_header}>
          			<div className = {style.head_text}>Cart</div>
          			<div className = {style.cart_status}>TOTAL &#8377;{cart_charges ? cart_charges.total_amount : 0} | {items.length} ITEMS</div>
          		</span>,
			back_button: true,
		},
		search_location: {
			header: "Address",
			back_button: true,
		},
		// previoud_order: {
		// 	header: "Previous Orders",
		// 	rightTextContent: <Link to='/cart' className= "link">{cartIconPurpel}</Link>,
		// 	secondLink: true,
		// 	showItemLength: true,
		// 	back_button: true,
		// },
		accounts: {
			header: "Account",
			back_button: true,
			no_scroll: true,
		},
		user: {
			header: "Account Detail",
			back_button: true,
			no_scroll: true,
		},
		"users/order": {
			header: "Order Details",
			back_button: true,
		},
		"users/reorder": {
			header: "Order Details",
			rightTextContent: <Link to='/cart' className= "link">{cartIconPurpel}</Link>,
			secondLink: true,
			showItemLength: true,
			back_button: true,
		},
		schedule_order: {
			header: "Schedule Order",
			back_button: true,
			no_scroll: true,
		},
		user_locations: {
			header: "Location",
			back_button: true,
		},
		image_prescription: {
			header: "Prescription",
			back_button: true,
		},
		edit_location: {
			header: "Edit Location",
			back_button: true,
			no_scroll: true,
		},
		select_location: {
			header: "Delivery Info.",
			rightTextContent: "2 of 2",
			back_button: true,
			no_scroll: true,
		},
		success_order: {
			header: "Success Order",
			back_button: true,
		},
		"attach": {
			header: "Attach Prescription",
			back_button: false,
		},
		login: {
			header: "Enter Phone Number",
			// back_button: true,
		},
		recomended_items_for_you: {
			header: "Recommended For You",
			back_button: true,
		},
		product_categories: {
			header: "Products",
			back_button: true,
		},
		"sub_category": {
			header: categoryName ? categoryName : "SubCategory",
			back_button: true,
		},
		"payments": {
			header: "Payment",
			back_button: true,
		},
		"track": {
			header: "Track",
			back_button: true,
		},
		"recall_order": {
			header: "Recall Order",
			back_button: false,
		},
		"search_medicine": {
			header: "Search Medicine",
			back_button: true,
		},
		"user_wallet":{
			header: "Myra Wallet",
			back_button : true
		}
	}
	// let mrgTop = 106;
	// if (route_path === 'prescription' || route_path === 'previoud_order' || route_path === "page" || !route_path) {
	// 	if (isAuthed) mrgTop = 143
	// }

	// if(route_path !== 'user_wallet'){
	// 	// mrgTop = '-10px'
	// 	document.querySelector(".main_centered_container").style.marginTop = `${mrgTop}px`;
		
	// }
	return (
			<nav className={style.navContainer}>
				{route_path === 'prescription' || route_path === 'previoud_order' || route_path === "page" || !route_path
					?(<div className={`full_width ` }>
							<HomeNavigation
								maintainRoutes = {maintainRoutes}
								userActiveLocation = {userActiveLocation}
								isAuthed = {isAuthed}
								history = {history}
								handleShowSearch = {handleShowSearch}
								route_path = {route_path}
								// changeHomepageTab = {changeHomepageTab}
								homepageTabType = {homepageTabType}
								userName = {userName}
							/>
						</div>
				):
				(<div className={`${style.fix_header}`}>
				 	<div className = {`${style.white_header} ${route_path == 'user_wallet' ? style.border_less:null} ${style.nevListWraper}`}>
						{navbarInfo[route_path] && navbarInfo[route_path].back_button ? <span className = {style.back_arrow_header} onClick = {pathname}>{backArrow} </span> : ""}
						<div className={style.afterScroll}>
							<span> {navbarInfo[route_path] ? navbarInfo[route_path].header : "Enter Phone Number"}</span>
							{navbarInfo[route_path] && navbarInfo[route_path].rightTextContent ? <span className = {`${style.left_element} right`}>  {navbarInfo[route_path].rightTextContent}  {items.length && navbarInfo[route_path].showItemLength ? <Link to='/cart' className= {style.link}><span className= {style.item_count_icon}>{Object.keys(items).length}</span></Link>: ""}</span> : ""}
						</div>
					</div>
						
				</div>
				 )
			}
			</nav>
	)
}
