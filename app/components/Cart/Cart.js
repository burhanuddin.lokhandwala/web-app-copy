import React, { Component }      from 'react'
import { Link }                  from 'react-router-dom'
import {Input}                   from '$PARTIALS/Input/Input'
import {CheckBox}                from '$PARTIALS/CheckBox/CheckBox'
import * as style                from './style.scss'
import remove from "../../sharedImage/remove.svg";

// Cart.propTypes = {
// 	isRequestCall: PropTypes.bool.isRequired,
// 	promo_code: PropTypes.string,
// 	setRequestCall: PropTypes.func.isRequired,
// 	handlePromoChange: PropTypes.func.isRequired,
// 	handlePromoCode: PropTypes.func.isRequired,
// 	promoError: PropTypes.string,
// 	updateItemQuantity: PropTypes.func,
// 	removeOrderItem: PropTypes.func,

// }

export class CartList extends Component {
	constructor(props) {
	  super(props);

	  this.state = {
			itemIndex: this.props.itemIndex,

	  };
	}

	deleteExtendedListItem = () => {
		if(confirm("Are you sure you want to delete!"))
			this.props.deleteExtendedListItem(this.state.itemIndex)
	}

	changeListItem = (e) => {
		this.props.changeListItem(e.target.value, this.props.index)
	}

	changeListQuantity = (e) => {
		this.props.changeListQuantity(e.target.value, this.props.index)
	}

	render () {
		return (
			(<li >
				<div className = "container">
					<div className = {style.itemName}>{this.props.item_text}</div>
					<div className= "container" style={{'marginTop': '20px'}}>
						<div className={style.small_disable_text} style={{'marginTop': '7px'}}>Receive a call back for item details</div>
						{/*<span className = {style.item_textval}>{this.props.item_text}</span>*/}

						{/*<div style = {{width: "75%"}} className= {left} >
							<Input className = {style.input_field} onChange= {this.changeListItem} label = "Item name"  value= {this.props.item_text}/>
						</div>
						<div style = {{width: "25%"}} className= {left} >
							<Input className = {style.input_field} onChange= {this.changeListQuantity} label = "Quantity" value= {this.props.quantity_text}/>
						</div>*/}

						<div className = {style.normal_prize}>
							<img src={remove} alt="remove-icon" style={{ width: '24px', height: '24px' }} onClick={this.deleteExtendedListItem}/>
	                        <select className={style.selectQty} value={this.props.quantity_text} onChange={this.changeListQuantity}>
	                            <option value="1">1</option>
	                            <option value="2">2</option>
	                            <option value="3">3</option>
	                            <option value="4">4</option>
	                            <option value="5">5</option>
	                            <option value="6">6</option>
	                            <option value="7">7</option>
	                            <option value="8">8</option>
	                            <option value="9">9</option>
	                            <option value="10">10</option>
	                            {(this.props.quantity_text > 10) ? <option value={this.props.quantity_text}>{this.props.quantity_text}</option> : ''}

	                        </select>
	                    </div>
                    </div>


					{/*<span className = {style.quantity_textval}>{this.props.quantity_text}</span>*/}
				</div>
			</li>)
		);
	}
};

export default function Cart (props){
		let currentId = '', extendedNoteEl = '', medicinesList = ''
		let scrollBodyValue = navigator.userAgent.indexOf('Chrome')>0 ? 250 : 300
		if(props.extended_notes && props.extended_notes.length){
			let user_notes = props.extended_notes.slice();
			// user_notes.reverse()
			extendedNoteEl = user_notes.map( (x, index) => {
				if(x){
					return (<CartList key = {index} index={index} changeListQuantity= {props.changeListQuantity} changeListItem= {props.changeListItem} itemIndex = {index} item_text = {x.item_text} deleteExtendedListItem = {props.deleteExtendedListItem} quantity_text = {x.quantity_text} />)
				}
			})
		}

		if(props.items && props.items.length){
			medicinesList = props.items.map((x, index) => {
				return (
					<li key = {index} className = {(x.medicine.is_discontinued || x.medicine.is_banned) ? style.pink_background : ""}><CartItems item = {x} updateItem = {props.updateItemQuantity} removeOrderItem = {props.removeOrderItem}/></li>
					)
			})
		}


		return (
			<div className = "container">
				<div className = "container">
					<label className = "field_label">
						Items Added
						<Link to = "/search_medicine"><small className="right">Add more </small></Link>
					</label>

					{/*<div style = {{paddinTop: "5px"}} className = {style.field_wraper}>
						<div className= {style.extended_input_wraper}>
							<label htmlFor= "items" className= {style.extended_item_input_wrp} >
								<Input data = {scrollBodyValue} name= "items" id= "items" type= "text" value= {props.user_selected_item} onChange= {props.addItemName} label = "Item name" />
							</label>
							<label htmlFor= "quantity" className= {style.extended_quantity_input_wrp}>
								<Input data = {scrollBodyValue} name= "quantity" id= "quantity"  type= "text" value= {props.user_selected_item_quantity} onChange= {props.addQuantity} label = "Quantity" />
							</label>
						</div>
						<div className= {full_width}>
							<div className= {style.item_add_button_wrap}>
								<span onClick= {props.dismissItemModel} className= {style.clear_item_button}>
									<button className= {border_button}>Clear</button>
								</span>
								<span onClick= {props.addUserChoseItem} className= {style.extended_add_item_button}>
									<button className = {green_button}>+ Add</button>
								</span>
							</div>
						</div>
					</div>*/}

					{(props.items && props.items.length) || (props.extended_notes && props.extended_notes.length)
						?(
						  <div className = {style.field_wraper}>
						  		<ul className = {style.item_list_wraper}>
									{medicinesList}
								</ul>
								<ul className = {style.medicine_list_wraper}>
									{extendedNoteEl}
								</ul>

							</div>)
							: null
					}
				</div>

				<div className = "container">
					<div className = "field_container">
						<div className= {style.request_call_label}>
							<CheckBox label= {"Request call from pharmacist"} checked = {props.isRequestCall} onChange = {props.setRequestCall} name = "request_call_label" id = "request_call_label" type = "checkbox"/>
						</div>

					</div>
				</div>

				<div className = {style.propm_container}>
					<input onChange = {props.handlePromoChange} value = {props.promo_code} type = "text" className = {style.promocode_input} placeholder = "Enter Promo code"/>
					<button onClick = {props.handlePromoCode} className = {style.promocode_button}> Apply </button>
					{ props.promoError ? <p className = {style.error}> {props.promoError}</p> : null}
				</div>

				<div className = "container">
					<p className = {style.description}>
						You will get flat 20% off on the MRP. We will confirm the price before delivery. You can only order FMCG and baby products using the app. Medicines require a physical verification of the prescription until pending government approval.
					</p>
				</div>

				<div id="next_button" className = {style.next_button_container}>
					<button onClick= {props.handleNextButton} className= "green_flat_button">{'Next'}</button>
				</div>
			</div>
		)
}
