import React from 'react';
import * as style from './style.scss'


function BillInformation(props) {
    let wallateInfo = props.wallateInfo
    let totalExtraCharges = 0 ;
    wallateInfo && wallateInfo.extra_charges.length ? wallateInfo.extra_charges.map((item)=>{
      totalExtraCharges += item.amount
    }) : 0
    let total_temp_amount = wallateInfo ? (wallateInfo.total_mrp - wallateInfo.total_discount - wallateInfo.coupon_discount + totalExtraCharges) : 0
    return(
            <div className = {`full_width ${style.bill_container} ${style.box_container}`} >
              <div className = {`full_width ${style.bill_details}`}>
                <div className = "full_width">
                	<span className="left">Total MRP</span>
                	<span className="right">&#8377;{wallateInfo.total_mrp}</span>
                </div>
                <div className = "full_width">
                	<span className="left">Discount</span>
                	<span className="right">
                		{wallateInfo.total_discount && (wallateInfo.total_discount > 0)
                			? <span>&#8722;</span>
                			: (wallateInfo.total_discount == 0) ? "" : <span>&#43;</span>
                		}
                		&#8377;{wallateInfo.total_discount}
                	</span>
                </div>
                

                {wallateInfo.extra_charges && wallateInfo.extra_charges.length
                  ? (<div className = {`full_width ${style.bordered_box}`}>
                      {wallateInfo.extra_charges.map((x, index) => {
                        return (<div key={index}>
                                    <div>
                                        {x.desc}
                                        <span className="right">
                                            {x.amount ? <span>&#8377;{x.amount}</span> : 'FREE'}
                                        </span>
                                    </div>
                                    {x.amount ? <div className = {style.amount_message}>{x.extras.messages}</div> : ""}
                                </div>)
                      })}
                    </div>):""
                }
                {wallateInfo.coupon_discount ? (
                	<div className = "green_text">
                		Coupon - <span className = "upper_text">{wallateInfo.promo_code}</span>
                		<span className = "right">-&#8377;{wallateInfo.coupon_discount}</span>
                	</div>
                ): ""}

                
              
              <div className = "full_width" >
                <div className = "full_width" style = {{fontWeight: '600'}}>Total Amount<span className="right">&#8377;{total_temp_amount.toFixed(2)}</span></div>
              </div> 
              <div className = {`full_width `} >
                  <span className="left">
                  Wallet Credits
                  </span>
                  <span className="right">
                  	{wallateInfo.wallet_credits && (wallateInfo.wallet_credits > 0)
                  		? <span>&#8722;</span>
                  		: (wallateInfo.wallet_credits == 0) ? "" : <span>&#43;</span>
                  	}
                  	&#8377;{wallateInfo.wallet_credits ? Number(wallateInfo.wallet_credits).toFixed(2) - (wallateInfo.online_paid_amount ? Number(wallateInfo.online_paid_amount).toFixed(2) : 0 ): 0}
                  </span>
                </div> 

            </div>
              <div className = "full_width" style = {{padding: "12px"}}>
                <div className = "full_width" style = {{fontWeight: '600'}}>Total to be Paid <span className="right">&#8377;{wallateInfo.total_payable_amount}</span></div>
                <div className = {`full_width ${style.saving_text}`}>You save &#8377;{wallateInfo.coupon_discount+wallateInfo.total_discount} on your order!</div>
            	</div>
            </div>
        )
}

export default BillInformation;