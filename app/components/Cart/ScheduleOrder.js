import React, {Component} from 'react';
import * as style from './style.scss'
import SelectBox from '$PARTIALS/SelectBox/SelectBox'

export default class ScheduleOrder extends Component{

	constructor(props) {
	  super(props);

		let state = {}, timing = props.timing, scheduleTime = '', ampm = ''

		if(timing){
			// = timing.schedule_hour;
			if(timing.schedule_hour >12){
				scheduleTime = (timing.schedule_hour-12);
				ampm = 'pm';
			}
			else{
				scheduleTime = (timing.schedule_hour);
				ampm = 'am';
			}
		}
		state.startHour = timing.start_hour;
		state.endHour = timing.end_hour;
		state.hour = scheduleTime;
		state.min = "00";
		state.ampm = ampm;
		state.amList = [];
		state.pmList = [];
		state.timing = timing;

		for(var i = 1; i < 12; i++){
			if(i >= state.startHour){
				state.amList.push(i);
			}
			if((i+12) <= state.endHour){
				state.pmList.push(i);
			}
		}

	  this.state = state;
	}

	handleChangeHour = (value) => {
		let hour = parseInt(value);
		let updates = {hour: hour, ampm: this.state.ampm};
		if(hour == 12){
			updates.ampm = 'noon';
		}
		else{
			if(!this.isAMAllowed(hour)){
				updates.ampm = 'pm';
			}
			if(!this.isPMAllowed(hour)){
				updates.ampm = 'am';
			}
		}
		let fullHour = hour;
		if(updates.ampm === 'pm'){
			fullHour += 12;
		}

		if(fullHour === this.state.startHour && this.state.min < 30){
			updates.min = 30;
		}
		if(fullHour === this.state.endHour && this.state.min !== 0){
			updates.min = 0;
		}

		this.setState(updates);
	}

	handleChangeMinute = (value) => {
		this.setState({min: parseInt(value)});
	}

	handleChangeMode = (value) => {
		let updates = {ampm: value};
		let fullHour = this.state.hour;
		if(updates.ampm == 'pm'){
			fullHour += 12;
		}

		if(fullHour === this.state.startHour && this.state.min < 30){
			updates.min = 30;
		}
		if(fullHour === this.state.endHour && this.state.min !== 0){
			updates.min = 0;
		}

		this.setState(updates);

	}

	handleDoneButton = () => {
		let hour = this.state.hour;
		if(this.state.ampm == 'pm'){
			hour += 12;
		}
		// if(!this.state.hour === 12 || (!this.isAMAllowed(this.state.hour) && !this.isPMAllowed(this.state.hour))){
			// alert("test")
			// return
		// }
		this.props.handleDoneButton({hour: hour, minute: this.state.min})
	}

	isAMAllowed(hour){
		return this.state.amList.indexOf(hour) !== -1;
	}

	isPMAllowed(hour){
		return this.state.pmList.indexOf(hour) !== -1;
	}

	getHoursList = () => {
		let timing = this.state.timing, hourList = []
		for (var i = timing.start_hour; i <= timing.end_hour; i++) {
			if(i <= 12){
				hourList.push(i)
			}else{
				hourList.push(i-12)

			}
		}
		return hourList
	}

	render () {
		let timing = this.state.timing;

		// set milute list

		let minuteOptions = [];
		let fullHour = this.state.hour;
		if(this.state.ampm == 'pm'){
			fullHour += 12;
		}
		if(fullHour === this.state.startHour){
			minuteOptions = ["00", "30", "45"]
		}
		else if(fullHour === this.state.endHour){
			minuteOptions = ["00"]
		}
		else{
			minuteOptions = ["00", "15", "30", "45"]
		}

		// set mode list

		let ampmOptions = [];
		if(this.state.hour === 12){
			ampmOptions.push("Noon");
		}
		else{
			if(this.isAMAllowed(this.state.hour)){
				ampmOptions.push("am");
			}
			if(this.isPMAllowed(this.state.hour)){
				ampmOptions.push("pm");
			}
		}

		// console.log(ampmOptions, minuteOptions, this.state.ampm, this.state, this.getHoursList())
		return (
			<div className = "full_width">
				<div className="section_heading">
	                <span className="section_title">Choose Delivery time</span>

					<div className = {style.select_box_wrp}>
						<span className = {style.select_hour}>
							<SelectBox currentSelected = {this.state.hour} optionList = {this.getHoursList()}  handleChangeOption = {this.handleChangeHour}/>
						</span>
						<span className = {style.select_hour}>
							<SelectBox currentSelected = {((this.state.min === 0)?'00':this.state.min).toString()} optionList = {minuteOptions}  handleChangeOption = {this.handleChangeMinute}/>
						</span>
						<span className = {style.select_timemode}>
							<SelectBox currentSelected = {this.state.ampm ? this.state.ampm : ampmOptions[0]} optionList = {ampmOptions}  handleChangeOption = {this.handleChangeMode}/>
						</span>
					</div>
				</div>

			  <p className = {style.company_message_line}>
					{this.props.error || !ampmOptions.length
						?(
						   <span className = "error_text">{this.props.error || "Invalit timing!!"}</span>)
						: (
						  <span> Your order will be delivered by {this.state.hour}:{(this.state.min === 0)?'00':this.state.min} {this.state.ampm ? this.state.ampm : ''} {timing.date}</span>
						  )
					}
				</p>

				<div className ="fixed_btn button">
					<div onClick = {ampmOptions.length ? this.handleDoneButton : ""} >Place Order &amp; Pay Later</div>
				</div>
			</div>
		);
	}
}

