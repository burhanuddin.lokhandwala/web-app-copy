import React, { Component } from 'react';
import * as style from './style.scss'
import {getFormattedString} from '$HELPERS/helperFunction'
import SmallLoader from '$PARTIALS/SmallLoader/SmallLoader'

export default class CartRecommendation extends Component{
		constructor(props) {
        super(props);
        this.state = {
            showImageList: '',
        }
    }

    handleUpdateMedicineCount = (json) => {
    	this.setState({changeId: json.id})
    	this.props.handleUpdateMedicineCount(json)
    }

    render(){

			let windowWidth = document.body.offsetWidth
			let length = this.props.recommendationList.length
			let width = parseInt(178*length)
			let bodyWidth = document.querySelector('#main_container') ? document.querySelector('#main_container').offsetWidth : windowWidth > 770 ? parseInt((windowWidth*6)/10) : windowWidth

   		return(
   		<div className = "full_width " style = {{overflow: 'hidden' }}>
   			<div className = "full_width section_heading grey_back" style = {{paddingBottom: 0}}>
   				<span className = "section_title ">
   					{this.props.header ? this.props.header : "Recommended For You"}
   				</span>
   			</div>
   		  <div style = {{"width": bodyWidth-length+5,  "maxWidth": 650}} className={`${style.recommendation_container} full_width`} >
	        <div className="left" style = {{width: width}}>
	        	{this.props.recommendationList.map((x, index) => {
	        		return (
	        			<div key = {index} className={`${style.recommendation_wrp} left`}>
		        			<div className = "half_width center_text relative">
		        				<span style = {{top: "-3px", left: "-3px"}} className = "discount_tag">
				     					<div>{x.discount}%</div>
				     					<div>OFF</div>
				     				</span>
		        				<img src = {x.image_urls[0]}/>
		        			</div>
		        			<div style = {{paddingLeft: "10px"}} className = "half_width">
		        				<div>
		        					{x.formatted_name ? getFormattedString(x.formatted_name, 18) : ""}
		        				</div>
		        				<div>
		        					&#8377; {x.discounted_price}
		        				</div>
		        				{this.props.isFetchingMedicine && x.id == this.state.changeId ?
		        					(<span className = {style.adding_button}>Adding</span>)
		        				: (<div onClick={() => this.handleUpdateMedicineCount({...x, ...{recomend: true}, ...{query: {context: "cart" , action: "added", source: "web_manual"}}})} className = "border_button green_border center_text">
		        					  ADD
		        					</div>)}
		        			</div>
	        			</div>
	        		)
	        	})}
	        </div>
	      </div>
	    </div>
      )
   	}
}
