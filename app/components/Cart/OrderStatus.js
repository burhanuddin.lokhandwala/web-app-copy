import React from 'react';
import * as style from './style.scss'


function OrderStatus(props) {
    return(
            <div className={style.status_container}>
                <div className={style.container}>
                    <div className={style.content}>
                        <div>
                        <figure className={style.chart} data-percent="100">
                            <svg width="200" height="200">
                              <path d="M78.012 120.165l-21.866-25.05a5.21 5.21 0 0 0-7.376-.486 5.266 5.266 0 0 0-.484 7.408l25.545 29.266a5.211 5.211 0 0 0 7.626.25l62.565-62.84a5.266 5.266 0 0 0 0-7.425 5.21 5.21 0 0 0-7.392 0l-58.618 58.877z"/>
                              <circle className={style.outer} cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                            </svg>
                         </figure>
                        </div>
                        <div className={style.message}>Order Placed!</div>
                    </div>
                </div>
            </div>
        )
}

export default OrderStatus;