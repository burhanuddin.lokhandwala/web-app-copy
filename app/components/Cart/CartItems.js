import React, {Component} from 'react'
import * as style from './style.scss'
import {formatedRupee} from '$HELPERS/helperFunction';
import remove from "../../sharedImage/remove.svg";

 export default class CartItems extends Component {

 	constructor(props) {
 	  super(props);

 	  this.state = {
			itemCount: props.item.normalized_quantity,
			packetType: props.item.medicine.allowed_quantity_types[props.item.packet_type_display_info.show_quantity_type_index].label
 	  };

 	}
	handleUpdateQuantity = (event) =>{
		// this.setState({itemCount: e.target.value*1})
		// this.props.updateItem(this.props.item.id, e.target.value*1)

		this.props.updateItem({orders: {items: [{id: this.props.item.id, quantity: event.target.value, quantity_type_selected: this.state.packetType}]}})
	}

	handlePacketTypeChange = (event) =>{
		let qty = this.props.item[this.props.item.packet_type_display_info.show_quantity_type]
		let label = this.props.item.medicine.allowed_quantity_types[event.target.value].label
		this.props.updateItem({orders: {items: [{id: this.props.item.id, quantity: qty, quantity_type_selected: label}]}})
	}

	handleIncreaseQuantity = () =>{
		if(this.state.itemCount*1+1>99){
			return
		}
		let timer = ''
		clearTimeout(this.state.timer)
		timer = setTimeout(function(){
			this.props.updateItem(this.props.item.id, this.state.itemCount*1)
		}.bind(this), 1000)
		this.setState({itemCount: this.state.itemCount*1+1, timer: timer})
	}

	handleDecreaseQuantity = () =>{
		if(this.state.itemCount*1==1){
			return
		}
		let timer = ''
		clearTimeout(this.state.timer)
		timer = setTimeout(function(){
			this.props.updateItem(this.props.item.id, this.state.itemCount*1)
		}.bind(this), 1000)
		this.setState({itemCount: this.state.itemCount*1-1,  timer: timer})
	}

	removeOrderItem = () =>{
		if(confirm("Are you sure you want to delete "+ this.props.item.medicine.formatted_name)){
			this.props.removeOrderItem(this.props.item.id)
		}
	}

	handleRorderItem = () =>{
		this.props.handleRorderItem({id: this.props.item.medicine.id,quantity:this.props.item.normalized_quantity}, this.props.item.id)
	}

	render() {
		let show_quantity_type_index = this.props.item.packet_type_display_info.show_quantity_type_index
		let show_quantity_type = this.props.item.packet_type_display_info.show_quantity_type

		return (
			<div className = "container">
				{/* !this.props.immutable ? <span className = {style.cancel_button} onClick = {this.removeOrderItem}>{smallCancel}</span> : null*/}
				{this.props.item.medicine.allowed_quantity_types[show_quantity_type_index].discount ?
					<div className={style.discount}>{this.props.item.medicine.allowed_quantity_types[show_quantity_type_index].discount}% off</div>
					: ''}
				<div className={style.itemName}>
					{this.props.item.medicine.formatted_name}
					<span className={style.rightAlign}>&#8377; {formatedRupee((this.props.item.medicine.allowed_quantity_types[show_quantity_type_index].mrp_discounted) * (this.props.item[show_quantity_type]))}</span>
				</div>
				<div className = {style.medium_disable_text}>

					{this.props.item.medicine.manufacturer_name}
					<span className={style.rightAlign + ' ' + style.strikeLine}>&#8377; {formatedRupee((this.props.item.medicine.allowed_quantity_types[show_quantity_type_index].mrp) * (this.props.item[show_quantity_type]))}</span>
				</div>
				{((!this.props.item.medicine.is_discontinued && !this.props.item.medicine.is_banned) || this.props.oldOrder)
					?(
					  <div className = {container}>
					  	{/* this.props.immutable ? <div className={style.orderQty}><span>{this.props.item[show_quantity_type]}</span> {this.props.item.medicine.allowed_quantity_types[show_quantity_type_index].label_formatted_full}</div> : ''*/}
							{/*<div className = {style.count_item_bar}>
								{ !this.props.immutable
									?(
										<span>
											<span onClick = {this.handleDecreaseQuantity} className = {style.minus}>{circleMinus}</span>
											<span className = {style.count_element}>
												<select value = {this.state.itemCount < 10 ? "0"+this.state.itemCount : this.state.itemCount} onChange = {this.handleUpdateQuantity}>
												<option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option>
												</select>
											</span>
											<span onClick = {this.handleIncreaseQuantity} className = {style.plus}>{circlePlus}</span>
										</span>
										)
									: (<span >Quantity: {this.state.itemCount}</span>)

								}
							</div>*/}
							{/*!this.props.showReorder
								? <span className = {!this.props.immutable ? style.discounted_prize : style. rate_box}>
										{<span>₹ {formatedRupee((this.state.itemCount*this.props.item.medicine.normalized_mrp)-((20*this.state.itemCount*this.props.item.medicine.normalized_mrp)/100))}</span>}

									</span>
								: null*/}
							<div className = {container} style={{'margin': '10px 0px'}}>

								{(!this.props.showReorder && !this.props.oldOrder && this.props.item.medicine.allowed_quantity_types.length > 1) ?
			                        <select value={show_quantity_type_index} onChange={this.handlePacketTypeChange}>
			                            {this.props.item.medicine.allowed_quantity_types.map((key, index) => {
			                                return(<option key={key.label_formatted_full} value={index}>{key.label_formatted_full}</option>)
			                            })}
			                        </select>
			                     :
			                     <div>
			                     { this.props.immutable ?
			                     	<div className={style.orderQty}><span>{this.props.item[show_quantity_type]}</span> {this.props.item.medicine.allowed_quantity_types[show_quantity_type_index].label_formatted_full}</div>
			                     	:
			                     	<div className={style.small_disable_text}>Packing: {this.props.item.medicine.packing_quantity}</div>
			                     }
			                     </div>

			                    }


								{(!this.props.showReorder && !this.props.oldOrder) ?
									<span className = {style.normal_prize}>

									<div>
										<img src={remove} alt="remove-icon" style={{ width: '24px', height: '24px' }} onClick={this.removeOrderItem}/>
				                        <select className={style.selectQty} value={this.props.item[show_quantity_type]} onChange={this.handleUpdateQuantity}>
				                            <option value="1">1</option>
				                            <option value="2">2</option>
				                            <option value="3">3</option>
				                            <option value="4">4</option>
				                            <option value="5">5</option>
				                            <option value="6">6</option>
				                            <option value="7">7</option>
				                            <option value="8">8</option>
				                            <option value="9">9</option>
				                            <option value="10">10</option>
				                            {(this.props.item[show_quantity_type] > 10) ? <option value={this.props.item[show_quantity_type]}>{this.props.item[show_quantity_type]}</option> : ''}
				                            <option value="other">other</option>
				                        </select>
			                        </div>

									</span> : null}
							</div>
						</div>)
						: null
				}

				<div className = {container}>
					{this.props.item.medicine.is_banned ? <span className={style.not_available_med}>Banned</span> : null}
					{this.props.item.medicine.is_discontinued ? <span className={style.not_available_med}>Discontinued</span> : null}

				</div>
				{this.props.showReorder
					?(
						<div className = {style.show_reorder_item_button}>
							{/*<span className = {style.reorder_discounted_prize}><span className={style.discount_box}>{this.props.item.discount}% OFF</span> ₹ {formatedRupee((this.state.itemCount*this.props.item.medicine.normalized_mrp)-((20*this.state.itemCount*this.props.item.medicine.normalized_mrp)/100))}</span>
							<span className = {style.reorder_normal_prize}><span className = {style.strick_threw}></span> ₹ {formatedRupee(this.state.itemCount*this.props.item.medicine.normalized_mrp)}</span>*/}
							{this.props.item.reordered
								?  <span className = {style.reorder_item_done} >Added to cart</span>
								: (!this.props.item.medicine.is_discontinued && !this.props.item.medicine.is_banned) ? <span className = {style.reorder_item_button} onClick = {this.handleRorderItem}>Reorder item</span> : null
							}
						</div>
					)
					: null
				}
			</div>
		)
	}
}

