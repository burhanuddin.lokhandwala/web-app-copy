import React, { Component } from 'react';
import * as style from './style.scss'


class Promocode extends Component {
    render(){
        return(
                <div className = "full_width" style = {{padding: "0 15px"}}>
                    <div className = {style.box_container}>
                        <input onChange = {this.props.handlePromoChange} value = {this.props.promo_code} type = "text" className = {style.promocode_input} placeholder = "Enter coupon code here" ref="promoInput"/>
                        <button onClick = {this.props.handlePromoCode} className = {`${this.props.promo_code ? "" : "grey_text"} ${style.promocode_button}`}> Apply </button>
                    </div>
                    { this.props.promoError ? <p className = {style.promocode_error}> {this.props.promoError}</p> : null}
                </div>
            )
    }
}

export default Promocode;