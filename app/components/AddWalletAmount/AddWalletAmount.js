import React, {Component} from 'react'; 
import * as style from './style.scss';
import offer from '../../sharedImage/offers-icon.svg';
import {infoNewIcon} from '$SHAREDSTYLES/icons'

export default class AddWalletAmount extends Component {
    constructor(props){
        super(props);
        this.state = {
            amountSelected : 100
        }
    }

    handleInputChange = (event) => {
        if(Number(event.target.value.split("₹")[1]) > 0){
            this.setState({amountSelected: Number(event.target.value.split("₹")[1])})
        }else{
            this.setState({amountSelected: ''})
        }   
    }

    handleWalletAdd = () => {
        if(this.state.amountSelected){
            this.props.updateUserWallet(this.state.amountSelected)
        }else{
            alert('Select some amount to Proceed')
        }
    }

    handleAmountChange= (amount)=> {
        if(amount){
            this.setState({amountSelected:amount})
        }
    }


    render = () => {
        return (
            <div>
                <div className="paddingBox">
                    <div className={style.current_bal_box}>
                        <svg width="19px" height="19px" viewBox="0 0 19 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" style={{marginRight:'12px',float:'left'}}>
                            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="Option-02--Add-Money" transform="translate(-15.000000, -151.000000)" fill="#000000" fillRule="nonzero">
                                    <g id="noun_Wallet_2568152" transform="translate(15.000000, 151.000000)">
                                        <path d="M1,7.25256348 C1,6.25752116 1.45333333,5.70333333 2.36,5.59 L5.72206704,5.60858408 L10.7475659,3.08947265 L15.0097266,1.04191894 C15.0910742,1.00273924 15.184541,0.997426744 15.269707,1.02764159 C15.354707,1.0575244 15.4244336,1.11994629 15.4634473,1.20129394 L17.5655994,5.57447754 L17.66,5.57447754 C17.8477637,5.57447754 18,5.72654784 18,5.91447754 L18,10.0867822 L18,13.4794775 L18,17.6517822 C18,17.8397119 17.8477637,17.9917822 17.66,17.9917822 L2.6777539,17.9917822 C1.75271485,17.9917822 1,17.2390674 1,16.3140283 L1,7.25256348 Z M5.58086913,17.3117822 L17.32,17.3117822 L17.32,13.8194775 L12.5083691,13.8194775 C12.3890039,13.8194775 12.2784375,13.7570556 12.2170117,13.65479 L11.1970117,11.9584424 C11.1320996,11.8505322 11.1320996,11.7157275 11.1970117,11.6078174 L12.2170117,9.9114697 C12.2784375,9.80920409 12.3890039,9.7467822 12.5083691,9.7467822 L17.32,9.7467822 L17.32,6.26352538 C17.2990405,6.26095216 17.2776868,6.26086913 17.2576611,6.25447757 L11.8018066,6.25447757 C11.784375,6.25705083 11.7669433,6.26443852 11.7496777,6.26443852 C11.7261243,6.26443852 11.7035669,6.25920901 11.6809473,6.25447757 L9.63980593,6.25447757 C9.61442629,6.2602881 9.58954468,6.27174318 9.56408204,6.27174318 C9.52888674,6.27174318 9.49456299,6.26501958 9.46138062,6.25447757 L5.6924524,6.25447757 C5.66707276,6.2602881 5.64219116,6.27174318 5.61672852,6.27174318 C5.6045471,6.27174318 5.59290527,6.2680078 5.58086913,6.2666797 L5.58086913,17.3117822 Z M17.32,13.1394775 L17.32,10.4267822 L12.7006152,10.4267822 L11.8851465,11.7831299 L12.7006152,13.1394775 L17.32,13.1394775 Z M11.0920068,5.57447754 L16.8112451,5.57447754 L15.7377051,3.34140136 L11.0920068,5.57447754 Z M15.4430689,2.7285547 L14.9976074,1.80193848 L7.14531739,5.57447754 L9.52234987,5.57447754 L15.4430689,2.7285547 Z M3.03,6.27 C2.28441559,6.27 1.68,6.87441559 1.68,7.62 L1.68,15.97 C1.68,16.7155844 2.28441559,17.32 3.03,17.32 L4.91,17.32 L4.91,6.27 L3.03,6.27 Z" id="Combined-Shape" stroke="#000000" strokeWidth="0.15"></path>
                                        <path d="M12.5508691,11.7831299 C12.5508691,11.9710596 12.7031055,12.1231299 12.8908691,12.1231299 L13.5708691,12.1231299 C13.7586328,12.1231299 13.9108691,11.9710596 13.9108691,11.7831299 C13.9108691,11.5952002 13.7586328,11.4431299 13.5708691,11.4431299 L12.8908691,11.4431299 C12.7031055,11.4431299 12.5508691,11.5952002 12.5508691,11.7831299 Z" id="Path"></path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <span className={style.cur_bal_head}>Available Balance</span>
                        <span className={style.prev_amount}>&#8377;{this.props.userCurrentWallet ? this.props.userCurrentWallet.toFixed(2) : '00.00'}</span>
                    </div>
                    <input className={`full-width ${style.amount_field}`} value={"₹ "+this.state.amountSelected} onChange={(e)=> this.handleInputChange(e)} />
                    <div className={style.scroll_cont} >
                        <div className={style.btn_box} >
                            <button className={`${style.amount_btn} ${this.state.amountSelected == 100 ? style.active_btn : null}`} onClick={()=> this.handleAmountChange(100)}>100</button>
                            <button className={`${style.amount_btn} ${this.state.amountSelected == 200 ? style.active_btn : null}`} onClick={()=> this.handleAmountChange(200)}>200</button>
                            <button className={`${style.amount_btn} ${this.state.amountSelected == 500 ? style.active_btn : null}`} onClick={()=> this.handleAmountChange(500)}>500</button>
                            <button className={`${style.amount_btn} ${this.state.amountSelected == 1000 ? style.active_btn : null}`} onClick={()=> this.handleAmountChange(1000)}>1000</button>
                            <button className={`${style.amount_btn} ${this.state.amountSelected == 2000 ? style.active_btn : null}`} onClick={()=> this.handleAmountChange(2000)}>2000</button>
                            <button className={`${style.amount_btn} ${this.state.amountSelected == 5000 ? style.active_btn : null}`} onClick={()=> this.handleAmountChange(5000)}>5000</button>
                        </div> 
                    </div>      
                    <div className={style.action_box}>
                        <button className={`full_width ${style.money_add_btn}`} onClick={()=> this.handleWalletAdd()}>Proceed to Add Money</button>
                    </div>
                </div>
                <div className="full-width seprator"></div>
                <div className="promocode paddingBox bold_text flax_box" style={{lineHeight:'20px'}}>
                    <img src={offer} style={{ height: '18px', width: '18px', marginRight:'10px' }} alt="offer" />
                    <span className={style.cur_bal_head}>Apply Promocode</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="7" height="11" viewBox="0 0 7 11" style={{alignSelf:'center'}}>
                        <path fill="#000" fillRule="nonzero" d="M0 1.363L1.39 0 7 5.5 1.39 11 0 9.637 4.22 5.5z"/>
                    </svg>
                </div>
                <div className="full-width seprator big_sep_blank"></div>
                <div className="tandc_Box paddingBox">
                    <div className={style.current_bal_box}>
                        <span style={{fontSize:'12px',opacity:.6,textTransform:'uppercase'}}>Terms & Conditions</span>
                    </div>
                    <div className={style.terms_box}>
                        <span className={style.terms_icon}>{infoNewIcon}</span>
                        <span className = {style.terms_text}>Money cannot be transferred to bank account as per updated RBI guidelines</span>
                    </div>
                </div>
            </div>
        );
    }
}