import React, {Component} from 'react'
import * as style from './style.scss'

export default class ResetOtp extends Component{
	// propTypes: {
	// 	isFetching: PropTypes.bool.isRequired,
	// 	isResetOPTFlag: PropTypes.bool.isRequired,
	// 	onAuth: PropTypes.func.isRequired,
	// },

	componentDidMount(){
		this.props.counterHandler()
	}

	render() {
		return (
			<div className = {style.resetOtpWrap}>
				<div onClick = {this.props.isResetOPTFlag ? this.props.onAuth : null} className = {style.resetSMSContainer}>
					<span className = {this.props.isResetOPTFlag ? "green_text" : null}>Resend SMS</span>
          <span className = {this.props.isResetOPTFlag ? "green_text" : null}>{!!this.props.callbackCount ? (" in 00:"+ this.props.callbackCount) : ""}</span>
				</div>
			</div>
		);
	}

}
