import React from 'react'
import * as style from './style.scss'

// OTPRequestButton.propTypes = {
// 	isFetching: PropTypes.bool.isRequired,
// 	onAuth: PropTypes.func.isRequired,
// 	otp: PropTypes.bool.isRequired,
// }

export default function OTPRequestButton ({isFetching, onAuth, otp} ){
    return (
      <div  className = "center_container">
      	<span className = {style.button_wraper}>
	        <button onClick={onAuth} className = "button green_button">
		      {isFetching === true
		        ? 'Loading'
		        : (otp === true ? 'Login' : 'Request for OTP')}
		    </button>
		</span>
      </div>
    )
}
