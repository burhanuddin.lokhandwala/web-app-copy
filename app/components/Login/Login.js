import React from "react";
import OTPRequestButton from "$COMPONENTS/Login/OTPRequestButton";
import ResetOtp from "$COMPONENTS/Login/ResetOtp";
import * as style from "./style.scss";
import { loginIcon, otpIcon } from "$SHAREDSTYLES/icons";

// Login.propTypes = {
// 	isFetching: PropTypes.bool.isRequired,
// 	onAuth: PropTypes.func.isRequired,
// 	error: PropTypes.string.isRequired,
// 	otp: PropTypes.bool.isRequired,
// 	updateInput: PropTypes.func.isRequired,
// 	handleOTP: PropTypes.func.isRequired,
// 	isResetOPTFlag: PropTypes.bool.isRequired,
// 	counterHandler:PropTypes.func.isRequired,
// 	onCallMeNow: PropTypes.func.isRequired,
// 	callbackCount: PropTypes.oneOfType([
// 		React.PropTypes.string,
// 		React.PropTypes.number,
// 	])
// }

export default function Login({
    error,
    isFetching,
    onAuth,
    otp,
    updateInput,
    handleOTP,
    callbackCount,
    isResetOPTFlag,
    counterHandler,
    onCallMeNow,
}) {
    return (
        <div className={`${style.loginContainer}`}>
            <p className="head_title">{otp === true ? otpIcon : loginIcon}</p>
            {otp === true ? (
                <div className="center_container">
                    <span className={style.otp_input_wraper}>
                        {" "}
                        <input
                            className="text_input"
                            onChange={updateInput}
                            type="text"
                            placeholder="Enter code (eg. 123456)"
                        />
                    </span>{" "}
                </div>
            ) : (
                <div className="container">
                    <div className="center_container">
                        <span className={style.phone_small_box}>+91</span>
                        <span className={style.phone_field_containe}>
                            <input
                                className="text_input"
                                onChange={updateInput}
                                type="text"
                                placeholder="10 digit mobile number"
                            />
                        </span>
                    </div>
                </div>
            )}
            {otp === true ? (
                <ResetOtp
                    isFetching={isFetching}
                    isResetOPTFlag={isResetOPTFlag}
                    callbackCount={callbackCount}
                    counterHandler={counterHandler}
                    onCallMeNow={onCallMeNow}
                    onAuth={handleOTP}
                />
            ) : null}
            <OTPRequestButton
                isFetching={isFetching}
                onAuth={onAuth}
                otp={otp}
            />
            {otp === true ? null : (
                <p className={style.agreement_text}>
                    {" "}
                    By continuing, you agree to our{" "}
                    <a
                        href="https://myramed.in/legal.html"
                        target="_blank"
                        className="green_text">
                        Terms & Conditions.
                    </a>
                </p>
            )}
            {error ? <p className="errorMessage">{error}</p> : null}
        </div>
    );
}
