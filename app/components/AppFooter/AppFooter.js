import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import * as style from './style.scss';
import {heart, orderList, account, cart, categories} from '$SHAREDSTYLES/icons';

export default class AppFooter extends Component{
	render(){
		let route_path = this.props.route_path;
		let isAuthed = this.props.isAuthed;
		return (
			<div className = {style.footer_container}>
				<Link to = "/page/home" className={style.footer_tab}>
					<div>{route_path && route_path === "page/home" ? heart("#ff7300") : heart("#B7B7B7")}</div>
					<div className={route_path && route_path == "page/home" ? style.active : style.inactive}>For you</div>
				</Link>
				<Link to = "/product_categories" className={style.footer_tab}>
					<div>{route_path && route_path === "product_categories" ? categories("#ff7300") : categories("#B7B7B7")}</div>
					<div className ={route_path && route_path === "product_categories" ? style.active : style.inactive}>Categories</div>
				</Link>
				<Link to = {isAuthed ? "/previoud_order" : "/login"} className={style.footer_tab}>
					<div>{route_path && route_path == "previoud_order" ? orderList("#ff7300") : orderList("#B7B7B7")}</div>
					<div className={route_path && route_path == "previoud_order"  ? style.active : style.inactive}>Orders</div>
				</Link>
				<Link to = {isAuthed ? "/accounts" : "/login"} className={style.footer_tab}>
					<div>{route_path && (route_path === "accounts" || route_path === "login") ? account("#ff7300") : account("#B7B7B7")}</div>
					<div className ={route_path && (route_path === "accounts" || route_path === "login") ? style.active : style.inactive}>Account</div>
				</Link>
				<Link to = {isAuthed ? "/cart" : "/login"} className={style.footer_tab}>
					<div>{route_path && route_path === "cart" ? cart("#ff7300") : cart("#B7B7B7")}</div>
					<div className ={route_path && route_path === "cart" ? style.active : style.inactive}>Cart</div>
				</Link>
			</div>
		)
	}
}
