import React, {Component}                    from 'react'
import { connect }                           from 'react-redux'
import { createStore }                       from 'redux'
import { bindActionCreators }                from 'redux'
import * as prescriptionActionCreater        from '$REDUX/modules/ordersPrescription'
import * as userActionCreater        from '$REDUX/modules/users'
import {handleRorderItems, handleRorderItem, handleWalletInfo} from '$REDUX/modules/previousOrder'
import * as style                            from './style.scss'
import OverLayPopover                        from '$PARTIALS/OverLayPopover/OverLayPopover'
import Loader                                from '$PARTIALS/Loader/Loader'
import {formatedRupee}                       from '$HELPERS/helperFunction'
import CartItems                             from '$COMPONENTS/Cart/CartItems'
import BillInformation                       from '$COMPONENTS/Cart/BillInformation'
import ItemsListContainer                    from '$CONTAINERS/Search/ItemsListContainer'
import Clevertap                             from '../../helpers/clevertap'

class OrderDetailView extends Component {
	constructor(props) {
	  super(props);

		let id = props.match.params.order_id
		let order = "", orderList = []

		if((props.previousOrderList && props.previousOrderList.length) || (props.currentOrderList && props.currentOrderList.length)){
			orderList = props.showReorder ? props.previousOrderList : props.currentOrderList
			order = this.getCurrentOrder(id, orderList)
		}
		else{
			props.handleGetOrder(id)
			props.handleWalletInfo(id)
		}

	  this.state = {
			order: order,
			showImageList: '',
	  };
	}

	componentDidMount = () => {
		document.body.scrollTop = 0
		let id = this.props.match.params.order_id
		let url = window.location.href
		this.props.handleWalletInfo(id)
		if(url.indexOf("/reorder") > 0){
			this.props.handleReorderFlag(true)
		}

	}

	getCurrentOrder = (id, orders)  => {
		if(orders && orders.length){
			let newOrder = orders.filter(order => {
				return order.id == id
			})[0]
			return newOrder
		}

	}

	handleRorderOrder = () => {
		Clevertap.event('mWeb - Order Details - Reorder', {
			'orderId': this.state.order ? this.state.order.display_id : ''
		})
		this.props.handleRorderItems({id: this.props.match.params.order_id}).then(x => {
			if(x){
				this.props.history.push('/cart')
			}
		})
	}

	handleRorderItem = (data, itemId) => {
		this.props.handleRorderItem(data, itemId, this.props.match.params.order_id)
	}

	cancelButton = () => {
		this.setState({showImageList: ''})
	}

	showImage = (ImageListIndex) => {
		let order =  this.state.order ? this.state.order : (this.props && this.props.currentOrder ? this.props.currentOrder : "")
		this.setState({showImageList: order.prescriptions[ImageListIndex]})
	}

	render (){
		let order =  this.state.order ? this.state.order : (this.props && this.props.currentOrder ? this.props.currentOrder : "")
		let date = new Date()
		if(order){
			let timestamp = this.props.isCurrentOrder
			? (order.checkout_timestamp
				? (order.checkout_timestamp)*1000
				: (order.expected_delivery_timestamp
					? (order.expected_delivery_timestamp)*1000
					: (order.entry_timestamp)*1000)
			)
			: (order.completed_timestamp ? order.completed_timestamp*1000
			   : order.expected_delivery_timestamp ? order.expected_delivery_timestamp*1000 : (order.entry_timestamp)*1000)

			date = new Date(timestamp)
			date = date.toString()
		}

		let noteList = ''
		if(order && order.extended_notes && order.extended_notes.length){
			noteList = order.extended_notes.map( (x, index) => {
				return (
					<p key = {index} className = "full_width">
						<span className = {style.item_text}>{x.item_text}</span>
						<span className = {style.quantity_text}>{x.quantity_text}</span>
					</p>
					)
			})
		}

		let imgList = ''

		if(order && order.prescriptions && order.prescriptions.length){
			imgList = order.prescriptions.map((x, index) => {
				return (
						<li key = {index}> <img onClick = {this.showImage.bind(this, index)} src = {x.image_url}/></li>
					)
			})
		}
		if(order && order.items){
			var orderItems = order.items.map(function(item){return item['medicine']})
		}
		return ((order && order.id)
			?(<div className = "gray_back_container ">
				{this.state.showImageList ? <OverLayPopover showImageList = {this.state.showImageList}  imageView = {true} cancelButton = {this.cancelButton}/> : null}
				{/*<span className = {style.show_id}> #{order.display_id}</span>*/}
				<div className = "container">
					<div className="section_heading">
              <span className= "section_title">View Order Details</span>
          </div>
					<div className = "field_container">
						<ul className = {style.order_detail_list}>
							<li>
								<span>Order ID &#183; </span>
								<span> &nbsp;{order.display_id}</span>
							</li>
							<li>
								<span>Order Delivered &#183;</span>
								<span>&nbsp;{date.slice(4,15)}</span>
							</li>
							<li>
								<span>Bill Total &#183;</span>
								<span>&nbsp; &#8377;{formatedRupee(order.total_amount)}</span>
							</li>
						</ul>
					</div>
				</div>


				{(order.extended_notes && order.extended_notes.length) || (order.items && order.items.length)
					?(
						<div className = "container">
							<div className="section_heading">
                <span className= "section_title">Items Ordered</span>
                {order.items[0].medicine.id && <span className= "section_right" onClick={this.handleRorderOrder}>REORDER ALL </span> }
              </div>
              {(order && order.items) ? <ItemsListContainer imageS itemList={orderItems} history = {this.props.history} isAuthed = {this.props.isAuthed} parentRef="Order Details" extraInfo={{orderId: order.id}}/> : ""}
							{<div className = {style.field_wraper}>
								{noteList}
								{/*itemList*/}
							</div>}
						</div>

						)
					: null
				}

				{order.prescriptions && order.prescriptions.length
					?(<div className = "container">
							<div className="section_heading">
	                <span className= "section_title">Prescription Uploaded</span>
	            </div>
							<div className = "field_container no_border">
								<ul className = {style.order_img_container}>
									{imgList}
								</ul>
							</div>
						</div>
					)
					: null
				}

				<div className = "container">
					<div className="section_heading">
              <span className= "section_title">Delivery Address</span>
          </div>
					<div>
						<div className= "box_container">
							{/*<div><b> {order.delivery_location && order.delivery_location.label ? order.delivery_location.label : null}</b></div>*/}
							{order.delivery_location ? order.delivery_location.address : null}
						</div>
					</div>
				</div>

				{order.is_price_confirmed && order.status.id != 7
					?(<div className = "container">
						<div className="section_heading">
                <span className= "section_title">Billing Information</span>
            </div>
						<BillInformation name={order.delivery_location.name}
							address={order.delivery_location.address}
							wallateInfo = {this.props.wallateInfo}
							totalMRP={formatedRupee(order.total_mrp)}
							totalDiscount={formatedRupee(order.total_discount)}
							totalAmount={formatedRupee(order.total_amount)}
							editNotAllowed={true}
    				/>
					</div>
					)
					: null
				}

			</div>)
			:(<Loader/>)
		)}

}


export default connect(
	({ordersPrescription, previousOrder,users}) => ({
		currentOrderList  : ordersPrescription.currentOrderList,
		currentOrder      : ordersPrescription.currentOrder,
		showReorder       : ordersPrescription.showReorder,
		isCurrentOrder    : ordersPrescription.isCurrentOrder,
		previousOrderList : previousOrder.previousOrderList,
		wallateInfo       : previousOrder.wallateInfo,
		isAuthed					: users.isAuthed
	}),
	(dispatch) => bindActionCreators({
		...prescriptionActionCreater,
		...userActionCreater,
		handleRorderItems,
		handleRorderItem,
		handleWalletInfo,
	}, dispatch)
)(OrderDetailView)
