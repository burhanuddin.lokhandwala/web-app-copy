import React, { Component } from "react";
import * as style from "./style.scss";
import { escalate } from "$SHAREDSTYLES/icons";
import SelectBox from "$PARTIALS/SelectBox/SelectBox";
import SmallLoader from "$PARTIALS/SmallLoader/SmallLoader";
import dumy_item from "../../sharedImage/dumy_item.png";
import express from "../../sharedImage/express-icon.svg";
import remove from "../../sharedImage/remove.svg";
import standard from "../../sharedImage/standard.svg";

export default class ItemCard extends Component {
    imgUrlError = e => {
        e.target.src = dumy_item;
    };

    render() {
        let props = this.props;
        let item = props.item,
            quantityOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "Other"];
        props.cartItems && props.cartItems[item.id] && props.cartItems[item.id][0] > 10
            ? quantityOptions.splice(quantityOptions.length - 1, 0, props.cartItems[item.id][0])
            : "";
        let qty =
            props.cartItems && !props.cartItems[item.id]
                ? item.packings && item.packings.length > 0 && item.packings.filter(x => x.is_saleable)[0].loose_quantity
                : props.cartItems && props.cartItems[item.id]
                ? props.cartItems[item.id][1]
                : 1;
        //	let qty= 1 ;
        let url = `https://cdn.myramed.in/${item.id}-1-150x150.jpg`;
        let itemAddedInCart = props.cartItems && item.id && item.id in props.cartItems;
        let cartItem = props.cartItems && props.cartItems[item.id] && props.cartItems[item.id][3];

        return (
            <li className="sku-list-item">
                <div style={{ marginRight: "15px" }}>
                    <img onError={e => this.imgUrlError(e)} src={url ? url : dumy_item} />
                </div>
                <div className="flex-grow-1">
                    <div className="d-flex">
                        <div style={{ maxWidth: "90%", width: "100%" }}>
                            <p className="company-name">{props.manufacturerName}</p>
                            <h3 className="item-name">{props.medicineName}</h3>
                        </div>
                        {itemAddedInCart && (
                            <img
                                src={remove}
                                alt="remove-icon"
                                style={{ width: "24px", height: "24px" }}
                                onClick={() => props.handleRemoveItem(item.id)}
                            />
                        )}
                    </div>
                    <div className="d-flex justify-content-between align-items-center">
                        <div>
                            <span style={{ fontSize: "14px" }}>&#8377; {(item.discounted_price * qty).toFixed(2)}</span>
                            <span className="lined_text small_disable_text m-l-10">&#8377; {(item.mrp * qty).toFixed(2)}</span>
                            <span className="discount">{item.discount}% OFF</span>
                        </div>
                        {props.isFetching && props.updatedMedicine == item.id ? (
                            <SmallLoader />
                        ) : itemAddedInCart ? (
                            <div style={{ maxWidth: "64px" }}>
                                <SelectBox
                                    noSelect={true}
                                    currentSelected={props.cartItems[item.id][0]}
                                    optionList={quantityOptions}
                                    handleChangeOption={props.handleQtyChange.bind(this, item.id)}
                                />
                            </div>
                        ) : item.out_of_stock || !item.id ? (
                            ""
                        ) : item.substitute && Object.keys(item.substitute).length ? (
                            <button
                                type="button"
                                className="add-btn"
                                onClick={() =>
                                    props.handleGetAvailability({
                                        dv_id: item.id,
                                        quantity: 1,
                                        context: props.parentRef,
                                        sub_context: "retrieve_substitutes",
                                        source: "web_manual",
                                    })
                                }>
                                {this.props.isFetchingAvailability && this.props.updatedMedicine == item.id ? <SmallLoader /> : "Substitutes"}
                            </button>
                        ) : (
                            <button type="button" className="add-btn" onClick={() => props.handleAddItem(item)}>
                                ADD
                            </button>
                        )}
                    </div>
                    <div className="m-t-5 d-flex align-items-center font-12">
                        {cartItem && (
                            <React.Fragment>
                                <img
                                    src={
                                        props.cartItems[item.id][3].is_delivery_scheduling_enabled &&
                                        props.cartItems[item.id][3].bucket_name !== "pseudo"
                                            ? express
                                            : standard
                                    }
                                    alt="delivery-type"
                                    style={{ marginRight: "10px" }}
                                />
                                <p>{props.cartItems[item.id][3].fulfilment_text}</p>
                            </React.Fragment>
                        )}
                        {item.out_of_stock && <p>{escalate} Out of stock</p>}
                        {props.parentRef == "Search" && item.substitute && item.substitute_reason && (
                            <p>
                                {escalate} {item.substitute_reason}
                            </p>
                        )}
                    </div>
                </div>
            </li>
        );
    }
}
