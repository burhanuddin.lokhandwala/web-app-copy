import React from 'react';
import { Link } from 'react-router'
import {card_container, itemName, manufacturerName, discountedPrice, discount, mrp, packing, cardActions, addBtn, removeBtn, selectQty, extended} from './style.scss'
// import { removeIcon } from '$SHAREDSTYLES/icons'
import remove from "../../sharedImage/remove.svg";


function SearchItemCard(props) {
    return (
       <div>
        {props.itemList ?
            props.itemList.map((item) => {
                item = item.medicine ? item.medicine : item
                return(<li key={item.id} className={card_container}>
                    <div className={manufacturerName}>{item.manufacturer_name}</div>
                    <div className={itemName}>{item.formatted_name}</div>
                    <div>
                    {(item.id in props.packetTypeChange) ?
                        <span>
                            <span >&#8377;{item.allowed_quantity_types[props.packetTypeChange[item.id]].mrp_discounted.toFixed(2)}</span>
                            {/*item.allowed_quantity_types[props.packetTypeChange[item.id]].discount ? <span className={discount}><small>{item.allowed_quantity_types[0].discount}% OFF</small></span>: ''*/}
                            {item.allowed_quantity_types[props.packetTypeChange[item.id]].discount ? <span className={mrp}>SAVED &#8377;{item.allowed_quantity_types[props.packetTypeChange[item.id]].mrp.toFixed(2)}</span>: ''}
                        </span>
                        :
                        <span>
                            <span >&#8377;{item.allowed_quantity_types[0].mrp_discounted.toFixed(2)}</span>
                            {/*item.allowed_quantity_types[0].discount ? <span className={discount}><small>{item.allowed_quantity_types[0].discount}% OFF</small></span>: ''*/}
                            {item.allowed_quantity_types[0].discount ? <span className={mrp}>SAVED &#8377;{item.allowed_quantity_types[0].mrp.toFixed(2)}</span>: ''}
                        </span>
                    }
                    </div>
                    <div>
                    {(item.allowed_quantity_types.length > 1) ?
                        <select onChange={props.handlePacketTypeChange.bind(this, item.id)} value={props.packetTypeChange[item.id]}>
                            {item.allowed_quantity_types.map((key, index) => {
                                return(<option key={key.label_formatted_full} value={index}>{key.label_formatted_full}</option>)
                            })}
                        </select>
                     :
                        <div className={packing}>Packing: {item.packing_quantity}</div>
                    }
                    <div className={cardActions}>
                    {(item.id in props.cartItems) ?
                        <div>
                        <img src={remove} alt="remove-icon" style={{ width: '24px', height: '24px' }} onClick={() => props.handleRemoveItem(item.id)}/>
                        <select className={selectQty} value={props.cartItems[item.id][0]} onChange={props.handleQtyChange.bind(this, item.id)}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            {(props.cartItems[item.id][0] > 10) ? <option value={props.cartItems[item.id][0]}>{props.cartItems[item.id][0]}</option> : ''}
                            <option value="other">Other</option>
                        </select>
                        </div>
                        :
                        <div className={addBtn} onClick={() => props.handleAddItem(item.id)}>ADD</div>}
                    </div>
                    </div>
                </li>)
            })
        : ''}
        {props.itemList ?
        <li className={card_container}>
            <div className={itemName}>{props.searchInput}</div>
            <div className={extended}>
                <div className={packing}>Receive a call back for item details</div>
                <div className={cardActions}>
                    {(props.searchInput in props.extendedNotes) ?
                        <div>
                        <img src={remove} alt="remove-icon" style={{ width: '24px', height: '24px' }} onClick={() => props.handleExtendedNotes(props.searchInput, 'remove')}/>
                        <select className={selectQty} value={props.extendedNotes[props.searchInput]} onChange={props.handleExtendedNotes.bind(this, props.searchInput, 'update')}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            {(props.extendedNotes[props.searchInput] > 10) ? <option value={props.extendedNotes[props.searchInput]}>{props.extendedNotes[props.searchInput]}</option> : ''}
                            <option value="other">Other</option>
                        </select>
                        </div>
                        :
                        <div className={addBtn} onClick={() => props.handleExtendedNotes(props.searchInput, 'add')}>ADD</div>}
                </div>
            </div>
        </li> : ''}
       </div>
    );

}

export default SearchItemCard;
