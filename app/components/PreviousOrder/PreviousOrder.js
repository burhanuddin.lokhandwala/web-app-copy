import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as style from "./style.scss";
import { getFormatedDate } from "$HELPERS/helperFunction";
import { arrow } from "$SHAREDSTYLES/icons";

export default class PreviousOrder extends Component {
    handleRorderItems = e => {
        this.props.handleRorderItems(this.props.orderItem.id);
    };

    handleViewItem = () => {
        this.props.handleViewItem(true);
    };

    render() {
        let orderItem = this.props.orderItem;
        let completed_timestamp =
            orderItem && orderItem.completed_timestamp !== undefined
                ? orderItem.completed_timestamp
                : 1;
        let formatedDate = getFormatedDate(completed_timestamp);

        let isReorderAvailable =
            orderItem &&
            orderItem.items &&
            orderItem.items.filter(x => x.reordered).length
                ? true
                : false;
        let ID = (orderItem && orderItem.id) || 0;

        return (
            <div className={style.order_container}>
                <div className={style.calendar_wrapper}>
                    <h3 className={style.calender_date}>{formatedDate.date}</h3>
                    <span>
                        {formatedDate.month} {formatedDate.year}
                    </span>
                </div>
                <div style={{ flexGrow: "1", marginLeft: '12px', maxWidth: '80%' }}>
                    <Link
                        className={style.link_wrapper}
                        to={{ pathname: "/users/reorder/" + ID }}
                        onClick={this.handleViewItem}>
                        <p>
                            {orderItem.items.length} Item
                            {orderItem.items.length > 1 ? "s" : ""}
							&nbsp;-&nbsp;
                            <span className="m-r-10">
                                #{orderItem.display_id}
                            </span>
                        </p>
                        {arrow}
                    </Link>
                    <div className={style.medicine_list_wraper}>
                        {orderItem.items &&
                            orderItem.items.length &&
                            orderItem.items.slice(0, 3).map(x => {
                                return (
                                    x.medicine &&
                                    x.medicine.formatted_name && (
                                        <div
                                            className={style.medicine_name}
                                            key={x.id}>
                                            {x.medicine.formatted_name}
                                        </div>
                                    )
                                );
                            })}
                    </div>

                    <div className="d-flex align-items-center justify-content-between">
                        <p style={{ lineHeight: '1' }}>
                            {orderItem.process_state.toLowerCase() ===
                            "cancelled" ? (
                                <span style={{ fontSize: "12px" }}>
                                    <span className={style.cancelled_icon}>x</span>
                                    Order Cancelled
                                </span>
                            ) : (
                                <React.Fragment>
                                    <span
                                        style={{
                                            fontSize: "14px",
                                            marginRight: "5px",
                                        }}>
                                        &#8377;{orderItem.total_amount}
                                    </span>
                                    <span
                                        style={{
                                            color: "#005073",
                                            fontSize: "12px",
                                        }}>
										saved&nbsp;&#8377;
                                        {orderItem.total_discount}
                                    </span>
                                </React.Fragment>
                            )}
                        </p>
                        {isReorderAvailable && (
                            <button
                                type="button"
                                onClick={this.handleRorderItems}
                                className={style.reorder}>
                                Reorder
                            </button>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}
