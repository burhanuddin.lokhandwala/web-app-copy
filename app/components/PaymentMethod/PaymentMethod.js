import React, {Component} from 'react'
import * as style from './style.scss'

export default class PaymentMethod extends Component{

	render () {
		let paymentMethod = this.props.paymentList.map((x, index) => {
			// debugger
			return <li key={index} >
							<label htmlFor= {x.label} >
								<input checked= {x.id == this.props.paymentMethod} onChange= {this.props.updatePaymentMethod} type= "radio" name= {"payment"} value= {x.id} id= {x.label}/>
								<span  >{x.label}</span>
							</label>

						</li>
					})
		return (
			<ul className = {style.paymentList}>

				{paymentMethod}
			</ul>
		)
	}
}

