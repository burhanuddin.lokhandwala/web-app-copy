import React from 'react';
import * as style from './style.scss'
import {PayTM_icon, MobiKwik_icon, Freecharge_icon, Simpl_icon, Card_icon, Cash_icon, upiIcon} from '$SHAREDSTYLES/icons'

const DirectPayment = (props) => {
    const iconMap = {
        1 : PayTM_icon,
        2 : Freecharge_icon,
        3 : MobiKwik_icon,
        4 : Card_icon,
        5 : Card_icon,
        6 : Cash_icon,
        8 : Simpl_icon,
        12 : upiIcon
    }

    return (
        <div>
            {props.methods && props.methods.length ?
                props.methods.map((item) => {
                    let url = item.url || ''
                    if(props.disabledMethodIDs && props.disabledMethodIDs.indexOf(item.id) != -1){
                        return;
                    }
                    return(
                        <div key={item.label} className={style.payment_option} onClick={() => props.handlePaymentMethod({'id':item.id, 'gateway': item.gateway, 'url': url})}>
                            <span className={style.icon}>{item.id ? iconMap[item.id] : ""}</span>
                            <span className={style.label}>{item.label}</span>
                            <span className={style.checked}>
                                {props.paymentMethod == item.id ?
                                    <div className={style.checkmark_border + " " + style.checkmark_border_none}>
                                        <div className={style.checkmark + " " + style.green_border}></div>
                                    </div>
                                :
                                    <div className={style.checkmark_border}>
                                        <div className={style.checkmark}></div>
                                    </div>
                                }
                            </span>
                            {item.offers ? <div className={style.offers}>{item.offers}</div> : ''}
                            {item.id == 6 ? <div className={style.offers}>Please keep exact change at the time of delivery to help you serve better.</div> : ''}
                        </div>
                        )
                })
            : ''}
        </div>
    );
};


export default DirectPayment;
