import React from "react";
import * as style from "./style.scss";
import Input from "$PARTIALS/Input/Input";


export default function EditLocation(props) {
    return (
        <div className="container">
            <ul className={style.location_edit_container}>
                <li>
                    <Input
                        disabled={props.isLocationName}
                        name="name"
                        id="name"
                        type="text"
                        value={props.current_edit_location.name == null ? "" : props.current_edit_location.name}
                        onChange={props.updateAddressName}
                        label="Name"
                        className={style.input_field}
                    />
                </li>
                <li>
                    <Input
                        name="label"
                        id="label"
                        type="text"
                        value={props.current_edit_location.label == null ? "" : props.current_edit_location.label}
                        onChange={props.updateAddressLabel}
                        label="Label"
                        className={style.input_field}
                    />
                </li>
                <li>
                    <div className={style.label_text}>Address</div>
                    <p style={{ fontSize: '14px' }}>{props.current_edit_location.address}</p>
                </li>
				<li className="m-t-10">
					<button onClick={props.handleDeleteLocation} className={`red_button button ${style.button}`}>
                        Delete Address
                    </button>
				</li>
            </ul>
        </div>
    );
}
