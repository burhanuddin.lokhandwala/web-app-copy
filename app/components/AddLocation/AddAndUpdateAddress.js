import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as style from "./style.scss";
import Input from "$PARTIALS/Input/Input";
import { homeIcon, selectedHomeIcon, officeIcon, selectedOfficeIcon, otherPlaceIcon, selectedOtherPlaceIcon } from "$SHAREDSTYLES/icons";
import Clevertap from "../../helpers/clevertap";

export default class AddAndUpdateAddress extends Component {
    constructor(props) {
        super(props);
        let formatedAddress = props.formatedAddress && props.formatedAddress.length ? props.formatedAddress : "";
        // console.log(props);
        this.state = {
            label: "",
            address: formatedAddress && formatedAddress[0] ? formatedAddress[0].content : "",
            streetName: formatedAddress && formatedAddress[1] ? formatedAddress[1].content : "",
            location: formatedAddress && formatedAddress[2] ? formatedAddress[2].content : "",
            landmark: "",
            name: this.props.userInfo ? this.props.userInfo.name : "",
            other: "",
            isOtherLabel: false,
            isHomeLabel: false,
            isOffoceLabel: false,
        };
    }

    componentDidMount() {
        // console.log("origin", this.props.origin, this.state.streetName,this.state.location);
        if (this.state.streetName && this.state.streetName.length && this.state.location && this.state.location.length) {
            let origin = "";
            if (this.props.origin != "GPS") {
                origin = "GooglePlaceSearch";
            } else {
                origin = this.props.origin;
            }
            Clevertap.event("mWeb - Address Edit Loaded", { origin: origin, location: this.props.location, streetName: this.props.streetName });
        }
    }

    handleLabel = e => {
        this.setState({ label: e.target.getAttribute("data"), other: "" });
    };

    handleAddressChange = e => {
        let state = {};

        let nameOfField = e.target.name;
        let valueOfField = e.target.value;
        Clevertap.event("mWeb - Address Edit Typing", { nameOfField: valueOfField });
        state[e.target.name] = e.target.value;
        if (e.target.name == "other") state.label = "";
        this.setState(state);
    };

    // handleLandmarkChange = (e) => {
    // 	this.setState({landmark: e.target.value})
    // }

    // handleStreetNameChange = (e) => {
    // 	this.setState({streetName: e.target.value, noStreetName: false})
    // }

    // handleNameChange = (e) => {
    // 	this.setState({name: e.target.value, noName: false})
    // }

    // handleHomeLabel = () => {
    // 	this.setState({label: 'Home', isOtherLabel: false, isHomeLabel: true, isOffoceLabel: false})
    // }

    // handleOfficeLabel = () => {
    // 	this.setState({label: 'Office', isOtherLabel: false, isHomeLabel: false, isOffoceLabel: true})
    // }

    // handleOtherPlaceLabel = () => {
    // 	this.setState({label: '', isOtherLabel: true, isHomeLabel: false, isOffoceLabel: false})
    // }

    handleDone = () => {
        let state = this.state;
        let location = {};
        if (!state.streetName || !state.name || (!state.label && !state.other) || !state.address) {
            if (!state.label && !state.address) {
                this.setState({ noAddress: true });
                alert("Please add your house address(e.g. Flat/house no.) and Label(e.g. Home, Office or Others)!!");
                // return
            } else if (!state.label && !state.other) {
                alert("Please Select Label e.g. Home, Office or Others!!");
                // return
            } else if (!state.address) {
                this.setState({ noAddress: true });
                alert("Please add your house address!!");
                // return
            }

            if (!state.streetName) {
                this.setState({ noStreetName: true });
            }
            if (!state.name) {
                this.setState({ noName: true });
            }
            return;
        }
        if (this.props.googleAddress) {
            location = {
                label: state.other ? state.other : state.label,
                google_place_id: this.props.googleAddress.place_id,
                longitude: this.props.googleAddress.geometry.location.lng,
                latitude: this.props.googleAddress.geometry.location.lat,
                name: state.name,
                address: `${state.address} ${state.streetName} ${state.location} ${state.location}`,
                landmark: state.landmark,
                streetName: state.streetName,
            };
        } else {
            location = {
                label: state.other ? state.other : state.label,
                name: state.name,
                address: `${state.address} ${state.streetName} ${state.location} ${state.location}`,
                landmark: state.landmark,
                streetName: state.streetName,
            };
        }

        this.props.handleUpdateAddress(location);
    };

    render() {
        let formatedAddress = this.props.formatedAddress && this.props.formatedAddress.length ? this.props.formatedAddress : "";
        return (
            <div className={style.location_container}>
                <div className={style.location_wrap}>
                    <div className={style.location_label}>Address Details</div>
                    <div>
                        <Input
                            wrap_style={{ marginBottom: "25px" }}
                            disabled={formatedAddress && formatedAddress[0] ? !formatedAddress[0].is_editable : false}
                            value={this.state.address ? this.state.address : ""}
                            name="address"
                            id="address"
                            type="text"
                            onChange={this.handleAddressChange}
                            label="Flat/ House/ Floor Number"
                        />
                        <Input
                            wrap_style={{ marginBottom: "25px" }}
                            disabled={formatedAddress && formatedAddress[1] ? !formatedAddress[1].is_editable : false}
                            value={this.state.streetName ? this.state.streetName : ""}
                            name="streetName"
                            id="streetName"
                            type="text"
                            onChange={this.handleAddressChange}
                            label="Building/ Street"
                        />
                        <Input
                            wrap_style={{ marginBottom: "25px" }}
                            disabled={formatedAddress && formatedAddress[2] ? !formatedAddress[2].is_editable : false}
                            value={this.state.location ? this.state.location : ""}
                            name="location"
                            id="location"
                            type="text"
                            onChange={this.handleAddressChange}
                            label="Locality"
                        />
                    </div>
                    <div className="d-flex align-items-center m-b-15">
                        <span
                            data="home"
                            onClick={this.handleLabel}
                            className={`${this.state.label == "home" ? style.location_lable_selected : ""} ${style.location_label_button}`}>
                            Home
                        </span>
                        <span
                            data="work"
                            onClick={this.handleLabel}
                            className={`${this.state.label == "work" ? style.location_lable_selected : ""} ${style.location_label_button}`}>
                            Work
                        </span>
                        <span className={style.location_label_input}>
                            <Input
                                value={this.state.other ? this.state.other : ""}
                                name="other"
                                id="other"
                                type="text"
                                onChange={this.handleAddressChange}
                                label="Other"
                            />
                        </span>
                    </div>
                    <Input
                        value={this.state.landmark ? this.state.landmark : ""}
                        name="landmark"
                        id="landmark"
                        type="text"
                        onChange={this.handleAddressChange}
                        label="Landmark to helps us reach on time (optional)"
                    />
                </div>
                <div className={style.location_wrap}>
                    <div className={style.location_label}>Customer Details</div>
                    <Input
                        value={this.state.name ? this.state.name : ""}
                        name="name"
                        id="name"
                        type="text"
                        onChange={this.handleAddressChange}
                        label="Name"
                    />
                </div>
                {/*<span className = "field_label">Address</span>
								<div className = {style.loc_edit_container}>
									<div className = {style.fild_text} >{this.props.formatedAddress[2].content} </div>
								</div>
								<span className = "field_label">Details</span>
								<div className = {style.loc_edit_container}>
									<input type = "text" onChange = {this.handleAddressChange} className = {this.state.noAddress ? style.error_fild_text : style.fild_text} placeholder = "Flat/house no."/>
									<input type = "text" onChange = {this.handleStreetNameChange} value = {this.state.streetName} className = {this.state.noStreetName ? style.error_fild_text : style.fild_text} placeholder = "Street Name"/>
									<input type = "text" onChange = {this.handleLandmarkChange} className = {style.fild_text} placeholder = "Landmark(optional)"/>
								</div>
								<div className = {style.customer_name_wraper}>
									<input type = "text" onChange = {this.handleNameChange} value = {this.state.name}  className = {this.state.noName ? style.error_fild_text : style.fild_text} placeholder = "Customer name"/>
								</div>
								<span className = "field_label">pick address type</span>
								<div className = {style.loc_icon_container} >
									<span className = {style.loc_type_icon_wraper}>
										<span style = {{background: this.state.isHomeLabel ? "#ff7300" : "#fff", color: !this.state.isHomeLabel ? "#ff7300" : "#fff"}} onClick = {this.handleHomeLabel} className = {style.home_type_icon}>{this.state.isHomeLabel ? selectedHomeIcon : homeIcon}</span>
										<span style = {{background: this.state.isOffoceLabel ? "#ff7300" : "#fff", color: !this.state.isOffoceLabel ? "#ff7300" : "#fff"}} onClick = {this.handleOfficeLabel} className = {style.office_type_icon}>{this.state.isOffoceLabel ? selectedOfficeIcon : officeIcon}</span>
										<span style = {{background: this.state.isOtherLabel ? "#ff7300" : "#fff", color: !this.state.isOtherLabel ? "#ff7300" : "#fff"}} onClick = {this.handleOtherPlaceLabel} className = {style.otherPlace_type_icon}>{this.state.isOtherLabel ? selectedOtherPlaceIcon : otherPlaceIcon}</span>
									</span>
								</div>
								{this.state.isOtherLabel
									?(<p className = "full_width">
										<input type = "text" onChange = {this.handleLabel} className = {style.other_label_text} placeholder = "Label"/>
									</p>)
									: null
								}
								*/}
				<button onClick={this.handleDone} className="fixed_btn button">
					Save Address
				</button>
            </div>
        );
    }
}
