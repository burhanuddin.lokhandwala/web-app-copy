import React from "react";
import { Link } from "react-router-dom";
import * as style from "./style.scss";
import {
    sorryIcon,
    smallSearchIcon,
    scooter,
    work,
    home,
    other,
    unavailable,
    truck,
    captureLocation,
} from "$SHAREDSTYLES/icons";
import express from '../../sharedImage/express-icon.svg';
import standard from '../../sharedImage/standard.svg';
import Clevertap from "../../helpers/clevertap";

// AllLocationsList.propTypes = {
//   	unAvailLocation: PropTypes.array,
//   	availLocation: PropTypes.array,
//   	editLocation: PropTypes.func,
// }

export default function AllLocationsList({
    unAvailLocation,
    availLocation,
    changeLocation,
    backToSearchLocation,
    isChangeLocation,
    editLocation,
    locationListView,
    route,
    askForLocation,
    geoError,
    handleShowAllAddress,
    addressCount,
}) {
    let avainList = "",
        unAvainList = "";
    let position = { Home: home, Work: work, Office: work, express: scooter, next_day: truck };
    if (availLocation && availLocation.length) {
        avainList = availLocation.slice(0, addressCount).map((list, index) => {
            return (
                <li key={index} onClick={isChangeLocation ? changeLocation.bind(null, list) : editLocation.bind(null, list)}>
                    <div className={style.location_icon}>{position[list.label] ? position[list.label] : other}</div>
                    <div className={style.location_section}>
                        <h3 className={style.location_label}> {list.label}</h3>
                        <div className="d-flex">
                            <div className={style.detailed_address}>
                                {list.address}
                            </div>
							<img src={list.delivery_type == "express" ? express : standard} alt="delivery-type" />
                        </div>
                    </div>
                </li>
            );
        });
    }

    if (unAvailLocation && unAvailLocation.length) {
        Clevertap.event("mWeb - Empty Address Screen", { origin: "UnAvailableLoc" });
        unAvainList = unAvailLocation.map((list, index) => {
            return (
                <li className={style.disable_text} key={index} onClick={isChangeLocation ? null : editLocation.bind(null, list)}>
                    <div className={style.location_icon}>{unavailable}</div>
                    <div className={style.location_section}>
                        <div>{list.label}</div>
						<div className={style.detailed_address}>
							{list.address}
						</div>
                    </div>
                </li>
            );
        });
    }
    return (
        <div>
            <div className="p-15 section-border">
                {isChangeLocation && !route && (
                    <div className={style.sorry_icon_wraper}>
                        <div className={style.sorry_icon}>
                            <span>{sorryIcon}</span>
                        </div>
                        <div>Sorry, we don't deliver here..yet.</div>
                    </div>
                )}
                <h3 className={style.sec_header}>Add Location</h3>
                <Link to="/search_location" onClick={backToSearchLocation} className={style.add_location_wrp}>
                    <span className={style.location_icon}>{smallSearchIcon}</span>
                    <span className="m-l-5">Search for area or street name..</span>
                </Link>
            </div>
            <div className="p-15 section-border">
                <div className={style.add_location_wrp} onClick={askForLocation}>
                    <span className={style.location_icon}>{captureLocation}</span>
                    <span className={`m-t-0 ${style.location_text}`}> Pick Your Current Location</span>
                </div>
                {geoError && <div className="full_width errorMessage">{geoError}</div>}
            </div>
            {availLocation && availLocation.length && (
                <div className="p-15 section-border">
                    <h3 className={style.sec_header}>Saved Addresses</h3>
                    <ul className={style.location_list_container}>{avainList}</ul>
                    {availLocation.length !== addressCount && (
                        <div onClick={handleShowAllAddress} className={style.show_address_button}>
                            View All Address
                        </div>
                    )}
                </div>
            )}
            {unAvailLocation && unAvailLocation.length && !route && (
                <div className="p-15">
                    <h3 className={style.sec_header}>Sorry we are not here yet..</h3>
                    <ul className={style.location_list_container}>{unAvainList}</ul>
                </div>
            )}
        </div>
    );
}
