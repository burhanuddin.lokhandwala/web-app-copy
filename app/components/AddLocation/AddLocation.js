import React from "react";
import * as style from "./style.scss";
import SearchBox from "$PARTIALS/SearchBox/SearchBox";
import { searchIcon } from "$SHAREDSTYLES/icons";

// AddLocation.propTypes = {
//   	askForLocation: PropTypes.func.isRequired,
//   	locationAutoComplete: PropTypes.func.isRequired,
//   	googleAddressList: PropTypes.array,
//   	changeAddress: PropTypes.func.isRequired,
//   	geoError: PropTypes.string,
// }

export default function AddLocation({ locationAutoComplete, googleAddressList, changeAddress, geoError }) {
    console.log("googleAddressList", googleAddressList);
    return (
        <div className="p-15">
            <div className={style.search_location_container}>
				<div className={style.google_search_location}>
					<span className={style.search_icon}>{searchIcon}</span>
					<SearchBox
						resultList={googleAddressList}
						onListClick={changeAddress}
						keyVel="description"
						keySelector="place_id"
						inputChangeHandler={locationAutoComplete}
						placeholder={"Search Area (eg. M.G. Road)"}
					/>
				</div>
                <div className="full_width errorMessage"> {geoError} </div>
            </div>
            <div className={style.google_logo} />
        </div>
    );
}
