import React from "react";
import { Link } from "react-router-dom";

export default function GoToPage(pageID, markup) {
    return (
        <Link to={{ pathname: "/page/".concat(pageID) }}>
            {markup}
        </Link>
    );
}
