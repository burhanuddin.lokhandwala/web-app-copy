import React from 'react';
import { myraIcon, starRating, gPlay } from '$SHAREDSTYLES/icons'
import {prompt_container, logo, relative_layout, close_btn, install_btn, left_side, right_side, clear, cta} from './style.scss'

const INSTALL_LINK = "https://go.onelink.me/PnPT?pid=Website&c=InstallPrompt"

function Prompt(props) {

    return (
        <div className={prompt_container}>
            <div className={relative_layout}>
                <div className={logo}>{ myraIcon }</div>
                <div><h3>Myra</h3></div>
                <div>{starRating}</div>
                <div className={close_btn} onClick={props.handleCloseBtn}>&times;</div>
                <div className={install_btn}>
                    <a href={INSTALL_LINK} target="_blank"><div className={cta}>INSTALL</div></a>
                </div>
            </div>

            <div className={clear}></div>

            <div>
            <div className={left_side}>
                <div><strong>Save Big On Medicines</strong></div>
                <div>Install app & get flat 20% off on medicines. 2-hour delivery!</div>
            </div>
            </div>
        </div>
    );

}

export default Prompt;
