import React, {Component} from "react";
import { Link } from "react-router-dom";

export default function Image(props) {
    const handleActions = (props) => {
      console.log(props);
      switch (props.action_type) {
        case "go_to_page":
            location.href=`page/${props.page_id}`;
            break;
        case "go_to_web_view":
        window.location.href=props.url;
            break;
        case "go_to_search":
             location.href=`search_medicine`;
            break;
        
    }
      }
    
    //   const listItems = props.sub_components.map((i) => <div onClick={()=>handleActions(i)} className="actionItem" key={i.attributes.page_id}><img className="scroller-image" src={i.attributes.image}/></div>);
      // <div key={i.page_id}><img src={i.image}/></div>);
      // let subComponents = getSubComponentsFromData(props);

        return (
            <div className="d-flex section-border" onClick={()=>handleActions(props)}>
                <img className="scroller-image" src={props.image_url} alt={props.text}/>
            </div>
        );
  }
  
