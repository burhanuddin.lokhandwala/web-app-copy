import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";

// const handleActions = (prop) => {
// console.log(prop);
// }

export default function ImageScroller(props) {
    const handleActions = prop => {
        switch (prop.attributes.action_type) {
            case "go_to_page":
                location.href = `page/${prop.attributes.page_id}`;
                break;
            case "go_to_web_view":
                window.location.href = prop.attributes.url;
                break;
            case "go_to_search":
                location.href = `search_medicine`;
                break;
        }
    };

    const listItems = props.sub_components.map(i => (
        <div
            onClick={() => handleActions(i)}
            className="actionItem"
            key={i.attributes.page_id}>
            <img className="scroller-image" src={i.attributes.image} />
        </div>
    ));
    // <div key={i.page_id}><img src={i.image}/></div>);
    // let subComponents = getSubComponentsFromData(props);
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        autoplaySpeed: 5000,
        slidesToScroll: 1,
    };
    return (
        <div className="image-scroller-container section-border">
            <Slider {...settings}>{listItems}</Slider>
        </div>
    );
}
