import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateSearchInput } from "$REDUX/modules/cart";

class GoToSearch extends Component {
    handleClick = () => {
        const { updateSearchInput, searchTerm } = this.props;
        updateSearchInput(searchTerm);
    };

    render() {
        const { markup } = this.props;
        return (
            <Link
                to={{ pathname: "/search_medicine/" }}
                onClick={this.handleClick}>
                {markup}
            </Link>
        );
    }
}

export default connect(
    null,
    dispatch => bindActionCreators({ updateSearchInput }, dispatch)
)(GoToSearch);
