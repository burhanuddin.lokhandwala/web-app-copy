import React from "react";
import GoToPage from "../GoToPage/GoToPage";
import GoToSearch from "../GoToSearch/GoToSearch";
import dumy_item from "../../sharedImage/dumy_item.png";

function imgUrlError(e) {
    e.target.src = dumy_item;
}

function handleUpdateMedicineCount(handleUpdateMedicineCount, item, referenceName) {
	item.query = {context: referenceName , action: "added", source: "web_manual"}; //check the context with the developers
	handleUpdateMedicineCount(item);
}

function SkuListItem(props) {
    let item = props.medicine;
    let url = `https://cdn.myramed.in/${item.id}-1-150x150.jpg`;
    let markup = (
        <div className="sku-list-item">
            <div style={{ marginRight: "15px" }}>
                <img
                    onError={e => imgUrlError(e)}
                    src={url ? url : dumy_item}
                />
            </div>
            <div className="flex-grow-1">
                <p className="company-name">{item.manufacturer_name}</p>
                <h3 className="item-name">{item.formatted_name}</h3>
                <div className="d-flex justify-content-between align-items-center">
                    <div>
                        <span>&#8377; {item.discounted_price.toFixed(2)}</span>
                        <span className="lined_text small_disable_text m-l-10">
                            &#8377; {item.mrp.toFixed(2)}
                        </span>
                        <span className="discount">{item.discount}% OFF</span>
                    </div>
                    <button type="button" className="add-btn" onClick={() => handleUpdateMedicineCount(props.handleUpdateMedicineCount, item, props.referenceName)}>
                        {props.button_text}
                    </button>
                </div>
            </div>
        </div>
    );
    return markup;
}

function getSubComponentsFromData(subComponentsData, handleUpdateMedicineCount, referenceName) {
    let subComponents = [];

    for (const [index, data] of subComponentsData.entries()) {
        let subComponentProps = {
            ...data["attributes"],
			key: index,
            handleUpdateMedicineCount,
            referenceName,
        };
        subComponents.push(<SkuListItem {...subComponentProps} />);
    }
    return subComponents;
}

function renderActionType(actionType, buttonText, id, searchTerm) {
    let markup =
        buttonText != null ? (
            <button className="view-all-button">{buttonText}</button>
        ) : null;
    switch (actionType) {
        case "go_to_page":
            markup = GoToPage(id, markup);
            break;
        case "go_to_search":
            markup = <GoToSearch searchTerm={searchTerm} markup={markup} />;
            break;
    }
    return markup;
}

export default function SkuList(props) {
    let subComponentsData = props["sub_components"] || [];
    let referenceName = props["name"] || '';
    let subComponents = subComponentsData.length > 0 ? getSubComponentsFromData(subComponentsData, props.handleUpdateMedicineCount, referenceName) : null;
    // console.log("skulist props: ", props);
    let attributes, title, actionType, id, buttonText, searchTerm;
    attributes = props["attributes"] ? props["attributes"] : null;
    if (attributes != null) {
        title = attributes.title ? attributes.title : null;
        actionType = attributes.action_type ? attributes.action_type : null;
        id = attributes.page_id ? attributes.page_id : null;
        buttonText = attributes.button_text ? attributes.button_text : null;
        searchTerm = attributes.search_term ? attributes.search_term : null;
    }

    return (
        <React.Fragment>
            {attributes && subComponentsData.length > 0 && (
                <div className="p-15 sku-list white-background section-border">
                    {attributes && <h3 className="sec-header">{title}</h3>}
                    {subComponents}
                    {attributes &&
                        renderActionType(
                            actionType,
                            buttonText,
                            id,
                            searchTerm
                        )}
                </div>
            )}
        </React.Fragment>
    );
}
