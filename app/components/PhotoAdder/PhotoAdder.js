import React, {Component} from 'react'
import * as style from './style.scss'
import {cancel, plus, imageLoader} from '$SHAREDSTYLES/icons'

// PhotoAdder.propTypes = {
//   imageList: PropTypes.array,
//   handleDeleteImage: PropTypes.func,
//   showImage: PropTypes.func.isRequired,
// }

/**
 * To get list of Images
 */

export class PhotoList extends Component{
	constructor(props) {
	  super(props);

	  this.state = {
			isImageLoad: false,
	  };

	}

	DeleteImage = () => {
		if(confirm("This will remove your image, are you sure?")){
			this.props.handleDeleteImage(this.props.imageList)
		}
	}

	showImage = () => {
		this.props.showImage(this.props.imageList)
	}

	loade = (e) => {
		var imgHeight = e.target.height;
    var imgWidth = e.target.width;
		this.setState({isImageLoad: true, imgHeight: imgHeight, imgWidth: imgWidth})
	}

	render(){
		return (
	 		<li style = {{"width": this.props.elementWidth}} className = {this.props.centeredImage ? style.no_border : ""}>
	 			<span className = {style.image_list_container} >
	 				<span className = {style.image_list_wraper} style = {{"width": this.state.imgWidth, "height": this.state.imgHeight, "display": "inline-block"}}>
	 					{this.props.noDeleteButton ? null : <span onClick = {this.DeleteImage} className = {style.cross_button_wraper}>{cancel}</span>}
	 					<img onLoad = {this.loade} onClick = {this.showImage} src={this.props.imageList.image_url} alt=""/>
	 					{this.state.isImageLoad ? '' : imageLoader}
	 				</span>
	 			</span>
	 		</li>
			)
	}
}


export default function PhotoAdder ({handleDeleteImage, imageList, getImage, showImage, noDeleteButton, centeredImage, showUploadBtn}){
	let windowWidth = document.body.offsetWidth
	let imageListLength = imageList.length
	let bodyWidth = document.querySelector('#main_container') ? document.querySelector('#main_container').offsetWidth : windowWidth > 770 ? parseInt((windowWidth*6)/10) : windowWidth
	let elementWidth = 110
	// elementWidth = elementWidth + elementWidth/4;
	let imageWrapStyle = {"width": elementWidth*(imageListLength+1)}
	let imageListStyle = {"width": elementWidth}
	if(centeredImage) {
		imageWrapStyle["float"] = "none"
		imageWrapStyle["display"] = "inline-block"
		imageWrapStyle["marginBottom"] = "-7px"
		imageListStyle["border"] = 0
	}
	return (
			<div className = "container">

				<div className = {style.main_container} id = "imageContainer">
					 <div className = {style.image_container} >
					 	<div style = {{"width": bodyWidth-imageListLength+1,  "maxWidth": 650}} className = {style.image_box} >
						 	<ul style = {imageWrapStyle} className = {style.image_wraper}>
						 	{showUploadBtn ?
						 		<label htmlFor = "imageUploader" >
						 			<li style = {imageListStyle} className = {style.image_wraper}>
						 				<span className = {style.image_list_container + " "+ style.greyBorder}> <span className = {style.plus_icon}>{plus}</span> </span>
										<input className = {style.image_uploader_input} id= "imageUploader" name = "imageUploader" onChange = {getImage} type="file" accept=".png, .jpg, .jpeg;capture=camera"/>
						 			</li>
								</label>
							: ''}
						 		{
						 			imageList && imageList.length
						 			? ( imageList.map((x, index) => {
						 				return <PhotoList key = {index} handleDeleteImage = {handleDeleteImage} imageList = {x} elementWidth = {elementWidth} showImage = {showImage} noDeleteButton = {noDeleteButton} centeredImage = {centeredImage}/>
						 			})
						 			)
						 			: null
						 		}
						 	</ul>
						 </div>
					 </div>
				</div>
			</div>
		)
}
