import React, { Component } from 'react';
import * as style from './style.scss';
import {genericWalletIcon,cashbackNewIcon} from '$SHAREDSTYLES/icons'


export default class AllTransactions extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount = () => {
        this.fetchPastTransactions()
    }

    fetchPastTransactions = () => {
        this.props.fetchUserTransactions()
    }

    renderMonthCard = (monthData) => {
        console.log(monthData)
        let monthsArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        let ell = []
        monthData.forEach((element) => {
            ell.push(
                <div key={element.month}>
                    <div className={`paddingBox`} style={{paddingBottom:'0px'}}>
                        <div className={` ${style.month_header}`} style={{ borderBottom: '1px solid #efefef' }} >
                            <span className="bold_text">{monthsArr[element.month] + " " + element.year}</span>
                        </div>
                        <ul>
                        {
                            element.gift_cards.map((transaction)=>{
                                return <li key ={transaction.id} className={style.transact_item} >
                                    <div style={{width:'10%',height:'100%'}}>
                                        <span style={{verticalAlign:'sub'}}>{transaction.status == "COMPLETED" ? cashbackNewIcon('black') : genericWalletIcon("black")}</span>
                                    </div>
                                    <div style={{flexGrow:1}}>
                                        <p style={{lineHeight:1,marginTop:'16px'}}>
                                            <span style={{fontSize:'14px'}}>
                                                {transaction.status == "PENDING" ? "Payment Pending" :(transaction.status == "COMPLETED" ? "Receieved Cashback" :
                                                (transaction.status == "FAILED" ? "Payment Failed" : "Paid to Myra"))   }
                                            </span><br/>
                                            <span style={{fontSize:'12px',color:'grey'}}>{new Date(transaction.entry_timestamp*1000).getDate()+ " "+ monthsArr[element.month]+" "}
                                            {transaction.status == "COMPLETED" ? "• Credited to Wallet" :(transaction.status == "FAILED" ? "• Payment Failed": null)}</span>
                                        </p>
                                    </div>
                                    <div className={transaction.status == "COMPLETED" ? style.success_text :(transaction.status == "FAILED" ? style.failed_text : null)}style={{width:'30%'}}>
                                        <p style={{float:'right',fontSize:'14px',fontWeight:"500"}}>
                                            {transaction.status == "COMPLETED" ? "+ " : ""}&#8377;
                                            {transaction.details.wallet_top_up_amount.toFixed(2)} 
                                        </p>
                                    </div>
                                </li>
                            })
                        }
                        </ul>
                    </div>
                    <div className="seprator"></div>
                </div>)
        });
        return ell;

    }

    render = () => {
        let pastTransactionList = this.props.userTransactions
        return (
            <div>
                {
                    pastTransactionList && pastTransactionList.length ?
                        this.renderMonthCard(pastTransactionList) : "Fetching Transactions List"
                }
                {
                    pastTransactionList && pastTransactionList.length ? 
                    <div className="big_sep_blank seprator"></div>:null
                }
            </div>
        );
    }
}