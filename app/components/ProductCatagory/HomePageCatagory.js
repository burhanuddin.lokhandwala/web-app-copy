import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {arrow} from '$SHAREDSTYLES/icons'
import * as style from './style.scss'

export default class HomePageCatagory extends Component {
	render() {
		return (
			<div className="field_blank_container" >
				<div className="full_width">
					<span className="bigHeaderText left">Popular Categories</span>
					<Link to = "/product_categories" className= "show_detail_arrow">
						<span >See All</span><span className="right_arrow">{arrow}</span>
					</Link>
				</div>
				<div className="flax_box full_width">
					{this.props.categoryList.slice(0).splice(0,3).map((x, index) => {
							return	(
								        <span key= {index} className = {style.product_img_wraper}>
									        <Link onClick ={() => this.props.updateCategoryName(x.name)} to = {"sub_category/" + x.name} >
															<img src={x.imageUrl2} alt = {x.name}/>
															<span className = {style.img_label_text}>{x.name}</span>
													</Link>
												</span>
										)
						})
					}

				</div>
			</div>
		);
	}
}
