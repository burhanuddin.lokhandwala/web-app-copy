import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {arrow} from '$SHAREDSTYLES/icons'
import * as style from './style.scss'

class AllCategoriesPage extends Component {
	render() {
		let categoryList = this.props.categoryList
		return (
			<div className={style.productContainer}>
			<Link onClick ={() => this.props.updateCategoryName(categoryList.name)} to = {`/sub_category/${categoryList.name}`} >
				<div className={style.img_wrp}>
					<img src={categoryList.imageUrl2} alt = {categoryList.name}/>
				</div>
				<div className={style.category_text_wrp}>
					<div className={style.category_text_head}>{categoryList.name}</div>
					<div className="full_width">
						{categoryList.sub_categories && categoryList.sub_categories.length
							? categoryList.sub_categories.map((i, index) => (<span className= {style.small_text} key= {index}>{i.name}{index+1 == categoryList.sub_categories.length ? "" : ","} </span>))
							: null
						}
					</div>
				</div>
			</Link>

			</div>
		);
	}
}

export default AllCategoriesPage

