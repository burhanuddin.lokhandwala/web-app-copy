import React, { Component } from 'react';
import {searchIcon, arrow}  from '$SHAREDSTYLES/icons'
import * as style           from './style.scss'

class Subcategory extends Component {

	render() {
		let categoryList = this.props.categoryList
		return (
			<div className={style.productContainer}>
				<div className="full_width">
					{categoryList.header_title
						?(
						  <div className={style.category_head}>
								<span className={`${style.category_head_text} left`}> {categoryList.header_title} </span>
								<span style = {{marginTop: '5px'}} onClick={this.props.handleSearchCategory.bind(null, categoryList.search_text)} className= "section_right green_text right">
									<span >See All</span>{/*<span className="right_arrow">{arrow}</span>*/}
								</span>
							</div>
						  ): null
					}

					<div className={`full_width ${(this.props.horizontal ? style.cat_list_wrap : "")}`}>
					{this.props.vartical
						?(categoryList.products.map((x, index) => {
								return (
											<div onClick={this.props.handleSearchCategory.bind(null, x.name)} key = {index} className={`full_width ${style.cat_list}`}>
												<span className={style.category_name}>
													<span className= {style.search_icon}>{searchIcon}</span>
													{x.name}
												</span>
												<span className={style.cat_img_wrp}><img src = {x.image}/></span>
											</div>
								)
						})
						):(
								<div style = {{width: (160*categoryList.length+30)+"px"}} className={`full_width ${style.cat_horizontal_list}`}>
									{categoryList.map((x, index) => {
											return (
															<div onClick={this.props.handleSearchCategory.bind(null, x.name)} className={style.cat_img_wrp} key = {index} >
																<div className={style.cat_img}><img src = {x.image}/></div>
																<div className = "medium_text">{x.name}</div>
																<div className={style.small_text}> {x.short_desc}</div>
															</div>
											)
									})}
								</div>
						)
					}

					</div>
				</div>

			</div>
		);
	}
}

export default Subcategory
