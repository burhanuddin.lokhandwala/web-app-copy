import React, {Component} from "react";
// import { getComponentsFromPageLayout, getPageLayout, componentMap } from "../../helpers/layout";

// class Page extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {pageLayout: null};
//     }

//     componentDidMount() {
//         getPageLayout(this.props.pageName).then((pageLayout) => {
//             this.setState({pageLayout: pageLayout});
//         });
//     }

//     render() {
//         let components = this.state.pageLayout ? getComponentsFromPageLayout(this.state.pageLayout, componentMap) : [];
//         return components;
//     }
// }

// export default Page;

export default function Page(props) {
    return props.children;
}