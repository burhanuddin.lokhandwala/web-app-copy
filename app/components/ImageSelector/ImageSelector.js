import React, {Component}          from 'react'
import { Link }                    from 'react-router-dom'
import * as style                  from './style.scss'
import { getMobileOperatingSystem} from '$HELPERS/helperFunction'
import Clevertap                   from '$HELPERS/clevertap'

export default class ImageSelector extends Component{

	componentDidMount(){
		document.getElementById('image_container').style.height = (window.innerHeight - 116) + 'px'
		if(this.props.img){
			let img = this.props.img
			document.getElementById('image_container').appendChild(img)
			img.style.maxHeight = (window.innerHeight - 115) + 'px'
			img.id = this.props.orientation_flag == 6 && getMobileOperatingSystem() == "Android" ? "image_rotate" : ''
		}

	}

	componentWillReceiveProps(newProps){
		let imgElement = document.getElementById('image_container').children[0]
		// if(newProps.img && newProps.img.src != this.props.img.src){
			imgElement ? imgElement.parentNode.removeChild(imgElement) : ''
			newProps.img ? document.getElementById('image_container').appendChild(newProps.img) : ''
		// }
	}

	nevigater = () => {
		// this.props.handleNavigate()
		if(!this.props.noDrower){
			this.props.handleActionDrower(false)
		}
		history.back()
	}

	handleShowActionDrower = () => {
		Clevertap.event('mWeb - prescription photo selected')
		// this.props.handleDeliverAllItems()
		this.props.handleActionDrower(true)
	}

	handleHideActionDrower = () => {
		Clevertap.event('mWeb - Photo - delivery type', {
			type: 'Cancel'
		})
		this.props.handleActionDrower(false)
	}

	render() {
		return (
			<div className = "container">
				<div id="image_container" className = {style.upload_image_container}>
				</div>
				<div className = {style.action_bar}>
					<label htmlFor = "imageUploader" className = "left">
						<span className = "left">Retake</span>
						<input className = {style.image_uploader_input} id= "imageUploader" name = "imageUploader" onChange = {this.props.getImage} type="file" accept=".png, .jpg, .jpeg;capture=camera"/>
					</label>
					<span onClick = {this.handleShowActionDrower} className = "right">Use Photo </span>
				</div>
				{this.props.is_show_action_dower
					?(<div className = {style.image_options_container}>
						<div className = {style.image_options_header}>Should Myra deliver all items in the prescription? </div>
						<ul className = {style.image_options_wrap}>
							<li onClick = {this.props.handleDeliverAllItems}>Deliver All Items</li>
							<li onClick = {this.props.showImageTagView}>Let Me Select from the Prescription</li>
							<li onClick = {this.props.handleCallBackUser}>Request Pharmacist to Call and Confirm</li>
							<li onClick = {this.handleHideActionDrower}>Cancel</li>
						</ul>
					</div>)
					: null
				}
			</div>
		);
	}

}

