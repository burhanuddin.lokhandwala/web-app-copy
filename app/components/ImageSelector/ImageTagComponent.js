import React, {Component} from 'react'
import * as style from './style.scss'
import {greenTick} from '$SHAREDSTYLES/icons'


export default class ImageTagComponent extends Component{
	// propTypes: {
	// 	cord:PropTypes.object.isRequired,
	// 	onQuantityChange: PropTypes.func.isRequired,
	// 	onRemoveTag: PropTypes.func.isRequired,
	// }
	constructor(props) {
	  super(props);

	  this.state = {
			showTag: false,
	  };

	}

	onQuantityChange = (e) => {
		this.props.onQuantityChange(e.target.value, this.props.index)
	}

	onRemoveTag = () => {
		this.setState({showTag: true})
		this.props.onRemoveTag(this.props.index)
	}

	handleShowTag = () => {
		this.setState({showTag: !this.state.showTag})
	}

	render() {
		let cord = this.props.cord
		let boxStyle = {}
		if (cord.top ) {
			boxStyle['top'] = "-100%"
			if (!cord.right ) {
				boxStyle['left'] = "30px"
			}
		}

		if (cord.left ) {
			boxStyle['left'] = "30px"
		}

		if (cord.right ) {
			boxStyle['right'] = "22px"
		}

		return (
				<div className = {style.image_tick} style = {{ top: cord.position.y+"px", left: cord.position.x+"px"}}>
					{!this.state.showTag ? <div onClick = {this.handleShowTag} className = {style.transparent_window}></div> : null}
					<span className = {style.green_tag} onClick = {this.handleShowTag}>{greenTick}</span>
					{this.state.showTag
						?(	cord.text
								?<div className = {style.quantity_info_container} style = {boxStyle}>
									<span className = {style.show_text_tag} > {cord.text}</span>
								</div>
								: null
						)
						:(
							<div className = {style.quantity_info_container} style = {boxStyle}>
								<div className = {style.quantity_input_wrp}>
									<input onBlur = {this.handleShowTag}  type = "text" value = {cord.text} placeholder = "Add Quantity" onChange = {this.onQuantityChange}/>
								</div>
								<span className = {style.delete_tag_button} onClick = {this.onRemoveTag}>Remove</span>
							</div>
						)
					}
				</div>
			)

		}
}
