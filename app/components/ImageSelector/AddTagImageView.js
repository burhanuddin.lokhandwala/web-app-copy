import React, {Component} from 'react'
import * as style         from './style.scss'
import ImageTagComponent  from '$COMPONENTS/ImageSelector/ImageTagComponent'
import {backArrow}        from '$SHAREDSTYLES/icons'

export default class AddTagImageView extends Component{

	componentDidMount(){
		document.getElementById('image_containers').style.height = (window.innerHeight - 116) + 'px'
		// debugger
		if(this.props.img){
			this.props.handleRemoveAllTag()
			let img = this.props.img
			document.getElementById('image_containers').appendChild(img)
			img.style.maxHeight = (window.innerHeight - 115) + 'px'
			// console.log("this", img)
			img.addEventListener('click', this.handleClickOnImage, false)
		}

	}

	componentWillReceiveProps(newProps){

		let imgElement = document.getElementById('image_containers').firstElementChild
		if(newProps.img && newProps.img.src != this.props.img.src){
			imgElement ? imgElement.parentNode.removeChild(imgElement) : ''
			newProps.img ? document.getElementById('image_containers').appendChild(newProps.img) : ''
			newProps.img.addEventListener('click', this.handleClickOnImage)
		}
	}

	handleClickOnImage = (e) => {
		// console.log("msg", e)
		let height = document.querySelector('img').height
		let width = document.querySelector('img').width
		let widthToSet = document.querySelector('img').naturalWidth < 720 ? document.querySelector('img').naturalWidth : 720
		let heightToSet = parseInt((widthToSet * height)/ width)
		let parent = e.target.parentNode.getBoundingClientRect()
		let newCordinate = '', sentCordinate = ""
		newCordinate = {
							"text": "",
							"position": {
								"x": e.clientX - parent.left - 14,
								"y": e.clientY - parent.top - 10,
							}
						}
		if(width - e.offsetX < 130){
			newCordinate['right'] = true
		}

		if(height - e.offsetY < 65){
			newCordinate['top'] = true
		}

		if(e.offsetX < 30){
			newCordinate['left'] = true
		}

		sentCordinate = {
							"text": "",
							"position": {
								"x": parseInt((e.offsetX * widthToSet)/width ),
								"y": parseInt((e.offsetY * heightToSet) / height ),
							}
						}
		this.props.updateMarkersInfo(newCordinate, sentCordinate)
	}

	handleNavigate = () => {
		this.props.handleNavigate()
		document.querySelector('img').removeEventListener('click', this.handleClickOnImage, false)
	}

	render() {
		let markers = ""
		if(this.props.markersInfo.length){
			markers = this.props.markersInfo.map((cord, index) => {
				return <ImageTagComponent key = {index} cord = {cord} index = {index} onQuantityChange = {this.props.onQuantityChange} onRemoveTag = {this.props.onRemoveTag}/>

			})
		}
		return (
			<div className = {style.tag_image_container} style = {this.props.is_show_tag_view ? {right: 0, opacity: 1} : {}}>
				<div className = {style.tag_view_nav_bar}>
					<div onClick = {this.props.handleMarkarsDoneButton} className = "right">Done</div>
				</div>
				<div id="image_containers" className = {style.upload_image_container}>
					{markers ? markers : null}
				</div>
				<div className = {style.tag_instruction_bar}>
					{this.props.markersInfo && this.props.markersInfo.length
						? (<div className = {style.redText} onClick = {this.props.handleRemoveAllTag}>Clear All</div>)
						: <span>Tap next to the item on the photo to select add quantities</span>
					}
				</div>
			</div>
		);
	}

}

