import React from "react";
import GoToPage from "../GoToPage/GoToPage";
import GoToSearch from "../GoToSearch/GoToSearch";

function Cell(props) {
    let markup = (
        <React.Fragment>
            <div className="cat-image">
                <img src={props["image_url"]} />
                {props["discount_text"] && <p className="cat-discount">{props["discount_text"]}</p>}
            </div>
            <h4 className="cat-name">{props["name"]}</h4>
        </React.Fragment>
    );
    switch (props["action_type"]) {
        case "go_to_page":
            markup = (
                <div className={`col-${props["columnNum"]}`}>
                    <div className="cell">
                        {GoToPage(props["page_id"], markup)}
                    </div>
                </div>
            );
            break;
        case "go_to_search":
            markup = (
                <div className={`col-${props["columnNum"]}`}>
                    <div className="cell">
                        <GoToSearch
                            searchTerm={props["search_term"]}
                            markup={markup}
                        />
                    </div>
                </div>
            );
            break;
    }
    return markup;
}

function getSubComponentsFromData(columns, subComponentsData) {
    let subComponents = [],
        columnNum = 12 / columns;
    for (const [index, data] of subComponentsData.entries()) {
        var props = { ...data["attributes"], key: index, columnNum };
        subComponents.push(<Cell {...props} />);
    }
    return <div className="row grid">{subComponents}</div>;
}

export default function Grid(props) {
    let { columns } = props;
    let subComponentsData = props["sub_components"]
        ? props["sub_components"]
        : [];
    let subComponents = getSubComponentsFromData(columns, subComponentsData);

    return (
        <div className="p-15 white-background section-border">
            <h3 className="sec-header" style={{ border: '0', padding: '0' }}>{props["title"]}</h3>
            {subComponents}
        </div>
    );
}
