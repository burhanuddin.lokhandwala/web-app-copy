import React            from 'react'
import { Link }         from 'react-router-dom'
import * as style       from './style.scss'
import {iconNameFormet} from '$HELPERS/helperFunction'
import {addressIcon, helpIcon, legalIcon, logoutIcon, bigAccountIcon, arrow, walletIcon2, recordIcon,
	 labtestIcon, docConsultIcon,addressNewIcon,reportIcon,blogsIcon,referIcon,logoutNewIcon,faqIcon,infoNewIcon} from '$SHAREDSTYLES/icons'

export default function  UserAccount ({userInfo, handleUserInfoChange, addressHandler, legalHandler, helpHandler, logoutHandler, handleWalletRedirect}){

	return (
		<div className = "full_width">
			<div className = {style.user_account_container}>
				<div className = {style.user_icon_wraper}>
					{userInfo.name ? <span className = {style.user_icon}> {iconNameFormet(userInfo.name)} </span>: bigAccountIcon}
				</div>
				<div className = {style.user_detail_info_wraper} >
					<Link to = "/user">
						<div className = {style.user_detail_info} >
							<span className = {style.user_info + " bold_text dark_black"}>{userInfo.name} </span>
							<span className = {style.user_info + " dark_black"}>+{userInfo.phone_number}</span>
							<span className = {style.user_info}>{userInfo.email} </span>
							<span className = {style.right_arrow + " theme_text"}>Edit</span>
						</div>
					</Link>
				</div>
			</div>
			<div className="full_width seprator"></div>
			<div className = {style.otherInfo_container}>
				<ul className = {style.otherInfo_wraper}>
					<li onClick = {handleWalletRedirect}>
						<Link to = "/user_wallet">
							<span className = {style.otherInfo_icon_wraper}>{walletIcon2}</span>
							<span className = {style.otherInfo_link}>
								Wallet
								<span className="grey_text" style={{float:'right',marginRight:'35px',fontWeight:'normal'}}>&#8377;{userInfo.wallet_credits}</span>
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>
					<li onClick = {handleWalletRedirect}>
						<Link to = "/user_wallet">
							<span className = {style.otherInfo_icon_wraper}>{recordIcon}</span>
							<span className = {style.otherInfo_link}>
								Health Records
								
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>
					<li onClick = {handleWalletRedirect}>
						<Link to = "/user_wallet">
							<span className = {style.otherInfo_icon_wraper}>{labtestIcon}</span>
							<span className = {style.otherInfo_link}>
								Lab Records
								
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>
					<li onClick = {handleWalletRedirect}>
						<Link to = "/user_wallet">
							<span className = {style.otherInfo_icon_wraper}>{docConsultIcon}</span>
							<span className = {style.otherInfo_link}>
								Doctor Consultation History
								
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>

					<li onClick = {addressHandler}>
						<Link to = "/user_locations">
							<span className = {style.otherInfo_icon_wraper}>{addressNewIcon}</span>
							<span className = {style.otherInfo_link}>
								Addresses
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>
					<li onClick = {addressHandler}>
						<Link to = "/user_locations">
							<span className = {style.otherInfo_icon_wraper}>{reportIcon}</span>
							<span className = {style.otherInfo_link}>
								Report an Issue
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>
					<div className="seprator big_sep_blank" ></div>
					<li onClick = {addressHandler}>
						<Link to = "/user_locations">
							<span className = {style.otherInfo_icon_wraper}>{blogsIcon}</span>
							<span className = {style.otherInfo_link}>
								Blogs
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>
					<li onClick = {addressHandler}>
						<Link to = "/user_locations">
							<span className = {style.otherInfo_icon_wraper}>{referIcon}</span>
							<span className = {style.otherInfo_link}>
								Share & Earn
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</Link>
					</li>
					<div className="seprator big_sep_blank" ></div>
					<li onClick = {helpHandler}>
						<a href="mailto:help@myramed.in?subject=Help">
							<span className = {style.otherInfo_icon_wraper}>{infoNewIcon}</span>
							<span className = {style.otherInfo_link}>
								About Medlife
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</a>
					</li>
					<li >
						<a href = "https://myramed.in/legal.html" target = "_blank">
							<span className = {style.otherInfo_icon_wraper}>{faqIcon}</span>
							<span className = {style.otherInfo_link}>
								FAQ
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</a>
					</li>
					
					
					<li onClick = {logoutHandler}>
						<div className="flax_box">
							<span className = {style.otherInfo_icon_wraper}>{logoutNewIcon}</span>
							<span className = {style.otherInfo_link}>
								Logout
							</span>
							<span className = {style.right_arrow}>{arrow}</span>
						</div>
					</li>
					<div className="seprator big_sep_blank" ></div>
					<div className="seprator big_sep_blank" ></div>
					<div className="seprator big_sep_blank" ></div>
					
					
				</ul>
				
            </div>
			</div>
    );
}
