import React, { Component } from "react";
import * as style from "./style.scss";
import { validateEmail } from "$HELPERS/helperFunction";
import Input from "$PARTIALS/Input/Input";

export default class UserInfoEditer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: props.userInfo.name ? props.userInfo.name : undefined,
            email: props.userInfo.email ? props.userInfo.email : undefined,
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.userInfo.name !== this.state.name) {
            this.setState({
                name: newProps.userInfo.name,
            });
        }
        if (newProps.userInfo.email !== this.state.email) {
            this.setState({
                email: newProps.userInfo.email,
            });
        }
    }

    handleNameChange = e => {
        this.setState({
            name: e.target.value,
        });
    };

    handleEmailChange = e => {
        this.setState({
            email: e.target.value,
        });
    };

    hendleDoneButton = () => {
        if (this.state.email && !validateEmail(this.state.email)) {
            alert("Please check Email Id!!");
            return;
        }
        this.props.hendleDoneButton({ email: this.state.email ? this.state.email : "", name: this.state.name ? this.state.name : "" });
    };

    render() {
        let userInfo = this.props.userInfo;
        return (
            <div className="p-15">
                <ul className={style.account_edit_wraper}>
                    <li>
						<Input
                            disabled
                            value={userInfo.phone_number}
                            name="phone_number"
                            id="phone_number"
                            type="text"
                            label="Phone Number"
                        />
                    </li>
                    <li>
						<Input
                            value={this.state.name}
							onChange={this.handleNameChange}
                            name="Name"
                            id="Name"
                            type="text"
                            label="Name"
                        />
                    </li>
                    <li>
						<Input
                            value={this.state.email}
							onChange={this.handleEmailChange}
                            name="Email"
                            id="Email"
                            type="text"
                            label="Email"
                        />
                    </li>
                    <li>
						<Input
                            disabled
                            value={userInfo.wallet_credits}
                            name="balance"
                            id="balance"
                            type="text"
                            label="Account Balance"
                        />
                    </li>
                </ul>
                <button onClick={this.hendleDoneButton} className="fixed_btn button">
                    Done
                </button>
            </div>
        );
    }
}
