import React from 'react';
import { Link } from 'react-router'
import * as style from './style.scss'
import {smallCirclePlus, scooter, truck} from '$SHAREDSTYLES/icons'
import {CheckBox} from '$PARTIALS/CheckBox/CheckBox'

export default function SelectLocation(props) {
	let locationsEl = "", billLocationsEl = ""
	let billingLocation = (props.userLocations && (props.userLocations.length >= 1))
													? props.deliveryLocationId ? props.userLocations.filter(x => x.id != props.deliveryLocationId) : props.userLocations
													: []
	if(props.userLocations && props.userLocations.length){
		let deliveryLocation = props.billingLocationId ? props.userLocations.filter(x => x.id != props.billingLocationId) : props.userLocations
		let w = document.querySelector(".main_centered_container").clientWidth - 30
			locationsEl = deliveryLocation.slice(0, props.showItemsCount).map( (x, index) => {
				let deliveryCheck = props.deliveryLocationId ? (x.id == props.deliveryLocationId ? true : false) : false
				return (
				        <li key= {index} >
				        	<label htmlFor= {"location_"+index}>
				        		{deliveryCheck
											? (<span className= { style.checkbox_check_border_r} > <span className = {style.checkbox_check_show_r}> </span> </span>)
											: (<span className= { style.checkbox_check_border } > <span className = {style.checkbox_check_show}> </span> </span>)
										}
				        		<input style= {{display: "none"}} onChange= {props.handleChangeLocation} checked = {deliveryCheck} type= "radio" name= {"location"} value= {x.id} id= {"location_"+index}/>
				        		<div className= {style.labelText}>
				        			<div className = {style.label_text}>{x.label}</div>
				        			<div style = {{width: parseInt(w - 135)}} className= {style.fulladdress}>{x.address}</div>
				        			<span className = {`${style.delivery_type} ${x.delivery_type == "express" ? "blue_text" : "orange_text"}`}>
												<span className = {style.delivery_icon}>{x.delivery_type == "express" ? scooter : truck}</span>
												<span>{x.delivery_type == "express" ? x.delivery_type : "NEXT DAY"}</span>
											</span>
				        		</div>
				        	</label>
				        </li>
					    )
			})
		}

	if(billingLocation && billingLocation.length && props.isBillingList){
		billingLocation = billingLocation.concat(props.unAvailUserLocations)
		let w = document.querySelector(".main_centered_container").clientWidth - 30
		billLocationsEl = billingLocation.map( (x, index) => {
			let billingCheck = props.billingLocationId ? (x.id == props.billingLocationId ? true : false) : false
				return (
				        <li key= {index}>
				        	<label htmlFor= {"billing_location_"+index}>
				        		{billingCheck
											? (<span className= { style.checkbox_check_border_r} > <span className = {style.checkbox_check_show_r}> </span> </span>)
											: (<span className= { style.checkbox_check_border } > <span className = {style.checkbox_check_show}> </span> </span>)
										}
					        	<input style= {{display: "none"}} onChange= {props.handleChangeBillingLocation} checked = {billingCheck} type= "radio" name= {"billing_location"} value= {x.id} id= {"billing_location_"+index}/>
					        	<div className= {style.labelText}>
				        			<div className = {style.label_text}>{x.label}</div>
				        			<div style = {{width: parseInt(w - 135)}} className= {style.fulladdress}>{x.address}</div>
				        			<span className = {`${style.delivery_type} ${x.delivery_type == "express" ? "blue_text" : "orange_text"}`}>
												<span className = {style.delivery_icon}>{x.delivery_type == "express" ? scooter : truck}</span>
												<span>{x.delivery_type == "express" ? x.delivery_type : "NEXT DAY"}</span>
											</span>
				        		</div>
				        	</label>
				        </li>
					    )
			})
	}

	return (
			<div >
				<div>
					{/*<label className = {field_label}>
						Delivery Address
					</label>*/}
					<ul className= {style.address_list}>
						{/*<li>
							<Link className={style.add_location_button} to = '/search_location'><span className= {style.addAddressIcon}>{smallCirclePlus}</span> Add Address</Link>
						</li>*/}
						{locationsEl}
					</ul>
				</div>

				{(props.userLocations && props.userLocations.length > 1) ?
				<div className={style.see_more} onClick={props.handleItemExpand}>
					{props.expandList ? 'Show less' : 'See All Addresses' }
				</div>
				:
				''}

				{/*<div className = {style.address_wrap}>
					<label className = {field_label}>
						Billing Address
					</label>

					<div className = {style.billing_location_selector}>
						<CheckBox label= {"Different from delivery address"} id= "billing_location_selector" checked= {props.isBillingList} onChange= {props.showBillingLocation} type= "checkbox"/>

					</div>*/}
					<div>

					{props.isBillingList?
						<div className="section_heading">
			                <span className="section_title">Billing Address</span>
				            <Link to = '/search_location'>
				            	<span className="section_right">+ Add Address</span>
				            </Link>
			            </div>
		            : null}

					{props.isBillingList
						?(
						  <ul className= {style.address_list}>
								{billLocationsEl}
							</ul>
						): null
					}
					</div>
				{/*</div>*/}

				{/*<div style= {{marginBottom: "30px"}} className = {container}>
					<label className = {field_label}>
						Delivery Instructions
					</label>
					<div className = {field_container}>
						<textarea value= {props.note} className= {full_width} onChange= {props.setUserNote} placeholder= "Enter your note...">
						</textarea>
					</div>

				</div>*/}


			</div>
	)
}
