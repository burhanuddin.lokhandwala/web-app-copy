// set json formet here
import {askForUserGEOLocation} from '../utils/googleLocationApi'
export function formatedData(data) {
	return JSON.Parse(data)
}

export function getFormattedString(string, stringCount)
{
	// let str = string.split(",")[0]
	if((string && string.length <= stringCount )|| !string.length)
		return string;
	else
		return string.substring(0, stringCount-3) + "..."
}

/**
 * [getImgUrlFromFiles set image url from input file image elemant]
 * @param  {[element]} input [input element]
 * @param  {[element]} img [img element]
 * @return {[type]}       [image url]
 */
export function setImgUrlFromFiles (input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader()
		let img = new Image()
		reader.onload = function (e) {
			img.src = e.target.result
			// img.classList = "uploaded_img"
		}

		reader.readAsDataURL(input.files[0])
		return img
	}
}

export function iconNameFormet(name) {
	if(!name){
		return '?'
	}
	let nameArray = name.split(' ', 2)
	let iconName = ""
	if(nameArray.length == 2){
		iconName = nameArray[0].split('', 1)
	}
	
	return iconName
}

export function clone(obj){
	let clone = {}
	clone.prototype = obj.prototype
	for (let property in obj) clone[property] = obj[property]
	return clone
 }

export function validateEmail(email) {
	let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

export function differenceBwtTimestamp(timeStart, timeEnd) {
	let hourDiff = timeEnd - timeStart; //in ms
	let secDiff = hourDiff / 1000; //in s
	let minDiff = hourDiff / 60 / 1000; //in minutes
	let hDiff = hourDiff / 3600 / 1000; //in hours
	let humanReadable = {};
	humanReadable.hours = Math.floor(hDiff);
	humanReadable.minutes = parseInt(minDiff - 60 * humanReadable.hours, 10);
	// console.log(humanReadable); //{hours: 0, minutes: 30}
	return humanReadable
}

/**
 * [formatedRupee give indian formated rupee]
 * @param  {[type]} value [description]
 * @return {[type]}       [description]
 */
export function formatedRupee(value) {
	value = value.toString()
	let vlaueArray = value.split('.', 2)
	if(vlaueArray.length > 1){
		if(vlaueArray[1].length == 1){
			return vlaueArray[0]+'.'+vlaueArray[1]+"0"
		}else if(vlaueArray.length > 1){
			return vlaueArray[0]+'.'+vlaueArray[1].substring(0, 2)
		}
	}else{
		return vlaueArray[0]+'.00'
	}
}

export function getMonthText (mathCount) {
	let month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',]
	return month[mathCount]
}

/**
 * [getFormatedDate will get a formated date in dd/mm/yy]
 * @param  {[int]} timestamp [description]
 * @return {[string]}           [description]
 */
export function getFormatedDate(timestamp) {
	let date = new Date(timestamp*1000)
	let dayNames = [
			'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
		]
			// 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
	let monthNames = [
			'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
		]
			// 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
	return {date: date.getDate() <10 ? "0"+date.getDate() : date.getDate(), month:  monthNames[date.getMonth()], year: (date.getFullYear()).toString().slice(-2)}
}

/**
 * [getOrientation ]
 * @param  {[object]} file [description]
 * @return {[string]}           [description]
 */
export function getOrientation(file, callback) {
	let reader = new FileReader()
	reader.onload = function(e) {
		let view = new DataView(e.target.result)
	  if(view.getUint16(0, false) === 0xFFFE) {
	    return callback(1)
	  }
		if (view.getUint16(0, false) != 0xFFD8) {
			return callback(-2)
		}
		let length = view.byteLength, offset = 2
		while (offset < length) {
			let marker = view.getUint16(offset, false)
			offset += 2
			if (marker == 0xFFE1) {
				if (view.getUint32(offset += 2, false) != 0x45786966) {
					return callback(-1)
				}
				let little = view.getUint16(offset += 6, false) == 0x4949
				offset += view.getUint32(offset + 4, little)
				let tags = view.getUint16(offset, little)
				offset += 2
				for (var i = 0; i < tags; i++){
					if (view.getUint16(offset + (i * 12), little) == 0x0112) {
						return callback(view.getUint16(offset + (i * 12) + 8, little))
					}
				}
			}
			else if ((marker & 0xFF00) != 0xFF00) {
				break
			}
			else {
				offset += view.getUint16(offset, false)
			}
		}
		return callback(-1)
	}
	reader.readAsArrayBuffer(file)
}

/**
 * [getMobileOperatingSystem get the info in which device the system opened]
 * @return {[string]} [device information]
 */
export function getMobileOperatingSystem() {
	let userAgent = navigator.userAgent || navigator.vendor || window.opera

	if (/windows phone/i.test(userAgent)) {
		return "Windows Phone"
	}

	if (/android/i.test(userAgent)) {
		return "Android"
	}

	if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
		return "iOS"
	}

	return "unknown"
}

/**
 * [isLocalStorageNameSupported "check if localStorage is sporting"]
 * @return {Boolean} [description]
 */
export function isLocalStorageSupported () {
	if (window && window.localStorage) {
    let testKey = 'test', storage = window.localStorage;
    try {
        storage.setItem(testKey, '1');
        storage.removeItem(testKey);
        return true;
    } catch (error) {
        return false;
    }
	}else{
		return false;
	}

}

/**
 * [createCookie "create a cookie for window when localstorage dosent work"]
 * @param  {[string]} name  [name of cookie]
 * @param  {[string]} value [value of cookie]
 * @param  {[integer]} days  [no of days cookie will save]
 */
export function createCookie(name, value, days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = "; expires=" + date.toUTCString();
  } else var expires = "";
  value = value.replace(/[\s, ]+/g,',')
  document.cookie = name + "=" + value + expires + "; path=/";
}

/**
 * [readCookie to get the cookie value]
 * @param  {[string]} name [name of the cookie which you want]
 * @return {[object]}      [value of cookie]
 */
export function readCookie(name) {
  let nameEQ = name + "=", ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length, c.length)
    }
  }
  return null;
}

/**
 * [eraseCookie erase cookie you setted]
 * @param  {[string]} name [name of the cookie want to erage]
 */
export function eraseCookie(name) {
  createCookie(name, "", -1);
}


export function formDataRequest(path, params, method) {
    method = method || "post";

    let form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            let hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}


export function shouldDisplayPrompt() {
	if(isLocalStorageSupported()){
		let check = window.localStorage.getItem('download_prompt')
		if(check){
			return false
		} else {
			window.localStorage.setItem('download_prompt', true);
			return true
		}
	}else{
		return false
	}
}

export function isRedirectRequired(){
    if(isLocalStorageSupported()){
        let url = window.localStorage.getItem('redirectURL')
        if(url){
            window.localStorage.removeItem('redirectURL');
            return url
        } else {
            return false
        }
    }else{
        return false
    }
}

export function updateLocalstorage(key, value) {
	localStorage[key] = JSON.stringify(value)

}

export function getLocalstorageValue(key) {
	return localStorage[key] ? JSON.parse(localStorage[key]) : ""

}

// export function checkLocationEnable(callback) {
// 	navigator.permissions && navigator.permissions.query({name: 'geolocation'}).then(function(PermissionStatus) {
// 		callback(PermissionStatus.state)
// 		// permissionStatus.onchange = function() {
// 	 //    console.log('geolocation permission status has changed to ', this.state);
// 	 //  };
// 	})

// }

export function enableGEOLocation(success, fail) {
	askForUserGEOLocation(success, fail);

}

export function getFormatedTime(time){
	if (time <= 12) {
		return time + 'AM'
	}else {
		return (time - 12) + "PM"
	}
}