import {getConfigMode} from '../config/config'

export function callAnalytics(type, options) {
  if(getConfigMode().serverMode.mode == 'server'){
    switch (type) {
      case 'adwords':
        if (typeof(window.google_trackConversion) === "function") {
          window.google_trackConversion({
            google_conversion_id: options.conversion_id || '',
            google_conversion_label: options.conversion_label || '',
            google_remarketing_only: false
          });
        }
        break;

      case 'analytics':
        ga('send', options.hitType, options.eventCategory, options.eventAction, options.eventLabel);
        break;
    }
  }
}
