import { Update, Post, Get, getExpressTokenFromMedlife} from './apiUtils'
import {isLocalStorageSupported, createCookie, readCookie, eraseCookie} from './helperFunction';
import Clevertap from './clevertap'

export default function otpRequest (data) {
	return Update({
		url : 'login',
		payload : data
	})
}

export function hanndleAuthedUser(users) {
	if(users.props.history && users.props.history.location.pathname && (users.props.history.location.pathname.indexOf("attach")  >= 0 || users.props.history.location.pathname.indexOf("payments")  >= 0 || users.props.history.location.pathname.indexOf("track") >= 0)){
		return
	}
	// if(!avalUser && users.props.location && users.props.location.pathname && users.props.location.pathname.indexOf("payments") >= 0){
	// 	window.localStorage.setItem('redirectURL', users.props.location.pathname);
	// 	users.props.history.replace('login')
	// 	return
	// }
	let mutant={};
	let avalUser = getStorageUserInfo() ;
	if(avalUser|| getExpressTokenFromMedlife()!==null){
		return Get({
			url : 'user',
		}).then(response => {
			// debugger
			// console.log('Login Success!!')
			if (response.data.status.code == 401) {
				users.props.fetchingUserFailure("something Wrong")
				users.props.history.replace('/login')
				localStorage.user = ''
				return response.data
			}
			console.log("After user API ",JSON.stringify({...{'access_token':getExpressTokenFromMedlife()},...response.data}));
			// debugger
			// window.localStorage.setItem('user',JSON.stringify({...{'access_token':getExpressTokenFromMedlife()},...response.data}));
			users.props.setUserOnlineStatus("online")

			Clevertap.profile({"Site": {Identity: response.data.user.id}})
			users.props.authUser("response.user")
			users.props.fetchingUserSuccess(response.data.user)
			users.getLocationList()
			// if (users.props.location.pathname === '/' || users.props.location.pathname === '/login') {
			// 	users.props.history.replace('/prescription')
			// }
			// return response.data.user
		}).catch((res) => {
			users.props.history.replace('/login')
			localStorage.user = ''
			users.props.fetchingUserFailure("something Wrong")
			return res.response
			// debugger
		})
	}else{
		// users.props.history.replace('/login')
		
		users.props.history.replace('/page/home')
		// offline user
		users.props.setUserOnlineStatus("offline")
	}
}

export function checkIfAuthed(store) {
	return store.getState().users.isAuthed
}

export function logout(data) {
	if(isLocalStorageSupported()){
		localStorage.user = ''
		localStorage.userLocation = ''
	}else{
		eraseCookie('user')
	}
}

export function login(data) {
	if(isLocalStorageSupported()){
		localStorage.user = ''
	}else{
		// window.LocalStorage = {}
	}

	return Update({
		url     : 'login',
		payload : data
	})
}

export function getStorageUserInfo() {
	if(isLocalStorageSupported()){
		return localStorage.user
	}else if(document.cookie && readCookie('user') != "undefined" || "" ){
		return JSON.parse(readCookie('user'))
	}else{
		return null
	}
}

