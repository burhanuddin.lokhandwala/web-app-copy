import React, {Component} from 'react';
import { Image } from "../components/Image/Image";

const layout = {
    "page": {
      "name": "home",
      "components": [
        {
          "id": 1,
          "name": "ad_scroller",
          "component_type": "image_scroller",
          "attributes": {
            "duration": 7000
          },
          "fetch_type": "STATIC",
          "sub_components": [
            {
              "id": 1,
              "component": 1,
              "sub_component_data_source": null,
              "sort_order": 1,
              "attributes": {
                "auth": true,
                "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/revital_h_banner.png",
                "action": "deep_link",
                "is_active": true,
                "action_type": "go_to_search",
                "banner_name": "revital_h",
                "search_term": "Revital H for daily health",
                "show_on_msite": true,
                "page_query_params": {}
              }
            },
            {
              "id": 1007,
              "component": 1,
              "sub_component_data_source": null,
              "sort_order": 1,
              "attributes": {
                "auth": true,
                "title": "April Fool's Day Offer",
                "page_id": 100002,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/april_s_fool_day_banner.png",
                "is_active": true,
                "action_type": "go_to_page",
                "banner_name": "April Fool",
                "show_on_msite": true,
                "page_query_params": {}
              }
            },
            {
              "id": 1012,
              "component": 1,
              "sub_component_data_source": null,
              "sort_order": 1,
              "attributes": {
                "auth": true,
                "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/happiness_sale_banner_day2.png",
                "page_id": 16,
                "is_active": true,
                "action_type": "go_to_page",
                "banner_name": "Happiness Sale Day2",
                "show_on_msite": true,
                "page_query_params": {}
              }
            },
            {
              "id": 27,
              "component": 1,
              "sub_component_data_source": null,
              "sort_order": 3,
              "attributes": {
                "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/phone_pe_banner.png",
                "action_type": "go_to_search",
                "banner_name": "adult diapers",
                "search_term": "Senior care comfort",
                "page_query_params": {}
              }
            },
            {
              "id": 63,
              "component": 1,
              "sub_component_data_source": null,
              "sort_order": 3,
              "attributes": {
                "auth": true,
                "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/lens_solution_banner_2.png",
                "action": "deep_link",
                "is_active": true,
                "action_type": "go_to_search",
                "banner_name": "lens solutions 2",
                "search_term": "All solutions for lens care",
                "show_on_msite": true,
                "page_query_params": {}
              }
            },
            {
              "id": 64,
              "component": 1,
              "sub_component_data_source": null,
              "sort_order": 4,
              "attributes": {
                "auth": true,
                "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/be_winter_ready_banner.png",
                "page_id": 100001,
                "is_active": true,
                "action_type": "go_to_page",
                "banner_name": "winter essentials",
                "show_on_msite": true,
                "page_query_params": {}
              }
            }
          ]
        },
        {
          "id": 30,
          "name": "ad_scroller_2",
          "component_type": "image_scroller",
          "attributes": {},
          "fetch_type": "STATIC",
          "sub_components": [
            {
              "id": 1003,
              "component": 30,
              "sub_component_data_source": null,
              "sort_order": 1,
              "attributes": {
                "url": "https://medium.com/@myrameds/february-delivery-fee-offer-t-c-9e3b2c13ac46",
                "auth": true,
                "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/free_delivery_banner_march.png",
                "title": "Free Delivery Offer",
                "action": "web_link",
                "is_active": true,
                "action_type": "go_to_web_view",
                "banner_name": "Free Del March",
                "show_on_msite": true,
                "page_query_params": {}
              }
            }
          ]
        },
        {
          "id": 27,
          "name": "back_in_stock_items",
          "component_type": "sku_listing",
          "attributes": {
            "title": "Back in stock items",
            "page_id": 11,
            "action_type": "go_to_page",
            "button_text": "View All",
            "preview_count": 4,
            "page_query_params": {}
          },
          "fetch_type": "DYNAMIC",
          "sub_components": [
            {
              "id": 2,
              "component": 27,
              "sub_component_data_source": "back_in_stock_items",
              "sort_order": 2,
              "attributes": {
                "action_type": "add_to_cart",
                "button_text": "Add"
              }
            }
          ]
        },
        {
          "id": 7,
          "name": "Top Categories",
          "component_type": "grid",
          "attributes": {
            "rows": 3,
            "title": "Top Categories",
            "columns": 3,
            "page_id": 15,
            "action_type": "go_to_page",
            "button_text": "View all",
            "page_query_params": {}
          },
          "fetch_type": "STATIC",
          "sub_components": [
            {
              "id": 14,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 1,
              "attributes": {
                "name": "Baby Category",
                "page_id": 3,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/baby_care_20190115.png",
                "action_type": "go_to_page",
                "discount_text": "Up to 25% Off",
                "categoryTileText": "Baby Diapers, Food & more",
                "page_query_params": {}
              }
            },
            {
              "id": 15,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 2,
              "attributes": {
                "name": "Women's Care",
                "page_id": 4,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Women+Hygiene.png",
                "action_type": "go_to_page",
                "discount_text": "Up to 10% off",
                "categoryTileText": "Sanitary Pads, Tampons & more",
                "page_query_params": {}
              }
            },
            {
              "id": 26,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 3,
              "attributes": {
                "name": "Senior Care",
                "page_id": 5,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/category_senior_care_20181023.png",
                "action_type": "go_to_page",
                "categoryTileText": "Senior Care",
                "page_query_params": {}
              }
            },
            {
              "id": 32,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 4,
              "attributes": {
                "name": "Health Drinks",
                "page_id": 6,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Health+Drinks.png",
                "action_type": "go_to_page",
                "discount_text": "Up to 18% Off",
                "categoryTileText": "Health Drinks",
                "page_query_params": {}
              }
            },
            {
              "id": 1004,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 5,
              "attributes": {
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Happiness+SALE-ICON-3.png",
                "action_type": "go_to_search",
                "search_term": "skin care",
                "categoryTileText": "Shampoos, Skin Care",
                "page_query_params": {}
              }
            },
            {
              "id": 36,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 6,
              "attributes": {
                "name": "Diabetes Care",
                "page_id": 7,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Diabetes+Care.png",
                "action_type": "go_to_page",
                "discount_text": "Flat 20% Off",
                "categoryTileText": "Glucometers, Insulins & more",
                "page_query_params": {}
              }
            },
            {
              "id": 39,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 7,
              "attributes": {
                "name": "Blood Pressure Care",
                "page_id": 8,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Blood+Pressure+Care.png",
                "action_type": "go_to_page",
                "discount_text": "Up to 20% Off",
                "categoryTileText": "BP Monitors & more",
                "page_query_params": {}
              }
            },
            {
              "id": 44,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 8,
              "attributes": {
                "name": "Generic Medicines",
                "page_id": 9,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/category_generic_medicine_20181023.png",
                "action_type": "go_to_page",
                "categoryTileText": "Antiseptics, Bandages & more",
                "page_query_params": {}
              }
            },
            {
              "id": 48,
              "component": 7,
              "sub_component_data_source": null,
              "sort_order": 9,
              "attributes": {
                "name": "Wellness",
                "page_id": 10,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/20181126_SexualWellness_Android.png",
                "action_type": "go_to_page",
                "categoryTileText": "Condoms, & more",
                "page_query_params": {}
              }
            }
          ]
        },
        {
          "id": 3,
          "name": "Wellness Circle",
          "component_type": "image",
          "attributes": {
            "url": "https://myra-wellness-challange.netlify.com/",
            "auth": true,
            "title": "Myra Savings Group",
            "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/wellness-circle-big-promo.png",
            "action_type": "go_to_web_view",
            "page_query_params": {}
          },
          "fetch_type": "STATIC",
          "sub_components": []
        },
        {
          "id": 100001,
          "name": "Flat 25% Off",
          "component_type": "image",
          "attributes": {
            "auth": true,
            "title": "Flat 25% off",
            "page_id": 100000,
            "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/myra_saving_store_flat_25_off.png",
            "action_type": "go_to_page",
            "page_query_params": {}
          },
          "fetch_type": "STATIC",
          "sub_components": []
        },
        {
          "id": 29,
          "name": "Common Ailments",
          "component_type": "grid",
          "attributes": {
            "rows": 2,
            "title": "Common Ailments",
            "columns": 3,
            "page_query_params": {}
          },
          "fetch_type": "STATIC",
          "sub_components": [
            {
              "id": 5,
              "component": 29,
              "sub_component_data_source": null,
              "sort_order": 1,
              "attributes": {
                "name": "Cough & Cold",
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/cough.png",
                "action_type": "go_to_search",
                "search_term": "Cough",
                "discount_text": "Up to 10% Off",
                "categoryTileText": "Cough & Cold",
                "page_query_params": {}
              }
            },
            {
              "id": 6,
              "component": 29,
              "sub_component_data_source": null,
              "sort_order": 2,
              "attributes": {
                "name": "Fever & Headache",
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/fever.png",
                "action_type": "go_to_search",
                "search_term": "Fever",
                "discount_text": "Up to 5% Off",
                "categoryTileText": "Fever & Headache",
                "page_query_params": {}
              }
            },
            {
              "id": 7,
              "component": 29,
              "sub_component_data_source": null,
              "sort_order": 3,
              "attributes": {
                "name": "Pain Relief",
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/pain.png",
                "action_type": "go_to_search",
                "search_term": "Pain",
                "discount_text": "Up to 15% Off",
                "categoryTileText": "Pain Relief",
                "page_query_params": {}
              }
            },
            {
              "id": 8,
              "component": 29,
              "sub_component_data_source": null,
              "sort_order": 4,
              "attributes": {
                "name": "Stomach Care",
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/stomach.png",
                "action_type": "go_to_search",
                "search_term": "Stomach Care",
                "discount_text": "Up to 25% Off",
                "categoryTileText": "Stomach Care",
                "page_query_params": {}
              }
            },
            {
              "id": 1001,
              "component": 29,
              "sub_component_data_source": null,
              "sort_order": 5,
              "attributes": {
                "name": "Respiratory Care",
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/respiratory.png",
                "action_type": "go_to_search",
                "search_term": "Masks",
                "discount_text": "Up to 9% Off",
                "categoryTileText": "Respiratory Care",
                "page_query_params": {}
              }
            },
            {
              "id": 1002,
              "component": 29,
              "sub_component_data_source": null,
              "sort_order": 6,
              "attributes": {
                "name": "First Aid",
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/first_aid.png",
                "action_type": "go_to_search",
                "search_term": "Dettol",
                "discount_text": "Up to 10% Off",
                "categoryTileText": "First Aid",
                "page_query_params": {}
              }
            }
          ]
        },
        {
          "id": 2,
          "name": "previously_ordered_items",
          "component_type": "sku_listing",
          "attributes": {
            "title": "Previously Ordered",
            "page_id": 2,
            "action_type": "go_to_page",
            "button_text": "View All",
            "preview_count": 4,
            "page_query_params": {}
          },
          "fetch_type": "DYNAMIC",
          "sub_components": [
            {
              "id": 3,
              "component": 2,
              "sub_component_data_source": "previous_order_listing",
              "sort_order": 2,
              "attributes": {
                "action_type": "add_to_cart",
                "button_text": "Add"
              }
            }
          ]
        },
        {
          "id": 31,
          "name": "Referral",
          "component_type": "image",
          "attributes": {
            "auth": true,
            "title": "Referral",
            "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/referral_card_4mar.png",
            "action_type": "go_to_referral",
            "page_query_params": {}
          },
          "fetch_type": "STATIC",
          "sub_components": []
        },
        {
          "id": 39,
          "name": "Grid_test2",
          "component_type": "grid",
          "attributes": {
            "rows": 1,
            "title": "Grid_test2",
            "columns": 1,
            "page_query_params": {}
          },
          "fetch_type": "STATIC",
          "sub_components": [
            {
              "id": 1008,
              "component": 39,
              "sub_component_data_source": null,
              "sort_order": 1,
              "attributes": {
                "name": "ABC",
                "page_id": 3,
                "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/revital_h_banner.png",
                "action_type": "go_to_page",
                "categoryTileText": "Abc",
                "page_query_params": {}
              }
            }
          ]
        },
        {
          "id": 100002,
          "name": "Go to Natarajs Page",
          "component_type": "image",
          "attributes": {
            "auth": true,
            "title": "Go to Natarajs Page",
            "page_id": 12,
            "image_url": "https://s3.ap-south-1.amazonaws.com/myra-static-images/myra_saving_store_flat_25_off.png",
            "action_type": "go_to_page",
            "page_query_params": {}
          },
          "fetch_type": "STATIC",
          "sub_components": []
        }
      ]
    }
  }

export let componentMap = {
    "grid"          : <div key={Math.random()}>GRID_COMPONENT</div>,
    "image"         : <div key={Math.random()}>IMAGE_COMPONENT</div>,
    "image_scroller": <div key={Math.random()}>IMAGE_SCROLLER_COMPONENT</div>,
    "sku_listing"   : <div key={Math.random()}>SKU_LISTING_COMPONENT</div>
}

export function getPageLayout(pageName) {
    let pageLayout = layout;
    return new Promise((resolve) => {
        resolve(pageLayout);
    });
}

export function getComponentsFromPageLayout(layout, componentMap) {
    let componentTree = [];
    let components = layout && layout["page"] && layout["page"]["components"] ? layout["page"]["components"] : [];

    for (const [index, component] of components.entries()) {
        if (component["component_type"]) {
            let componentType = component["component_type"];
            switch (componentType) {
                case "grid":
                    componentTree.push(componentMap[componentType]);
                    break;

                case "image":
                    let props = {
                      key: index,
                      image_url: component["attributes"]["image_url"],
                      text: component["name"]
                    }
                    componentTree.push(<Image {...props} />);
                    break;
                
                case "image_scroller":
                    componentTree.push(componentMap[componentType]);
                    break;
                
                case "sku_listing":
                    componentTree.push(componentMap[componentType]);
                    break;
            }
        }
    }
    return componentTree;
}

export function getSubComponentsFromData(subComponentsData, subComponentType) {
  let subComponents = [];

  for (const [index, data] of subComponentsData.entries()) {
      let subComponentProps = {
          ...data["attributes"],
          key: index
      };
      subComponents.push(<subComponentType {...subComponentProps} />);
  }
  return subComponents;
}