import axios from 'axios'
import {current_config_mode, getConfigMode} from '../config/config'

function getConfig(authOnly){
	if(getExpressTokenFromMedlife()!==null)
	{
		return	{
			'Authorization': "MRAuth " + getExpressTokenFromMedlife()
		}
	}
	let current_config_mode = getConfigMode()
	if( (current_config_mode.network.token || authOnly)){
		if(authOnly){
			return	{
					'Authorization': "MRAuth " + current_config_mode.network.token||expressAccessToken.access_token
				}
		}else{
			return	{
					'Content-Type' : 'application/json',
					'Authorization': "MRAuth " + current_config_mode.network.token
				}
		}

	}
}
export function getExpressTokenFromMedlife (){
	// return '69148329b0b34f4aaec71af718d1410f';
	if(window.hasOwnProperty('Android')){
		    let expressToken=Android.getExpressToken()
		    let avalUser =(expressToken.trim()!==''||expressToken!==undefined)?true:false;
		    if(avalUser){
			return expressToken;
		}else{
			return null;

		}
   }
   else if(localStorage && localStorage.getItem("expressAccessToken")!=='')
   {
	   return localStorage.getItem("expressAccessToken");

   }
	return null
	}
let apiUrl = ''
if((window && window.localStorage) || document.cookie){
	apiUrl = getConfigMode().network.$api
}else{
	alert("Please use Chrome browser")
}

export function Get(reqObj) {
	let defaultHeaders =  reqObj.removeAuth ? {'Content-Type' : 'application/json'} : getConfig()
	// console.log("url", reqObj.url)
	if (!reqObj.url) {
		return false
	}
	// reqObj.header = reqObj.header || {}
	return axios({
		method  : 'get',
		url     : reqObj.thirdPartyApi ? reqObj.url : apiUrl+reqObj.url,
		params    :  reqObj.payload,
		headers : {
			...defaultHeaders,
			...reqObj.headers
		}
	})
}

export function Delete(reqObj) {
	let defaultHeaders =  reqObj.removeAuth ? {'Content-Type' : 'application/json'} : getConfig()
	// let defaultHeaders = getConfig()
	if (!reqObj.url) {
		return false
	}
	// reqObj.header = reqObj.header || {}
	return axios({
		method  : 'delete',
		url     : apiUrl+reqObj.url,
		data    :  reqObj.data,
		headers : {
			...defaultHeaders,
			...reqObj.headers
		}
	})
}

export function Update(reqObj) {
	let defaultHeaders =  reqObj.removeAuth ? {'Content-Type' : 'application/json'} : getConfig()
	// let defaultHeaders = getConfig()
	// let defaultHeaders = getConfig("authOnly")
	if (reqObj !== Object(reqObj)) {
		return false
	}

	if (!reqObj.url) {
		return false
	}

	reqObj.payload = reqObj.payload || {}
	reqObj.headers = reqObj.headers || {}

	const data = new FormData()
	let payload = reqObj.payload
	if(reqObj.payload){
		for(let i in payload){
			if( payload[i].files){
				data.append(i, payload[i].files[0])
			}else{
				data.append(i, payload[i])
			}
		}
	}

	let auth = defaultHeaders && defaultHeaders.Authorization ? defaultHeaders : ''

	return axios({
		method  : 'post',
		url     : reqObj.thirdPartyApi && !reqObj.removeAuth ? reqObj.url : apiUrl+reqObj.url,
		// url     : apiUrl + reqObj.url,
		data    :  data,
		headers : {
			...auth,
			...reqObj.headers
		}
	})
}

export function Post(reqObj) {
	let defaultHeaders =  reqObj.removeAuth ? {'Content-Type' : 'application/json'} : getConfig()
	if (reqObj !== Object(reqObj)) {
		return false
	}

	if (!reqObj.url) {
		return false
	}
	reqObj.payload = reqObj.payload || {}
	
	return axios({
		method  : 'post',
		url     : reqObj.thirdPartyApi && !reqObj.removeAuth ? reqObj.url : apiUrl+reqObj.url,
		data    : reqObj.payload,
		headers : {
			...defaultHeaders,
			...reqObj.headers
		}
	})
}

export function Put(reqObj) {
	// debugger
	let defaultHeaders =  reqObj.removeAuth ? {'Content-Type' : 'application/json'} : getConfig()
	if (reqObj !== Object(reqObj)) {
		return false
	}

	if (!reqObj.url) {
		return false
	}

	reqObj.payload = reqObj.payload || {}
	// reqObj.headers = reqObj.headers || {}
	return axios({
		method  : 'put',
		url     : apiUrl+reqObj.url,
		data    :  reqObj.payload,
		headers : {
			...defaultHeaders,
			...reqObj.headers
		}
	})
}
