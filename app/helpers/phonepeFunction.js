import {Post,Get} from './apiUtils';
import { phonepeUserLogin } from '../redux/modules/users';

import { userInfo } from 'os';
let currentInstance = {};

let phonePe = {
    instanceCreate : function(){
        console.log()
        window.PhonePe.PhonePe.build(window.PhonePe.Constants.Species.web)
        .then((sdk)=>{
            // alert("instance created successfully");
            currentInstance = sdk

            this.fetchingToken();
        }).catch((err)=>{
            // alert(err);
            console.log("error in creating the instance");
        })
    },
    fetchingToken: function(){
        // if(currentInstance.keys.length > 0 ){
            currentInstance.fetchGrantToken().then((res) => {
                // alert(JSON.stringify(res))
                if(window.localStorage.user == undefined){
                    // alert("Next api"+JSON.stringify(res))
                    this.getMyraAccess(res).then((result)=> {
                        
                        this.gettingUserData(result.data.access_token)
                    }).catch((err)=>{
                        
                        // alert("api fail"+err);
                        // alert(JSON.stringify(err)); 
                    })
                }
                }).catch((err) => {
                    if(window.location.pathname !== '/prescription' && !localStorage.getItem('user')){
                        // logout()
                        setTimeout(()=>{
                            window.dispatchEvent(new Event('not_permitted'));
                            // window.localStorage.phonepeAuth = "false";
                            // alert(JSON.stringify(window.localStorage))
                            // alert("inside "+JSON.stringify(window.location.href = window.location.origin+"/prescription"))
                        },2000)
                        // logoutAndUnauth();

                    }
                    // window.location.replace('/prescription')
                // alert(err)
                console.log("Error occured while fetching the grant token")
              })

        // }
    },
    instanceStatus:function(){
        if(Object.keys(currentInstance).length > 0){
            return true;
        }else{
            return false;
        }
    },
    getMyraAccess: function(authDetails){
        // alert(authDetails.grantToken)
        return Post({
            url: 'phonepe/login',
            payload: {
                "merchantId" : String(authDetails.forMerchantId),
                "grantToken" : String(authDetails.grantToken)
            },
            removeAuth: true
        });
    },
    gettingUserData: function(access_token){
        // alert(access_token);
        return Get({
            url: 'user',
            removeAuth: true,
            headers: {
                'Authorization':access_token
            }
        }).then((res)=> {
            console.log(res);
            res.data.access_token = access_token.split(" ")[1];
            // window.localStorage.user = res;
            phonepeUserLogin(res.data,window.location.pathname);
            
        }).catch((err)=>{
            // alert("api failed"+err)
        })
    },
    initiatePayment(payload){
        let merchantName = "Myra"
        
        let imageURL = ""

        //Any metadata to show on PhonePe's payment screen
        //Array of key-value pairs
        let orderedMetadata = [{"OrderId":payload.displayId}]

        return Post({
            // url : 'https://ea54c7a4.ngrok.io/payments/phonepe/orders/web/IMGIVR',
            url:'payments/phonepe/orders/web/'+payload.displayId,
            payload:'',
            // thirdPartyApi: true

        }).then((res)=>{
            // alert("successfully initiated service request"+ JSON.stringify(res.data.phone_pe_data.data));
            currentInstance.openPaymentsPage(merchantName, res.data.phone_pe_data.data, payload.fallbackURL, imageURL, orderedMetadata).then((response) => {
                window.location.reload();
                // alert("Payment was successful = " + JSON.stringify(response));
            }).catch((err) => {
                // alert("Payment failed with error = " + JSON.stringify(err))
            })
        }).catch((err)=>{
            // alert("Payment cannot be processed at the moment. Please try again later!");
            console.log("failed service request call");
        })
    },
    getPhonePeSdkPermission(){
        let result = currentInstance.seekPermission([PhonePe.Constants.Permission.READ_SMS, PhonePe.Constants.Permission.LOCATION])
        .then((res)=>{
            // alert(JSON.stringify(res));
        }).catch(()=>{
            // alert(JSON.stringify(error));
        })
    }
}

export default phonePe;