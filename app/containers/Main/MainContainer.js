import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Navigation from "$COMPONENTS/Navigation/Navigation";
import AppFooter from "$COMPONENTS/AppFooter/AppFooter";
import Prompt from "$COMPONENTS/Popup/Prompt";
import OrderPrescriptionContainer from "$CONTAINERS/OrderPrescription/OrderPrescriptionContainer";
import { hanndleAuthedUser } from "$HELPERS/auth";
import {
    shouldDisplayPrompt,
    checkLocationEnable,
    setImgUrlFromFiles,
    getOrientation,
    getFormattedString,
    enableGEOLocation,
    getLocalstorageValue,
    isLocalStorageSupported,
} from "$HELPERS/helperFunction";
import { getRecomendedItemsList, getAndHandleCurrentOrder } from "$REDUX/modules/ordersPrescription";
import * as userActionCreators from "$REDUX/modules/users";
import { escalate, addedIcon, timeIcon } from "$SHAREDSTYLES/icons";
import {
    setAndHandleCartOrder,
    maintainRoutes,
    setMedicineNotAvail,
    handleGetDynamicText,
    handleGetActiveLocation,
    handleRemoveOrderItem,
    setActiveLocation,
} from "$REDUX/modules/cart";
import Clevertap from "$HELPERS/clevertap";
import phonePe from "$HELPERS/phonepeFunction";
import { Link } from "react-router-dom";
import { updateImageElement } from "$REDUX/modules/imageSelector";
import Alert from "$PARTIALS/Alert/Alert";
import ChordingPopup from "$PARTIALS/ChordingPopup/ChordingPopup";
import * as style from "./style.scss";

import Loader from "$PARTIALS/Loader/Loader";
import { camera3, arrow } from "$SHAREDSTYLES/icons";

import "$SHAREDSTYLES/_sharedStyle.scss";

class MainContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display_prompt: false,
            showLocationAlert: false,
        };
        // if(this.props.userOnlineStatus == "online" ){
        // this.props.handleGetDynamicText().then(x => {
        // 		checkLocationEnable(this.initGeolocation)
        // })
        // }
    }

    componentDidMount() {
        hanndleAuthedUser(this);

        let display_prompt = shouldDisplayPrompt();
        this.setState({ display_prompt: display_prompt });
        setTimeout(() => {
            const { isAuthed, items } = this.props;
            let pathname = window.location.pathname;
            let mainContainer = document.querySelector(".main_centered_container");
            mainContainer.classList.remove("checkout-margin");
            if (
                isAuthed &&
                items.length &&
                (pathname !== "/select_location" &&
                    pathname !== "/accounts" &&
                    pathname !== "/cart" &&
                    pathname !== "/page/offers" &&
                    pathname !== "/user" &&
                    pathname !== "/edit_location" &&
                    pathname !== "/search_location" &&
                    pathname !== "/user_locations")
            )
                mainContainer.classList.add("checkout-margin");
        }, 100);

        // document.addEventListener('scroll', this.scroll);
    }
    componentDidUpdate() {
        setTimeout(() => {
            const { isAuthed, items } = this.props;
            let pathname = window.location.pathname;
            let mainContainer = document.querySelector(".main_centered_container");
            mainContainer.classList.remove("checkout-margin");
            if (
                isAuthed &&
                items.length &&
                (pathname !== "/select_location" &&
                    pathname !== "/accounts" &&
                    pathname !== "/cart" &&
                    pathname !== "/page/offers" &&
                    pathname !== "/user" &&
                    pathname !== "/edit_location" &&
                    pathname !== "/search_location" &&
                    pathname !== "/user_locations")
            )
                mainContainer.classList.add("checkout-margin");
        }, 100);
    }

    getImage = e => {
        let rotate = "";
        if (e.target) {
            let file = e.target;
            getOrientation(file.files[0], function(orientation) {
                rotate = orientation;
            });
            setTimeout(() => {
                this.props.updateImageElement({
                    image_value: file,
                    image_element: setImgUrlFromFiles(file),
                    orientation_flag: rotate,
                });
                this.props.history.push("/image_prescription");
            }, 500);
        }
    };

    getLocationList = () => {
        this.props.handleGetUserLocations().then(data => {
            if (data && data.locations && data.locations.length > 0) {
                this.props.setAndHandleCartOrder();
                // 	this.props.handleGetDynamicText().then(x => {
                // 	enableGEOLocation(this.success, this.fail)
                // })
            }
            // this.props.history.replace('prescription')
            if (data && data.locations && data.locations.length > 0) {
                this.props.location.pathname === "/" || this.props.location.pathname === "/login" ? this.props.history.replace("page/home") : "";
            } else {
                // debugger
                this.props.history.replace("search_location");
            }
        });
    };

    componentWillReceiveProps(newProps) {
        // debugger
        if (
            newProps &&
            newProps.userActiveLocation &&
            newProps.userActiveLocation.label &&
            !this.state.locationAlertText &&
            newProps.changeLocation
        ) {
            let x = newProps.userActiveLocation;
            x && x.address
                ? this.setState({
                      showLocationAlert: true,
                      locationAlertText: {
                          body: getFormattedString(x.address, 50),
                          head: `Location set to ${x.label}`,
                      },
                  })
                : "";
            setTimeout(() => {
                this.hideLocationAlert();
            }, 5000);
            this.props.setActiveLocation(newProps.userActiveLocation, false);
        }
        if (this.props.userOnlineStatus == "online" && !this.props.appDynamicText && !this.props.fetchDynamicLocation && !this.state.getLocation) {
            // debugger
            this.setState({ getLocation: true, loadPage: true });
            this.props.handleGetDynamicText().then(x => {
                this.setState({ getLocation: false });
                let location = getLocalstorageValue("userLocation");
                // debugger
                if (location && Object.keys(location).length) {
                    this.success(location, "local");
                } else {
                    enableGEOLocation(this.success, this.fail);
                }
            });
        }
    }

    success = (position, local) => {
        // position = {LatLng: {lat: 12.982752, lng: 77.61422499999999}}
        this.hideLocationAlert();
        if ((position.latitude || position.LatLng) && (position.longitude || position.LatLng)) {
            this.acticeLocation({
                queryString: `?latitude=${local ? position.latitude : position.LatLng.lat}&longitude=${
                    local ? position.longitude : position.LatLng.lng
                }`,
            });
        } else {
            this.setState({ loadPage: false });
        }
    };

    fail = r => {
        this.hideLocationAlert();
        // let position = {"coords":{"accuracy":28,"altitude":null,"altitudeAccuracy":null,"heading":null,"latitude":12.982571,"longitude":77.6143622,"speed":null},"timestamp":1544794346706}
        console.log("fail ", r);
        this.acticeLocation("fail");
    };

    acticeLocation = queryString => {
        if (typeof queryString == "string") {
            let location = "";
            if (isLocalStorageSupported()) {
                location = getLocalstorageValue("userLocation");
                queryString =
                    location && location.id && location.latitude && location.longitude
                        ? {
                              queryString: `?latitude=${location.latitude}&longitude=${location.longitude}`,
                          }
                        : "";
            }
            // location ? this.props.setActiveLocation(location, false) : ""
            // return
        }
        if (this.props.userOnlineStatus == "online") {
            this.props.handleGetActiveLocation(queryString).then(x => {
                if (x && Object.keys(x).length) {
                    queryString && x && x.address
                        ? this.setState({
                              showLocationAlert: true,
                              locationAlertText: {
                                  body: getFormattedString(x.address, 50),
                                  head: `Location set to ${x.label}`,
                              },
                          })
                        : "";
                    setTimeout(() => {
                        this.hideLocationAlert();
                    }, 4000);
                    if (this.props.history.location.pathname != "/cart") {
                        this.props.setAndHandleCartOrder().then(x => {
                            this.props.getRecomendedItemsList(x).then(x => {
                                this.setState({ loadPage: false });
                            });
                        });
                    } else {
                        this.props.getRecomendedItemsList().then(x => {
                            this.setState({ loadPage: false });
                        });
                    }
                } else {
                    this.setState({ loadPage: false });
                    this.props.history.push("/search_location");
                }
            });
        }
    };

    hideLocationAlert = () => {
        this.setState({ showLocationAlert: false, locationAlertText: "" });
    };

    // scroll = () => {
    // 	let isTop = window.scrollY;
    //   if (isTop > 0 && !this.state.scrolled) {
    //      this.setState({scrolled: true})
    //   }else if(isTop < 10 && this.state.scrolled){
    //   	this.setState({scrolled: false})
    //   }
    // }

    componentWillUnmount() {
        // document.removeEventListener('scroll', this.scroll)
    }

    handleBackButton = e => {
        this.props.handleBackButton();
        this.props.counterHandler();
    };

    handleCloseBtn = e => {
        Clevertap.event("mWeb - Download App by Prompt!");
        this.setState({ display_prompt: false });
    };

    pathname = () => {
        if (window.history.length > 2) {
            history.back();
        } else {
            // alert(history.length)
            this.props.history.push("/page/home");
            setTimeout(() => {
                hanndleAuthedUser(this);
                window.dispatchEvent(new Event("currentOrder"));
            }, 1000);
            // window.location.reload()
        }
        // history.back()
    };

    handleShowSearch = (flag, route) => {
        this.props.isAuthed;
        this.props.history.push("/search_medicine");
        // this.props.handleShowSearch(flag, route)
    };

    cancelBtn = () => {
        this.props.setMedicineNotAvail("");
    };

    actionBtn = () => {
        // debugger
        if (
            this.props.notAvailMedicile.availItems &&
            this.props.notAvailMedicile.availItems["out of stock"] &&
            this.props.notAvailMedicile.availItems["out of stock"].items &&
            this.props.notAvailMedicile.availItems["out of stock"].items.length
        ) {
            this.props.notAvailMedicile.availItems["out of stock"].items.map((x, index) => {
                if (x.id) {
                    this.props.handleRemoveOrderItem(x.id).then(x => {
                        if (index == this.props.notAvailMedicile.availItems["out of stock"].items.length - 1) {
                            this.props
                                .handleGetActiveLocation({
                                    queryString: this.props.popover_query,
                                    order: { items: x },
                                })
                                .then(y => {
                                    y && y.address
                                        ? this.setState({
                                              showLocationAlert: true,
                                              locationAlertText: {
                                                  body: getFormattedString(y.address, 50),
                                                  head: `Location set to ${y.label}`,
                                              },
                                          })
                                        : "";
                                    setTimeout(() => {
                                        this.hideLocationAlert();
                                    }, 4000);
                                    this.props.setMedicineNotAvail("");
                                });
                        }
                    });
                }
            });
        } else if (this.props.notAvailMedicile.availItems && this.props.notAvailMedicile.availItems.id) {
            let x = this.props.notAvailMedicile.availItems;
            if (x.id) {
                this.props.handleRemoveOrderItem(x.id).then(x => {
                    this.cancelBtn();
                });
            }
        } else {
            this.props
                .handleGetActiveLocation({
                    queryString: this.props.popover_query,
                })
                .then(x => {
                    x && x.address
                        ? this.setState({
                              showLocationAlert: true,
                              locationAlertText: {
                                  body: getFormattedString(x.address, 50),
                                  head: `Location set to ${x.label}`,
                              },
                          })
                        : "";
                    setTimeout(() => {
                        this.hideLocationAlert();
                    }, 4000);
                    this.props.setMedicineNotAvail("");
                });
        }
    };

    render() {
        let route_path = this.props.history.location.pathname ? this.props.history.location.pathname.replace(/\//, "") : "";
        let auth_value = (this.props.match.path && this.props.match.path.indexOf("attach")) >= 0 || this.props.token ? true : this.props.isAuthed;
        let popup_body = "";
        let pathname = window.location.pathname;
        if (this.props.notAvailMedicile && Object.keys(this.props.notAvailMedicile.availItems).length) {
            let notAvailMedicile = this.props.notAvailMedicile.availItems;
            if (!notAvailMedicile.id) {
                popup_body = [];
                for (let i in notAvailMedicile) {
                    if (notAvailMedicile[i] && notAvailMedicile[i].items && notAvailMedicile[i].items.length) {
                        let availInfo = notAvailMedicile[i].items[0].availInfo;
                        let bucket = (
                            <div style={{ marginBottom: "10px" }}>
                                <div className={`${style.svg} upper_text`}>
                                    {availInfo.cart_bucket_text == "Out of Stock"
                                        ? escalate
                                        : availInfo.delivery_bucket_type == "pseudo"
                                        ? addedIcon
                                        : timeIcon(availInfo.notification_icon_color)}{" "}
                                    {availInfo.fulfilment_text}
                                </div>
                                <div>
                                    {notAvailMedicile[i].items.map((x, index) => {
                                        return (
                                            <div className="medium_disable_text" key={index}>
                                                {x.medicine.formatted_name}
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        );
                        popup_body.push(bucket);
                    }
                }
            } else {
                let medicine = notAvailMedicile.medicine ? notAvailMedicile.medicine : this.props.notAvailMedicile.availItems;
                popup_body = (
                    <div>
                        <div>{escalate} OUT OF STOCK</div>
                        <div>{medicine.formatted_name}</div>
                    </div>
                );
            }
        }
        const data = (
            <div
            // style={
            //     this.props.isAuthed &&
            //     pathname == "/prescription"
            //         ? { marginTop: "75px" }
            //         : { marginTop: "10px" }
            // }>
            >
                {pathname == "/prescription" || pathname === "/page/home" ? (
                    <div className={style.prescription_upload}>
                        <div className="d-flex align-items-center">
                            <input
                                className={style.image_uploader}
                                id="imageUploader"
                                name="imageUploader"
                                onClick={this.handleImageUpload}
                                onChange={this.getImage}
                                type="file"
                                accept=".png, .jpg, .jpeg/*;capture=camera"
                            />
                            <span>{camera3}</span>
                            <div className="m-l-15">
                                <p
                                    style={{
                                        fontSize: "16px",
                                        fontWeight: "500",
                                    }}>
                                    Order with Prescription
                                </p>
                                <p
                                    style={{
                                        fontSize: "12px",
                                        color: "#005073",
                                    }}>
                                    Flat 20% Off
                                </p>
                            </div>
                        </div>
                        <button
                            className={`button ${style.primary_button}`}
                            onClick={() =>
                                this.props.isAuthed ? document.querySelector("#imageUploader").click() : this.props.history.push("/login")
                            }>
                            Upload
                        </button>
                    </div>
                ) : null}
                {this.props.isActiveLocation || this.state.loadPage ? <Loader /> : ""}
                {this.props.notAvailMedicile && Object.keys(this.props.notAvailMedicile.availItems).length ? (
                    <Alert
                        header_text={
                            this.props.notAvailMedicile.avalHeadernew
                                ? this.props.notAvailMedicile.avalHeadernew
                                : this.props.appDynamicText.checkout_properties.value.dialog_new_availability
                        }
                        popup_body={popup_body}
                        cancelBtn={this.props.notAvailMedicile.availItems && this.props.notAvailMedicile.availItems.id ? "" : this.cancelBtn}
                        actionBtn={
                            this.props.notAvailMedicile.availItems &&
                            (this.props.notAvailMedicile.availItems.id || Object.keys(this.props.notAvailMedicile.availItems).length)
                                ? this.actionBtn
                                : this.cancelBtn
                        }
                    />
                ) : (
                    ""
                )}
                {this.state.showLocationAlert && this.state.locationAlertText && route_path === "prescription" ? (
                    <ChordingPopup
                        header_text={this.state.locationAlertText.head}
                        bodyText={this.state.locationAlertText.body}
                        actionButton={this.state.locationAlertText.showActionButton}
                        actionAlert={() => enableGEOLocation(this.success, this.fail)}
                        hideAlert={this.hideLocationAlert}
                    />
                ) : (
                    ""
                )}
                {route_path === "search_medicine" ? (
                    ""
                ) : (
                    <Navigation
                        isAuthed={auth_value}
                        userName={this.props.userName}
                        pathname={this.pathname}
                        otp={this.props.otp}
                        logoutAndUnauth={this.props.logoutAndUnauth}
                        handleBackButton={this.handleBackButton}
                        isLocationFlag={this.props.isLocationFlag}
                        scrolled={this.state.scrolled}
                        handleShowSearch={this.handleShowSearch}
                        userActiveLocation={this.props.userActiveLocation}
                        maintainRoutes={this.props.maintainRoutes}
                        homepageTabType={this.props.homepageTabType}
                        items={this.props.items}
                        history={this.props.history}
                        categoryName={this.props.categoryName}
                        route_path={route_path}
                        cart_charges={this.props.cart_charges}
                    />
                )}
                {/* @TODO false  to be changed to prompt variable*/}
                {/*this.props.showSearch ? <SearchContainer history = {this.props.history} handleShowSearch={this.handleShowSearch}/> : ""*/}
                {/*!route_path ? <div className = "main_centered_container "><OrderPrescriptionContainer history = {this.props.history}/></div> : "" */}
                {this.state.display_prompt && route_path.indexOf("payments") && false < 0 ? <Prompt handleCloseBtn={this.handleCloseBtn} /> : ""}
                {this.props.items.length
                    ? pathname !== "/select_location" &&
                      pathname !== "/accounts" &&
                      pathname !== "/cart" &&
                      pathname !== "/page/offers" &&
                      pathname !== "/user" &&
                      pathname !== "/edit_location" &&
                      pathname !== "/search_location" &&
                      pathname !== "/user_locations" && (
                          <Link
                              to="/cart"
                              style={{
                                  bottom: pathname == "/search_medicine" ? "0" : "58px",
                              }}
                              className="fixed_btn button">
                              <span>Check Out</span>
                              <span
                                  style={{
                                      position: "absolute",
                                      right: "15px",
                                  }}>
                                  {this.props.items.length} items
                              </span>
                          </Link>
                      )
                    : ""}
                {(pathname == "/page/home" ||
                    pathname.indexOf("/page/") === 0 ||
                    pathname == "/product_categories" ||
                    pathname == "/accounts" ||
                    pathname == "/login" ||
                    pathname == "/previoud_order") && <AppFooter isAuthed={this.props.isAuthed} route_path={route_path} />}
                <div id="google-maps-places" />
            </div>
        );

        return data;
    }
}

export default connect(
    ({ users, attachPrescription, cart, ordersPrescription }) => ({
        categoryName: ordersPrescription.categoryName,
        items: cart.items,
        notAvailMedicile: cart.notAvailMedicile,
        userActiveLocation: cart.userActiveLocation,
        changeLocation: cart.changeLocation,
        appDynamicText: cart.appDynamicText,
        fetchDynamicLocation: cart.fetchDynamicLocation,
        popover_query: cart.popover_query,
        userOnlineStatus: users.userOnlineStatus,
        token: attachPrescription.token,
        homepageTabType: users.homepageTabType,
        showSearch: users.showSearchFlag,
        isAuthed: users.isAuthed,
        otp: users.otp,
        isFetching: users.isFetching,
        historyLocation: users.historyLocation,
        isLocationFlag: users.isLocationFlag,
        userName: users.userInfo.name,
        isActiveLocation: cart.isActiveLocation,
        cart_charges: cart.cart_charges,
    }),
    dispatch =>
        bindActionCreators(
            {
                ...userActionCreators,
                setAndHandleCartOrder,
                maintainRoutes,
                handleGetDynamicText,
                setMedicineNotAvail,
                getRecomendedItemsList,
                handleGetActiveLocation,
                handleRemoveOrderItem,
                updateImageElement,
                setActiveLocation,
            },
            dispatch
        )
)(MainContainer);
