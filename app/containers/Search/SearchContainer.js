import React, { Component } from "react";
import * as style from "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { backArrow, cartIcon2, smallgreyCancel } from "$SHAREDSTYLES/icons";
import * as cartActionCreater from "$REDUX/modules/cart";
import Loader from "$PARTIALS/Loader/Loader";
import ItemsListContainer from "./ItemsListContainer";
import HomeNavigation from '../../components/Navigation/HomeNavigation';
import { getFormattedString } from "$HELPERS/helperFunction";
import Clevertap from "$HELPERS/clevertap";
import SmallLoader from "$PARTIALS/SmallLoader/SmallLoader";
// import { URLSearchParams } from 'url';

var debounce = require("lodash.debounce");

class SearchContainer extends Component {
    constructor(props) {
        super(props);
        let search_string = "";
        let string = props.location.search;
        if (string && string.indexOf("product_name=") >= 0) {
            search_string = string
                .split("product_name=")
                .pop()
                .replace(/%20/g, " ");
        }

        // let search_params = new URLSearchParams(window.location.search);
        this.state = {
            searchInput: this.props.searchInput ? this.props.searchInput : "",
            itemList: null,
        };
        this.debounceSearch = debounce(this.debounceSearch, 350);
    }

    componentDidMount() {
        // if(this.props.userOnlineStatus == "online"){
        //     this.props.setAndHandleCartOrder()
        // }
        // if(this.state.searchInput){
        //   this.debounceSearch()
        //   this.props.updateSearchInput("")
        // }
        this.props.searchInput
            ? this.debounceSearch(this.props.searchInput)
            : null;
        this.props.addSearchListSuccess("");
    }

    debounceSearch = val => {
        if (this.state.searchInput.length >= 1 || val) {
            Clevertap.event(`mWeb - Search`, {
                origin: this.props.searchOrigin,
                keyword: val ? val : this.state.searchInput,
            });
            this.props
                .handleSearch(val ? val : this.state.searchInput)
                .then(resp => {
                    if (resp) {
                        this.setState({ itemList: resp });
                    }
                })
                .catch(error => {
                    console.log("error", error);
                });
        } else {
            this.setState({ itemList: null });
        }
    };

    handleInputChange = event => {
        this.setState({ searchInput: event.target.value });
        this.debounceSearch(event.target.value);
    };

    handleGoCartPage = e => {
        e.stopPropagation();
        Clevertap.event(`mWeb - Goto Cart button from Search`);
        this.props.history.push("/cart");
        // this.props.handleShowSearch(false)
    };

    // handlePacketTypeChange = (id, event) => {
    //     if(id in this.state.cartItems){
    //         let item = this.props.items.filter(function(i){ return i["drug_variation_id"] ==  id })[0];
    //         let label = item.medicine.allowed_quantity_types[event.target.value].label
    //         let packetTypeChange = Object.assign({}, this.state.packetTypeChange);
    //         packetTypeChange[id] = parseInt(event.target.value);
    //         this.setState({packetTypeChange});
    //         this.props.handleUpdateMedicineCount({orders: {items: [{id: item.id, quantity: this.state.cartItems[id][0], quantity_type_selected: label}]}})
    //         // this.props.handleUpdateMedicineCount({orders: {medicines: [{id: id, normalized_quantity: this.state.cartItems[id][0], quantity_type_selected: label, added_via_search: true}]}})
    //     }
    //     else{
    //         let packetTypeChange = Object.assign({}, this.state.packetTypeChange);
    //         packetTypeChange[id] = parseInt(event.target.value);
    //         this.setState({packetTypeChange});
    //     }
    // }

    handleExtendedNotes = (data, action, event) => {
        let extendedNotes = Object.assign({}, this.state.extendedNotes);
        if (action == "add") {
            extendedNotes[data] = parseInt(1);
        } else if (action == "remove") {
            delete extendedNotes[data];
        } else if (action == "update") {
            extendedNotes[data] = event.target.value;
        }
        this.setState({ extendedNotes });
        let payload = [];
        for (let ext in extendedNotes) {
            let arr = {};
            arr["item_text"] = ext.toLowerCase();
            arr["quantity_text"] = extendedNotes[ext].toString();
            payload.push(arr);
        }
        this.props.handleUpdateItemList(payload);
    };

    removeInputText = e => {
        e.preventDefault();
        this.setState({ searchInput: "" });
        document.getElementsByTagName("input") &&
        document.getElementsByTagName("input").length
            ? document.getElementsByTagName("input")[0].focus()
            : "";
        // e.target.parentNode.previousSibling.focus()
    };

    handleShowSearch = e => {
        this.props.history.goBack();
        // this.props.handleShowSearch(false)
    };

    goAddressScreen = () => {
        Clevertap.event("mWeb - Add New Address Screen", {
            origin: "SearchBar",
        });
        this.props.maintainRoutes("home");
    };

    render() {
        let userActiveLocation = this.props.userActiveLocation;
        return (
            <div>
                <div className={style.search_container}>
                    <HomeNavigation
                        maintainRoutes={this.props.maintainRoutes}
                        userActiveLocation={userActiveLocation}
                        isAuthed={this.props.isAuthed}
                        handleShowSearch={this.handleShowSearch}
                        route_path="search_medicine"
                        searchInput={this.state.searchInput}
                        handleInputChange={this.handleInputChange}
                        isFetching={this.props.isFetching}
                        removeInputText={this.removeInputText}
                    />

                    {/* Search Result Container */}

                    <ItemsListContainer
                        imageView={true}
                        itemList={this.props.searchList}
                        searchInput={this.state.searchInput.toLowerCase()}
                        history={this.props.history}
                        isAuthed={this.props.isAuthed}
                        parentRef="Search"
                    />

                    {this.state.showQtyPrompt ? (
                        <div className={style.qtyPrompt}>
                            <div className={style.popup}>
                                <h4>Add Quantity</h4>
                                <input
                                    type="number"
                                    onChange={e =>
                                        this.setState({
                                            otherQtyValue: e.target.value,
                                        })
                                    }
                                    autoFocus
                                />
                                <div className={style.actionRow}>
                                    <span
                                        onClick={() =>
                                            this.setState({
                                                showQtyPrompt: false,
                                            })
                                        }>
                                        Cancel
                                    </span>
                                    <span onClick={this.handleOtherQtyChange}>
                                        OK
                                    </span>
                                </div>
                            </div>
                        </div>
                    ) : (
                        ""
                    )}
                </div>
                {/* Go to cart Button */}
                {/* {this.props.items && this.props.items.length ?
              <div className="fixed_btn button" onClick={this.handleGoCartPage}>
              	<span>Check Out</span>
							  <span style = {{ position: "absolute", right: "15px"}} >{Object.keys(this.props.items).length} items</span>
              </div>
              :''} */}
            </div>
        );
    }
}

export default connect(
    ({ cart, users }) => ({
        userOnlineStatus: users.userOnlineStatus,
        isAuthed: users.isAuthed,
        isFetching: cart.isFetching,
        searchInput: cart.searchInput,
        items: cart.items,
        extendedNotesInCart: cart.extended_notes,
        userActiveLocation: cart.userActiveLocation,
        searchOrigin: users.searchOrigin,
        searchList: cart.searchList,
    }),
    dispatch => bindActionCreators(cartActionCreater, dispatch)
)(SearchContainer);
