import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import ItemCard from "$COMPONENTS/Search/ItemCard";
import * as style from "./style.scss";
import * as cartActionCreater from "$REDUX/modules/cart";
var debounce = require("lodash.debounce");
import OverLayPopover from "$PARTIALS/OverLayPopover/OverLayPopover";
import CustomizeCalendar from "$PARTIALS/Calendar/CustomizeCalendar";
import SelectBox from "$PARTIALS/SelectBox/SelectBox";
import {
    card_container,
    itemName,
    packing,
    cardActions,
    addBtn,
    removeBtn,
    selectQty,
    extended,
    selectBoxWraper,
} from "../../components/Search/style.scss";

import Clevertap from "../../helpers/clevertap";
import { getFormatedTime, getMonthText } from "$HELPERS/helperFunction";
import remove from "../../sharedImage/remove.svg";

class ItemsListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartItems: null,
            showQtyPrompt: false,
            otherQtyItem: null,
            otherQtyValue: null,
            packetTypeChange: {},
            extendedNotes: {},
        };
    }
    static contextTypes = {
        //router: React.PropTypes.object.isRequired,
    };

    componentDidMount() {
        let cartItemsObj = "";
        // if (this.props.parentRef != "Order Details") {
        if (this.props.userOnlineStatus == "online" && this.props.items && this.props.items.length && this.props.parentRef != "Order Details") {
            cartItemsObj = this.props.items.reduce(
                (obj, item) => Object.assign(obj, { [item.drug_variation_id]: [item.normalized_quantity, item.quantity, true, item.availInfo] }),
                {}
            );
        } else {
            cartItemsObj = this.props.items.reduce(
                (obj, item) => Object.assign(obj, { [item.drug_variation_id]: [item.selected_quantity, item.quantity, true, item.availInfo] }),
                {}
            );
        }
        // }
        this.setState({ cartItems: cartItemsObj });

        let packetTypeChange = Object.assign({}, this.state.packetTypeChange);
        for (let o in cartItemsObj) {
            packetTypeChange[o] = parseInt(cartItemsObj[o][1]);
        }
        this.setState({ packetTypeChange });

        let extendedNotes = {};
        if (this.props.extendedNotesInCart.length) {
            this.props.extendedNotesInCart.map(key => {
                extendedNotes[key.item_text.toLowerCase()] = key.quantity_text;
            });
            this.setState({ extendedNotes });
        }
    }

    componentWillReceiveProps(nextProps) {
        let cartItemsObj = "";
        // if (this.props.parentRef != "Order Details") {
        if (this.props.userOnlineStatus == "online" && nextProps.parentRef != "Order Details") {
            cartItemsObj = nextProps.items.reduce(
                (obj, item) => Object.assign(obj, { [item.drug_variation_id]: [item.normalized_quantity, item.quantity, true, item.availInfo] }),
                {}
            );
        } else {
            cartItemsObj = nextProps.items.reduce(
                (obj, item) => Object.assign(obj, { [item.drug_variation_id]: [item.selected_quantity, item.quantity, true, item.availInfo] }),
                {}
            );
        }
        // }
        this.setState({ cartItems: cartItemsObj });

        let packetTypeChange = Object.assign({}, this.state.packetTypeChange);
        for (let o in cartItemsObj) {
            packetTypeChange[o] = parseInt(cartItemsObj[o][1]);
        }
        this.setState({ packetTypeChange });

        let extendedNotes = {};
        if (nextProps.extendedNotesInCart.length) {
            nextProps.extendedNotesInCart.map(key => {
                extendedNotes[key.item_text.toLowerCase()] = key.quantity_text;
            });
            this.setState({ extendedNotes });
        }
    }

    handleAddItem = (item, sub_context) => {
        if (!this.props.isAuthed) {
            return this.props.history.replace("/login");
        }
        if (this.props.isFetching) {
            return;
        }
        // let selectedTypeIndex = this.state.packetTypeChange[parseInt(id)] || 0
        // let item = this.props.itemList.filter(function(i){ return i["id"] == id })[0];
        this.setState({ updatedMedicine: item.id });
        item.add_quantity = 1;
        item.update_status = "add";
        let context = {};
        switch (this.props.parentRef) {
            case "Search":
                context = { context: "search" };
                break;
            case "Previously Ordered":
                context = { context: "previously_ordered" };
                break;
            case "Order Details":
                context = { context: "reorder_item", sub_context: "my_orders" };
                break;
            default:
                break;
        }
        item.query = { ...{ context: this.props.parentRef, action: "added", source: "web_manual" }, ...context };
        if (sub_context) {
            item.query.sub_context = "similar_items";
        }
        // debugger
        this.props.handleUpdateMedicineCount(item);

        if (!this.props.parentRef) {
            return;
        }
        let title = "";
        let payload = {};
        switch (this.props.parentRef) {
            case "Search":
                title = `mWeb - ${this.props.parentRef} - add to cart`;
                payload = {
                    itemName: item.formatted_name,
                    drugVariationId: item.id,
                    quantity: 1,
                };
                break;

            case "Previously Ordered":
                title = `mWeb - ${this.props.parentRef} - add to cart`;
                payload = {
                    itemName: item.formatted_name,
                    drugVariationId: item.id,
                    quantity: 1,
                    source: this.props.extraInfo ? this.props.extraInfo.source : "",
                };
                break;
            case "Order Details":
                title = `mWeb - ${this.props.parentRef} - Item Reorder`;
                payload = {
                    orderId: this.props.extraInfo ? this.props.extraInfo.orderId : "",
                    drugVariationId: item.id,
                };
                break;
            default:
                break;
        }

        Clevertap.event(title, payload);
    };

    handleQtyChange = (id, value) => {
        if (this.props.isFetching) {
            return;
        }
        this.setState({ updatedMedicine: id });
        if (value == "Other") {
            this.setState({ showQtyPrompt: true, otherQtyItem: id });
            return;
        }
        if (this.props.parentRef) {
            Clevertap.event(`mWeb - ${this.props.parentRef} - packing`);
        }
        // let selectedTypeIndex = this.state.packetTypeChange[parseInt(id)]
        let item = this.props.items.filter(function(i) {
            return i["drug_variation_id"] == id;
        })[0];
        // let packetType = this.props.itemList.filter(function(i){ return i["id"] == id })[0];
        item.add_quantity = parseInt(value);
        item.update_status = "changeQty";
        // debugger
        item.query = { context: this.props.parentRef, action: "changed quantity", source: "web_manual" };
        this.props.handleUpdateMedicineCount(item);
    };

    handleOtherQtyChange = () => {
        if (this.props.isFetching) {
            return;
        }
        let dv_id = this.state.otherQtyItem;
        // let selectedTypeIndex = this.state.packetTypeChange[parseInt(dv_id)]
        let item = this.props.items.filter(function(i) {
            return i["drug_variation_id"] == dv_id;
        })[0];
        // let packetType = this.props.itemList.filter(function(i){ return i["id"] == dv_id })[0];
        item.add_quantity = parseInt(this.state.otherQtyValue);
        item.update_status = "changeQty";
        item.query = { context: this.props.parentRef, action: "changed quantity", source: "web_manual" };
        this.props.handleUpdateMedicineCount(item);
        this.setState({ otherQtyItem: null, otherQtyValue: null, showQtyPrompt: false });
    };

    handleRemoveItem = id => {
        if (this.props.isFetching) {
            return;
        }
        this.setState({ updatedMedicine: id });
        let item = this.props.items.filter(function(i) {
            return i["drug_variation_id"] == id;
        })[0];
        if (confirm("Are you sure you want to delete " + item.medicine.formatted_name)) {
            this.props.handleRemoveOrderItem(item.id);
        }

        if (this.props.parentRef) {
            Clevertap.event(`mWeb - ${this.props.parentRef} - remove item`, {
                itemId: id,
                itemName: item.medicine.formatted_name,
            });
        }
    };

    handleExtendedNotes = (data, action, value) => {
        if (!this.props.isAuthed) {
            return this.props.history.replace("/login");
        }
        if (this.props.isFetching) {
            return;
        }
        let extendedNotes = Object.assign({}, this.state.extendedNotes);
        if (action == "add") {
            extendedNotes[data] = parseInt(1);
        } else if (action == "remove") {
            delete extendedNotes[data];
        } else if (action == "update") {
            extendedNotes[data] = value;
        }
        this.setState({ extendedNotes });
        let payload = [];
        for (let ext in extendedNotes) {
            let arr = {};
            arr["item_text"] = ext.toLowerCase();
            arr["quantity_text"] = extendedNotes[ext].toString();
            payload.push(arr);
        }
        this.props.handleUpdateItemList(payload);
    };

    handleShowCalender = (query, blocked_dates) => {
        // let queryString = ""
        // for (var i in query) {
        //  	queryString += `&${i == "number_of_days" ? "no_of_days" : i}=${i == "number_of_days" ? query[i]+1 : query[i]}`
        //  }
        // this.props.handleGetSchedulingTime(queryString).then(x => {
        // 	if(blocked_dates && blocked_dates.length){
        // 		blocked_dates.map(y => {
        //  		for(var i in x){
        // 		if(i.slice(-2) == y)
        // 		delete x[i]
        // 	}
        // 		})
        // 	}
        // })
        this.setState({ showCalender: true }); //, enableDate: Object.keys(x), scheduleTimeList: x
    };

    onChangeDate = (date, query, bucket_type) => {
        let changeddate = new Date(date);
        let bucketSchedule = this.state.bucketSchedule ? this.state.bucketSchedule : {};
        let year = changeddate.getFullYear(),
            month = changeddate.getMonth() + 1,
            newdate = changeddate.getDate();
        let key = `${year}-${month >= 10 ? month : "0" + month}-${newdate >= 10 ? newdate : "0" + newdate}`;
        bucketSchedule[bucket_type] = bucketSchedule[bucket_type] ? bucketSchedule[bucket_type] : {};
        bucketSchedule[bucket_type]["currentSchedule"] = this.props.schedule_time_unit[bucket_type][key]; //this.state.scheduleTimeList[key]
        bucketSchedule[bucket_type]["showTime"] = true;
        this.setState({ showScheduleTime: true, showCalender: false, bucketSchedule: bucketSchedule, scheduleTimeBucket: bucket_type });
    };

    selectScheduleTime = scheduleTime => {
        debugger;
        let changeddate = new Date(scheduleTime);
        let bucketSchedule = this.state.bucketSchedule ? this.state.bucketSchedule : {};
        bucketSchedule[scheduleTime.bucket_type]["showTime"] = false;
        bucketSchedule[scheduleTime.bucket_type].scheduleTime = scheduleTime;
        this.props.setScheduleTime(bucketSchedule);
        this.setState({ bucketSchedule: bucketSchedule });
    };

    dialogCancelButton = () => {
        this.setState({ showScheduleTime: false, showCalender: false, scheduleTimeList: "" });
    };

    handleGetAvailability = json => {
        if (!this.props.isAuthed) {
            return this.props.history.replace("/login");
        }
        this.setState({ updatedMedicine: json.dv_id });
        this.props.handleGetAvailability(json, "check_avail");
    };

    render() {
        let quantityOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        let cartList = [];
        if (this.props.itemList && Object.keys(this.props.itemList).length && this.props.parentRef == "Cart") {
            let itemList = this.props.itemList;
            for (let i in itemList) {
                // debugger
                if (itemList[i].items && itemList[i].items.length) {
                    let bucket_type = itemList[i].items[0].availInfo.delivery_bucket_type;
                    // let text = `${getFormatedTime(itemList[i].items[0].availInfo.timeslot_format.hour)} - ${getFormatedTime(itemList[i].items[0].availInfo.timeslot_format.hour+1)}`
                    let timeList = "";
                    if (
                        this.state.showScheduleTime &&
                        this.state.bucketSchedule[this.state.scheduleTimeBucket] &&
                        this.state.bucketSchedule[this.state.scheduleTimeBucket].currentSchedule &&
                        this.state.bucketSchedule[this.state.scheduleTimeBucket].showTime
                    ) {
                        timeList = (
                            <div>
                                <div className={style.time_slot_header}>Select Schedule Time</div>
                                <ul className={style.time_slot}>
                                    {this.state.bucketSchedule[this.state.scheduleTimeBucket].currentSchedule.map((x, index) => {
                                        x["bucket_type"] = this.state.scheduleTimeBucket;
                                        return (
                                            <li key={index} onClick={() => this.selectScheduleTime(x)}>
                                                {x.slot_label}
                                            </li>
                                        );
                                    })}
                                </ul>
                            </div>
                        );
                    }
                    let date = new Date(),
                        key_list,
                        key,
                        key_date;
                    let key_schedule = this.props.schedule_time_unit[i];
                    key_list = key_schedule ? Object.keys(key_schedule) : "";
                    if (this.state.bucketSchedule && this.state.bucketSchedule[bucket_type] && this.state.bucketSchedule[bucket_type].scheduleTime) {
                        date = new Date(this.state.bucketSchedule[bucket_type].scheduleTime.scheduled_timestamp * 1000);
                        key = this.state.bucketSchedule[bucket_type].scheduleTime;
                        // debugger
                    } else {
                        key = key_list && key_list.length ? key_schedule[key_list[0]][0] : "";
                        key_date = key ? new Date(key.scheduled_timestamp * 1000) : "";
                    }
                    // console.log("schedule", key_date, key_list, key)
                    let itm = itemList[i].items.map((item, index) => {
                        // debugger
                        return (
                            <div key={index}>
                                <ItemCard
                                    isFetchingAvailability={this.props.isFetchingAvailability}
                                    updatedMedicine={this.state.updatedMedicine}
                                    parentRef={this.props.parentRef}
                                    imageView={this.props.imageView}
                                    manufacturerName={item.medicine.manufacturer_name}
                                    medicineName={item.medicine.formatted_name}
                                    packingQuantity={item.medicine.packing_quantity}
                                    isFetching={this.props.isFetching}
                                    handleAddItem={this.handleAddItem}
                                    handleRemoveItem={this.handleRemoveItem}
                                    handleQtyChange={this.handleQtyChange}
                                    // handlePacketTypeChange = {this.handlePacketTypeChange}
                                    packetTypeChange={this.state.packetTypeChange}
                                    cartItems={this.state.cartItems}
                                    item={item.medicine}
                                    only_medicine_name={this.props.only_medicine_name}
                                    handleGetAvailability={this.handleGetAvailability}
                                />
                            </div>
                        );
                    });
                    cartList.push(itm);
                }
            }
        }
        return (
            <div className="full_width p-15">
                <ul>
                    {/* List Items */}
                    {this.props.itemList && this.props.parentRef != "Cart"
                        ? this.props.itemList.map((item, index) => {
                              return (
                                  <div key={index}>
                                      <ItemCard
                                          isFetchingAvailability={this.props.isFetchingAvailability}
                                          updatedMedicine={this.state.updatedMedicine}
                                          parentRef={this.props.parentRef}
                                          imageView={this.props.imageView}
                                          manufacturerName={item.manufacturer_name}
                                          medicineName={item.formatted_name}
                                          packingQuantity={item.packing_quantity}
                                          isFetching={this.props.isFetching}
                                          handleAddItem={this.handleAddItem}
                                          handleRemoveItem={this.handleRemoveItem}
                                          handleQtyChange={this.handleQtyChange}
                                          // handlePacketTypeChange = {this.handlePacketTypeChange}
                                          packetTypeChange={this.state.packetTypeChange}
                                          cartItems={this.state.cartItems}
                                          item={item}
                                          only_medicine_name={this.props.only_medicine_name}
                                          handleGetAvailability={this.handleGetAvailability}
                                      />
                                      {item &&
                                          item.avail &&
                                          item.avail.similar_items &&
                                          item.avail.similar_items.items &&
                                          item.avail.similar_items.items.length && (
                                              <div className={style.substitute_container}>
                                                  <h3 className={style.sub_lsbel}>Similar Products Available Faster</h3>
                                                  {item.avail.similar_items.items.map((x, index) => {
                                                      return (
                                                          <div key={index}>
                                                              <ItemCard
                                                                  isFetchingAvailability={this.props.isFetchingAvailability}
                                                                  updatedMedicine={this.state.updatedMedicine}
                                                                  parentRef={this.props.parentRef}
                                                                  imageView={this.props.imageView}
                                                                  medicineName={x.formatted_name}
                                                                  packingQuantity={x.packing_quantity}
                                                                  isFetching={this.props.isFetching}
                                                                  handleAddItem={item => this.handleAddItem(item, "similar_items")}
                                                                  handleRemoveItem={this.handleRemoveItem}
                                                                  handleQtyChange={this.handleQtyChange}
                                                                  packetTypeChange={this.state.packetTypeChange}
                                                                  cartItems={this.state.cartItems}
                                                                  item={x}
                                                                  only_medicine_name={this.props.only_medicine_name}
                                                              />
                                                          </div>
                                                      );
                                                  })}
                                              </div>
                                          )}
                                  </div>
                              );
                          })
                        : cartList}

                    {/* Add extended notes*/}
                    {this.props.itemList && this.props.searchInput ? (
                        <li className={card_container}>
                            <div className={itemName}>{this.props.searchInput}</div>
                            <div className={extended}>
                                <div className={packing}>Receive a call back for item details</div>
                                <div className={cardActions}>
                                    {this.props.searchInput in this.state.extendedNotes ? (
                                        <div className={style.extendedTextRemove}>
                                            <img
                                                src={remove}
                                                alt="remove-icon"
                                                style={{ width: "24px", height: "24px" }}
                                                onClick={() => this.handleExtendedNotes(this.props.searchInput, "remove")}
                                            />
                                            <div className={selectBoxWraper}>
                                                <SelectBox
                                                    noSelect={true}
                                                    currentSelected={this.state.extendedNotes[this.props.searchInput]}
                                                    optionList={quantityOptions}
                                                    handleChangeOption={this.handleExtendedNotes.bind(this, this.props.searchInput, "update")}
                                                />
                                            </div>
                                        </div>
                                    ) : (
                                        <div className={addBtn} onClick={() => this.handleExtendedNotes(this.props.searchInput, "add")}>
                                            ADD
                                        </div>
                                    )}
                                </div>
                            </div>
                        </li>
                    ) : (
                        ""
                    )}

                    {this.props.cartExtendedNotes ? (
                        <span>
                            {this.props.cartExtendedNotes.map(ext => {
                                return (
                                    <li className={card_container} key={ext.item_text}>
                                        <div className={itemName}>{ext.item_text}</div>
                                        <div className={extended}>
                                            <div className={packing}>Receive call back for item details</div>
                                            <div className={cardActions}>
                                                {ext.item_text in this.state.extendedNotes ? (
                                                    <div>
                                                        <img
                                                            src={remove}
                                                            alt="remove-icon"
                                                            style={{ width: "24px", height: "24px" }}
                                                            onClick={() => this.handleExtendedNotes(ext.item_text, "remove")}
                                                        />
                                                        <div className={selectBoxWraper}>
                                                            <SelectBox
                                                                noSelect={true}
                                                                currentSelected={this.state.extendedNotes[ext.item_text]}
                                                                optionList={quantityOptions}
                                                                handleChangeOption={this.handleExtendedNotes.bind(this, ext.item_text, "update")}
                                                            />
                                                        </div>
                                                    </div>
                                                ) : (
                                                    <div className={addBtn} onClick={() => this.handleExtendedNotes(ext.item_text, "add")}>
                                                        ADD
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </li>
                                );
                            })}
                        </span>
                    ) : (
                        ""
                    )}
                </ul>

                {this.state.showQtyPrompt ? (
                    <div className={style.qtyPrompt}>
                        <div className={style.popup}>
                            <h4>Add Quantity</h4>
                            <input
                                className={style.qty_input}
                                type="number"
                                onChange={e => this.setState({ otherQtyValue: e.target.value })}
                                autoFocus
                            />
                            <div className={style.actionRow}>
                                <span onClick={() => this.setState({ showQtyPrompt: false })}>Cancel</span>
                                <span onClick={this.handleOtherQtyChange}>OK</span>
                            </div>
                        </div>
                    </div>
                ) : (
                    ""
                )}
            </div>
        );
    }
}

export default connect(
    ({ cart, users }) => ({
        userOnlineStatus: users.userOnlineStatus,
        isFetching: cart.isFetchingMedicine,
        isFetchingAvailability: cart.isFetchingAvailability,
        items: cart.items,
        cartBucketItem: cart.cartBucketItem,
        extendedNotesInCart: cart.extended_notes,
        schedule_time_unit: cart.schedule_time_unit,
    }),
    dispatch => bindActionCreators(cartActionCreater, dispatch)
)(ItemsListContainer);
