import React, { Component }            from 'react'
import { connect }                     from 'react-redux'
import { bindActionCreators }          from 'redux'
import {PreviousOrder}                 from '$COMPONENTS/PreviousOrder/PreviousOrder'
import * as paymentMethodActionCreater from '$REDUX/modules/paymentMethod'
import {Loader}                        from '$PARTIALS/Loader/Loader'
import {Input}                         from '$PARTIALS/Input/Input'
import {CheckBox}                      from '$PARTIALS/CheckBox/CheckBox'
import {formDataRequest}               from '$HELPERS/helperFunction'

 class PaymentSuccessContainer extends Component{
	static contextTypes = {
		//router: React.PropTypes.object.isRequired,,
	}

	constructor(props) {
	  super(props);

	  this.state = {paytmResponse: props.paytmResponse};
	}

	callPaytm = () => {
		if(this.state.paytmResponse != null && this.state.paytmResponse != ''){
			debugger
			formDataRequest("https://pguat.paytm.com/oltp-web/processTransaction", this.state.paytmResponse, "post")
		} else {

		}
	}

	render() {
		this.callPaytm()

		return (
			<div className = "container">

			</div>
		)
	}
}

export default connect(
	({paymentMethod}) => ({
		isFetching    : paymentMethod.isFetching,
		paytmResponse : paymentMethod.paytmResponse,
}),
	(dispatch) => bindActionCreators({
		...paymentMethodActionCreater,
	}, dispatch)
)(PaymentSuccessContainer)
