import React, { Component}             from 'react';
import { bindActionCreators }          from 'redux'
import { connect }                     from 'react-redux'
import * as paymentMethodActionCreater from '$REDUX/modules/paymentMethod';
import { handleGetOrder }              from '$REDUX/modules/ordersPrescription'
import { trackOrder }                  from '$REDUX/modules/trackOrder'
import DirectPayment                   from '$COMPONENTS/PaymentMethod/DirectPayment'
import { formDataRequest }             from '$HELPERS/helperFunction'
import * as style                      from './style.scss'
import { orderNotExist }               from '$SHAREDSTYLES/icons'



class DirectPaymentContainer extends Component {

    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            paymentMethod: null,
            selectedPayment: null,
            errorMsg: null,
            orderNotFound: false
        }
    }

    componentDidMount(){
        console.log(this.props);
        // if(this.props.userOnlineStatus == "offline"){
            this.props.trackOrder(this.props.match.params.displayId)
            .then(res=> {
                if(!res.order){
                    this.setState({errorMsg: "This order does not exist", orderNotFound:true})
                    return
                }
                this.props.getPaymentList(this.props.match.params.displayId)
                .then(res => {
                    if(res.status.code == 400){
                        this.setState({errorMsg: "This order does not exist", orderNotFound:true})
                    }
                })
                .catch(err => console.log(err))
            })
            .catch((err) => console.log("error"))
    }

    handlePaymentMethod = (details) => {
        console.log(details);
        this.setState({paymentMethod: details['id'],
                       selectedPayment: details})
    }

    razorpayPayment = (id) => {
        this.props.razorpayPayment(id, this.props.match.params.displayId).then(res=> {
            if(res.code == 400 || res.code == 401 || res.code == 403 || res.code == 499){
                this.setState({errorMsg: res.message})
            }
        })
        .catch(error => {
            console.log('error', error)
            alert("error: "+error.message)
        })
    }

    updatePaymentMethod = (id) => {
        this.props.updatePaymentMethod(id, this.props.match.params.displayId).then(x=> {
            alert("You selected Cash on Delivery!")
            this.props.history.push('/prescription')
        })
    }

    paytmPayment = () => {
        let txnURL = "https://pguat.paytm.com/oltp-web/processTransaction"
        if(process.env.NODE_ENV == "production"){
            txnURL = "https://secure.paytm.in/oltp-web/processTransaction"
        }
        this.props.paytmPayment(this.props.match.params.displayId).then(x => {
            debugger
            if(x.code != 400){
                if(x.paytmResponse != null && x.paytmResponse != ''){
                    debugger
                    formDataRequest(txnURL, x.paytmResponse, "post")
                } else {

                }
            } else {
                this.setState({errorMsg: x.message})
            }
        })
    }

    handlePayNow = () => {
        if(this.state.orderNotFound){
            this.setState({errorMsg: "This order does not exist"})
            return;
        }
        if(this.state.paymentMethod && this.state.selectedPayment){
            let id = this.state.paymentMethod;

            switch (this.state.selectedPayment['gateway']) {
                case "instamojo":
                    window.location = this.state.selectedPayment['url']
                    break;
                case "paytm":
                    this.paytmPayment()
                    break;
                case "razorpay":
                    this.razorpayPayment(id)
                    break;
                case "cod":
                    this.updatePaymentMethod(id)
                    break;
                default:
                    this.razorpayPayment(id)
            }
        } else {
            this.setState({errorMsg: "Please select your payment option!"})
        }

    }

    handleCloseError = () => {
        this.setState({errorMsg: null})
    }

    render() {
        const methodFormattedName = {
            'wallet' : 'Wallets',
            'online' : 'Online Payment',
            'delivery': 'Pay on Delivery'
        }

        const disabledMethodIDs = [1,8] //disabled simpl payment

        let total_payable_amount;

        // if(this.props.userOnlineStatus == "offline"){
            total_payable_amount = this.props.unAuthOrder && this.props.unAuthOrder.amount_details ? this.props.unAuthOrder.amount_details.total_payable_amount : '---'
        // } else {
        //     total_payable_amount = this.props.currentOrder && this.props.currentOrder.total_payable_amount ? this.props.currentOrder.total_payable_amount : '---'
        // }
        if(this.state.orderNotFound){
            return(
                <div className="info_icon full_height">
                    <div>{orderNotExist}</div>
                    <div className="info_topic">order doesn’t</div>
                    <div className="info_sub_topic">Exist</div>
                </div>
            )
        }

        return (
            <div className = "blank_container">

                {this.props.unAuthOrder && Object.keys(this.props.unAuthOrder).length ?
                <div>
                <div className={style.total}>
                Total Payable: &#8377;{total_payable_amount}
                </div>
                <div className="fixed_btn_margin">

                {this.props.paymentMethodList && this.props.paymentMethodList.length ?
                    this.props.paymentMethodList[0]['options'].map((preferredMethod) => {
                        if(preferredMethod.is_user_preferred){
                        return(
                            <div key="preferred">
                                <div className="section_heading">
                                    <span className= "section_title">Preferred Payment</span>
                                </div>
                                <DirectPayment  methods={[preferredMethod]}
                                                paymentMethod={this.state.paymentMethod}
                                                handlePaymentMethod={(details) => this.handlePaymentMethod(details)} />
                            </div>
                            )}
                    })
                : ""}

                {this.props.paymentMethodList && this.props.paymentMethodList.length ?
                    this.props.paymentMethodList.map((method) => {
                        return(
                            <div key={method.type}>
                                <div className="section_heading">
                                    <span className= "section_title">{methodFormattedName[method.type]}</span>
                                </div>
                                <DirectPayment  methods={method.options}
                                                paymentMethod={this.state.paymentMethod}
                                                handlePaymentMethod={(details) => this.handlePaymentMethod(details)}
                                                disabledMethodIDs={disabledMethodIDs} />
                            </div>
                            )
                    })
                : ""}
                </div>
                <div className = "fixed_btn button fixed_btn_padding_8">
                    <div className="left">Total &#8377;{total_payable_amount}</div>
                    <div className="right" onClick={this.handlePayNow}>Pay Now</div>
                </div>

                {this.state.errorMsg ?
                    <div className={style.error_container}>
                        <div>{this.state.errorMsg}</div>
                        <div className={style.btn} onClick={this.handleCloseError}>Got it!</div>
                    </div>
                : ''
                }

                </div>
                :''}
            </div>
        );
    }
}

export default connect(
    ({users, paymentMethod, ordersPrescription, trackOrder }) => ({
        userOnlineStatus: users.userOnlineStatus,
        isFetching: paymentMethod.isFetching,
        paymentMethodList: paymentMethod.paymentMethodList,
        currentOrder: ordersPrescription.currentOrder,
        unAuthOrder: trackOrder.order
}),
    (dispatch) => bindActionCreators({
        ...paymentMethodActionCreater,
        handleGetOrder,
        trackOrder
    }, dispatch)
)(DirectPaymentContainer);
