import React, { Component }         from 'react';
import { Link }                     from 'react-router-dom'
import { bindActionCreators }       from 'redux'
import { connect }                  from 'react-redux'
import * as trackOrderActionCreater from '$REDUX/modules/trackOrder';
import BillInformation              from '$COMPONENTS/Cart/BillInformation'
import {formatedRupee}              from '$HELPERS/helperFunction'
import * as style                   from './style.scss'
import * as paymentMethodActionCreater from '$REDUX/modules/paymentMethod';
import Clevertap                    from '$HELPERS/clevertap'
import phonePe                      from '$HELPERS/phonepeFunction'
import Loader                       from '$PARTIALS/Loader/Loader'
import { orderNotExist, paidIcon }            from '$SHAREDSTYLES/icons'

class TrackOrderContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orderFetched: false,
            payBtnDisabled: false,
            showError : false,
            errorMessage: '',
            btnText: '',
            showLoader: false
        }
        props.trackOrder(this.props.match.params.displayId)
        this.payform = React.createRef();
        window.addEventListener('not_permitted',()=>{
            // alert("event catched");
            let url = window.location.pathname
            // alert(JSON.stringify(this.props.history))
            this.props.history.replace('/login')
            setTimeout(()=> {
                // alert("function call")
                // window.dispatchEvent(new CustomEvent("trackUrl",{detail:{trackUrl:"/sdjhfgs"}}))
                window.dispatchEvent(new CustomEvent("trackUrl",{detail:{trackUrl: url}}));
            },2000)
        })
    }

    componentDidMount(){
        
        console.log(this.props);
        this.checkOrderPaymentStatus();
        
    }

    handlePayNow = () => {
        this.setState({showLoader:true});
        Clevertap.event('mWeb - payNow', {
            source: 'Track Order',
            orderId: this.props.match.params.displayId
        })
   
        if(this.props.order.juspay_payment_enabled){
            this.juspayPayment()
        }else{
            this.props.history.push('/payments/' + this.props.match.params.displayId)
        }
    

    }

    checkOrderPaymentStatus(){
        let param = new URLSearchParams(window.location.search);
        let urlparam = '';
        if(param.has('status')){
            urlparam = param.get('status');
            console.log(urlparam);
        }
        this.props.checkPayStatus(this.props.match.params.displayId).then((result) => {
        if(result.payment_status == "PENDING" || result.payment_status == "SUCCESS" || urlparam == "PENDING" || urlparam == "SUCCESS"){
            this.setState({payBtnDisabled:true});
            if(result.payment_status == "SUCCESS" || urlparam == "SUCCESS")
                this.setState({showError : false, errorMessage : 'Thanks for completing the payment!!',btnText: 'OK'})
            else{
                this.setState({showError : true, errorMessage : 'We are processing the request. Please check status after few mins',btnText: 'OK'})
            }
        }else if(result.payment_status == "FAILED" && urlparam == "FAILED")
                this.setState({showError : true, errorMessage : 'Payment Failed. Please Try Again',btnText: 'OK'})
        }).catch((res)=>{
            this.setState({showError : true, errorMessage : 'We are experiencing some problem. Please check status after few mins',btnText: 'OK',payBtnDisabled:true})

        })
    }

    handleCloseError = () => {
        this.setState({showError: false})
    }

    juspayPayment = () => {
        this.props.juspayPayment("juspay", this.props.match.params.displayId).then((res)=> {
          if(res.code == 400 || res.code == 401 || res.code == 403 || res.code == 499){
              this.setState({errorMsg: res.message})
          }
          this.setState({showLoader:false});
          window.open(res.url,"_self");

        })
        .catch(error => {
            console.log('error', error)
            alert("error: "+error.message)
        })
    }


    render() {
        let { order } = this.props

        let deliveryCharge = 0
        if(order && order.amount_details && order.amount_details.extra_charges && order.amount_details.extra_charges.length && order.amount_details.extra_charges[0].amount != 0){
            deliveryCharge = formatedRupee(order.amount_details.extra_charges[0].amount)
        }

        if(!this.props.isFetching && !(this.props.timeline && this.props.timeline.orderTimelineItems)){
            return(
                <div className="info_icon full_height">
                    <div>{orderNotExist}</div>
                    <div className="info_topic">order doesn’t</div>
                    <div className="info_sub_topic">Exist</div>
                </div>
            )
        }

        return (
            <div className= "blank_container">
                {this.props.isFetching || this.state.showLoader ? (<Loader/>) : null}
                {order && order.eta_info && Object.keys(order.eta_info).length ?
                <div className="section_heading">
                    <span className="section_title">{order.eta_info.formatted_eta ? order.eta_info.formatted_eta.toString() : ''} </span><br/>
                    <small>{order.eta_info.eta_description ? order.eta_info.eta_description : ''}</small>
                </div>
                : ''}

                    {this.props.timeline && this.props.timeline.orderTimelineItems ?
                        <div className={style.order_timeline}>
                            <ul>
                                {this.props.timeline.orderTimelineItems.map((key, index) => {
                                    return(
                                        <li key={`${key.status}-${index}`}>
                                        {key.status == 'NOW' ?
                                            <div>
                                                <span className={`${style.status} ${style.focus}`}>{key.status}</span>
                                                <span className={`${style.description} ${style.focus_txt}`}>{key.description}</span>
                                            </div>
                                        :
                                            <div>
                                                <span className={style.status}>{key.status}</span>
                                                <span className={style.description}>{key.description}</span>
                                            </div>
                                        }
                                        </li>
                                        )
                                })}
                            </ul>
                        </div>
                    : '' }

                {order && order.display_id ?
                <div>
                    <div className="section_heading">
                        <span className="section_title">View Order Details</span>
                    </div>
                    <div >
                        <ul className={style.order_detail_list}>
                            <li>
                                <span>Order ID &#183; </span>
                                <span> &nbsp;{order.display_id}</span>
                            </li>
                            <li>
                                <span>Estimated &#183;</span>
                                <span>&nbsp; &#8377;{order && order.amount_details ? formatedRupee(order.amount_details.total_payable_amount): '---'}</span>
                            </li>
                        </ul>
                    </div>
                </div>
                : this.props.isFetching ? "" :<div className="section_heading field_label">Uh Oh! The content is no longer available, <Link to='/prescription' className="green_text">let's start over</Link>.</div>}


                {/* Items Added */}
                {order && (order.available_items || order.out_of_stock_items ) && Object.keys(order).length ?
                <div>
                    <div className="section_heading">
                        <span className="section_title">Items Added</span>
                    </div>
                    <div >
                        <div className="box_container">
                        <ul className={style.items_added}>
                            {order.available_items && order.available_items.length ?
                                order.available_items.map((key) => {
                                return(
                                    <li key={key.id}>
                                        <span >{key.medicine.formatted_name}</span>
                                        <span className="right">
                                            {key[key.packet_type_display_info.show_quantity_type]}&nbsp;
                                            {key[key.packet_type_display_info.show_quantity_type] <= 1 ?
                                            key.medicine.allowed_quantity_types[key.packet_type_display_info.show_quantity_type_index].label
                                            :
                                            key.medicine.allowed_quantity_types[key.packet_type_display_info.show_quantity_type_index].label_pluralized
                                            }
                                        </span>
                                    </li>
                                    )
                                })
                            : ''}
                            {order.out_of_stock_items && order.out_of_stock_items.length ?
                                order.out_of_stock_items.map((key) => {
                                return(
                                    <li key={`${key.medicine.formatted_name}${key.id}`} style={{'color': '#C8C8C8'}}>
                                        <span >{key.medicine.formatted_name}</span>
                                        <span className="right">
                                            Out of Stock
                                        </span>
                                    </li>
                                    )
                                })
                            : ''}
                        </ul>
                        </div>
                    </div>
                </div>
                : ''}

                {/* Delivery Address */}
                {order && order.delivery_location  && Object.keys(order.delivery_location).length ?
                    <div>
                        <div className="section_heading">
                            <span className="section_title">Delivery Address </span>
                        </div>
                        <div className="box_container">
                            {order && order.delivery_location ? order.delivery_location.address : null}
                        </div>
                    </div>
                : ''}

                {/* Bill */}
                {order  && Object.keys(order).length ?
                    <div>
                        <div className="section_heading">
                            <span className="section_title">Billing Information</span>
                        </div>

                        <BillInformation
                            wallateInfo    = {order.amount_details}
                            name           = {order.billing_location ? order.billing_location.name : order.delivery_location.name}
                            address        = {order.billing_location ? order.billing_location.address : order.delivery_location.address}
                            totalMRP       = {order && order.amount_details ? formatedRupee(order.amount_details.total_mrp) : '---'}
                            totalDiscount  = {order && order.amount_details ? formatedRupee(order.amount_details.total_discount) : '---'}
                            totalAmount    = {order && order.amount_details ? formatedRupee(order.amount_details.total_payable_amount) : '---'}
                            deliveryCharge = {deliveryCharge}
                            editNotAllowed = {true}
                        />
                    </div>
                : '' }


                {order && order.is_price_confirmed && (order.payment_status == "FAILED" || order.payment_status == null || order.payment_status == "not_paid" ) && !this.state.payBtnDisabled ?
                <div className="fixed_btn button fixed_btn_padding_8">
                    <div className="left">Total &#8377;{order && order.amount_details ? order.amount_details.total_payable_amount : '---'}</div>
                    <div className="right" type="submit" hidden={this.state.payBtnDisabled} onClick={this.handlePayNow}>Pay Now</div>
                </div>
                : (order && order.is_price_confirmed && (order.payment_status == "SUCCESS" || order.payment_status == "paid" )?
                <div className="fixed_btn button fixed_btn_padding_8">
                    <div className="left">Total &#8377;{order && order.amount_details ? order.amount_details.wallet_credits : '---'}</div>
                    <div className={style.paid}>
                        <span style={{verticalAlign:'sub'}}>{paidIcon}</span> <span style={{verticalAlign:'super'}}>Paid</span>
                    </div>
                </div> : "")
                 }

                {
                    this.state.showError ?
                    <div className={style.error_container}>
                        <div>{this.state.errorMessage}</div>
                        <div className={style.btn} onClick={this.handleCloseError}>{this.state.btnText}</div>
                    </div> : null
                }

            </div>
        );
    }
}

export default connect(
    ({ trackOrder }) => ({
        isFetching : trackOrder.isFetching,
        order      : trackOrder.order,
        timeline   : trackOrder.timeline
}),
    (dispatch) => bindActionCreators({
        ...trackOrderActionCreater,
        ...paymentMethodActionCreater
    }, dispatch)
)(TrackOrderContainer);

