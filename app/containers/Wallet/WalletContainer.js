import React, {Component} from 'react';
import * as style from './style.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActionCreators from '$REDUX/modules/users'
import AddWalletAmount from '../../components/AddWalletAmount/AddWalletAmount';
import AllTransactions from '../../components/AllTransactions/AllTransactions';
import Loader from '$PARTIALS/Loader/Loader'

class WalletContainer extends Component{
    constructor(props){
        super(props);
        this.state = {
            curSelTabIndex : 1,
            isLoading : false,
            pastTransactions: []
        }
    }
    componentDidMount = () => {
    
    }

    handleTabClick = (type) => {
       if(type == "allTranx"){
           document.querySelector('#addMoneyTab').classList.remove(style.active_tab)
           document.querySelector('#allTraxTab').classList.add(style.active_tab)
           this.setState({curSelTabIndex:2})
       }else{
           document.querySelector('#allTraxTab').classList.remove(style.active_tab)
           document.querySelector('#addMoneyTab').classList.add(style.active_tab)
           this.setState({curSelTabIndex:1})
       }
    }

    handleUpdateWallet = (amount) => {
        this.setState({isLoading: true})
        this.props.userWalletUpdate({amount : amount}).then((resp)=>{
            // console.log("res",resp)
            if(resp.status.code == 200){
                window.location.href = resp.payment_details.request_details.payment_links.web
            }
            this.setState({isLoading: false})
        }).catch((error)=>{
            console.log("inside error")
            this.setState({isLoading: false})
        })
    }

    getPastTrans = () => {
        this.setState({isLoading: true})
        this.props.getPastTransactions().then((resp)=>{
            this.setState({pastTransactions:resp,isLoading:false})
        })
    }

    render = () => {
        // document.querySelector('.main_centered_container').style = {marginTop : '0px'}
        return (
            <div>
                {this.state.isLoading ? <Loader/>: null}
                <ul className={style.tab_list}>
                    <li className={`col-6 center_text ${style.tab_button} ${style.active_tab}`} id="addMoneyTab" onClick={()=>this.handleTabClick("allMoney")}><span>ADD MONEY</span></li>
                    <li className={`col-6 center_text ${style.tab_button}`} id="allTraxTab" onClick={()=>this.handleTabClick("allTranx")}><span>ALL TRANSACTION</span></li>
                </ul>

                <div className={style.content_box}>
                    {
                        this.state.curSelTabIndex == 1 ?
                            <AddWalletAmount userCurrentWallet={this.props.userInfo.wallet_credits} updateUserWallet= {this.handleUpdateWallet}/>:<AllTransactions fetchUserTransactions={this.getPastTrans} userTransactions = {this.state.pastTransactions}/>
                    }
                    
                </div>
            </div>
        );
    }
}

export default connect(
	({users}) => ({isAuthed: users.isAuthed, otp: users.otp, isFetching: users.isFetching, userInfo: users.userInfo }),
	(dispatch) => bindActionCreators({...userActionCreators}, dispatch)
)(WalletContainer)
