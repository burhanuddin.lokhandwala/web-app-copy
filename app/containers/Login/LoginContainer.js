import React, {Component} from 'react'
import Login from '../../components/Login/Login'
// import {LoginForm} from '../../components'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActionCreators from '../../redux/modules/users'
import {handleUpdateMedicineCount, setAndHandleCartOrder, handleUpdateItemList} from '../../redux/modules/cart'
import { isRedirectRequired, getLocalstorageValue} from '../../helpers/helperFunction'

import Clevertap from '../../helpers/clevertap'

class LoginContainer extends Component{
	// propTypes: {
	// 	isFetching: PropTypes.bool.isRequired,
	// 	error: PropTypes.string.isRequired,
	// 	fetchAndHandleAuthedUser: PropTypes.func.isRequired,
	// 	getOTP: PropTypes.func.isRequired,
	// },
	constructor(props){
		super(props)
		this.state = {
			trackUrl:""
		}
		window.addEventListener("trackUrl",(event)=>{
			// this.setState({trackUrl:event.detail.trackUrl})
			// alert("event catched")
			// alert("text"+event.detail.trackUrl);
			this.setState({trackUrl:event.detail.trackUrl})
		})
	}

	static contextTypes = {
		//router: React.PropTypes.object.isRequired,,
	}

	

	componentDidMount() {
	    Clevertap.event('mWeb - Page visit', {
			'Page title': 'Login'
		})

		

		// let location = this.props.history.location
		// let action = this.props.history.action

		// this.props.history.listen((location, action) => {
		// 	alert(JSON.stringify(location)+JSON.stringify(action))
		// });
	}

	handleOTP = (e) => {
		e.preventDefault()
		this.props.getOTP()
			// .then(() => {
			// 	debugger
			// }).catch(() => {
			// 	alert('error')
			// })

	}

	handleUpdatePhone = (e) => {
		e.preventDefault()
		this.props.updatePhone(e.target.value)
	}

	handleUpdateOTP = (e) => {
		e.preventDefault()
		this.props.updateOTP(e.target.value)
	}

	handleAuth = (e) => {
		e.preventDefault()
		this.props.fetchAndHandleAuthedUser(this.props.phone, this.props.otpValue)
			.then((responce) => {
				if(responce){
					//cart item updating
					let itemList = getLocalstorageValue("userItems")
					let userItemsList = getLocalstorageValue("userItemsList")
					itemList && itemList.length ? this.props.handleUpdateMedicineCount(itemList, "addItems") : ""
					userItemsList && userItemsList.length ? this.props.handleUpdateItemList(userItemsList) : ""
					Clevertap.profile({"Site": {Identity: this.props.userId}})
					//**checking if no location go to location page else go to previous page

					this.props.handleGetUserLocations().then((data) => {
	        	(data && data.locations && data.locations.length > 0 ) ? this.props.setAndHandleCartOrder() : ""
						let redirectURL = isRedirectRequired()
						if(redirectURL){
							console.log(redirectURL)
							this.props.history.replace(redirectURL)
							return;
						}

						// alert(window.history.length);
						// this.props.history.replace('prescription')
						(data && data.locations && data.locations.length > 0 ) ? (window.history.length > 1 ? history.back() :
						this.state.trackUrl ? this.props.history.push(this.state.trackUrl):this.props.history.replace('prescription')) : this.props.history.replace('search_location')
					})
				}
			})
			.catch(err => {
				console.log('Login Failed')
			})

	}
	render() {
		return (
				<Login
					onAuth={this.props.otp ? this.handleAuth : this.handleOTP}
					handleOTP = {this.handleOTP}
					isFetching={this.props.isFetching}
					error={this.props.error}
					otp={this.props.otp}
					updateInput = {this.props.otp ? this.handleUpdateOTP : this.handleUpdatePhone}
					callbackCount = {this.props.callbackCount}
					counterHandler = {this.props.counterHandler}
					isResetOPTFlag = {this.props.isResetOPTFlag}
					onCallMeNow = {this.props.onCallMeNow}
				/>
		);
	}
}

export default connect(
	({users}) => ({
		isFetching: users.isFetching,
		phone: users.phone,
		otpValue: users.otpValue,
		error: users.error,
		otp: users.otp,
		callbackCount: users.callbackCount,
		counterHandler: users.counterHandler,
		isResetOPTFlag: users.isResetOPTFlag,
		userId: users.userInfo.id,
		// isLocationFlag: users.isLocationFlag,
	}),
	(dispatch) => bindActionCreators({
		...userActionCreators,
		handleUpdateMedicineCount,
		setAndHandleCartOrder,
		handleUpdateItemList,
	}, dispatch)
	)(LoginContainer)
