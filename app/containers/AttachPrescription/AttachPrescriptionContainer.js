import React, { Component}                  from 'react'
import { bindActionCreators }               from 'redux'
import { connect }                          from 'react-redux'
import PhotoAdder                           from '$COMPONENTS/PhotoAdder/PhotoAdder'
import OverLayPopover                       from '$PARTIALS/OverLayPopover/OverLayPopover'
import Loader                               from '$PARTIALS/Loader/Loader'
import {setImgUrlFromFiles, getOrientation} from '$HELPERS/helperFunction'
import {updateImageElement}                 from '$REDUX/modules/imageSelector'
import * as style                           from './style.scss'
import {prescriptionIcon}                   from '$SHAREDSTYLES/icons'
import * as prescriptionActionCreater       from '$REDUX/modules/attachPrescription'

class AttachPrescriptionContainer extends Component{
	constructor(props) {
	  super(props);

	  this.state = {
			updateTimer: '',
			showImageList: '',
	  };

	}

	componentDidMount() {
		let params = this.props.match.params.access_token ? this.props.match.params.access_token : true
		let token = params ? params : ""
		token ? this.props.updateToken(token) : alert("Not getting token!!")
	}

	showImage = (showImageList) => {
		this.setState({showImageList: showImageList})
	}

	cancelButton = () => {
		this.setState({showImageList: ''})
	}

	getImage = (e) => {
		let rotate = ""
		if(e.target){
			let file = e.target
			getOrientation(file.files[0], function(orientation) {
								rotate = orientation
							})
			setTimeout(() => {
				this.props.updateImageElement({
																				image_value: file,
																				image_element: setImgUrlFromFiles(file),
																				orientation_flag: rotate,
																				disableDrower: true
																			})
				this.props.history.push('/image_prescription')
			}, 500)
		}
	}

	backToSuccess = () => {
		this.props.history.push('/prescription')
	}

	handleDoneButton = () => {
		if(!this.props.imageList.length){
			alert("Please upload valid image!!")
			return
		}
		this.props.handleDoneButton(this.backToSuccess)
	}

	render() {
	// console.log("url",this.props)
		let imageList = this.props.imageList && this.props.imageList.length ? this.props.imageList : []
		return (
		    <div className ="full_width">
		    {this.props.isFetching ? <Loader/> : null}
			    <div className ={style.prescription_header_container}>
			    	{prescriptionIcon}
			    	<div className ={style.prescription_header}>Upload Prescription</div>
			    	<span className = {style.prescription_text} >
			    		Please upload a clear photo of a valid and recent prescription.
			    	</span>
			    </div>
					{this.state.showImageList
						? (<OverLayPopover
						   	showImageList = {this.state.showImageList}
						   	imageView = {true}
						   	cancelButton = {this.cancelButton}/>)
						: null}
					<PhotoAdder
						imageList = {imageList}
						getImage = {this.getImage}
						showImage = {this.showImage}
						noDeleteButton = {true}
						centeredImage = {true}
						showUploadBtn= {true}
					/>
		    	<div className ={style.prescrib_done_button}>
		    		<button onClick = {this.handleDoneButton} className = "green_button button">Submit</button>
					</div>
				</div>
		);
	}
}

export default connect(
	({attachPrescription}) => ({
		isFetching : attachPrescription.isFetching,
		imageList  : attachPrescription.imageList,
	}),
	(dispatch) => bindActionCreators({
		...prescriptionActionCreater,
		updateImageElement,
	}, dispatch)
)(AttachPrescriptionContainer)