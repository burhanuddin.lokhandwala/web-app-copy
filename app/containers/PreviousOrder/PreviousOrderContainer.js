import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PreviousOrder from "$COMPONENTS/PreviousOrder/PreviousOrder";
import * as previousOrderActionCreater from "$REDUX/modules/previousOrder";
import { handleReorderFlag } from "$REDUX/modules/ordersPrescription";
import { fetchingCartOrderSuccess } from "$REDUX/modules/cart";
import Loader from "$PARTIALS/Loader/Loader";

class PreviousOrderContainer extends Component {
    // 	propTypes: {
    // 	error: PropTypes.string.isRequired,
    // 	// previousOrderList: PropTypes.array,
    // },
    componentDidMount = () => {
        document.body.scrollTop = 0;
        this.props.getAndHandlePreviousOrder();
        document.addEventListener("scroll", this.scrollPage, false);
    };

    scrollPage = e => {
        let scrollSize = document.body.offsetHeight - window.innerHeight - 500,
            scrolling = window.scrollY;
        if (scrollSize <= scrolling && !this.props.isFetching) {
            this.props.getAndHandlePreviousOrder("url");
        }
    };

    componentWillUnmount() {
        document.removeEventListener("scroll", this.scrollPage, false);
    }

    handleRorderItems = orderId => {
        this.props.handleRorderItems({ id: orderId }).then(x => {
            if (x && x.order) {
                let orders = x.order;
                this.props.history.push("/cart");
            }
        });
    };

    handleViewItem = () => {
        this.props.handleReorderFlag(true);
    };

    render() {
        return (
            <div id="previous-order-container" className="p-15 container">
                {!this.props.isFetching && !!this.props.previousOrderList ? (
                    this.props.previousOrderList.length ? (
                        <React.Fragment>
                            <h3 className="sec-header m-b-0">Your Orders</h3>
                            {this.props.previousOrderList.map((item, index) => {
                                return (
                                    <PreviousOrder
                                        key={index}
                                        orderItem={item}
                                        handleRorderItems={
                                            this.handleRorderItems
                                        }
                                        handleViewItem={this.handleViewItem}
                                    />
                                );
                            })}
                        </React.Fragment>
                    ) : (
                        <div className="field_label">
                            {" "}
                            Seems you did not order before. Pleasure to serve
                            you now!!
                        </div>
                    )
                ) : (
                    <Loader />
                )}
            </div>
        );
    }
}

export default connect(
    ({ previousOrder }) => ({
        isFetching: previousOrder.isFetching,
        error: previousOrder.error,
        previousOrderList: previousOrder.previousOrderList,
    }),
    dispatch =>
        bindActionCreators(
            {
                ...previousOrderActionCreater,
                handleReorderFlag,
                fetchingCartOrderSuccess,
            },
            dispatch
        )
)(PreviousOrderContainer);
