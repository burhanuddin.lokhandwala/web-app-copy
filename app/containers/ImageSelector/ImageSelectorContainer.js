import React, {Component}                   from 'react'
import { connect }                          from 'react-redux'
import { bindActionCreators }               from 'redux'
import ImageSelector                        from '$COMPONENTS/ImageSelector/ImageSelector'
import AddTagImageView                      from '$COMPONENTS/ImageSelector/AddTagImageView'
import * as imageSelectorActionCreater      from '$REDUX/modules/imageSelector'
import {setImgUrlFromFiles, getOrientation} from '$HELPERS/helperFunction'
import {handleUpdatePrescriptions}          from '$REDUX/modules/attachPrescription'
import {updateImageList}                    from '$REDUX/modules/cart'
import Loader                               from '$PARTIALS/Loader/Loader'
import Clevertap                            from '$HELPERS/clevertap'

 class ImageSelectorContainer extends Component{

 // 	propTypes: {
	// 	is_show_action_dower: PropTypes.bool,
	// 	is_show_tag_view: PropTypes.bool.isRequired,
	// 	markersInfo: PropTypes.array,
	// 	updateMarkersInfo: PropTypes.func.isRequired,
	// 	updateImageElement: PropTypes.func.isRequired,
	// 	showImageTagView: PropTypes.func.isRequired,
	// },

	static contextTypes = {
		//router: React.PropTypes.object.isRequired,
	}

	componentWillMount() {
		if(!this.props.image_element ){
			this.props.history.replace('cart')
			return null
		}

	}

	componentDidMount() {
	    Clevertap.event('mWeb - prescription photo')
	}

	completeUploadImage = (imgList) => {
		this.props.updateImageList(imgList)
		this.props.history.replace('cart')
	}

	getImage = (e) => {
		let rotate = ""
		if(e.target){
			let file = e.target
			getOrientation(file.files[0], function(orientation) {
								rotate = orientation
							})
			setTimeout(() => {
				this.props.updateImageElement({image_value: file, image_element: setImgUrlFromFiles(file)})
			}, 500)

		}
	}

	updateMarkersInfo = (cordinates, sentCordinate) => {
		let markerArray = this.props.markersInfo.slice(0)
		let markerArrayToSend = this.props.markersToSent.slice(0)
		markerArrayToSend.push(sentCordinate)
		markerArray.push(cordinates)
		this.props.updateMarkersInfo(markerArray, markerArrayToSend)
	}

	onQuantityChange =  (changedText, index)  => {
		let markerArray = this.props.markersInfo.slice(0)
		let markerArrayToSend = this.props.markersToSent.slice(0)
		markerArray[index].text = changedText
		markerArrayToSend[index].text = changedText
		this.props.updateMarkersInfo(markerArray, markerArrayToSend)
	}

	onRemoveTag =  (index)  => {
		let markerArray = this.props.markersInfo.slice(0)
		let markerArrayToSend = this.props.markersToSent.slice(0)
		markerArray.splice(index, 1)
		markerArrayToSend.splice(index, 1)
		this.props.updateMarkersInfo(markerArray, markerArrayToSend)
	}

	handleRemoveAllTag = () => {
		let markerArray = []
		let markerArrayToSend = []
		this.props.updateMarkersInfo(markerArray, markerArrayToSend)
	}

	showImageTagView = () => {
		Clevertap.event('mWeb - Photo - delivery type', {
			type: 'Let me select from the prescription'
		})
		this.props.handleActionDrower()
		this.props.showImageTagView(true)
	}

	handleNavigate = () => {
		this.handleRemoveAllTag()
		this.props.showImageTagView(false)
	}

	handleDeliverAllItems = () => {
		Clevertap.event('mWeb - Photo - delivery type', {
			type: 'Deliver all items'
		})
		this.props.handleUploadImage({all_items: 1}, this.completeUploadImage)
	}

	handleCallBackUser = () => {
		Clevertap.event('mWeb - Photo - delivery type', {
			type: 'Request call from Pharmacist'
		})
		this.props.handleUploadImage({call_back_user: 1}, this.completeUploadImage)
	}

	handleMarkarsDoneButton = () => {
		this.props.handleUploadImage({markers: JSON.stringify(this.props.markersToSent)}, this.completeUploadImage)
	}

	/**
	 * [handleUpdateImage for attach prescription page direct hit api and redirect to attach prescription]
	 * @return {[type]} [description]
	 */
	handleUpdateImage = () => {
		this.props.handleUpdatePrescriptions(this.props.image_value)
	}

	render() {

		let img = this.props.image_element
		let imgs = img ? img.cloneNode() : ''
		return (
			<div style = {{float: 'left',width: '100%',overflow: 'hidden',position: 'relative'}}>
					{(this.props.isFetching || this.props.isImageFetching) ? (<Loader/>) : null}
					<ImageSelector
						img = {img}
						getImage = {this.getImage}
						handleActionDrower = {this.props.token ? this.handleUpdateImage : this.props.handleActionDrower}
						is_show_action_dower= {this.props.is_show_action_dower}
						showImageTagView = {this.showImageTagView}
						handleDeliverAllItems = {this.handleDeliverAllItems}
						handleCallBackUser= {this.handleCallBackUser}
						orientation_flag = {this.props.orientation_flag}
						noDrower = {this.props.token ? true : false}
					 />
					<AddTagImageView
						img = {imgs}
						markersInfo = {this.props.markersInfo}
						onQuantityChange = {this.onQuantityChange}
						updateMarkersInfo = {this.updateMarkersInfo}
						onRemoveTag = {this.onRemoveTag}
						handleRemoveAllTag = {this.handleRemoveAllTag}
						handleNavigate = {this.handleNavigate}
						handleMarkarsDoneButton = {this.handleMarkarsDoneButton}
						orientation_flag = {this.props.orientation_flag}
						is_show_tag_view = {this.props.is_show_tag_view}
					 />
			</div>
		)
	}
}

export default connect(
	({imageSelector, attachPrescription, }) => ({
		isFetching           : imageSelector.isFetching,
		image_element        : imageSelector.image_element,
		is_show_tag_view     : imageSelector.is_show_tag_view,
		is_show_action_dower : imageSelector.is_show_action_dower,
		markersInfo          : imageSelector.markersInfo,
		markersToSent        : imageSelector.markersToSent,
		orientation_flag     : imageSelector.orientation_flag,
		image_value          : imageSelector.image_value,
		token                : attachPrescription.token,
		isImageFetching      : attachPrescription.isImageFetching
	}),
	(dispatch) => bindActionCreators({
		...imageSelectorActionCreater,
		handleUpdatePrescriptions,
		updateImageList,
	}, dispatch)
)(ImageSelectorContainer)
