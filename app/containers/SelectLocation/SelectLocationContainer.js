import React, {Component}                                      from 'react'
import { bindActionCreators }                                  from 'redux'
import { connect }                                             from 'react-redux'
import { Link }                                                from 'react-router-dom'
import SelectLocation                                          from '$COMPONENTS/SelectLocation/SelectLocation'
import BillInformation                                         from '$COMPONENTS/Cart/BillInformation'
import OrderStatus                                             from '$COMPONENTS/Cart/OrderStatus'
import PromocodeContainer                                      from '../Cart/PromocodeContainer'
import {setAdwordFlage, resteCartStore, setAndHandleCartOrder, maintainRoutes} from '$REDUX/modules/cart';
import {handleWalletInfo} 																		 from '$REDUX/modules/previousOrder'
import * as selectLocationActionCreater                        from '$REDUX/modules/selectLocation';
import OverLayPopover                                          from '$PARTIALS/OverLayPopover/OverLayPopover'
import Loader                                                  from '$PARTIALS/Loader/Loader'
import {callAnalytics}                                         from '$HELPERS/analytics'
import Clevertap                                               from '$HELPERS/clevertap'
import { getLocalstorageValue, isLocalStorageSupported}        from '../../helpers/helperFunction'

class SelectLocationContainer extends Component{

	constructor(props) {
	  super(props);

	  this.state = {
	  	showItemsCount:1,
			expandList:false,
			focusInput:0,
			orderSuccess:false,
			setDeliveryLocation: props.deliveryLocationId ? true : false,
	  };
	}

	componentDidMount(){
		Clevertap.event('mWeb - Page visit', {
			'Page title': 'Order Placed'
		})
		this.props.setAndHandleUserLocations();
		setTimeout(x => {
				this.props.cartList && this.props.cartList.id ? this.props.handleWalletInfo(this.props.cartList.id) : ""
		}, 1500)

	}

	setUserNote = (e) => {
		this.props.setUserNote(e.target.value)
	}

	handleChangeLocation = (e) => {
		this.props.handleChangeUserLocation(e.target.value).then(x => {
			if(x == "successful"){
				this.setState({setDeliveryLocation: true})
			}
		}).catch(x => {
			// alert("Please try again")
		})
	}

	handleChangeBillingLocation = (e) => {
		this.props.handleChangeBillingLocation(e.target.value)
	}

	handleSuccessOrder = () => {
		let pixelurl = ""
		if(document.referrer == "https://myramed.in/?utm_source=affiliates&utm_medium=cps&utm_campaign=optimise-revshare-campaign"){
			pixelurl = this.props.cartList && this.props.cartList.display_id ? "https://track.in.omgpm.com/apptag.asp?APPID="+ this.props.cartList.display_id + "&MID=1077751&PID=31603&status=0" : ""
		}
		callAnalytics('analytics',{hitType:'event', eventCategory: 'myra-web-app', eventAction: 'success page', eventLabel:'order placed successfully'})
		callAnalytics('adwords',{'conversion_id':878349016, 'conversion_label': 's9-pCLj_1XAQ2JXqogM'})
		this.props.resteCartStore()
		this.props.setAdwordFlage(true)
		this.setState({orderSuccess: true, pixelurl: pixelurl})

		Clevertap.event('mWeb - Cart - Order Placed')

		this.props.setAndHandleCartOrder()
		setTimeout(() => {
			this.setState({orderSuccess: false, pixelurl: ""})
		  	this.props.history.replace('prescription')
		}, 2500);

	}

	handleChooseDifferentTime = () => {
		this.props.placeOrderFailur({error: '', timing: this.props.timing})
		this.props.history.replace('/schedule_order')
	}

	dialogCancelButton = () => {
		this.props.placeOrderFailur({error: '', timer: {}})
	}

	handleScheduleOrder = () => {
		this.props.handleScheduleOrder({hour: this.props.timing.schedule_hour, minute: this.props.timing.schedule_minute}, this.handleSuccessOrder)
		// this.props.setAdwordFlage(true)
	}

	handlePlaceOrder = () => {
		if(!this.props.deliveryLocationId || !this.state.setDeliveryLocation){
			alert("Please select your delivery location!!")
			return
		}

		let dvIds = []
		if(this.props.cartList && this.props.cartList.items && this.props.cartList.items.length){
			dvIds = this.props.cartList.items.map(i => i.drug_variation_id)
		}

		Clevertap.event('mWeb - Cart - Place order', {
			dvIds: dvIds.toString()
		})

		this.props.handlePlaceOrder(this.handleSuccessOrder)
	}

	showBillingLocation = (e) => {
		// debugger
		if (!e.target.checked && this.props.billingLocationId) {

			this.props.handleChangeBillingLocation("null")
		}
		this.props.showBillingLocation()
	}

	handleItemExpand = () => {
		if(this.state.showItemsCount == 1){
			this.setState({expandList:true, showItemsCount: this.props.userLocations.length})
		} else {
			this.setState({expandList:false, showItemsCount:1})
		}
	}

	focusCouponInput = () => {
		this.setState({focusInput: Math.floor(1000 + Math.random() * 9000)})
	}

	handleBillAddressChange = () => {
		this.props.showBillingLocation()
	}

	handleGoLocation = () => {
		this.props.maintainRoutes("")
		Clevertap.event("mWeb - Add New Address Screen",{"origin":"Cart"})
	}

	render() {

		let doneButtonDialogBody = ""
		if(this.props.placeOrderError && this.props.timing){
			let schedule_hour = (this.props.timing.schedule_hour > 12 ) ? (this.props.timing.schedule_hour - 12) : (this.props.timing.schedule_hour)
			let schedule_minute = (this.props.timing.schedule_minute < 10) ? ('0'+this.props.timing.schedule_minute) : (this.props.timing.schedule_minute)
			let ampm = (this.props.timing.schedule_hour > 12 ) ? "pm" : "am"
			doneButtonDialogBody = (
				<div className = "dialog_body">
					<span >{this.props.placeOrderError}</span>
					<span onClick = {this.handleScheduleOrder} className = "green_text">{'Place Order @'+ schedule_hour + ':' + schedule_minute +' '+ ampm +' '+ this.props.timing.date}</span>
					<span onClick = {this.handleChooseDifferentTime} className = "green_text">{'Choose other delivery time'}</span>
					<span onClick = {this.dialogCancelButton} className = "green_text">{'Cancel'}</span>
				</div>
				)
		}

		let deliveryCharge = 0
        if(this.props.cartList &&  this.props.cartList.extra_charges && this.props.cartList.extra_charges.length && this.props.cartList.extra_charges[0].amount != 0){
            deliveryCharge = this.props.cartList.extra_charges[0].amount
		}

		let location = this.props.userLocations
		let localLocation = getLocalstorageValue("userLocation")
		location = location.filter(x => x.id != localLocation.id)
		location.unshift(localLocation)
		return (
		  <div className= "container ">
		  	{this.props.isFetching ? <Loader/> : null}
		  	<div className="section_heading">
          <span className="section_title">Deliver To</span>
          <Link to = '/user_locations' onClick={() => Clevertap.event("mWeb - Add New Address Screen",{"origin":"Cart"})}>
          	<span className="section_right green_text">+ Add Address</span>
          </Link>
        </div>

				{(this.props.placeOrderError && this.props.timing) ? <OverLayPopover cancelButton = {this.dialogCancelButton}  messageView = {true} body = {doneButtonDialogBody} /> : null}
				<SelectLocation
					userLocations= {location}
					handlePaymentMode= {this.handlePaymentMode}
					handleChangeLocation= {this.handleChangeLocation}
					handleChangeBillingLocation= {this.handleChangeBillingLocation}
					setUserNote= {this.setUserNote}
					note= {this.props.note}
					deliveryLocationId= {this.props.deliveryLocationId}
					billingLocationId= {this.props.billingLocationId}
					showBillingLocation= {this.showBillingLocation}
					isBillingList= {this.props.isBillingList}
					unAvailUserLocations= {this.props.unAvailUserLocations}
					showItemsCount={this.state.showItemsCount}
					expandList={this.state.expandList}
					handleItemExpand={this.handleItemExpand}
					/>

			<div className="section_heading full_width">
        <span className="section_title">Have a Coupon Code?</span>
      </div>
      <PromocodeContainer focusInput={this.state.focusInput? this.state.focusInput : false}/>

      <div className="section_heading full_width">
          <span className="section_title">Estimated Bill</span>
      </div>

      <BillInformation
      	wallateInfo = {this.props.wallateInfo}
				name={this.props.cartList.delivery_location && this.props.cartList.delivery_location.name}
				address={this.props.cartList.delivery_location && this.props.cartList.delivery_location.address}
				totalMRP={this.props.cartList.total_mrp}
				totalDiscount={this.props.cartList.total_discount}
				totalAmount={this.props.cartList.total_amount}
				focusCouponInput={this.focusCouponInput}
				handleAddressChange={this.handleBillAddressChange}
				deliveryCharge={deliveryCharge}
      />

      <div className = "fixed_btn button">
				<div onClick= {this.handlePlaceOrder} >Place Order &amp; Pay Later</div>
			</div>

			{this.state.orderSuccess
				?(<div>
						{this.state.pixelurl ? <img src= {this.state.pixelurl} border="0" height="1" width="1"/> : ""}
						<OrderStatus />
					</div>
				): ''}

			</div>
		);
	}
}

export default connect(
	({selectLocation, cart, previousOrder}) => ({
		isFetching           : selectLocation.isFetching,
		userLocations        : selectLocation.userLocations,
		deliveryLocationId   : selectLocation.deliveryLocationId,
		billingLocationId    : selectLocation.billingLocationId,
		note                 : selectLocation.note,
		placeOrderError      : selectLocation.placeOrderError,
		timing               : selectLocation.timing,
		isBillingList        : selectLocation.isBillingList,
		isPlaceOrder         : selectLocation.isPlaceOrder,
		unAvailUserLocations : selectLocation.unAvailUserLocations,
		cartList             : cart.cartList,
		wallateInfo          : previousOrder.wallateInfo,

}),
	(dispatch) => bindActionCreators({
		...selectLocationActionCreater,
		setAdwordFlage,resteCartStore,setAndHandleCartOrder,handleWalletInfo, maintainRoutes,
	}, dispatch)
)(SelectLocationContainer)