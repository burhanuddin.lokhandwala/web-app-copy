import React, { Component}            from 'react';
import { bindActionCreators }         from 'redux'
import { connect }                    from 'react-redux'
import * as prescriptionActionCreater from '$REDUX/modules/ordersPrescription'
import * as usersAction from '$REDUX/modules/users'

import ItemsListContainer             from '$CONTAINERS/Search/ItemsListContainer'

class RecomendedItemsContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
				showSearch: false
		}
	}

	componentDidMount(){
		// this.props.recomendedItemsList.length <= 0 ? this.props.getRecomendedItemsList() : "";
	}

	static contextTypes = {
		//router: React.PropTypes.object.isRequired,
	}


	render(){
		let recomendedItemsLists = this.props.recomendedItemsList.length ?this.props.recomendedItemsList.map(function(item){return item['medicine']}) : []
		return(
			<div className= "blank_container">
				<ul className="full_width">
					<ItemsListContainer
						itemList = {recomendedItemsLists}
						parentRef='Previously Ordered'
						extraInfo={{source: 'View all'}}
						isAuthed = {this.props.isAuthed}
						history = {this.props.history}
					/>
				</ul>
			</div>
		)
	}
}


export default connect(
	({ordersPrescription,users}) => ({
		recomendedItemsList: ordersPrescription.recomendedItemsList,
		isAuthed : users.isAuthed
	}),
	(dispatch) => bindActionCreators({
		...prescriptionActionCreater,
		...usersAction
	}, dispatch)
)(RecomendedItemsContainer)