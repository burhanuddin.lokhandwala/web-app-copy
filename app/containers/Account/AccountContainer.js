import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import UserAccount from "$COMPONENTS/Account/UserAccount";
import UserInfoEditer from "$COMPONENTS/Account/UserInfoEditer";
import * as userActionCreators from "$REDUX/modules/users";
import Loader from "$PARTIALS/Loader/Loader";
import * as locationActionCreater from "$REDUX/modules/addGEOLocation";
import { resteCartStore, maintainRoutes } from "$REDUX/modules/cart";
import { resteOrderStore } from "$REDUX/modules/ordersPrescription";
import { fetchingPreviousOrderSuccess } from "$REDUX/modules/previousOrder";

import Clevertap from "$HELPERS/clevertap";

class AccountContainer extends Component {
    componentDidMount() {
        Clevertap.event("mWeb - Page visit", {
            "Page title": "Account",
        });
    }

    handleUserInfoChange = () => {
        this.props.history.replace("/user");
    };

    logoutHandler = () => {
        Clevertap.event("mWeb - Page visit", {
            "Page title": "Account - Log out",
        });
        this.props.logoutAndUnauth();
        this.props.resteCartStore();
        this.props.resteOrderStore();
        this.props.fetchingPreviousOrderSuccess([], "");
        if (localStorage && localStorage.getItem("expressAccessToken") !== "") {
            localStorage.removeItem("expressAccessToken");
            localStorage.removeItem("user");
            window.location.href = "/login";
        } else {
            this.props.history.replace("/login");
        }
    };

    addressHandler = () => {
        this.props.maintainRoutes("");
        Clevertap.event("mWeb - Add New Address Screen", { origin: "Account" });
        this.props.flagLocationChange(false);
    };

	helpHandler = () => {
		Clevertap.event('mWeb - Page visit', {
			'Page title': 'Account - Help'
		})
	}

	addressHandler = () => {
		this.props.maintainRoutes("")
		Clevertap.event("mWeb - Add New Address Screen",{"origin":"Account"});
		this.props.flagLocationChange(false)
	}

	handleWalletRedirect = () => {
		console.log("function for event capture");
	}

	render() {
		// if(getConfigMode && getConfigMode().serverMode.mode == 'server'){
		// 	ga('send', 'event', 'myra-web-app', 'account page', 'test account label')
		// }
		// document.querySelector('.main_centered_container').style = {marginTop : '0px'}
		document.querySelector('.main_centered_container').style = {marginTop : 0}
		let userInfo = null
		if(this.props.userInfo.id){
			userInfo = (<UserAccount
				userInfo = {this.props.userInfo}
				addressHandler = {this.addressHandler}
				helpHandler = {this.helpHandler}
				logoutHandler = {this.logoutHandler}
				handleUserInfoChange = {this.handleUserInfoChange}
				handleWalletRedirect = {this.handleWalletRedirect}
			/>)
		}else{
			userInfo = (<Loader/>)
		}
		return (
			<div className = "full_height">
				{userInfo}
			</div>
		)
	}
}

export default connect(
    ({ users }) => ({ isAuthed: users.isAuthed, otp: users.otp, isFetching: users.isFetching, userInfo: users.userInfo }),
    dispatch =>
        bindActionCreators(
            { ...userActionCreators, ...locationActionCreater, resteCartStore, resteOrderStore, fetchingPreviousOrderSuccess, maintainRoutes },
            dispatch
        )
)(AccountContainer);
