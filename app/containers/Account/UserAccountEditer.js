import React, {Component}      from 'react'
import { connect }             from 'react-redux'
import { bindActionCreators }  from 'redux'
import UserInfoEditer          from '$COMPONENTS/Account/UserInfoEditer'
import * as userActionCreators from '$REDUX/modules/users'
import Loader                  from '$PARTIALS/Loader/Loader'
import Clevertap               from '$HELPERS/clevertap'

class UserAccountEditer extends Component{
	componentDidMount() {
		Clevertap.event('mWeb - Page visit', {
			'Page title': 'Account UserInfo'
		})
	}

	hendleDoneButton = (data) => {
			// debugger
		// if(!data.email && !data.name){
		// 	this.props.history.replace('account')
		// 	return
		// }
		this.props.handleUpdateUserInfo(data).then(() =>{
			this.props.history.replace('accounts')
		})
	}

	render() {
		return (
			<div className = "full_height">
				{this.props.userInfo.id
				? <UserInfoEditer
					userInfo = {this.props.userInfo}
					hendleDoneButton = {this.hendleDoneButton}
				/>
				:<Loader/>
				}
			</div>
		)
	}
}

export default connect(
	({users}) => ({isAuthed: users.isAuthed, otp: users.otp, isFetching: users.isFetching, userInfo: users.userInfo }),
	(dispatch) => bindActionCreators(userActionCreators, dispatch)
)(UserAccountEditer)
