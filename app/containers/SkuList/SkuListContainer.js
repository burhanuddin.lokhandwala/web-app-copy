import React, { Component } from "react";
import { getComponentDetails } from "../../utils/layoutServiceApi";
import SkuList from "../../components/SkuList/SkuList";

export default class SkuListContainer extends Component {
	constructor(props) {
        super(props);
        this.state = {
            componentDetails: {},
        };
    }
    componentDidMount() {
        getComponentDetails(this.props.componentID).then((componentDetails) => {
            this.setState({componentDetails: componentDetails.data});
        });
    }

    render() {
		let props = {
			...this.state.componentDetails,
			handleUpdateMedicineCount: this.props.handleUpdateMedicineCount,
		}
        return (
            <SkuList {...props} />
        );
    }
}
