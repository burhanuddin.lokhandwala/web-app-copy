import React,{Component}                                       from 'react'
import { bindActionCreators }                                  from 'redux'
import { connect }                                             from 'react-redux'
import ScheduleOrder                                           from '$COMPONENTS/Cart/ScheduleOrder'
import OrderStatus                                             from '$COMPONENTS/Cart/OrderStatus'
import {setAdwordFlage, resteCartStore, setAndHandleCartOrder} from '$REDUX/modules/cart';
import * as selectLocationActionCreater                        from '$REDUX/modules/selectLocation';
import Loader                                                  from '$PARTIALS/Loader/Loader'

import Clevertap                                               from '$HELPERS/clevertap'

class ScheduleOrderContainer extends Component{
	static contextTypes = {
		//router: React.PropTypes.object.isRequired,,
	}

	constructor(props) {
	  super(props);

	  this.state = {
			orderSuccess:false
	  };

	}

	goToSuccess = () => {
		this.props.setAdwordFlage(true)
		this.props.resteCartStore()
		this.setState({orderSuccess: true})
		Clevertap.event('mWeb - Cart - Order Placed')
		this.props.setAndHandleCartOrder()
		setTimeout(() => {
			this.setState({orderSuccess: false})
		  	this.props.history.push('/prescription')
		}, 2500);
	}

	handleDoneButton = (data) => {
		this.props.handleScheduleOrder(data, this.goToSuccess)
	}


	render() {
		return (
		    <div className= "blank_container">
					{this.props.isFetching ? (<Loader/>) : null}
					<ScheduleOrder error = {this.props.error} timing = {this.props.timing} handleDoneButton = {this.handleDoneButton} />
					{this.state.orderSuccess ? <OrderStatus /> : ''}
		    </div>
		);
	}

}


export default connect(
	({cart, selectLocation}) => ({
		isFetching   : selectLocation.isFetching,
		timing       : selectLocation.timing,
		isPlaceOrder : selectLocation.isPlaceOrder,
		error        : selectLocation.placeOrderError,
	}),
	(dispatch) => bindActionCreators({
		...selectLocationActionCreater,
		setAdwordFlage,
		resteCartStore,
		setAndHandleCartOrder,
	}, dispatch)
)(ScheduleOrderContainer)
