import React, {Component}                     from 'react';
import { Link }                                from 'react-router-dom'
import { bindActionCreators }                  from 'redux'
import { connect }                             from 'react-redux'
import * as cartActionCreater                  from '$REDUX/modules/cart'
import {handleDeleteImage}                     from '$REDUX/modules/prescriptionImage'
import {updateImageElement}                    from '$REDUX/modules/imageSelector'
import {handleShowSearch}                      from '$REDUX/modules/users'
import ItemsListContainer                      from '../Search/ItemsListContainer'
import  PhotoAdder                             from '$COMPONENTS/PhotoAdder/PhotoAdder'
import  CartRecommendation                     from '$COMPONENTS/Cart/CartRecommendation'
import Loader                                  from '$PARTIALS/Loader/Loader'
import OverLayPopover                          from '$PARTIALS/OverLayPopover/OverLayPopover'
import * as style                              from './style.scss'
import { addPrescription, addItem, emptyCart } from '$SHAREDSTYLES/icons'
import {callAnalytics}                         from '$HELPERS/analytics'
import Clevertap                               from '$HELPERS/clevertap'
import {setImgUrlFromFiles, getOrientation, getMobileOperatingSystem, getLocalstorageValue, updateLocalstorage} from '$HELPERS/helperFunction'

class CartContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartItems: [],
            showImageList: '',
        }
    }

    componentDidMount(){
        this.props.setAndHandleCartOrder("current_order")
        Clevertap.event('mWeb - View Cart')
        let cartItemsObj = this.props.items.map(function(item){return item['medicine']})
        this.setState({cartItems: cartItemsObj})
    }

    componentWillReceiveProps = (nextProps) => {
        let cartItemsObj = nextProps.items && nextProps.items ? nextProps.items.map(function(item){return item['medicine']}) : []
        this.setState({cartItems: cartItemsObj})

    }

    handleDeleteImage = (list) => {
        Clevertap.event('mWeb - Cart - Remove photo')
        if(this.props.userOnlineStatus == "online"){
            this.props.handleDeleteImage({orderId: list.order_id, pId: list.id})
            .then(response => {
                if(response){
                    this.props.updateImageList(response)
                }
            })
        }else{
            let imgList = this.props.imageList
            imgList = imgList.filter(x => x.id !=list.id)
            // updateLocalstorage("imgList", imgList)
            this.props.updateImageList(imgList)
        }
    }

    getImage = (e) => {
        let rotate = ""
        if(e.target){
            let file = e.target
            getOrientation(file.files[0], function(orientation) {
                                rotate = orientation
                            })
            setTimeout(() => {
                this.props.updateImageElement({image_value: file, image_element: setImgUrlFromFiles(file), orientation_flag: rotate})
                this.props.history.push('/image_prescription')
            }, 500)
        }
    }

    showImage = (showImageList) =>{
        this.setState({showImageList: showImageList})
    }

    cancelButton = () => {
        this.setState({showImageList: ''})
    }

    handleNextButton = () => {
        Clevertap.event('mWeb - Cart - Proceed')
        if(this.props.userOnlineStatus == "online"){
            if(!this.props.extendedNotesInCart.length && !this.props.imageList.length && !this.state.cartItems.length){
                alert("Please upload photo or add items to your list to place the order!")
                return
            }
            this.props.history.push('/select_location')
        }else{
            if(!this.props.extendedNotesInCart.length && !this.props.imageList.length && !this.state.cartItems.length){
                alert("Please upload photo or add items to your list to place the order!")
                return
            }
            this.props.history.push('/login')
        }
    }

    handleImageUpload = (e) => {
        if(this.props.userOnlineStatus == "offline"){
            e.preventDefault()
            this.props.history.push('/login')
        }
    }

    render(){
        return(
            <div className={`${style.cart_container} full_width`}>
                {this.props.isFetching ? (<Loader/>) : null}
                {(this.state.cartItems && this.state.cartItems.length) || (this.props.imageList && this.props.imageList.length) || (this.props.extendedNotesInCart && this.props.extendedNotesInCart.length) ?
                <div className = "">
	                {this.props.cartRecommendation && this.props.cartRecommendation.length ? (
	                		<CartRecommendation
	                			header = {this.props.cart_charges ? this.props.cart_charges.extra_charges[0].extras.messages[0] : ""}
	                			isFetchingMedicine = {this.props.isFetchingMedicine}
	                			recommendationList = {this.props.cartRecommendation}
	                			handleUpdateMedicineCount = {this.props.handleUpdateMedicineCount}
	                		/>
	                	) : ""}

                    {/* Medicine List */}
                    {(this.state.cartItems && this.state.cartItems.length || (this.props.extendedNotesInCart && this.props.extendedNotesInCart.length)) ?
                        <div className="full_width section_heading full_width">
                            <span className="section_title">Items Added</span>
                           	<Link to = "/search_medicine" className="section_right" >+ Add More </Link>
                        </div>
                    :
                        <Link to = "/search_medicine" className={style.add_container} >
                            <div className={style.content}>
                                <span className={style.icon}>{addItem}</span>
                                <span className={style.text}> Add Items by Search </span>
                            </div>
                        </Link>
                    }
                    {<ItemsListContainer itemList={this.props.cartBucketItem} history={this.props.history} isAuthed={this.props.isAuthed} cartExtendedNotes={this.props.extendedNotesInCart} parentRef="Cart"/>}

                    {/* Prescription */}
                    {this.props.imageList && this.props.imageList.length ?
                    <div>
                      <div className="section_heading full_width">
                        <span className="section_title">Prescriptions</span>
                        <span className="section_right">
	                        <label htmlFor = "imageUploader" >
	                          <span>+ Add More </span>
	                          <input className = {style.image_uploader_input} id= "imageUploader" name = "imageUploader" onChange = {this.getImage} type="file" accept=".png, .jpg, .jpeg;capture=camera"/>
	                        </label>
                        </span>
                      </div>
                      <div className={style.prescription_section}>
                        {this.state.showImageList ? <OverLayPopover showImageList = {this.state.showImageList}  imageView = {true} cancelButton = {this.cancelButton}/> : null}
                        <PhotoAdder imageList = {this.props.imageList} handleDeleteImage = {this.handleDeleteImage} getImage = {this.getImage} showImage = {this.showImage}/>
                      </div>
                    </div>
                    :
                    <label className = "full_width" htmlFor = "imageUploader" >
                        <div className={style.add_container}>
                            <div className={style.content}>
                                <span className={style.icon}>{addPrescription}</span>
                                <span className={style.text}> Add a Prescription/ Med's Photo </span>
                            </div>
                        </div>
                        <input className = {style.image_uploader_input} id= "imageUploader" onClick={this.handleImageUpload} name = "imageUploader" onChange = {this.getImage} type="file" accept=".png, .jpg, .jpeg;capture=camera"/>
                    </label>
                    }

                    {/* CTA */}
                    <div className = {style.fixed_bottm_button}>
                    	<div className = {style.total_cost_text}>
                    		<div>&#8377;{this.props.cart_charges ? this.props.cart_charges.total_amount : ""}</div>
                    		<div className = "green_text small_text">CART TOTAL</div>
                    	</div>
                  		<div onClick= {this.handleNextButton} className = {style.next_button}>Next</div>
                    </div>

                </div>
                :
                (this.props.isFetching ? "" :
                 <div className={`${style.empty_cart} full_height`}>
                    <div>{emptyCart}</div>
                    <div className={style.no_items}>Uh-oh your cart is empty :(</div>
                    <Link to = "/page/home" className={style.added}>Start Ordering</Link>
                </div> )
            }
            </div>
        )
    }
}


export default connect(
    ({cart, users}) => ({
        userOnlineStatus    : users.userOnlineStatus,
        isAuthed            : users.isAuthed,
        isFetching          : cart.isFetching,
        items               : cart.items,
        extendedNotesInCart : cart.extended_notes,
        imageList           : cart.imageList,
        cartRecommendation  : cart.cartRecommendation,
        cartBucketItem      : cart.cartBucketItem,
        isFetchingMedicine  : cart.isFetchingMedicine,
        cart_charges				: cart.cart_charges,
    }),
    (dispatch) => bindActionCreators({
        ...cartActionCreater,
        handleDeleteImage,
        updateImageElement,
        handleShowSearch,
    }, dispatch)
)(CartContainer)