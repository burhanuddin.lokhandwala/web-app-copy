import React, { Component}    from 'react';
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'
import * as cartActionCreater from '$REDUX/modules/cart'
import Promocode              from '$COMPONENTS/Cart/Promocode'
import * as style             from './style.scss'


class PromocodeContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            promo_code:'',
            focusInput: (props.focusInput ? props.focusInput : false)
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.focusInput){
            this.setState({focusInput: nextProps.focusInput})
            // this.handlefocusInput()
        }
    }

    handlePromoChange = (e) => {
        this.setState({promo_code: e.target.value})
    }

    handlePromoCode = () => {
        if(this.state.promo_code){
            this.props.handlePromoCode(this.state.promo_code)
        } else {
            alert("Please enter coupon code !")
        }
    }

    handlefocusInput = () => {
        this.refs.promo.refs.promoInput.focus();
    }


    render(){
        if(this.state.focusInput){
            this.handlefocusInput()
        }
        return(
            <Promocode promo_code = {this.state.promo_code}
                       handlePromoChange = {this.handlePromoChange}
                       handlePromoCode = {this.handlePromoCode}
                       promoError = {this.props.promoError}
                       ref = "promo"/>
        )
    }
}


export default connect(
    ({cart}) => ({
        promoError: cart.promoError,
    }),
    (dispatch) => bindActionCreators({
        ...cartActionCreater
    }, dispatch)
)(PromocodeContainer)