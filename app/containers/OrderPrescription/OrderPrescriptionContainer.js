import React, {Component}              from 'react'
import { connect }                     from 'react-redux'
import { Link }                        from 'react-router-dom'
import { bindActionCreators }          from 'redux'
import CurrentOrderListCard            from '$COMPONENTS/CurrentOrderListCard/CurrentOrderListCard'
import HomePageCatagory                from '$COMPONENTS/ProductCatagory/HomePageCatagory'
import ShowOrderProcess                from '$PARTIALS/DummyView/ShowOrderProcess'
import Loader                          from '$PARTIALS/Loader/Loader'
import * as cartActionCreater          from '$REDUX/modules/cart'
import * as prescriptionActionCreater  from '$REDUX/modules/ordersPrescription'
import * as previousOrderActionCreater from '$REDUX/modules/previousOrder'
import {juspayPayment}				         from '$REDUX/modules/paymentMethod'
import { arrow }                       from '$SHAREDSTYLES/icons'
import ItemsListContainer              from '$CONTAINERS/Search/ItemsListContainer'
import {callAnalytics}                 from '$HELPERS/analytics'
import Clevertap                       from '$HELPERS/clevertap'

class OrderPrescriptionContainer extends Component{
	constructor(props){
		super(props)
		window.addEventListener("currentOrder",()=>{
			// alert("getting currentOrderList");
			this.props.getAndHandleCurrentOrder();
		})
	}

	componentDidMount(){
		Clevertap.event('mWeb - Page visit', {
			'Page title': 'Home'
		})
		callAnalytics('analytics',{hitType:'event', eventCategory: 'myra-web-app', eventAction: 'order page', eventLabel:'order prescription container'})
		if(this.props.userOnlineStatus == 'online'){
			// this.props.getAndHandlePreviousOrder();
				this.props.getAndHandleCurrentOrder();
				setInterval(()=>{
					if(window.location.pathname == "/prescription") 
					this.props.getAndHandleCurrentOrder();
				},15000)
				
			// this.props.recomendedItemsList && this.props.recomendedItemsList.length > 0 ? "" : this.props.getRecomendedItemsList();
		}

		this.props.categoryList && this.props.categoryList.length ? "" :this.props.getCategoryList();
	}

	handleMakeList = () => {
		setTimeout(() => {
			this.props.handleAdditemPopover(true)
		}, 100)
	}
	render() {
		let dummyText = this.props.userOnlineStatus == 'offline' ? <ShowOrderProcess/> : (this.props.previousOrderFetching ? "Loading..." : <ShowOrderProcess/> )
		let recomendedItemsLists = (this.props.recomendedItemsList && this.props.recomendedItemsList.length) ?this.props.recomendedItemsList.slice(0, 4).map(function(item){return item['medicine']}) : ""
		return (
			<div className = "full_width" >
					{!this.props.isFetching
						? (<div className="full_width">
						   	{this.props.currentOrderList && this.props.currentOrderList.length && this.props.userOnlineStatus == 'online'
									?(<div className="field_blank_container">
											<div style={{marginBottom: "15px"}} className="full_width">
												<span className="bigHeaderText">Active Orders</span>
											</div>
										  {this.props.currentOrderList.map((x, index) => {
												return <CurrentOrderListCard
																key= {index}
																orderItem = {x}
																history = {this.props.history}
																handleViewItem = {this.props.handleReorderFlag}
															/>
										})}
									</div>) : null
								}
								{<HomePageCatagory
									categoryList= {this.props.categoryList}
									updateCategoryName= {this.props.updateCategoryName}
								/>}

								{recomendedItemsLists && recomendedItemsLists.length
									?(<div>
											<div className="field_blank_container">
												<span className="bigHeaderText">Previously Ordered</span>
												<Link to = "/recomended_items_for_you" className= "show_detail_arrow">
													<span >See All</span><span className="right_arrow">{arrow}</span>
												</Link>
											</div>
											{this.props.cartIsFetching ? <Loader/> : ""}
											<ul style = {{marginBottom: "115px"}} className="full_width">
												<ItemsListContainer
													itemList = {recomendedItemsLists}
													parentRef='Previously Ordered'
													extraInfo={{source: 'Home'}}
													history = {this.props.history}
													isAuthed = {this.props.isAuthed}
												/>
											</ul>
										</div>
									)
								: (<div > {dummyText} </div>)}
							</div>)
					: <Loader/>
					}
			</div>
		);
	}

}

export default connect(
	({ordersPrescription, users, cart, previousOrder}) => ({
		userName:users.userInfo.name,
		userOnlineStatus: users.userOnlineStatus,
		isFetching: ordersPrescription.isFetching,
		imgValue: ordersPrescription.imgValue,
		recomendedItemsList: ordersPrescription.recomendedItemsList,
		error: ordersPrescription.error,
		processingPipeline: ordersPrescription.processingPipeline,
		currentOrderList: ordersPrescription.currentOrderList,
		isCurrentOrder: ordersPrescription.isCurrentOrder,
		categoryList: ordersPrescription.categoryList,
		cartIsFetching: cart.isFetching,
		displayId: cart.cartList.display_id,
		isAuthed : users.isAuthed
	}),
	(dispatch) => bindActionCreators({
		...prescriptionActionCreater,
		...cartActionCreater,
		...previousOrderActionCreater,
		juspayPayment
	}, dispatch)
)(OrderPrescriptionContainer)
