import React, {Component}                    from 'react'
import { connect }                           from 'react-redux'
import { bindActionCreators }                from 'redux'
import AllCategoriesPage                     from '$COMPONENTS/ProductCatagory/AllCategoriesPage'
import {getCategoryList, updateCategoryName} from '$REDUX/modules/ordersPrescription'

class ProductCategoriesContainer extends Component{

	componentDidMount(){
		this.props.categoryList && this.props.categoryList.length ? "" :this.props.getCategoryList();
	}

	render() {
		return (
			<div className = "field_blank_container" >
				<div className = "full_width" >
					{this.props.categoryList.length ?
							(this.props.categoryList.map((x, index) => {
								return <AllCategoriesPage
												updateCategoryName= {this.props.updateCategoryName}
												key = {index}
												categoryList = {x}
											/>
							})
						): null
					}
				</div>
			</div>
		);
	}

}

export default connect(
	({ordersPrescription}) => ({
		categoryList: ordersPrescription.categoryList,
	}),
	(dispatch) => bindActionCreators({getCategoryList, updateCategoryName}, dispatch)
)(ProductCategoriesContainer)
