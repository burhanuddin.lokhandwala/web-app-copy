import React, {Component}             from 'react'
import { connect }                    from 'react-redux'
import { Link }                       from 'react-router-dom'
import { bindActionCreators }         from 'redux'
import Subcategory                    from '$COMPONENTS/ProductCatagory/Subcategory'
import {handleShowSearch}             from '$REDUX/modules/users'
import {updateSearchInput}            from '$REDUX/modules/cart'
import * as prescriptionActionCreater from '$REDUX/modules/ordersPrescription'

class SubcategoryContainer extends Component{
	constructor(props) {
	  super(props);
		let subcat_name = props.match.params.subcat_name
		props.categoryList && props.categoryList.length ? "" :props.getCategoryList();
		props.updateCategoryName(subcat_name)
	  this.state = {
			subcat_name: subcat_name,
	  };

	}

	handleSearchCategory = (searchInput) => {
		this.setState({searchInput: searchInput})
		// this.props.isAuthed ? (
			this.props.updateSearchInput(searchInput),
			this.props.history.push("/search_medicine")
		// )
		// : this.props.history.push("/login")
		this.props.handleShowSearch(true, 'Trending Categories Search')
	}

	render() {
		let subcat_name = this.props.match.params.subcat_name
		let subcat_list = this.props.categoryList.length ? this.props.categoryList.filter(x => x.name == subcat_name)[0] : ""

		let horizontalList = subcat_list.sub_categories && subcat_list.sub_categories.length ? subcat_list.sub_categories.filter(x => !x.expanded) : []
		let verticalList = subcat_list.sub_categories && subcat_list.sub_categories.length ? subcat_list.sub_categories.filter(x => x.expanded) : []

		return (
			<div className = "full_width grey_back" >

					{verticalList && verticalList.length
						? <Subcategory
								vartical = {true}
								categoryList = {verticalList[0]}
								handleSearchCategory = {this.handleSearchCategory}
							/>
						: null
					}
					{horizontalList && horizontalList.length
						? <Subcategory
								horizontal = {true}
								categoryList = {horizontalList}
								handleSearchCategory = {this.handleSearchCategory}
							/>
						: null
					}

					{verticalList && verticalList.length > 1
						? <Subcategory
								vartical = {true}
								categoryList = {verticalList[1]}
								handleSearchCategory = {this.handleSearchCategory}
							/>
						: null
					}
			</div>
		);
	}

}

export default connect(
	({ordersPrescription, users}) => ({
		categoryList: ordersPrescription.categoryList,
		isAuthed: users.isAuthed
	}),
	(dispatch) => bindActionCreators({
		...prescriptionActionCreater,
		handleShowSearch,
		updateSearchInput,
	}, dispatch)
)(SubcategoryContainer)
