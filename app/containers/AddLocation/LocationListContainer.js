import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AllLocationsList from "$COMPONENTS/AddLocation/AllLocationsList";
import AddAndUpdateAddress from "$COMPONENTS/AddLocation/AddAndUpdateAddress";
import * as locationActionCreater from "$REDUX/modules/addGEOLocation";
import { handleGetActiveLocation, getAvailabilityInfo } from "$REDUX/modules/cart";
import Loader from "$PARTIALS/Loader/Loader";
import { askForUserGEOLocation } from "$UTILS/googleLocationApi";
import googleAutoComplete, { getPlaceInfo } from "$UTILS/googleAutoComplete";
import Clevertap from "$HELPERS/clevertap";

class LocationListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addressCount: 2,
            curOrigin: "",
        };
    }

    componentDidMount() {
        this.props.handleGetUserLocations();
        Clevertap.event("mWeb - Page visit", {
            "Page title": "Account Addresses",
        });
    }
    changeLocation = value => {
        this.props.handleChangeLocation(value).then(() => {
            this.props.history.replace("cart");
        });
    };

    editLocation = value => {
        this.props.currentEditLocation(value);
        this.props.history.push("/edit_location");
    };

    updateAddressHandler = queryJson => {
        this.props.handleUpdateAddress(queryJson, this.showListView);
    };

    getLocation = () => {
        askForUserGEOLocation(this.addLocation, this.props.setUserGEOLocationFailur, this.showListView);
    };

    addLocation = loc => {
        // console.log("debug code", loc)
        Clevertap.event("mWeb - Address Add By", { origin: "GPS" });
        this.setState({ curOrigin: "GPS" });
        this.props.handleGetLocationFormet(loc);
    };

    showListView = () => {
        if (this.props.locationListView) {
            // alert('Sorry, we do not deliver to this location currently. we will notify you when we start!!')
            this.props.flagLocationChange(true);
            this.props.history.replace("user_locations");
        } else {
            if (this.props.isLocationFlag) {
                this.props.checkUserLocation(false);
                this.props.history.replace("prescription");
            } else {
                this.props.flagLocationChange(false);
                window.history.length > 2 ? history.back() : this.props.history.replace("prescription");
                // history.back()
                // this.props.history.replace('cart')
            }
        }
    };

    backToSearchLocation = () => {
        this.setState({ curOrigin: "GooglePlace" });
        Clevertap.event("mWeb - Address Add By", { origin: "GooglePlaceSearch" });
        this.props.addressUpdatedAndConfirmed();
    };

    handleChangeUserLocation = value => {
        let order = this.props.cartList;
        order.query = { context: "home", sub_context: "address_change", source: "web_manual" };
        this.props.getAvailabilityInfo(this.props.cartList, { queryString: `?location_id=${value.id}`, location: value }).then(x => {
            window.history.length > 2 ? history.back() : this.props.history.push("/page/home");
        });
    };

    handleShowAllAddress = () => {
        this.setState({ addressCount: this.props.availLocation.length });
    };

    render() {
        return (
            <div className="full_width">
                {this.props.isGEOSuccess && !this.props.isFetching ? (
                    this.props.formatedAddress ? (
                        <AddAndUpdateAddress
                            formatedAddress={this.props.formatedAddress}
                            origin={this.state.curOrigin}
                            userInfo={this.props.userInfo}
                            googleAddress={this.props.googleLocation}
                            handleUpdateAddress={this.updateAddressHandler}
                        />
                    ) : (
                        <AllLocationsList
                            changeLocation={this.props.route ? this.handleChangeUserLocation : this.changeLocation}
                            backToSearchLocation={this.backToSearchLocation}
                            unAvailLocation={this.props.unAvailLocation}
                            availLocation={this.props.availLocation}
                            isChangeLocation={this.props.route ? true : this.props.isChangeLocation}
                            route={this.props.route}
                            editLocation={this.editLocation}
                            locationListView={this.props.locationListView}
                            googleAddressList={this.props.googleAddressList ? this.props.googleAddressList : []}
                            geoError={this.props.geoError}
                            askForLocation={this.getLocation}
                            handleShowAllAddress={this.handleShowAllAddress}
                            addressCount={this.state.addressCount}
                            handleShowAllAddress={this.handleShowAllAddress}
                        />
                    )
                ) : (
                    <Loader />
                )}
            </div>
        );
    }
}

export default connect(
    ({ addGEOLocation, cart, users }) => ({
        isFetching: addGEOLocation.isFetching,
        geoError: addGEOLocation.geoError,
        unAvailLocation: addGEOLocation.unAvailLocation,
        availLocation: addGEOLocation.availLocation,
        isGEOSuccess: addGEOLocation.isGEOSuccess,
        isChangeLocation: addGEOLocation.isChangeLocation,
        locationListView: addGEOLocation.locationListView,
        formatedAddress: addGEOLocation.formatedAddress,
        googleLocation: addGEOLocation.googleLocation,
        route: cart.route,
        cartList: cart.cartList,
        userInfo: users.userInfo,
    }),
    dispatch =>
        bindActionCreators(
            {
                ...locationActionCreater,
                handleGetActiveLocation,
                getAvailabilityInfo,
            },
            dispatch
        )
)(LocationListContainer);
