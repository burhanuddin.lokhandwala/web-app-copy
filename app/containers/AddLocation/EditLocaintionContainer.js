import React, { Component}        from 'react'
import { connect }                from 'react-redux'
import { bindActionCreators }     from 'redux'
import EditLocation               from '$COMPONENTS/AddLocation/EditLocation'
import Loader                     from '$PARTIALS/Loader/Loader'
import * as locationActionCreater from '$REDUX/modules/addGEOLocation'

class EditLocaintionContainer extends Component{
	constructor(props) {
	  super(props);

	  this.state = {
			updateLabelTimer: '',
			updateNameTimer: '',
			current_edit_location: props.current_edit_location,
			isLocationName: props.current_edit_location.name ? true : false,
	  };

	}

	componentDidMount(){
		if(!this.props.current_edit_location){
			history.back()
		}
	}

	componentWillReceiveProps = (nextProps)  => {
		this.setState({current_edit_location: nextProps.current_edit_location})
	}

	updateAddressLabel = (e) => {
		let value = e.target.value
		let newLocation = JSON.parse(JSON.stringify(this.state.current_edit_location))
		newLocation.label = value
		// this.props.currentEditLocation(newLocation)
		clearTimeout(this.state.updateLabelTimer ? this.state.updateLabelTimer : null)
		let updateLabelTimer = setTimeout(function() {
			this.props.handleEditLocation({label: value}, newLocation.id)
		}.bind(this), 1000)

		this.setState({updateLabelTimer: updateLabelTimer, current_edit_location: newLocation})

	}

	updateAddressName = (e) => {
		let value = e.target.value
		let newLocation = JSON.parse(JSON.stringify(this.state.current_edit_location))
		newLocation.name = value
		// this.props.currentEditLocation(newLocation)

		clearTimeout(this.state.updateNameTimer ? this.state.updateNameTimer : null)
		let updateNameTimer = setTimeout(function() {
			this.props.handleEditLocation({name: value}, newLocation.id)
		}.bind(this), 1000)

		this.setState({updateNameTimer: updateNameTimer, current_edit_location: newLocation})
	}

	handleDeleteLocation = () => {
		if(confirm("Are you sure you want to delete this location")){
			this.props.handleDeleteLocation(this.props.current_edit_location.id).then(x => {
				if(x){
					history.back()
				}else{
					alert("Something wrong happened please try again!!")
				}
			})
		}
	}

	render() {
		return (
			<EditLocation
				current_edit_location = {this.state.current_edit_location}
				handleDeleteLocation = {this.handleDeleteLocation}
				updateAddressLabel = {this.updateAddressLabel}
				updateAddressName = {this.updateAddressName}
				isLocationName = {this.state.isLocationName}
			/>
		)
	}
}

export default connect(
	({addGEOLocation}) => ({
		isFetching            : addGEOLocation.isFetching,
		current_edit_location : addGEOLocation.current_edit_location,
	}),
	(dispatch) => bindActionCreators(locationActionCreater, dispatch)
)(EditLocaintionContainer)