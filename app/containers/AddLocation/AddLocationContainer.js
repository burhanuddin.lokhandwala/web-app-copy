import React, { Component } from "react";
import AddLocation from "$COMPONENTS/AddLocation/AddLocation";
import AddAndUpdateAddress from "$COMPONENTS/AddLocation/AddAndUpdateAddress";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { askForUserGEOLocation } from "$UTILS/googleLocationApi";
import * as locationActionCreater from "$REDUX/modules/addGEOLocation";
import googleAutoComplete, { getPlaceInfo } from "$UTILS/googleAutoComplete";
import { checkUserLocation } from "$REDUX/modules/users";

class AddLocationContainer extends Component {
    static contextTypes = {
        //router: React.PropTypes.object.isRequired,
    };
    componentDidMount() {
        this.props.addressUpdatedAndConfirmed();
    }
    getLocation = () => {
        askForUserGEOLocation(this.addLocation, this.props.setUserGEOLocationFailur, this.showListView);
    };

    showListView = () => {
        debugger;
        if (this.props.locationListView) {
            // alert('Sorry, we do not deliver to this location currently. we will notify you when we start!!')
            this.props.flagLocationChange(true);
            this.props.history.replace("user_locations");
        } else {
            if (this.props.isLocationFlag) {
                this.props.checkUserLocation(false);
                this.props.history.replace("prescription");
            } else {
                this.props.flagLocationChange(false);
                window.history.length > 2 ? history.back() : this.props.history.replace("prescription");
                // history.back()
                // this.props.history.replace('cart')
            }
        }
    };

    locationAutoComplete = e => {
        if (!e.target.value) {
            return;
        }
        let value = e.target.value,
            timer = this.state && this.state.timer ? this.state.timer : "";
        clearTimeout(timer);
        timer = setTimeout(
            function() {
                googleAutoComplete(value, this.props.autoCompleteList);
            }.bind(this),
            1000
        );

        this.setState({ timer: timer });
    };

    addLocation = loc => {
        // debugger
        // this.props.addLocation(loc)

        this.props.handleGetLocationFormet(loc);
    };

    changeAddress = item => {
        console.log("item", item);
        getPlaceInfo(item.place_id, this.addLocation, this.showListView);
    };

    updateAddressHandler = queryJson => {
        let loc = this.props.addressDetail.lat_long_info;
        queryJson = {
            latitude: loc.latitude,
            longitude: loc.longitude,
            google_place_id: this.props.googleLocation.place_id,
            ...queryJson,
        };
        this.props.handleUpdateAddress(queryJson, this.showListView);
    };

    render() {
        return (
            <div>
                {this.props.isGEOSuccess && (this.props.locationListView || this.props.isUpdateLocation) ? (
                    this.props.locationListView ? null : (
                        <AddAndUpdateAddress
                            formatedAddress={this.props.formatedAddress}
                            userInfo={this.props.userInfo}
                            handleUpdateAddress={this.updateAddressHandler}
                        />
                    )
                ) : (
                    <AddLocation
                        changeAddress={this.changeAddress}
                        askForLocation={this.getLocation}
                        locationAutoComplete={this.locationAutoComplete}
                        googleAddressList={this.props.googleAddressList ? this.props.googleAddressList : []}
                        geoError={this.props.geoError}
                    />
                )}
            </div>
        );
    }
}

export default connect(
    ({ addGEOLocation, users }) => ({
        isFetching: addGEOLocation.isFetching,
        geoError: addGEOLocation.geoError,
        isGEOSuccess: addGEOLocation.isGEOSuccess,
        locationListView: addGEOLocation.locationListView,
        unAvailLocation: addGEOLocation.unAvailLocation,
        availLocation: addGEOLocation.availLocation,
        isUpdateLocation: addGEOLocation.isUpdateLocation,
        googleAddressList: addGEOLocation.googleAddressList,
        formatedAddress: addGEOLocation.formatedAddress,
        googleLocation: addGEOLocation.googleLocation,
        addressDetail: addGEOLocation.addressDetail,
        isLocationFlag: users.isLocationFlag,
        userInfo: users.userInfo,
    }),
    dispatch =>
        bindActionCreators(
            {
                ...locationActionCreater,
                checkUserLocation,
            },
            dispatch
        )
)(AddLocationContainer);
