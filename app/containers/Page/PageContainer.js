import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getLocalstorageValue } from "../../helpers/helperFunction";
import getPageLayout from "../../utils/layoutServiceApi";
import Page from "../../components/Page/Page";
import Grid from "../../components/Grid/Grid";
import Image from "../../components/Image/Image";
import SkuListContainer from "../SkuList/SkuListContainer";
import { handleUpdateMedicineCount } from "../../redux/modules/cart";
import ImageScroller from "../../components/Image/ImageScroller";

class PageContainer extends Component {
    constructor(props) {
        super(props);
        let nearestFcId = this.getIdOfFcNearestToUsersLocation(
            props.userActiveLocation
        );
        this.state = {
            pageLayout: null,
            nearestFcId: nearestFcId,
        };
    }

    getIdOfFcNearestToUsersLocation(usersLocation) {
        if (usersLocation && usersLocation["nearest_fc_id"]) {
            return usersLocation["nearest_fc_id"];
        }
        usersLocation = getLocalstorageValue("userLocation")||1;
        return usersLocation.nearest_fc_id||1;
    }

    fetchAndStorePageLayout(pageNameOrID, nearestFcId) {
        getPageLayout(pageNameOrID, nearestFcId).then(pageLayoutData => {
            this.setState({ pageLayout: pageLayoutData.data });
        });
    }

    componentDidMount() {
        let pageNameOrID = this.props.match.params.pageNameOrID;
        let nearestFcId = this.getIdOfFcNearestToUsersLocation(
            this.props.userActiveLocation
        );
        this.fetchAndStorePageLayout(pageNameOrID, nearestFcId);
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.match.params.pageNameOrID !==
            this.props.match.params.pageNameOrID
        ) {
            let pageNameOrID = this.props.match.params.pageNameOrID;
            let nearestFcId = this.getIdOfFcNearestToUsersLocation(
                this.props.userActiveLocation
            );
            this.fetchAndStorePageLayout(pageNameOrID, nearestFcId);
        }
    }

    getComponentsFromPageLayout(layout) {
        let componentTree = [];
        let components =
            layout && layout["page"] && layout["page"]["components"]
                ? layout["page"]["components"]
                : [];

        for (const [index, component] of components.entries()) {
            if (component["component_type"]) {
                let componentType = component["component_type"];
                let props = {
                    ...component["attributes"],
                    key: index,
                };

                switch (componentType) {
                    case "grid":
                        props["sub_components"] = component["sub_components"];
                        componentTree.push(<Grid {...props} />);
                        break;

                    case "image":
                        componentTree.push(<Image {...props} />);
                        break;

                    case "image_scroller":
                        props["sub_components"] = component["sub_components"];
                        componentTree.push(<ImageScroller {...props} />);
                        break;

                    case "sku_listing":
                        props.componentID = component["id"];
                        props.subComponents = component["sub_components"];
                        props.handleUpdateMedicineCount = this.props.handleUpdateMedicineCount;
                        componentTree.push(<SkuListContainer {...props} />);
                        break;
                }
            }
        }
        return componentTree;
    }

    render() {
        let components = this.state.pageLayout
            ? this.getComponentsFromPageLayout(this.state.pageLayout)
            : [];

        if (components) {
            return (
                <div className="page-wrapper">
                    <Page children={components} />
                </div>
            );
        }
        return null;
    }
}

export default connect(
    ({ cart }) => ({
        userActiveLocation: cart.userActiveLocation,
    }),
    dispatch =>
        bindActionCreators(
            {
                handleUpdateMedicineCount,
            },
            dispatch
        )
)(PageContainer);
