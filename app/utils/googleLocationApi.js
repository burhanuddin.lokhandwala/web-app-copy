// Google search location APis  AIzaSyDIgiRD9rJ-0Z_3CyAAPVPcBzjvv819hYI


export function askForUserGEOLocation(success, failur, showListView){
	/**
	 * [displayLocation will get user's current GEO let, long and will get all detailed user address]
	 * @param  {float} latitude  [user's current GEO latitude]
	 * @param  {float} longitude [user's current GEO longitude]
	 * @return {[type]}           [description]
	 */
	var timeout = "", watchID
	function displayLocation(latitude, longitude) {
		let request = new XMLHttpRequest();
		// alert(latitude+"  aa  "+ longitude)
		let method = 'GET';
		let url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true&key=AIzaSyDIgiRD9rJ-0Z_3CyAAPVPcBzjvv819hYI';
		let async = true;

		request.open(method, url, async);
		request.onreadystatechange = function() {
			
			if (request.readyState == 4 && request.status == 200) {
				console.log("debug code", request)
				let data = JSON.parse(request.responseText);
				let address = data.results[0];
				
				address && address.geometry && address.geometry.location ?
					success({LatLng: (address.geometry.location ) , fullAddress: address}, showListView)
					: failur("Unable to fetch location!")
				// console.log(address.formatted_address, request, address);
			}
		};
		request.send();
	};

	/**
	 * [@callback successCallback call after navigator.geo function become successfull ]
	 * @param  {[object]} position [have coordinate details]
	 * @return {[type]}          [description]
	 */
	var successCallback = function(position) {
		console.log("s", timeout)
		navigator.geolocation.clearWatch( watchID )
		let x = position.coords.latitude;
		let y = position.coords.longitude;
		displayLocation(x, y);
	};

	/**
	 * [@callback errorCallback call if navigator.gro function through error]
	 * @param  {[type]} error [error detail]
	 * @return {[type]}       [description]
	 */
	function errorCallback(error) {
		console.log("error", error)
		// alert(error.message)
	  var errorMessage = 'Unknown error';
		switch (error.code) {
		case 1:
			errorMessage = 'Cannot find your current location, please turn location services on';
			break;
		case 2:
			errorMessage = 'Position unavailable';
			break;
		case 3:
			errorMessage = 'Unknown error while trying to get location';
			break;
		}
		failur(errorMessage)
		// LocationAPIResult(errorMessage)
		console.log(errorMessage, error)
	};

	var options = {
		enableHighAccuracy: true,
		maximumAge: 100,
		timeout: 80000,
	};

	/**
	 * google function to get user's current GEO location
	 */
	if( navigator.geolocation) {
		watchID = navigator.geolocation.watchPosition( successCallback, errorCallback, options );
		// timeout = setTimeout(() => { navigator.geolocation.clearWatch( watchID ); }, 5000 );
	} else {
		errorCallback();
	}

	// navigator.geolocation.getCurrentPosition(successCallback, errorCallback, options)
}