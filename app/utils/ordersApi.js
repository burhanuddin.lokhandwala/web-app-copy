import {Update, Get, Delete, Post} from '../helpers/apiUtils'

export  function getRecomendedItems(data) {
	return Get({
		url : `user/order_items/v2?&location_id=${data.location_id}&fc_id=${data.fc_id}`,
	})
}

export  function uploadImageAction(data, callType, rotation) {
	let key = Object.keys(callType)[0]
	let queryJson = {'prescription_image' : data}
	queryJson[key] = callType[key]
	return Update({
		url : 'user/orders/',
		payload : queryJson,
	})
}

export default function getCurrentOrder() {
	return Get({
		url : 'user/orders/current/',
	})
}

export function deleteImageHandler(data) {
	return Delete({
		url : 'user/orders/' + data.orderId + '/prescription/' + data.pId + '/',
	})
}

export function getOrderHandler(orderId) {
	return Get({
		url : 'user/orders/' +orderId ,
	})
}

export function getCategoriesList(argument) {
	return Get({
		removeAuth: true,
		headers: {'Content-Type': "application/x-www-form-urlencoded"},
		url: 'https://ccm.myramed.in/properties/TopCategories',
		thirdPartyApi: true
	})
}

export function trackOrderDetails(orderId) {
	return Get({
		removeAuth: false,
		url : 'tracker/33b9dbb2-51ee-11e8-9c2d-fa7ae01bbebc/order/' + orderId + '/details',
	})
}