import {Get, Post, Put} from '../helpers/apiUtils'

export function getUserLocations () {
	return Get({
		url : 'user/locations/',
	})

}

export function updateBillingLocation(orderId, addressId) {
	return Post({
		url : 'user/orders/pending',
		payload: {"orders": {"billing_location_id": addressId}},
	})
}

/**
 * [updateOrderHandler Api request for request call]
 * @param  {string} orderId     [description]
 * @param  {Boolean} requestFlag [description]
 * @return {string}             [description]
 */
export function updateOrderHandler(orderId, queryJson) {
	debugger
	return Post({
		url : 'user/v2/orders/pending',
		payload: queryJson,
	})
}

/**
 * [quickOrderHandler quick place order api]
 * @param  {string} orderId [description]
 * @param  {string} note    [description]
 * @return {[type]}         [description]
 */
export function quickOrderHandler(queryJson, note) {
	return Post({
		url : `user/v2/orders/${queryJson.orderId}/quick_checkout?source=web`,
		payload: note,
	})
}


/**
 * [scheduleOrderHandler schedule order when store is closed]
 * @param  {[type]} id   [order id]
 * @param  {[type]} data [time when to schedule order]
 * @return {[type]}      [description]
 */
export function scheduleOrderHandler(orderId, data) {
	return Post({
		url : 'user/orders/' + orderId + '/schedule?source=web',
		payload: data,
	})
}
