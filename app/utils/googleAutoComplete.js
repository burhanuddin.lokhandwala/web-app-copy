// Google auto complete function for locations

/**
 * [googleComplete to get the list of locations from google APIs]
 * @param  {[string]} value [keyword to search the location]
 * @param  {[function]} callback [is a callback to update location list]
 * @return {[type]}       [description]
 */
export default function googleAutoComplete(value, callback) {
  let geocoder = new google.maps.Geocoder();
	let service = new google.maps.places.AutocompleteService();
	service.getPlacePredictions({
			input: value,
			componentRestrictions: {country: "in"},
		},function(autocompleteResults, status) {
			callback(autocompleteResults, status)
	})
}

export function getPlaceInfo(place_Id, callback, showListView) {
	let place = new google.maps.places.PlacesService(document.getElementById('google-maps-places'))
	place.getDetails({
	      'placeId': place_Id
	}, function (place, status) {
		callback({LatLng: {lat:place.geometry.location.lat(), lng: place.geometry.location.lng()}, fullAddress: place, status: status}, showListView)
	})
}