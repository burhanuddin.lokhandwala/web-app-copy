
import { Get } from '../helpers/apiUtils'

export default function getPageLayout(pageNameOrID, nearest_fc_id) {
	let pageID = Number(pageNameOrID);
	if (pageID) {
		return Get({
			url: `app_layout_service/pages/${pageNameOrID}/details`
		});
	}

	return Get({
		url: `app_layout_service/page_listings/page/${pageNameOrID}/?fc_id=${nearest_fc_id||1}&os_type=2&build_number=21`
	});
}

export function getComponentDetails(componentID) {
	return Get({
		url: `app_layout_service/components/${componentID}/details/?location_id=530989&fc_id=1&os_type=2&utc_seconds=19800`
	});
}
