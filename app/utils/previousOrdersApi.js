import {Get, Post} from '../helpers/apiUtils'

export default function getPreviousOrder(url) {
	return Get({
		url :'user/orders/v3/previous/',
	})
}

export function handleReorder(data) {
	return Post({
		url : 'user/orders/'+ data.id +'/reorder/',
		payload: data.force ? {"force": true} : "",
	})
}

export  function handleReorderItem(data) {
	return Post({
		url : 'user/orders/pending',
		payload : {"orders": {  "medicines": [ {"id": data.id, "quantity": data.quantity} ] }},
	})
}

export function walletInfo(orderId) {
	return Get({
		url : `orders/${orderId}/charges` ,
	})
}
