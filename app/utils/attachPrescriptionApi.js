import {Update, Get, Delete, Post} from '../helpers/apiUtils'

export function updatePrescription(data, token) {
	return Update({
		url: 'users/orders/prescriptions',
		payload : {'photos': data},
		headers: {Authorization: 'OTAuth ' + token},
	})
}

export function submitPrescription(token) {
	return Post({
		url : 'users/orders/submit_prescription',
		headers: {Authorization: 'OTAuth ' + token},
	})
}
