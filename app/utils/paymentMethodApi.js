import {Get, Post, Update} from '../helpers/apiUtils'

export function getPaymentMethodList(displayId) {
	return Get({
		url: `user/orders/payment_method/${displayId}`,
		removeAuth: true,
	})
}

export function updatePaymentMethodList(displayId, queryJson) {
	return Post({
		url : `user/orders/payment_method/${displayId}`,
		payload: queryJson,
		removeAuth: true,
	})
}

export function getPaytmTransitionParam(displayId) {
	return Post({
		url : `payments/paytm/checksum/${displayId}`,
		removeAuth: true,
	})
}

export function paytmSuccessOrder(queryJson) {
	return Post({
		url : 'paytm/success',
		payload: queryJson,
	})
}

export function getRazorPayTransitionParam(queryJson, displayId) {
	return Post({
		url : `payments/razorpay/transaction_id/${displayId}`,
		removeAuth: true,
		payload: queryJson,
	})
}

export function razorPaySuccessOrder(queryJson) {
	return Post({
		url : 'razorpay/app_success',
		removeAuth: true,
		payload: queryJson,
	})
}

export function juspayPayTransitionParam(queryJson,displayId){
	return Post({
		url : `payments/orders/web/${displayId}`,
		removeAuth: true,
		payload: queryJson,
	})
}

export function paymentStatus(displayId){
	return Get({
		url : `payments/orders/web/${displayId}/status`,
		removeAuth: true,
	})
}

export function juspaySuccessOrder(queryJson){
	return Post({
		url: 'juspay/app_success',
		removeAuth:true,
		payload: queryJson
	})
}