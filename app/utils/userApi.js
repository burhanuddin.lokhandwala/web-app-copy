
import {Update, Get, Delete, Post} from '../helpers/apiUtils'

export function updateOrderHandler(queryJson) {
	// debugger
	return Post({
		url : 'user',
		payload: queryJson,
	})
}

export function handleCallMeNow(queryJson) {
	return Post({
		url : 'login',
		payload: queryJson,
	})
}

export function updateWalletUser(queryJson) {
	return Post({
		url:'gift_cards',
		payload: queryJson
	})
}

export function getAllTransactions(){
	return Get({
		url:'gift_cards'
	})
}

