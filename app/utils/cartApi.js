import {Get, Post, Delete} from '../helpers/apiUtils'

export default function getCartOrder (token) {
	let header = token ? {headers: {Authorization: 'MRAuth ' + token}} : {}
	return Get({
		...header,
		url : 'user/v2/orders/pending',
	})

}

// export function getUserLocations () {
// 	return Get({
// 		url : 'user/locations/',
// 	})

// }

export function promoCodeHandler(code) {
	return Post({
		url : 'user/promo_codes/' + code,
	})
}

/**
 * [updateMedicineQuantity update the medicine quantity]
 * @param  {string} orderId [description]
 * @param  {string} quantity    [description]
 * @return {[type]}         [description]
 */
export function updateMedicineQuantity(queryJson, data) {
	// debugger
	return Post({
		url : `user/v2/orders/pending?location_id=${data.location_id}&fc_id=${data.fc_id||1}`,
		payload: queryJson
	})
}

/**
 * [removeOrderItem delete the medicine ]
 * @param  {string} orderId [description]
 * @param  {string} itemId    [description]
 * @return {[type]}         [description]
 */
export function removeOrderItem(orderId, itemId) {
	return Delete({
		url : 'user/orders/' + orderId + '/items/' + itemId,
	})
}

export function searchHandler(txt, queryJson) {
	return Get({
		url : `medicines/search_medicine/${txt}?quick=true`,//&user_id=${info.userID}&location_id=${info.location_id}&fc_id=${info.fc_id}
		payload: queryJson,
		removeAuth: true
	})
}

export function searchHandler2(txt, queryJson) {
	return Get({
		url : `medicines/search/${txt}?quick=true`,//&user_id=${info.userID}&location_id=${info.location_id}&fc_id=${info.fc_id}
		payload: queryJson
	})
}


// export function updateOrder(orderId, queryJson) {
// 	debugger
// 	return Post({
// 		url : 'user/orders/pending',
// 		payload: queryJson,
// 	})
// }

export function getBulkMedAvailability(queryJson) {
	// debugger
	return Post({
		url : 'medicines/v2/bulk_availability',
		payload: queryJson,
	})
}

export function getMedAvailability(queryJson) {
	// debugger
	return Get({
		url : `medicines/availability`,
		payload: queryJson
	})
}


export function getDynamicText(argument) {
	return Get({
		removeAuth: true,
		headers: {'Content-Type': "application/x-www-form-urlencoded"},
		url: 'https://ccm.myramed.in/properties/load_properties,checkout_properties,delivery_buckets',
		thirdPartyApi: true
	})
}

export function getCartRecommendation(queryString) {
	return Get({
		url: `https://supply.myramed.in/recommendations/${queryString.orderId}/?dv_ids=${queryString.dv_ids ? queryString.dv_ids : 14858}&fc_id=${queryString.fc_id||1}`,
		thirdPartyApi: true
	})
}

export function getCartCharges(queryString) {
	return Get({
		url: `orders/${queryString.orderId}/charges?location_id=${queryString.location_id}&fc_id=${queryString.fc_id||1}`,
	})
}

export function getUserActiveLocation(user_id, getUserActiveLocation) {
	return Get({
		url: `user/${user_id}/active_location${getUserActiveLocation ? getUserActiveLocation : ""}`
	})
}

export function getSchedulingTime(queryString) {
	return Get({
		url: `time-slot-service/slot_availability/details/?${queryString}`
	})
}