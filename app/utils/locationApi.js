import {Get, Post, Delete} from '../helpers/apiUtils'

export function confirmAndAddLocation(queryJson) {
	debugger
	return Post({
		url : 'user/v2/locations/',
		payload: queryJson,
	})
}

// export function checkLocation(location) {
// 	return Get({
// 		url : 'check_location?ll='+ location.lat + ',' + location.lng,
// 	})
// }

export function getUserLocations() {
	return Get({
		url : 'user/locations/',
	})
}

export function updateAndAddLocation(data) {
	return Post({
		url : 'user/locations/' + data.id,
		payload: data.queryJson,
	})
}

export function changeLocation (queryJson) {
	return Post({
		url : 'user/locations/',
		payload: queryJson,
	})
}

export function deleteLocationHandler(id) {
	return Delete({
		url : 'user/locations/' + id,
	})
}

export function getLocationFormet(location_id) {
	return Get({
		url: `locations/${location_id}/location_info`
	})
}