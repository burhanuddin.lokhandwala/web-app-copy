import otpRequest from '../../helpers/auth'
import getCurrentOrder, {getOrderHandler, getRecomendedItems, getCategoriesList} from '../../utils/ordersApi'
import { updateLocalstorage, getLocalstorageValue, isLocalStorageSupported} from '../../helpers/helperFunction'

const UPDATE_IMAGE = 'UPDATE_IMAGE'
const FETCHING_CURRENT_ORDER = 'FETCHING_CURRENT_ORDER'
const FETCHING_CURRENT_ORDER_SUCCESS = 'FETCHING_CURRENT_ORDER_SUCCESS'
const FETCHING_CURRENT_ORDER_FAILURE = 'FETCHING_CURRENT_ORDER_FAILURE'
const FETCHING_ORDER = 'FETCHING_ORDER'
const FETCHING_ORDER_SUCCESS = 'FETCHING_ORDER_SUCCESS'
const FETCHING_ORDER_FAILURE = 'FETCHING_ORDER_FAILURE'
const HANDLE_REORDER_FLAG = 'HANDLE_REORDER_FLAG'
const FETCHING_RECOMENDED_ITEMS = 'FETCHING_RECOMENDED_ITEMS'
const FETCHING_RECOMENDED_ITEMS_SUCCESS = 'FETCHING_RECOMENDED_ITEMS_SUCCESS'
const FETCHING_RECOMENDED_ITEMS_FAILURE = 'FETCHING_RECOMENDED_ITEMS_FAILURE'
const RESET_STORE = 'RESET_STORE'

const GET_CATEGORY_LIST = 'GET_CATEGORY_LIST'
const GET_CATEGORY_LIST_SUCCESS = 'GET_CATEGORY_LIST_SUCCESS'
const GET_CATEGORY_LIST_FAILURE = 'GET_CATEGORY_LIST_FAILURE'
const UPDATE_CATEGORY_NAME = 'UPDATE_CATEGORY_NAME'

export function resteOrderStore(argument) {
	return {
		type: RESET_STORE,
	}
}

function fetchingCurrentOrder() {
	return{
		type: FETCHING_CURRENT_ORDER,
	}
}

function fetchingCurrentOrderSuccess(order) {
	return{
		type: FETCHING_CURRENT_ORDER_SUCCESS,
		currentOrderList: order.orders,
		processingPipeline: order.processing_pipeline,
	}
}

function fetchingCurrentOrderFailure(error) {
	return{
		type: FETCHING_CURRENT_ORDER_FAILURE,
		error: error,
	}
}

function fetchingRecomendedItems() {
	return{
		type: FETCHING_RECOMENDED_ITEMS,
	}
}

function fetchingRecomendedItemsSuccess(items) {
	return{
		type: FETCHING_RECOMENDED_ITEMS_SUCCESS,
		recomendedItemsList: items,
	}
}

function fetchingRecomendedItemsFailure(error) {
	return{
		type: FETCHING_RECOMENDED_ITEMS_FAILURE,
		error: error,
	}
}

function fetchingOrder() {
	return{
		type: FETCHING_ORDER,
	}
}

function fetchingOrderSuccess(order) {
	return{
		type: FETCHING_ORDER_SUCCESS,
		currentOrder: order.orders,
	}
}

function fetchingOrderFailure(error) {
	return{
		type: FETCHING_ORDER_FAILURE,
		error: error,
	}
}

function fetchCategoryList() {
	return{
		type: GET_CATEGORY_LIST,
	}
}

function fetchCategoryListSuccess(categoryList) {
	return{
		type: GET_CATEGORY_LIST_SUCCESS,
		categoryList: categoryList,
	}
}

function fetchCategoryListFailure(error) {
	return{
		type: GET_CATEGORY_LIST_FAILURE,
		error: error,
	}
}

export function updateImage(imgValue) {
	return{
		type: UPDATE_IMAGE,
		imgValue: imgValue,
	}
}

export function updateCategoryName(categoryName) {
	return{
		type: UPDATE_CATEGORY_NAME,
		categoryName: categoryName,
	}
}

export function handleReorderFlag (showReorder) {
	return {
		type: HANDLE_REORDER_FLAG,
		showReorder: showReorder,
		isCurrentOrder: !showReorder,
	}
}

export function getCategoryList(argument) {
	return function(dispatch){
		return getCategoriesList().then(resp => {
			dispatch(fetchCategoryListSuccess(resp.data.TopCategories.value))
		})
		.catch((error) => {
			console.log('error', error)
			dispatch(fetchCategoryListSuccess([{"clickActionUrl": "b", "name": "Baby Products", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/category-baby-products.png", "search_bar_text": "Search for Baby products", "enabled": true, "imageOverlayText": "Baby Products", "sub_categories": [{"header_title": "Best selling Baby Diapers", "name": "Diapers", "title": "Baby Diapers", "enabled": true, "expanded": true, "short_desc": "Pampers, Mamy Poko Pants, Huggies & more", "products": [{"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/mamy-poko-+pants.jpg", "name": "Mamy Poko Pants"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/pampers.jpg", "name": "Pampers"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/huggies.jpg", "name": "Huggies"}], "search_text": "Baby Diapers"}, {"name": "Baby Wipes", "title": "Baby Wipes", "enabled": true, "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/baby-wipes.jpg", "expanded": false, "short_desc": "Johnson's, Pigeon, Huggies & more", "rank": 1, "search_text": "Baby wipes"}, {"name": "Baby Food", "title": "Baby Food", "short_desc": "Nan Pro, Lactogen & more", "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/nanpro.jpg", "expanded": false, "enabled": true, "search_text": "Baby Food"}, {"name": "Lotion", "title": "Baby Lotion", "short_desc": "Johnsons, Himalaya, Sebamed & more", "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/baby-lotion.png", "expanded": false, "enabled": true, "search_text": "Baby Lotion"}, {"name": "Gripe water", "title": "Gripe water", "short_desc": "Woordwards, Dabur & more", "image": "https://s3-ap-southeast-1.amazonaws.com/myra-images-test/s-l300.jpg", "expanded": false, "enabled": true, "search_text": "Gripe water"}, {"header_title": "Top Selling Baby Foods", "name": "Baby Food", "title": "Baby Food", "enabled": true, "expanded": true, "short_desc": "Cerelac, Farex & more", "products": [{"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/baby-food.jpg", "name": "Cerelac"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/farex.jpeg", "name": "Farex"}], "search_text": "Baby Food"}], "categoryTileText2": "Diapers, Baby Food, Baby Bath, Baby Oils, Baby Skin & more", "categoryTileText": "Baby Diapers, Soaps & more", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/baby_bath_natureloc.png"}, {"clickActionUrl": "na", "header_title": "Sanitary pads, Tampons & more", "name": "Women Health Care", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Women_1.png", "search_bar_text": "Search for Women health care products", "enabled": true, "imageOverlayText": "Women Care", "sub_categories": [{"header_title": "Popular Sanitary Napkins", "name": "Sa", "title": "Popular Sanitary Napkins", "enabled": true, "expanded": true, "short_desc": "not applicable", "products": [{"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/whisper-pads-1.jpeg", "name": "Whisper"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/stayfree.jpeg", "name": "Stayfree"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/soft-pads.jpeg", "name": "Sofy"}], "search_text": "Sanitary pads"}, {"name": "Tampons", "title": "Tampons", "short_desc": "O.B & more", "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/tampon.jpeg", "expanded": false, "enabled": true, "search_text": "Tampons"}, {"name": "Intimate Care", "title": "Intimate Care", "short_desc": "V Wash plus & more", "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/v-wash.jpeg", "expanded": false, "enabled": true, "search_text": "v wash"}, {"header_title": "Maternity care", "name": "Sa", "title": "Maternity care", "enabled": true, "expanded": true, "short_desc": "Breast pumps & more", "products": [{"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/nipple-care.png", "name": "Nipple care"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/horlicks-chocolate-flavour-tetra-pack.jpg", "name": "Mothers Horlicks"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Health_supplements_1.jpeg", "name": "Vitamin Tablet"}], "search_text": "breast"}, {"name": "Intimate Care", "title": "Intimate Care", "short_desc": "V Wash plus & more", "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/v-wash.jpeg", "expanded": false, "enabled": false, "search_text": "v wash"}], "categoryTileText2": "Sanitary Napkins, Tampons, Menstrual Cups, V-wash & more", "categoryTileText": "Sanitary pads, Tampons", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/women-category.png"}, {"clickActionUrl": "b", "name": "Senior Care", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-broader-tile-image.jpg", "search_bar_text": "Search for senior care products", "enabled": true, "imageOverlayText": "Senior Care", "sub_categories": [{"header_title": "Best selling Adult Diapers", "name": "Diapers", "title": "Adult Diapers", "enabled": true, "expanded": true, "short_desc": "Friends, Kare, Seni & more", "products": [{"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-friends-diapers.jpg", "name": "Friends"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-kare-diapers.jpg", "name": "Kare"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-seni-diapers.jpg", "name": "Seni"}], "search_text": "Adult Diapers"}, {"name": "Devices", "title": "Devices", "short_desc": "Accu Chek, Omron BP monitor, Nebulisers & more", "image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-device.png", "expanded": false, "enabled": true, "search_text": "Accu Chek"}, {"header_title": "Others", "name": "Others", "title": "Hot Water bags", "enabled": true, "expanded": true, "short_desc": "Hot Water Bags, Heat Belts & more", "products": [{"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-hot-water-bags.jpg", "name": "Hot Water Bags"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-heat-pack.jpg", "name": "Heat belts"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-knee-caps.jpg", "name": "Knee Cap"}], "search_text": "Belt"}], "categoryTileText2": "Adult Diapers, Devices & more", "categoryTileText": "Adult Diapers & more", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sc-category-banner.png"}, {"clickActionUrl": "b", "name": "Health Drinks", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Health+Drink+Tile_Original.jpg", "search_bar_text": "Search for Health Drinks", "enabled": true, "imageOverlayText": "Health Drinks", "sub_categories": [{"header_title": "Popular searches", "name": "horlicks", "title": "Health Drinks", "enabled": true, "expanded": true, "short_desc": "Health Drinks", "products": [{"name": "Boost"}, {"name": "Horlicks"}, {"name": "Pediasure"}, {"name": "Bournvita"}, {"name": "Complan"}, {"name": "Ensure"}, {"name": "Protinex"}, {"name": "GRD"}], "search_text": "Horlicks"}], "categoryTileText2": "Boost, Horlicks & more", "categoryTileText": "Health Drinks", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Health+Drink+Tile_HomeScreen.png"}, {"clickActionUrl": "b", "name": "Diabetes Care", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Diabetes_2.jpg", "search_bar_text": "Search for Diabetic Medicines", "enabled": true, "imageOverlayText": "Diabetes Care", "sub_categories": [{"header_title": "Popular searches", "name": "Diabetes", "title": "Diabetes Care", "enabled": true, "expanded": true, "short_desc": "For Diabetes Patients", "products": [{"name": "Human Mixtard"}, {"name": "Accu Chek"}, {"name": "Galvus Met"}, {"name": "Novofine Needle"}, {"name": "Glycomet"}, {"name": "Lantus"}, {"name": "Prolomet"}, {"name": "Novomix"}], "search_text": "diabetes"}], "categoryTileText2": "Glucometers, Insulins & Accu chek", "categoryTileText": "Diabetes", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Diabetes_2.jpg"}, {"clickActionUrl": "b", "name": "Blood Pressure Care", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/blood-pressure-category.jpg", "search_bar_text": "Search for Blood Pressure Medicines", "enabled": true, "imageOverlayText": "Blood Pressure", "sub_categories": [{"header_title": "Popular searches", "name": "Blood Pressure", "title": "Blood Pressure Care", "enabled": true, "expanded": true, "short_desc": "For Blood pressure Patients", "products": [{"name": "Ecosprin"}, {"name": "Amlong"}, {"name": "Cilacar"}, {"name": "Telma"}, {"name": "Clopilet"}, {"name": "Concor"}, {"name": "Losar H"}], "search_text": "blood pressure"}], "categoryTileText2": "Blood Pressure", "categoryTileText": "Blood Pressure", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/blood_pressure_Category.png"}, {"clickActionUrl": "b", "name": "Generic Medicines", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/Medicines_2.jpg", "search_bar_text": "Search for Medicines", "enabled": true, "imageOverlayText": "OTC Drugs", "sub_categories": [{"header_title": "Popular searches", "name": "Generic drugs", "title": "Blood Pressure Care", "enabled": true, "expanded": true, "short_desc": "For Blood pressure Patients", "products": [{"name": "Dolo"}, {"name": "Shelcal"}, {"name": "Neurobion Forte"}, {"name": "Rantac"}, {"name": "Becosules"}, {"name": "Pan"}, {"name": "Crocin Advance"}], "search_text": "Crocin"}], "categoryTileText2": "Antiseptic Creams/ Lotions, Pain Relief Sprays/ Balms, Bandages & more", "categoryTileText": "Dolo, Gelusil & More", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/generic_meds.png"}, {"clickActionUrl": "b", "name": "Wellness", "imageUrl": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sexual-wellness.jpg", "search_bar_text": "Search for Wellness Products", "enabled": true, "imageOverlayText": "Wellness", "sub_categories": [{"header_title": "Popular searches", "name": "Wellness", "title": "Wellness", "enabled": true, "expanded": true, "short_desc": "Durex, Moods, Skore, manforce, Lubes & more", "products": [{"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/men-1.jpg", "name": "Durex"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/men-2.jpg", "name": "Skore"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/men-c-3.jpg", "name": "Kamasutra"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/men-c-4.jpg", "name": "Manforce"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/men-c-5.jpg", "name": "Durex Play"}, {"image": "https://s3.ap-south-1.amazonaws.com/myra-static-images/men-3.jpg", "name": "Moods"}], "search_text": "Condoms"}], "categoryTileText2": "Condoms, Lubes & more", "categoryTileText": "Wellness", "imageUrl2": "https://s3.ap-south-1.amazonaws.com/myra-static-images/sexual-wellness.jpg"}]))
		})
	}
}


/*
* fetch user's current orders
 */

export function getAndHandleCurrentOrder() {
	return function (dispatch) {
		// dispatch(fetchingCurrentOrder())
		return getCurrentOrder().then((resp) => {
			dispatch(fetchingCurrentOrderSuccess(resp.data))
		})
		.catch((error) => {
			console.log('error', error)
			alert("error: "+ error.message)
			dispatch(fetchingCurrentOrderFailure(error.message))
		})
	}
}

export function getRecomendedItemsList(){
	return function (dispatch, store) {
		let location = getLocalstorageValue("userLocation")
		let cart = store().cart
		dispatch(fetchingRecomendedItems())
		if (!location.id || !location.nearest_fc_id) return
		return getRecomendedItems({location_id: location.id, fc_id: location.nearest_fc_id}).then((resp) => {
			let recomendedItemsList =  resp.data.items && resp.data.items.length ? resp.data.items.map(function(item){return item['medicine']}) : []
			return dispatch(fetchingRecomendedItemsSuccess(resp.data.items))
		})
		.catch((error) => {
			console.log('error', error)
			alert("error: "+ error.message)
			return dispatch(fetchingRecomendedItemsFailure(error.message))
		})
	}
}

export function handleGetOrder(orderId) {
	return function (dispatch) {
		dispatch(fetchingOrder())
		return getOrderHandler(orderId).then((resp) => {
			dispatch(fetchingOrderSuccess(resp.data))
			return resp.data
		})
		.catch((error) => {
			console.log('error', error)
			alert("error: "+ error.message)
			dispatch(fetchingOrderFailure(error.message))
		})
	}
}

const initialState ={
	isFetching: false,
	recomendedItemsList: [],
	currentOrderList: [],
	currentOrder: {},
	processingPipeline: {},
	error: '',
	imgValue: {},
	showReorder: false,
	isCurrentOrder: true,
	categoryList: [],
	categoryName: '',
}

// Dispatures
export default function ordersPrescription(state = initialState, action){
	switch(action.type){
		case RESET_STORE:
			return{
				...state,
				isFetching: false,
				recomendedItemsList: [],
				currentOrderList: [],
				currentOrder: {},
				processingPipeline: {},
				error: '',
				imgValue: {},
				showReorder: false,
				isCurrentOrder: true,
				categoryList: [],
				categoryName: '',
			}
		case UPDATE_IMAGE:
			return{
				...state,
				imgValue: action.imgValue,
				error: '',
			}
		case FETCHING_CURRENT_ORDER:
			return{
				...state,
				isFetching: true,
				currentOrderList: '',
				processingPipeline: '',
				error: '',
			}
		case FETCHING_CURRENT_ORDER_FAILURE:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case FETCHING_CURRENT_ORDER_SUCCESS:
			return{
				...state,
				isFetching: false,
				currentOrderList: action.currentOrderList,
				processingPipeline: action.processingPipeline,
				error: '',
			}
		case FETCHING_RECOMENDED_ITEMS:
			return{
				...state,
				isFetching: true,
				recomendedItemsList: [],
				error: '',
			}
		case FETCHING_RECOMENDED_ITEMS_FAILURE:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case FETCHING_RECOMENDED_ITEMS_SUCCESS:
			return{
				...state,
				isFetching: false,
				recomendedItemsList: action.recomendedItemsList,
				error: '',
			}
		case GET_CATEGORY_LIST:
			return{
				...state,
				isFetching: true,
				categoryList: '',
				error: '',
			}
		case GET_CATEGORY_LIST_FAILURE:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case GET_CATEGORY_LIST_SUCCESS:
			return{
				...state,
				isFetching: false,
				categoryList: action.categoryList,
				error: '',
			}
		case FETCHING_ORDER:
			return{
				...state,
				isFetching: true,
				currentOrder: {},
				processingPipeline: '',
				error: '',
			}
		case FETCHING_ORDER_FAILURE:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case FETCHING_ORDER_SUCCESS:
			return{
				...state,
				isFetching: false,
				currentOrder: action.currentOrder,
				error: '',
			}
		case HANDLE_REORDER_FLAG:
			return {
				...state,
				isFetching: false,
				showReorder: action.showReorder,
				isCurrentOrder: action.isCurrentOrder,
			}
		case UPDATE_CATEGORY_NAME:
			return {
				...state,
				categoryName: action.categoryName,
			}
		default :
			return state
	}
}
