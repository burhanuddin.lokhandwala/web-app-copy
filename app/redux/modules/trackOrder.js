import { trackOrderDetails } from '../../utils/ordersApi'

const TRACK_ORDER_DETAILS = 'TRACK_ORDER_DETAILS'
const TRACK_ORDER_DETAILS_SUCCESS = 'TRACK_ORDER_DETAILS_SUCCESS'
const TRACK_ORDER_DETAILS_ERROR = 'TRACK_ORDER_DETAILS_ERROR'


function trackOrderData() {
    return{
        type: TRACK_ORDER_DETAILS,
    }
}

function trackOrderSuccess(details) {
    return{
        type: TRACK_ORDER_DETAILS_SUCCESS,
        timeline: details.timeline,
        order:details.order
    }
}

function trackOrderError(error) {
    return{
        type: TRACK_ORDER_DETAILS_ERROR,
        error: error,
    }
}


export function trackOrder(orderID) {
    return function (dispatch) {
        dispatch(trackOrderData())
        return trackOrderDetails(orderID).then(resp => {
            if(resp && resp.status != 200){
                // return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
            }else{
                dispatch(trackOrderSuccess(resp.data))
                return resp.data
            }
        })
        .catch(error => {
            // dispatch(trackOrderSuccess({"order":{"eta_info":{},"scheduled_timestamp":null,"process_state":null,"delivery_location_id":79053,"is_bill_printed":false,"payment_status":"not_paid","baskets":null,"fake_doctor_id":null,"out_of_stock_items":[],"id":785277,"referral_id":null,"bags":null,"vehicle_id":4,"user_id":8329,"doctor":null,"show_delivery_time":false,"preferred_payment_mode_id":7,"can_skip_call_center":null,"status_remark_id":null,"source":null,"details":null,"entry_timestamp":1509951091,"extended_notes":null,"call_back_user":false,"display_id":"MTL7XM","delivery_location":{"is_default":false,"whitelisted":false,"name":"Nidhi","address":"4th floor 12, Hubtown Prime, Rukmani Colony, Ulsoor, Bengaluru, Karnataka 560042, India, Street: Sent jons road, Landmark: Ajanta theater ","latitude":12.982748,"delivery_available":true,"accuracy":23,"id":79053,"longitude":77.614187,"label":"Office"},"amount_details":{"delivery_charges":0,"total_mrp":253.8,"discount_percentage":15.0,"total_payable_amount":215.73,"wallet_credits":0.0,"total_discount":38.07,"return_amount":0.0,"tax_percentage":5.5,"total_payable_amount_after_returns":215.73,"total_amount":215.73},"prescriptions":[],"status_id":12,"retail_store_id":null,"partial_items":[],"completed_time":null,"discount":15.0,"checkout_space_id":null,"retail_invoice_id":null,"dispatch_space_id":null,"preferred_payment_mode":{"id":7,"label":"Swipe Card on Delivery"},"home_fc_id":null,"first_checkout_timestamp":1513968483,"notes":null,"delivery_id":null,"reached_timestamp":null,"available_items":[{"drug_variation_id":4100,"tax":null,"fulfilment_timestamp":null,"packet_type_display_info":{"show_quantity_type_index":0,"show_quantity_type":"normalized_quantity"},"id":3403776,"purchase_id":null,"normalized_quantity":2,"medicine":{"unit_name":"mg","classification":{"id":1,"label":"Schedule G"},"is_batch_compulsory":true,"default_quantity_type_index":0,"uuid":"6c465928960d4f01805ad058a85ec160","discounted_price":101.52,"big_box_allowed":false,"equivalents":[],"validation_timestamp_type":"expiry_time","recurring":false,"packing_quantity":"1","not_selling":false,"show_packing":true,"fast_moving":false,"refrigerated":false,"dv_classification":1,"cutting_allowed":false,"name":"Brevoxyl Creamy Wash","normalized_mrp":126.9,"common":true,"normalized_discounted_price":101.52,"formatted_name":"Brevoxyl Creamy Wash 50 mg (ointment)","id":4100,"substitute":null,"hsn_id":30,"manufacturer_name":"Stiefel India Pvt Ltd ","is_discontinued":false,"temp_events":{},"dosage_form_label":"ointment","expiry_timestamp":1543602600,"packing_info":"50","is_temp":null,"image_urls":null,"allowed_quantity_types":[{"mrp_discounted":101.52,"label_formatted":"1 ointment","mrp":126.9,"label_formatted_full":"Box (1 ointment)","label":"box","discount":15.0,"label_pluralized":"boxes"}],"discount":null,"mrp_updated_timestamp":1513066344,"packet_type":"ointment","is_banned":false,"mrp":126.9,"expiry_duration":null,"drug_id":3001},"entry_timestamp":1513968418,"quantity_pending_removal":0,"reminder":null,"added_by_customer":false,"db_deleted_timestamp":null,"order_id":785277,"is_temp":false,"quantity_repr_normalized":true,"discount":15.0,"fulfilment_status":null,"prescription_quantity":null,"invoice_id":786590,"mrp":126.9,"rack_container_id":null,"normalized_quantity_remainder":0,"quantity":2}],"is_price_confirmed":false,"assigned_timestamp":null,"corrected_source":null,"doctor_id":null,"completed_timestamp":null,"post_successful_checkout":false,"checkout_timestamp":1513968483},"timeline":null}))
            dispatch(trackOrderError(error));
            console.log('error', error)
            return false
        })
    }
}



const initialState ={
    isFetching: false,
    error: '',
    order: {},
    timeline: {}
}

export default function trackOrderReducer(state = initialState, action){
    switch(action.type){
        case TRACK_ORDER_DETAILS:
            return{
                ...state,
                isFetching: true
            }
        case TRACK_ORDER_DETAILS_SUCCESS:
            return{
                ...state,
                isFetching: false,
                order: action.order,
                timeline: action.timeline
            }
        case TRACK_ORDER_DETAILS_ERROR:
            return{
                ...state,
                isFetching: false,
                error: action.error,
            }
        default :
            return state
    }
}
