import getPreviousOrder, {handleReorder, handleReorderItem, walletInfo} from '../../utils/previousOrdersApi'

const FETCHING_PREVIOUS_ORDER = 'FETCHING_PREVIOUS_ORDER'
const FETCHING_PREVIOUS_ORDER_SUCCESS = 'FETCHING_PREVIOUS_ORDER_SUCCESS'
const FETCHING_PREVIOUS_ORDER_FAILURE = 'FETCHING_PREVIOUS_ORDER_FAILURE'

const UPDATING_PREVIOUS_ORDER = 'UPDATING_PREVIOUS_ORDER'
const UPDATING_PREVIOUS_ORDER_SUCCESS = 'UPDATING_PREVIOUS_ORDER_SUCCESS'
const UPDATING_PREVIOUS_ORDER_FAILURE = 'UPDATING_PREVIOUS_ORDER_FAILURE'

const REORDER_ITEM = 'REORDER_ITEM'
const REORDER_ITEM_SUCCESS = 'REORDER_ITEM_SUCCESS'
const REORDER_ITEM_FAILUR = 'REORDER_ITEM_FAILUR'
const REORDER_ITEM_UPDATE = 'REORDER_ITEM_UPDATE'
const UPDATE_WALLATE_INFO = 'UPDATE_WALLATE_INFO'


function fetchingPreviousOrder() {
	return{
		type: FETCHING_PREVIOUS_ORDER,
	}
}

export function fetchingPreviousOrderSuccess(orders, next) {
	return{
		type: FETCHING_PREVIOUS_ORDER_SUCCESS,
		previousOrderList: orders,
		nextUrl: next,
	}
}

function fetchingPreviousOrderFailure(error) {
	return{
		type: FETCHING_PREVIOUS_ORDER_FAILURE,
		error: error,
	}
}

function updatingPreviousOrder() {
	return{
		type: UPDATING_PREVIOUS_ORDER,
	}
}

function updateWallateInfo(wallateInfo){
	return {
		type: 'UPDATE_WALLATE_INFO',
		wallateInfo: wallateInfo,
	}
}

function updatingPreviousOrderSuccess(orders, next) {
	return{
		type: UPDATING_PREVIOUS_ORDER_SUCCESS,
		previousOrderList: orders,
		nextUrl: next,
	}
}

function updatingPreviousOrderFailure(error) {
	return{
		type: UPDATING_PREVIOUS_ORDER_FAILURE,
		error: error,
	}
}

function reorderItem() {
	return{
		type: REORDER_ITEM,
	}
}

function reorderItemSuccess() {
	return{
		type: REORDER_ITEM_SUCCESS,
	}
}

function reorderItemFailur(error) {
	return{
		type: REORDER_ITEM_FAILUR,
		error: error,
	}
}

function reorderItemUpdate(orders) {
	return{
		type: REORDER_ITEM_UPDATE,
		previousOrderList: orders,
	}
}

export function handleEnableItem(medicineId) {
	return function (dispatch, store) {
		let previousOrder = store().previousOrder

		let list = previousOrder.previousOrderList.map(x => {
					// console.log('order', x.id, orderId)
				if(x.items && x.items.length){
					x.items = x.items.map(y => {
						if(y.medicine.id == medicineId){
							y['reordered'] = false
						}
						return y
					})
				}
				return x
			})
	}
}

export function handleWalletInfo(orderId) {
	return function (dispatch, store) {
		// let cart = store().cart
		// let user = store().users
		// let orderId = cart.orderId
		dispatch(updateWallateInfo(''))
		return walletInfo(orderId).then(resp => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400){
				// console.log(resp.data.status.message)
			}
				return dispatch(updateWallateInfo(resp.data.data))
		})
		.catch(error => {
			console.log('error', error)
			return dispatch(updateWallateInfo(""))
		})
	}
}

export function getAndHandlePreviousOrder(url) {
	return function (dispatch, store) {
		let previousOrder = store().previousOrder;
		if(url && !previousOrder.nextUrl){
			return
		}
		if(url){
			dispatch(updatingPreviousOrder())
		}else{
			if(previousOrder.previousOrderList && previousOrder.previousOrderList.length > 1){
				return
			}else{
				dispatch(fetchingPreviousOrder())
			}
		}

		return getPreviousOrder(url ? previousOrder.nextUrl : '').then((resp) => {
			// debugger
			let previousOrderList = previousOrder.previousOrderList, nextUrl = ''
			if(resp.data.next){
				let urlIndex = resp.data.next.indexOf("user")
				nextUrl = resp.data.next.substr(urlIndex)
			}

			if(previousOrderList.length > 1 && url){
				previousOrderList = previousOrderList.concat(resp.data.orders)
				dispatch(updatingPreviousOrderSuccess(previousOrderList, nextUrl))
			}
			else{
				previousOrderList = resp.data.orders
				dispatch(fetchingPreviousOrderSuccess(previousOrderList, nextUrl))
			}
		})
		.catch((error) => {
			console.log('error', error)
			alert("error: "+ error.message)
			url ? dispatch(updatingPreviousOrderFailure(error.message)) : dispatch(fetchingPreviousOrderFailure(error.message))
		})
	}
}

export function handleRorderItems(data) {
	return function (dispatch, store) {
		let previousOrder = store().previousOrder;
		dispatch(reorderItem())
		return handleReorder(data).then((resp) => {
			if(resp.data.status.code == 409){
				if(confirm(resp.data.status.message)){
					// debugger
					return dispatch(handleRorderItems({id: data.id, force: true}))
				}else{
					return dispatch(reorderItemSuccess())
				}
			}
			let list = previousOrder.previousOrderList.map(x => {
				if(x.id == data.id){
					x.items = x.items.map(y => {
						y['reordered'] = true
						return y
					})
				}else{
					x.items = x.items.map(y => {
						y['reordered'] = false
						return y
					})
				}
				return x
			})
			dispatch(reorderItemUpdate(list))
			return resp.data
		})
		.catch((error) => {
			console.log('error', error)
			alert("error: "+ error.message)
			dispatch(reorderItemFailur(error.message))
		})
	}
}

export function handleRorderItem(data, itemId, orderId ) {
	// debugger
	return function (dispatch, store) {
		let previousOrder = store().previousOrder;
		dispatch(reorderItem())
		return handleReorderItem(data).then((resp) => {
			let list = previousOrder.previousOrderList.map(x => {
				if(x.id == orderId){
					// console.log('order', x)
					x.items = x.items.map(y => {
						if(y.id == itemId){
							y['reordered'] = true
						}
						return y
					})
				}
				return x
			})
			// debugger
			dispatch(reorderItemUpdate(list))
		})
		.catch((error) => {
			console.log('error', error)
			alert("error: "+ error.message)
			dispatch(reorderItemFailur(error.message))
		})
	}
}

const initialState ={
	isFetching: false,
	previousOrderList: [],
	error: '',
	nextUrl: '',
	reorderedFlag: false,
	wallateInfo: '',
}

// Dispatures
export default function previousOrder(state = initialState, action){
	switch(action.type){
		case FETCHING_PREVIOUS_ORDER:
			return{
				...state,
				isFetching: true,
				previousOrderList: '',
				error: '',
			}
		case FETCHING_PREVIOUS_ORDER_FAILURE:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case FETCHING_PREVIOUS_ORDER_SUCCESS:
			return{
				...state,
				isFetching: false,
				nextUrl: action.nextUrl,
				previousOrderList: action.previousOrderList,
				error: '',
			}
		case UPDATING_PREVIOUS_ORDER:
			return{
				...state,
				isUpdating: true,
				error: '',
			}
		case UPDATING_PREVIOUS_ORDER_FAILURE:
			return{
				...state,
				isUpdating: false,
				error: action.error,
			}
		case UPDATING_PREVIOUS_ORDER_SUCCESS:
			return{
				...state,
				isUpdating: false,
				nextUrl: action.nextUrl,
				previousOrderList: action.previousOrderList,
				error: '',
			}
		case REORDER_ITEM:
			return{
				...state,
				isFetching: true,
				error: '',
			}
		case REORDER_ITEM_SUCCESS:
			return{
				...state,
				isFetching: false,
				error: '',
			}
		case REORDER_ITEM_FAILUR:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case REORDER_ITEM_UPDATE:
			return{
				...state,
				isFetching: false,
				previousOrderList: action.previousOrderList,
				error: '',
				reorderedFlag: true,
			}
		case UPDATE_WALLATE_INFO:
			return{
				...state,
				wallateInfo: action.wallateInfo,
			}
		default :
			return state
	}
}

