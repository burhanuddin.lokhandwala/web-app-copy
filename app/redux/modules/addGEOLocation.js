import {askForUserGEOLocation} from '../../utils/googleLocationApi'
import {confirmAndAddLocation, checkLocation, getUserLocations, updateAndAddLocation, deleteLocationHandler, getLocationFormet} from '../../utils/locationApi'
import {updateOrderHandler} from '../../utils/selectLocationApi'
import {setActiveLocation} from './cart'
import Clevertap from '../../helpers/clevertap';

const SET_USER_GEO_LOCATION = 'SET_USER_GEO_LOCATION'
const SET_USER_GEO_LOCATION_FAILUR = 'SET_USER_GEO_LOCATION_FAILUR'
const SET_USER_GEO_LOCATION_SUCCESS = 'SET_USER_GEO_LOCATION_SUCCESS'
const DELIVERY_FAILED_LOCATION = 'DELIVERY_FAILED_LOCATION'
const DELIVERY_LOCATION_AVAILABLE = 'DELIVERY_LOCATION_AVAILABLE'
const AUTO_COMPLETE_LIST = 'AUTO_COMPLETE_LIST'
const ADDRESS_UPDATED_AND_CONFIRMES = 'ADDRESS_UPDATED_AND_CONFIRMES'
const CHECK_LOCATION_CHANGE = 'CHECK_LOCATION_CHANGE'
const HANDLE_CONFIRM_LOCATION = 'HANDLE_CONFIRM_LOCATION'
const CURRENT_EDIT_LOCATION = 'CURRENT_EDIT_LOCATION'
const ADD_LOCATION = 'ADD_LOCATION'

function setUserGEOLocation () {
	return {
		type: 'SET_USER_GEO_LOCATION',
	}
}

export function setUserGEOLocationSuccess (userGEOAddress) {
	return {
		type: 'SET_USER_GEO_LOCATION_SUCCESS',
		userGEOAddress: userGEOAddress,
		geoError: '',
	}
}

export function flagLocationChange(isChangeLocation) {
	return {
		type: 'CHECK_LOCATION_CHANGE',
		isChangeLocation: isChangeLocation,
	}
}

export function autoCompleteList(googleAddressList) {
	return {
		type: 'AUTO_COMPLETE_LIST',
		googleAddressList: googleAddressList,
	}
}

export function setUserGEOLocationFailur (geoError) {
	return {
		type: 'SET_USER_GEO_LOCATION_FAILUR',
		geoError: geoError,
	}
}

function deliveryFailedLocation(locations) {
	return{
		type: 'DELIVERY_FAILED_LOCATION',
		availLocation: locations.availLocation,
		unAvailLocation: locations.unAvailLocation,
	}
}

function deliveryLocationAvailable(locations) {
	return {
		type: 'DELIVERY_LOCATION_AVAILABLE',
		formatedAddress: locations.address,
		addressDetail: locations
	}
}

export function addLocation(location, googleLocation){
	return {
		type: 'ADD_LOCATION',
		formatedAddress: location.address_template,
		addressDetail: location,
		googleLocation: googleLocation,
	}
}

export function addressUpdatedAndConfirmed(argument) {
	return {
		type: 'ADDRESS_UPDATED_AND_CONFIRMES',
		isGEOSuccess: false,
	}
}

function handleConfirmAddress() {
	return {
		type: 'HANDLE_CONFIRM_LOCATION'
	}
}

export function currentEditLocation(current_edit_location){
	return {
		type: 'CURRENT_EDIT_LOCATION',
		current_edit_location: current_edit_location,
	}
}

/**
 * [confirmAddress after getting location from google autocomplete List or GEO location confirm if we are delevering to that location]
 * @param  {[object]} address [letlong and full-address of location]
 * @return {[type]}         [description]
 */
export function confirmAddress(address, showListView) {
	// console.log("current location", address, showListView)
	return function (dispatch, store) {
		let location = store().addGEOLocation;
		dispatch(handleConfirmAddress())
		if(location.locationListView || location.isFetchingCurrentLocation){
			return
		}
		let queryJson = {"locations":
						{
							"longitude": address.LatLng.lng,
							"label": '',
							"latitude": address.LatLng.lat,
							"address": address.fullAddress.formatted_address,
							"accuracy": 1000
						}
					}
		confirmAndAddLocation(queryJson).then(response => {
			// if(response.data.locations && response.data.locations.delivery_available === false){
			// 	getUserLocations().then(address => {
			// 		let availLocation = address.data.locations.filter(x => { return x.delivery_available === true})
			// 		let unAvailLocation = address.data.locations.filter(x => { return x.delivery_available === false})
			// 		// console.log('avainlable_location', avainlable_location, unAvainlable_location)
			// 		dispatch(deliveryFailedLocation({availLocation: availLocation, unAvailLocation: unAvailLocation}))
			// 		showListView()
			// 	})
			// }else{
			// 	// console.log('yes it happens!!!', response)
			// 	dispatch(deliveryLocationAvailable(response.data.locations))
			// }
		})
	}
}

export function handleGetUserLocations(){
	return function (dispatch) {
		return getUserLocations().then(address => {
				let availLocation = address.data.locations.filter(x => { return x.delivery_available === true})
				let unAvailLocation = address.data.locations.filter(x => { return x.delivery_available === false})
				// console.log('avainlable_location', avainlable_location, unAvainlable_location)
				dispatch(deliveryFailedLocation({availLocation: availLocation, unAvailLocation: unAvailLocation}))
		})
		.catch(error => {
			console.log('error', error)
			// return dispatch(fetchingUserLocationsFailur(error.message))
		})
	}
}

/**
 * [handleUpdateAddress update and save after selecting new location]
 * @param  {[object]} updatedAddress [set of address that need to update]
 * @return {[type]}                [description]
 */
export function handleUpdateAddress(updatedAddress, showListView) {
	return function (dispatch, store) {
		let location = store().addGEOLocation;
		let address = updatedAddress.address + ' ' + location.formatedAddress + ', Street: ' + updatedAddress.streetName
		let queryJson = {"locations":
							{
								"accuracy": 1000,
								...updatedAddress
							}
						}
						debugger
		return confirmAndAddLocation(queryJson).then(response => {
			if(response.data.locations && response.data.locations.delivery_available === false){
				Clevertap.event("mWeb - Address Add New",{"label":queryJson.locations.label,"serviceable":"false"})
				getUserLocations().then(address => {
					let availLocation = address.data.locations.filter(x => { return x.delivery_available === true})
					let unAvailLocation = address.data.locations.filter(x => { return x.delivery_available === false})
					// console.log('avainlable_location', avainlable_location, unAvainlable_location)
					dispatch(deliveryFailedLocation({availLocation: availLocation, unAvailLocation: unAvailLocation}))
					showListView()
				})
			}else{
				// console.log('yes it happens!!!', response)
				Clevertap.event("mWeb - Address Add New",{"label":queryJson.locations.label,"serviceable":"true"})
				dispatch(deliveryLocationAvailable(response.data.locations))
				dispatch(setActiveLocation(response.data.locations,false))
				showListView()
			}
			// return true
		})
		.catch(error => {
			console.log('error', error)
			// return dispatch(setRequestCall(requestFlag))
		})
	}
}

/**
 * [handleChangeLocation to change the order location of user]
 * @return {[type]}    [description]
 * @param data
 */
export function handleChangeLocation(data) {
	return function (dispatch, store) {
		let cart = store().cart;
			dispatch(setUserGEOLocation())
			debugger
		return updateOrderHandler(cart.orderId, {orders: {delivery_location_id: data.id}}).then(response => {
			dispatch(addressUpdatedAndConfirmed())
			// if(!!(response.data.orders.call_back_user) !== cart.isRequestCall){
			// 	return dispatch(setRequestCall(!!(response.data.orders.call_back_user)))
			// }else{
			// 	return true
			// }
		})
		.catch(error => {
			console.log('error', error)
			// return dispatch(setRequestCall(requestFlag))
		})
	}
}

export function handleDeleteLocation(id) {
	return function (dispatch, store) {
		let cart = store().cart;
		return deleteLocationHandler(id).then(response => {
			if(response.data.status.code !== 400 && response.data.status.code !== 403 && response.data.status.code !== 499 && response.data.status.code !== 404){
				return true
			}else{
				return false
			}
		})
		.catch(error => {
			alert(error.message)
			console.log('error', error)
		})
	}
}

export function handleEditLocation(json, id) {
	return function (dispatch, store) {
		let addGEOLocation = store().addGEOLocation;
		return updateAndAddLocation({id: id, queryJson: json}).then(response => {
			if(response.data.status.code !== 400 && response.data.status.code !== 403 && response.data.status.code !== 499 && response.data.status.code !== 404){
				return dispatch(currentEditLocation(response.data.locations))
			}else{
				return dispatch(currentEditLocation(addGEOLocation.current_edit_location))
			}
		})
		.catch(error => {
			dispatch(currentEditLocation(addGEOLocation.current_edit_location))
			alert(error.message)
			console.log('error', error)
		})
	}
}

export function handleGetLocationFormet(location) {
	return function (dispatch, store) {
		return getLocationFormet(location.fullAddress.place_id).then(response => {
			return dispatch(addLocation(response.data, location))
		})
		.catch(error => {
			debugger
			console.log('error', error)
		})
	}
}

const initialState = {
	isFetching: false,
	geoError: '',
	userGEOAddress: '',
	isGEOSuccess: false,
	locationListView: false,
	isUpdateLocation: false,
	unAvailLocation: [],
	availLocation: [],
	googleAddressList: [],
	addressDetail: {},
	formatedAddress: '',
	isChangeLocation: false,
	isFetchingCurrentLocation: false,
	current_edit_location: '',
	googleLocation: '',
}

export default function addGEOLocation(state = initialState, action) {
	switch (action.type){
		case SET_USER_GEO_LOCATION:
			return {
				...state,
				isFetching: true,
				geoError: '',
			}
		case SET_USER_GEO_LOCATION_SUCCESS:
			return {
				...state,
				isFetching: false,
				isGEOSuccess: true,
				geoError: '',
			}
		case SET_USER_GEO_LOCATION_FAILUR:
			return {
				...state,
				isFetching: false,
				// isGEOSuccess: false,
				geoError: action.geoError,
			}
		case DELIVERY_FAILED_LOCATION:
			return{
				...state,
				unAvailLocation: action.unAvailLocation,
				availLocation: action.availLocation,
				isGEOSuccess: true,
				isUpdateLocation: false,
				locationListView: true,
				isFetchingCurrentLocation: false,
				formatedAddress: "",
			}
		case HANDLE_CONFIRM_LOCATION:
			return {
				...state,
				isFetchingCurrentLocation: true,
			}
		case DELIVERY_LOCATION_AVAILABLE:
			return {
				...state,
				isGEOSuccess: true,
				isUpdateLocation: true,
				locationListView: false,
				formatedAddress: action.formatedAddress,
				isFetchingCurrentLocation: true,
				addressDetail: action.addressDetail,
			}
		case ADDRESS_UPDATED_AND_CONFIRMES:
			return {
				...state,
				isUpdateLocation: false,
				locationListView: false,
				isGEOSuccess: action.isGEOSuccess,
				googleAddressList: [],
				addressDetail: {},
				isFetching: false,
			}
		case AUTO_COMPLETE_LIST:
			return {
				...state,
				googleAddressList: action.googleAddressList,
			}
		case CHECK_LOCATION_CHANGE:
			return {
				...state,
				isChangeLocation: action.isChangeLocation,
			}
		case CURRENT_EDIT_LOCATION:
			return {
				...state,
				current_edit_location: action.current_edit_location,
			}
		case ADD_LOCATION:
			return {
				...state,
				isGEOSuccess: true,

				formatedAddress: action.formatedAddress,
				addressDetail: action.addressDetail,
				googleLocation: action.googleLocation.fullAddress,
				isUpdateLocation: true,
			}
		default:
			return state
	}
}