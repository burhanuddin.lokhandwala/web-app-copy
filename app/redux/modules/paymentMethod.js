import {
	getPaymentMethodList,
	updatePaymentMethodList,
	getPaytmTransitionParam,
	paytmSuccessOrder,
	getRazorPayTransitionParam,
	razorPaySuccessOrder,
	juspayPayTransitionParam,
	juspaySuccessOrder,
	paymentStatus
} from '../../utils/paymentMethodApi'

const FETCHING_PAYMENT_METHOD_LIST = 'FETCHING_PAYMENT_METHOD_LIST'
const SET_PAYMENT_METHOD_LIST = 'SET_PAYMENT_METHOD_LIST'
const UPDATE_PAYMENT_METHOD = 'UPDATE_PAYMENT_METHOD'
const SET_PAYTM_RESULT = 'SET_PAYTM_RESULT'


function fetchingPaymentList(paymentMethodList) {
	return{
		type: 'FETCHING_PAYMENT_METHOD_LIST',
	}
}

function setPaymentList(paymentInfo) {
	return{
		type: 'SET_PAYMENT_METHOD_LIST',
		paymentMethodList: paymentInfo.paymentMethodList,
		order_id: paymentInfo.order_id,
	}
}

function setPaytmResult(paytmResponse) {
	return {
		type: 'SET_PAYTM_RESULT',
		paytmResponse: paytmResponse,
	}
}

export function getPaymentList(id) {
	return function (dispatch, store) {
		// let cart = store().cart;
		// let id = cart.orderId;
		dispatch(fetchingPaymentList())
		return getPaymentMethodList(id).then(resp => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 499){
				// return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
				return resp.data
			}else{
				dispatch(setPaymentList({paymentMethodList: resp.data.payment_options, order_id: id}))
				return resp.data
			}
		})
		.catch(error => {
			console.log('error', error)
			return false
		})
	}
}

export function updatePaymentMethod(method_id, displayId) {
	return function (dispatch, store) {
		// let paymentMethod = store().paymentMethod;
		return updatePaymentMethodList(displayId, {id: method_id}).then(resp => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 499){
				// return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
			}else{
				return resp
			}
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return false
		})
	}
}

export function paytmPayment(displayId) {
	return function (dispatch, store) {
		// let paymentMethod = store().paymentMethod;
		// let userInfo = store().users.userInfo;
		// let queryJson = {
		// 									"order_id":paymentMethod.order_id,
		// 									"mobile_number": userInfo.phone_number,
		// 									"email":userInfo.email,
		// 									"source":"web",
		// 								}
		return getPaytmTransitionParam(displayId).then(resp => {
			// debugger
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 499){
				// return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
				return resp.data.status
			}else{
				return dispatch(setPaytmResult(resp.data.response))
			}
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return false
		})
	}
}

export function razorpayPayment(payment_type_id, displayId) {
	return function (dispatch, store) {
		// let paymentMethod = store().paymentMethod;
		let queryJson = {"payment_type_id": payment_type_id}
		return getRazorPayTransitionParam(queryJson, displayId).then(resp => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 499){
				return resp.data.status
				// return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
			}else{
				let options = {
								...resp.data.response,
								"description": `Payment for order #${displayId}`,
								"image": "https://d388w23p6r1vqc.cloudfront.net/img/startups/11511/logo-1456280128.png",
								"handler": function (response){
									alert("Payment Successfull!!");
									razorPaySuccessOrder({razorpay_payment_id: response.razorpay_payment_id, amount: resp.data.response.amount, transaction_id: resp.data.response.notes.shopping_order_id})
									window.location = "http://payments.myramed.in/?type=thanks&amount="+(resp.data.response.amount/100)
								},
								"theme": {
									"color": "#1FC887"
								}
							}

				// debugger
				let rzp1 = new Razorpay(options);
				rzp1.open();
				return resp
			}
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return false
		})
	}
}

export function juspayPayment(payment_type_id,displayId){
	return function (dispatch, store) {
		let queryJson = {"payment_type_id": payment_type_id , "source":"web"};
		return juspayPayTransitionParam(queryJson, displayId).then(resp => {
			return resp.data;
		}).catch((err) => {
			console.log(err);
		})
	}
}

export function checkPayStatus(orderId){
	return function (dispatch, store) {
		return paymentStatus(orderId).then((res)=> {
			if(res.data.status.code === 401 || res.data.status.code === 403 || res.data.status.code === 400 || res.data.status.code === 499){
				return res.data.status
			}else 
				return res.data;
		}).catch((err)=>{
			console.log("error",err);
		})
	}
}

const initialState = {
	isFetching: false,
	error: '',
	paymentMethodList: [],
	order_id: '',
	paytmResponse: "",
}

export default function paymentMethod(state = initialState, action) {
	switch(action.type){
		case FETCHING_PAYMENT_METHOD_LIST:
			return{
				...state,
				isFetching: true,
				order_id: '',
				paymentMethodList: [],
				error: '',
			}

		case SET_PAYMENT_METHOD_LIST:
			return{
				...state,
				isFetching: false,
				paymentMethodList: action.paymentMethodList,
				order_id: action.order_id,
				error: '',
			}
		case SET_PAYTM_RESULT:
			return{
				...state,
				paytmResponse: action.paytmResponse,
			}

		default :
			return state
	}

}