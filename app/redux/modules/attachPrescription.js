import {submitPrescription, updatePrescription} from '../../utils/attachPrescriptionApi';
import getCartOrder from '../../utils/cartApi'

const UPDATE_TOKEN = 'UPDATE_TOKEN'
const SUBMIT_ORDER = 'SUBMIT_ORDER'
const SUBMIT_ORDER_SUCCESS = 'SUBMIT_ORDER_SUCCESS'
const SUBMIT_ORDER_ERROR = 'SUBMIT_ORDER_ERROR'
const UPDATE_PRESCRIPTION = 'UPDATE_PRESCRIPTION'
const UPDATE_PRESCRIPTION_SUCCESS = 'UPDATE_PRESCRIPTION_SUCCESS'
const UPDATE_PRESCRIPTION_ERROR = 'UPDATE_PRESCRIPTION_ERROR'

function updatePrescriptions() {
	return {
		type: UPDATE_PRESCRIPTION
	}
}

function updatePrescriptionSuccess(imageList) {
	return {
		type: UPDATE_PRESCRIPTION_SUCCESS,
		imageList: imageList,
	}
}

function updatePrescriptionError(error) {
	return {
		type: UPDATE_PRESCRIPTION_ERROR,
		error: error,
	}
}

export function updateToken(token) {
	return {
		type: UPDATE_TOKEN,
		token: token,
	}
}

function submitOrder(argument) {
	return{
		type: SUBMIT_ORDER,
	}
}

function submitOrderSuccess(argument) {
	return{
		type: SUBMIT_ORDER_SUCCESS,
	}
}

function submitOrderError(error) {
	return{
		type: SUBMIT_ORDER_ERROR,
		error: error,
	}
}

export function handleUpdatePrescriptions(data) {
	return function (dispatch, store) {
		let attachPrescription = store().attachPrescription
		dispatch(updatePrescriptions())
		return updatePrescription(data, attachPrescription.token).then(response => {
			if(response.data.status.code === 401 || response.data.status.code === 403 || response.data.status.code === 400 || response.data.status.code === 500 || !response.data.orders){
				console.log('resp', response.data.status)
				alert(response.data.status.message)
				history.back()
				return dispatch(updatePrescriptionError(response.data.status.message))
			}
			history.back()
			return dispatch(updatePrescriptionSuccess(response.data.orders.prescriptions))
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			history.back()
			return dispatch(updatePrescriptionError(error.message))
		})
	}
}

export function handleDoneButton(callBack) {
	return function (dispatch, store) {
		let attachPrescription = store().attachPrescription
		dispatch(submitOrder())
		return submitPrescription(attachPrescription.token).then(response => {
			if(response.data.status.code === 401 || response.data.status.code === 403 || response.data.status.code === 400 || response.data.status.code === 500 ){
				console.log('resp', response.data.status)
				alert(response.data.status.message)
				return dispatch(submitOrderError(response.data.status.message))
			}
			callBack()
			// dispatch(updateToken())
			return dispatch(submitOrderSuccess())
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return dispatch(submitOrderError(error.message))
		})
	}
}

const initialState = {
	imageList: [],
	isFetching: false,
	isImageFetching: false,
	error: '',
	token: '',
}

export default function attachPrescription(state = initialState, action) {
	switch(action.type){
		case UPDATE_PRESCRIPTION:
			return{
				...state,
				isFetching: true,
				error: '',
				isImageFetching: true,
			}
		case UPDATE_TOKEN:
			return{
				...state,
				token: action.token,
			}
		case UPDATE_PRESCRIPTION_SUCCESS:
			return{
				...state,
				isFetching: false,
				isImageFetching: false,
				imageList: action.imageList,
				error: '',
			}
		case UPDATE_PRESCRIPTION_ERROR:
			return{
				...state,
				isFetching: false,
				isImageFetching: false,
				error: action.error,
			}
		case SUBMIT_ORDER:
			return{
				...state,
				isFetching: true,
				error: '',
			}
		case SUBMIT_ORDER_SUCCESS:
			return{
				...state,
				isFetching: false,
				error: '',
				imageList: [],
			}
		case SUBMIT_ORDER_ERROR:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		default :
			return state
	}

}