import {uploadImageAction} from '../../utils/ordersApi'
import { updateLocalstorage, getLocalstorageValue} from '../../helpers/helperFunction'

const UPLOAD_IMAGE = 'UPLOAD_IMAGE'
const UPLOAD_IMAGE_SUCCESS = 'UPLOAD_IMAGE_SUCCESS'
const UPLOAD_IMAGE_FAILURE = 'UPLOAD_IMAGE_FAILURE'

const UPDATE_IMAGE_ELEMENT = 'UPDATE_IMAGE_ELEMENT'
const HANDLE_ACTION_DROWER = 'HANDLE_ACTION_DROWER'
const UPDATE_MARKERS_INFO = 'UPDATE_MARKERS_INFO'
const SHOW_IMAGE_TAG_VIEW = 'SHOW_IMAGE_TAG_VIEW'

// ************ upload image actions Creators start ********///

function uploadImage(argument) {
	return {
		type: UPLOAD_IMAGE
	}
}

function uploadImageSuccess(argument) {
	return {
		type: UPLOAD_IMAGE_SUCCESS
	}
}

function upaoalImageFailure(error) {
	return {
		type: UPLOAD_IMAGE_FAILURE,
		error: error
	}
}

export function updateImageElement(image_info) {
	return {
		type: UPDATE_IMAGE_ELEMENT,
		image_element: image_info.image_element,
		image_value: image_info.image_value,
		orientation_flag: image_info.orientation_flag ? image_info.orientation_flag : '',
	}
}

export function showImageTagView(is_show_tag_view) {
	return {
		type: SHOW_IMAGE_TAG_VIEW,
		is_show_tag_view: is_show_tag_view,
	}
}

export function handleActionDrower(is_show_action_dower) {
	return {
		type: HANDLE_ACTION_DROWER,
		is_show_action_dower: is_show_action_dower
	}
}

export function updateMarkersInfo(markersInfo, markersToSent) {
	return {
		type: UPDATE_MARKERS_INFO,
		markersInfo: markersInfo,
		markersToSent: markersToSent,
	}
}

export function handleUploadImage(callType, completeUploadImage) {
	return function (dispatch, store) {
		let imageSelector = store().imageSelector
		let cart = store().cart
		if(!imageSelector.image_value){
			return dispatch(upaoalImageFailure('please upload image!'))
		}
		let user = store().users
		if(user.userOnlineStatus == "online"){
			dispatch(uploadImage())
			return uploadImageAction(imageSelector.image_value, callType, imageSelector.orientation_flag).then((resp) => {
				if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 500 || !resp.data.orders){
					// console.log('resp', resp.data.status)
					alert(resp.data.status.message)
					return dispatch(upaoalImageFailure(resp.data.status.message))
				}
				completeUploadImage(resp.data.orders.prescriptions)
				dispatch(uploadImageSuccess())
				// return (resp.data && resp.data.orders ? resp.data.orders.prescriptions : [])

			})
			.catch((error) => {
				console.log('error', error)
				alert(error.message)
				dispatch(upaoalImageFailure(error.message))
			})
		}else{
			let imgList = cart.imageList
			let imgInfo = {id: imgList.length+1, image_value: JSON.stringify(imageSelector.image_value), markers: imageSelector.markersToSent, image_url: imageSelector.image_element.src}
			imgList.push(imgInfo)
			// updateLocalstorage("imgList", imgList)
			dispatch(uploadImageSuccess())
			completeUploadImage(imgList)
		}
	}
}

const initialState = {
	isFetching: false,
	image_element: '',
	is_show_action_dower: false,
	is_show_tag_view: false,
	markersInfo: [],
	image_value: '',
	markersToSent: [],
	orientation_flag: '',
}


export default function imageSelector(state = initialState, action) {
	switch(action.type){
		case UPLOAD_IMAGE:
			return{
				...state,
				isFetching: true,
				error: '',
			}
		case UPLOAD_IMAGE_SUCCESS:
			return{
				...state,
				isFetching: false,
				is_show_action_dower: false,
				is_show_tag_view: false,
				error: '',
			}
		case UPLOAD_IMAGE_FAILURE:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case UPDATE_IMAGE_ELEMENT:
			return{
				...state,
				image_element: action.image_element,
				image_value: action.image_value,
				orientation_flag: action.orientation_flag,
			}
		case HANDLE_ACTION_DROWER:
			return {
				...state,
				is_show_action_dower: action.is_show_action_dower,
			}
		case UPDATE_MARKERS_INFO:
			return {
				...state,
				markersInfo: action.markersInfo,
				markersToSent: action.markersToSent,
			}
		case SHOW_IMAGE_TAG_VIEW:
			return {
				...state,
				is_show_tag_view: action.is_show_tag_view,
			}
		default :
			return state
	}
}