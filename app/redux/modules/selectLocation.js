import {
	scheduleOrderHandler,
	getUserLocations,
	quickOrderHandler,
	updateBillingLocation,
	updateOrderHandler} from '../../utils/selectLocationApi'

const FETCHING_USER_LOCATION = 'FETCHING_USER_LOCATION'
const FETCHING_USER_LOCATION_SUCCESS = 'FETCHING_USER_LOCATION_SUCCESS'
const FETCHING_USER_LOCATION_FAILUR = 'FETCHING_USER_LOCATION_FAILUR'
const UPDATE_DELIVERY_LOCATION_ID = 'UPDATE_DELIVERY_LOCATION_ID'
const SET_USER_NOTE = 'SET_USER_NOTE'
const PLACE_ORDER = 'PLACE_ORDER'
const PLACE_ORDER_SUCCESS = 'PLACE_ORDER_SUCCESS'
const PLACE_ORDER_FAILUR = 'PLACE_ORDER_FAILUR'
const UPDATE_BILLING_LOCATION_ID = 'UPDATE_BILLING_LOCATION_ID'
const SHOW_BILLING_LOCATIONS = 'SHOW_BILLING_LOCATIONS'

function fetchingUserLocations() {
	return{
		type: 'FETCHING_USER_LOCATION',
		isFetching: false,
	}
}

function fetchingUserLocationsSuccess(locations) {
	return{
		type: 'FETCHING_USER_LOCATION_SUCCESS',
		userLocations: locations ? locations.availLoc : [],
		unAvailUserLocations: locations ? locations.unAvailLoc : [],
		isFetching: false,
	}
}

function fetchingUserLocationsFailur(error) {
	return{
		type: 'FETCHING_USER_LOCATION_FAILUR',
		isFetching: false,
		error: error,
	}
}

function updateDeliveryLocationId(deliveryLocationId) {
	return{
		type: 'UPDATE_DELIVERY_LOCATION_ID',
		deliveryLocationId: deliveryLocationId,
	}
}

function updateBillingLocationId(billingLocationId) {
	return{
		type: 'UPDATE_BILLING_LOCATION_ID',
		billingLocationId: billingLocationId,
	}
}

export function placeOrder() {
	return {
		type: 'PLACE_ORDER'
	}
}

export function placeOrderSuccess() {
	return {
		type: 'PLACE_ORDER_SUCCESS'
	}
}

export function placeOrderFailur(data) {
	return{
		type: 'PLACE_ORDER_FAILUR',
		placeOrderError: data.error,
		timing: data.timing,
	}
}

export function setUserNote(note) {
	return{
		type: 'SET_USER_NOTE',
		note: note,
	}
}

export function showBillingLocation(flag) {
	return{
		type: 'SHOW_BILLING_LOCATIONS',
	}
}

export function handleChangeUserLocation(id) {
	return function (dispatch, store) {
		let cart = store().cart;
		let selectLocation = store().selectLocation;
		dispatch(updateDeliveryLocationId(id*1))
		return updateOrderHandler(cart.orderId, {orders: {delivery_location_id: id*1}}).then(response => {
			if(response.data.status.code === 401 || response.data.status.code === 403 || response.data.status.code === 400 || response.data.status.code === 499 || response.data.status.code === 404){
				alert("error: "+response.data.status.message)
				dispatch(updateDeliveryLocationId(""))
				return Promise.reject("Please try again!");
				// return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
			}else{
				return Promise.resolve("successful");
				// return dispatch(placeOrderSuccess())
			}
		})
		.catch(error => {
			console.log('error', error)
			// alert("Please try again!")
			dispatch(updateDeliveryLocationId(""))
			return Promise.reject("Please try again!");
			//  dispatch(setRequestCall(requestFlag))
		})
	}
}

export function handleChangeBillingLocation(id) {
	return function (dispatch, store) {
		let cart = store().cart;
		let selectLocation = store().selectLocation;
		dispatch(updateBillingLocationId(id*1))
		return updateBillingLocation(cart.orderId, id*1).then(response => {
			if(response.data.status.code === 401 || response.data.status.code === 403 || response.data.status.code === 400 || response.data.status.code === 499 || response.data.status.code === 404){
				alert("error: "+response.data.status.message)
				// dispatch(updateDeliveryLocationId(cart.cartList.delivery_location_id))
				// return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
			}else{
				// return dispatch(placeOrderSuccess())
			}
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return dispatch(updateDeliveryLocationId((cart.cartList.delivery_location_id)*1))
			//  dispatch(setRequestCall(requestFlag))
		})
	}
}

export function setAndHandleUserLocations() {
	return function (dispatch) {
		dispatch(fetchingUserLocations)
		return getUserLocations().then(response => {
			if(response.data.locations && response.data.locations.length){
				let loc = response.data.locations.filter(x => x.delivery_available== true)
				let unAvailLoc = response.data.locations.filter(x => x.delivery_available== false)
				return dispatch(fetchingUserLocationsSuccess({availLoc: loc, unAvailLoc: unAvailLoc}))
			}else{
				return dispatch(fetchingUserLocationsSuccess())
			}

		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return dispatch(fetchingUserLocationsFailur(error.message))
		})
	}
}

export function handlePlaceOrder(callBack) {
	return function (dispatch, store) {
		let cart = store().cart;
		let users = store().users;
		let selectLocation = store().selectLocation;
		let note = selectLocation.note;

		let scheduleSlot = cart.schedule_time, queryJson = [];
		let fulfilment_status = cart.cartBucketItem
		for(let i in fulfilment_status){
			if (fulfilment_status[i].items && fulfilment_status[i].items.length && i != "out of stock") {
				let map = fulfilment_status[i].items.map(x => {
					return {dv_id: x.medicine.id, quantity: x.quantity}
				})
				let json = {
											bucket_type: i,
											dv_id_quantity_map: map
										}
				if(scheduleSlot[i] && scheduleSlot[i].scheduleTime){
					json.bucket_slot_id = scheduleSlot[i] && scheduleSlot[i].scheduleTime.id
				}else{
					let bucket = cart.schedule_time_unit[i]
					let key_list = bucket[Object.keys(bucket)[0]]
					json.bucket_slot_id = key_list[0].id
				}
				queryJson.push(json)
			}
		}

		if(!cart.orderId){
			alert("Something went wrong please try refreshing the page!")
			return
		}
		dispatch(setUserNote(''))
		dispatch(placeOrder())
		debugger
		return quickOrderHandler({orderId: cart.orderId, userId: users.userInfo.id}, {delivery_buckets: queryJson}).then(resp => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 500 || resp.data.status.code === 499){
				alert("Something wrong please try again!")
				return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
				dispatch(setUserNote(note))
			}else{
				callBack()
				return dispatch(placeOrderSuccess())
			}
		})
		.catch(error => {
			console.log('error', error)
			dispatch(setUserNote(note))
			alert("error: "+error.message)
			return false
		})
	}
}

export function handleScheduleOrder(data, callBack) {
	return function (dispatch, store) {
		let cart = store().cart;
		let id = cart.orderId;
		dispatch(placeOrder())
		return scheduleOrderHandler(id, data).then(resp => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 500 || resp.data.status.code === 499){
				alert("something wrong is happening please try again!!")
				return dispatch(placeOrderFailur({error: resp.data.status.message, timing: resp.data.timing}))
			}else{
				callBack()
				return dispatch(placeOrderSuccess())
			}
		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return false
		})
	}
}

const initialState = {
	isFetching: false,
	error: '',
	userLocations: [],
	unAvailUserLocations: [],
	note: '',
	deliveryLocationId: '',
	billingLocationId: '',
	timing: {},
	placeOrderError: '',
	isPlaceOrder: false,
	isBillingList: false,
	isRefresh: false
}

export default function selectLocation(state = initialState, action) {
	switch(action.type){
		case FETCHING_USER_LOCATION:
			return{
				...state,
				isFetching: true,
				userLocations: [],
				error: '',
			}
		case FETCHING_USER_LOCATION_SUCCESS:
			return{
				...state,
				userLocations: action.userLocations,
				unAvailUserLocations: action.unAvailUserLocations,
				isFetching: false,
			}
		case FETCHING_USER_LOCATION_FAILUR:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case UPDATE_DELIVERY_LOCATION_ID:
			return {
				...state,
				deliveryLocationId: action.deliveryLocationId,
			}
		case UPDATE_BILLING_LOCATION_ID:
			return {
				...state,
				billingLocationId: action.billingLocationId,
			}
		case SHOW_BILLING_LOCATIONS:
			return{
				...state,
				isBillingList: !state.isBillingList,
			}
		case SET_USER_NOTE:
			return{
				...state,
				note: action.note,
			}

		case PLACE_ORDER:
			return {
				...state,
				placeOrderError: "",
				isFetching: true,
			}
		case PLACE_ORDER_SUCCESS:
			return {
				...state,
				placeOrderError: "",
				timing: {},
				isFetching: false,
				isPlaceOrder: true,
			}
		case PLACE_ORDER_FAILUR:
			return {
				...state,
				isPlaceOrder: false,
				placeOrderError: action.placeOrderError,
				isFetching: false,
				timing: action.timing,
			}
		default :
			return state
	}

}
