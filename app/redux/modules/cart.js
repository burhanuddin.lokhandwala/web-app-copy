import getCartOrder, {
					promoCodeHandler,
					updateMedicineQuantity,
					removeOrderItem,
					searchHandler,
					searchHandler2,
					getBulkMedAvailability,
					getMedAvailability,
					getDynamicText,
					getCartRecommendation,
					getCartCharges,
					getUserActiveLocation,
					getSchedulingTime,
				} from '../../utils/cartApi'
import {formatedData, updateLocalstorage, getLocalstorageValue, isLocalStorageSupported} from '../../helpers/helperFunction'
import {updateOrderHandler} from '../../utils/selectLocationApi.js'

const FETCHING_CART_ORDER = 'FETCHING_CART_ORDER'
const FETCHING_CART_ORDER_SUCCESS = 'FETCHING_CART_ORDER_SUCCESS'
const FETCHING_CART_ORDER_FAILUR = 'FETCHING_CART_ORDER_FAILUR'
const SET_REQUEST_CALL = 'SET_REQUEST_CALL'
const UPDATE_PROMO_MESSAGE = 'UPDATE_PROMO_MESSAGE'
const RESET_STORE = 'RESET_STORE'
const UPDATE_IMAGE_LIST = 'UPDATE_IMAGE_LIST'
const SET_ITEM_LIST = 'SET_ITEM_LIST'
const HANDLE_ADDITEM_POPOVER = 'HANDLE_ADDITEM_POPOVER'
const FETCHING_UPDATE_MEDICINE_COUNT = 'FETCHING_UPDATE_MEDICINE_COUNT'
const FETCHING_UPDATE_MEDICINE_COUNT_SUCCESS = 'FETCHING_UPDATE_MEDICINE_COUNT_SUCCESS'
const FETCHING_UPDATE_MEDICINE_COUNT_FAILUR = 'FETCHING_UPDATE_MEDICINE_COUNT_FAILUR'
const SET_ADWORD_FLAGE = 'SET_ADWORD_FLAGE'
const ADD_SEARCH_LIST = 'ADD_SEARCH_LIST'
const ADD_SEARCH_LIST_SUCCESS = 'ADD_SEARCH_LIST_SUCCESS'
const ADD_SEARCH_LIST_FAILUR = 'ADD_SEARCH_LIST_FAILUR'
const UPDATE_SEARCH_INPUT = 'UPDATE_SEARCH_INPUT'
const LOCATION_UPDATE = 'LOCATION_UPDATE'
const SET_DYNAMIC_TEXT = 'SET_DYNAMIC_TEXT'
const ADD_SIMILAR_ITEMS = 'ADD_SIMILAR_ITEMS'
const SET_CART_RECOMMENDATION = "SET_CART_RECOMMENDATION"
const SET_CART_ITEMS = 'SET_CART_ITEMS'
const SET_MEDICINE_NOT_AVAIL = "SET_MEDICINE_NOT_AVAIL"
const FETCHING_ACTIVE_LOCATION = 'FETCHING_ACTIVE_LOCATION'
const SET_ACTIVE_LOCATION = 'SET_ACTIVE_LOCATION'
const ACTIVE_LOCATION_FAILUR = 'ACTIVE_LOCATION_FAILUR'
const UPDATE_BULK_AVAILABILITY = 'UPDATE_BULK_AVAILABILITY'
const SET_AVAILPOPOVER_QUERY = 'SET_AVAILPOPOVER_QUERY'
const FETCH_DYNAMIC_TEXT = 'FETCH_DYNAMIC_TEXT'
const SET_SCHEDULE_TIME = 'SET_SCHEDULE_TIME'
const GET_AVAILABILITY = 'GET_AVAILABILITY'
const SET_CART_CAHRGES = 'SET_CART_CAHRGES'
const SET_SCHEDULE_TIME_UNIT = 'SET_SCHEDULE_TIME_UNIT'

export function setPopoverQuery(popover_query) {
	return {
		type: SET_AVAILPOPOVER_QUERY,
		popover_query: popover_query,
	}
}

function setScheduleTimeUnit(schedule_time_unit) {
	return {
		type: SET_SCHEDULE_TIME_UNIT,
		schedule_time_unit: schedule_time_unit,
	}
}

export function setScheduleTime(schedule_time) {
	return {
		type: SET_SCHEDULE_TIME,
		schedule_time: schedule_time,
	}
}

function setCartCharges(cart_charges) {
	return{
		type: SET_CART_CAHRGES,
		cart_charges: cart_charges
	}
}

export function setMedicineNotAvail(notAvailMedicile) {
	return {
		type: SET_MEDICINE_NOT_AVAIL,
		notAvailMedicile: notAvailMedicile,
	}
}

export function setCartItems(cartBucketItem) {
	return {
		type: SET_CART_ITEMS,
		cartBucketItem: cartBucketItem,
	}
}

export function fetchingActiveLocation() {
	return{
		type: FETCHING_ACTIVE_LOCATION,
	}
}

function fetchingActiveLocationFailur() {
	return{
		type: ACTIVE_LOCATION_FAILUR,
	}
}

export function setActiveLocation(userActiveLocation, changeLocation) {
	return {
		type: SET_ACTIVE_LOCATION,
		userActiveLocation: userActiveLocation,
		changeLocation: changeLocation,
	}
}

export function updateBulkAvailability(bulk_Availability_list) {
	return {
		type: UPDATE_BULK_AVAILABILITY,
		bulk_Availability_list: bulk_Availability_list,
	}
}

export function setCartRecommendation(cartRecommendation) {
	return {
		type: SET_CART_RECOMMENDATION,
		cartRecommendation: cartRecommendation
	}
}

export function resteCartStore(argument) {
	return {
		type: RESET_STORE,
	}
}

export function maintainRoutes(route) {
	return {
		type: LOCATION_UPDATE,
		route: route,
	}
}

export function updateSearchInput(searchInput){
	return {
		type: UPDATE_SEARCH_INPUT,
		searchInput: searchInput,
	}
}
/**
 * [fetchingCartOrder initiaize getting order]
 * @return {[object]}
 */
function fetchingCartOrder() {
	return{
		type: 'FETCHING_CART_ORDER',
	}
}

export function handleAdditemPopover(isAddItemPopover){
	return {
		type: 'HANDLE_ADDITEM_POPOVER',
		isAddItemPopover: isAddItemPopover,
	}
}

export function updateImageList(list) {

	return{
		type: 'UPDATE_IMAGE_LIST',
		imageList: list,

	}
}

export function setAdwordFlage(isCallAdword){
	return {
		type: 'SET_ADWORD_FLAGE',
		isCallAdword: isCallAdword,
	}
}

export function fetchingCartOrderSuccess(data) {
	return{
		type: 'FETCHING_CART_ORDER_SUCCESS',
		isFetching: false,
		cartList: data.order,
		availability: data.availability,
		orderId: data.orderId,
		isRequestCall: data.isRequestCall ? true : false,
		items: data.order ? data.order.items : [],
		// deliveryLocationId: data.deliveryLocationId,
		imageList: data.imageList,
		extended_notes: data.extended_notes
	}
}

function fatchingUpdateMedicineCount() {
	return{
		type: 'FETCHING_UPDATE_MEDICINE_COUNT',
	}
}

function setDynamicText(appDynamicText) {
	return {
		type: SET_DYNAMIC_TEXT,
		appDynamicText: appDynamicText,
	}
}

function fetchDynamicText(fetchDynamicLocation) {
	return {
		type: FETCH_DYNAMIC_TEXT,
		fetchDynamicLocation: fetchDynamicLocation,
	}
}

function fatchingUpdateMedicineCountSuccess(items, order) {
	return{
		type: 'FETCHING_UPDATE_MEDICINE_COUNT_SUCCESS',
		items: items,
		order: order,
	}
}

function fatchingUpdateMedicineCountFailur(error) {
	return{
		type: 'FETCHING_UPDATE_MEDICINE_COUNT_FAILUR',
		error: error,
	}
}

function fetchingCartOrderFailur(error) {
	return{
		type: 'FETCHING_CART_ORDER_FAILUR',
		isFetching: false,
		error: error,
	}
}

function updatePromoMessage(error) {
	return{
		type: 'UPDATE_PROMO_MESSAGE',
		promoError: error,
	}
}

/**
 * [setRequestCall set if callcenter guy call]
 * @param {Boolean} isRequestCall [description]
 */
export function setRequestCall(isRequestCall) {
	return{
		type: 'SET_REQUEST_CALL',
		isRequestCall: isRequestCall,
	}
}

export function setItemList(extended_notes) {
	return{
		type: 'SET_ITEM_LIST',
		extended_notes: extended_notes,
	}
}

export function addSearchList(searchList) {
	return{
		type: 'ADD_SEARCH_LIST',
	}
}

function getAvailability(argument) {
	return{
		type: "GET_AVAILABILITY"
	}
}

export function addSearchListSuccess(searchList) {
	return{
		type: 'ADD_SEARCH_LIST_SUCCESS',
		searchList: searchList,
	}
}

export function addSearchListFailur(searchList) {
	return{
		type: 'ADD_SEARCH_LIST_FAILUR',
	}
}

export function addSimilarItems(similarItems) {
	return{
		type: 'ADD_SIMILAR_ITEMS',
		similarItems: similarItems,
	}
}

export function handleSearch(txt) {
	return function (dispatch, store) {
		let user = store().users
		let location = getLocalstorageValue("userLocation")
		console.log("infoe ",user);
		dispatch(addSearchList())
		if(!user.isAuthed){
			return searchHandler(txt, {userID: user.userInfo.id, location_id: location.id, fc_id: location.nearest_fc_id}).then(resp => {
				if(resp.data.status.code === 200){
					
					dispatch(addSearchListSuccess(resp.data.medicines))
					return resp.data.medicines
				}
			})
			.catch(error => {
				console.log('error', error)
				dispatch(addSearchListFailur(resp.data.medicines))
				return resp.data.status.message
			})
		}else{
			return searchHandler2(txt, {userID: user.userInfo.id, location_id: location.id, fc_id: location.nearest_fc_id}).then(resp => {
				if(resp.data.status.code === 200){
					dispatch(addSearchListSuccess(resp.data.medicines))
					return resp.data.medicines
				}
			})
			.catch(error => {
				console.log('error', error)
				dispatch(addSearchListFailur(resp.data.medicines))
				return resp.data.status.message
			})
		}
	}
}

export function handleGetAvailability(query, check_avail) {
	return function (dispatch, store) {
		let user = store().users
		let cart = store().cart
		let location = cart.userActiveLocation
		dispatch(getAvailability())
		return getMedAvailability({...query, ...{fc_id: location.nearest_fc_id, location_id: location.id}}).then(resp => {
			if(resp.data.status.code === 200){
				let similarItems = cart.similarItems
				similarItems[query.dv_id] = resp.data
				let searchList = cart.searchList
				if (searchList && searchList.length) {
					searchList = searchList.map(x => {
						if(x.id == query.dv_id){
							x.avail = resp.data
						}
						return x
					})
					check_avail ? dispatch(addSearchListSuccess(searchList)) : ""

				}
				dispatch(addSimilarItems(resp.data))

				return resp.data
			}
		})
		.catch(error => {
			console.log('error', error)
			// dispatch(addSearchListFailur(resp.data.medicines))
			return error
		})
	}
}


/**
 * [handleRequestCall call back request handler]
 * @return {[type]} [description]
 */
export function handleRequestCall() {
	return function (dispatch, store) {
		let cart = store().cart;
		return updateOrderHandler(cart.orderId, {orders: {order_call_back: cart.isRequestCall}}).then(response => {
			if(response.data.status.code === 401 || response.data.status.code === 403 || response.data.status.code === 400 || response.data.status.code === 499){
				alert(response.data.status.message)
				return dispatch(setRequestCall(!!cart.cartList.call_back_user))
			}else{
				if(!!(response.data.orders.call_back_user) !== cart.isRequestCall){
					return dispatch(setRequestCall(!!(response.data.orders.call_back_user)))
				}else{
					return true
				}
			}
		})
		.catch(error => {
			console.log('error', error)
			alert(error.message)
			return dispatch(setRequestCall('error'))
		})
	}
}

export function handleUpdateItemList (itemList) {
	return function (dispatch, store) {
		dispatch(setItemList(itemList))
		let cart = store().cart;
		let orderId = cart.orderId
		let user = store().users

		if(user.userOnlineStatus == "online"){
			return updateOrderHandler(cart.orderId, {orders: {extended_notes: itemList}}).then(response => {
				if(response.data.status.code === 401 || response.data.status.code === 403 || response.data.status.code === 400 || response.data.status.code === 499){
					alert(response.data.status.message)
					return dispatch(setRequestCall('error'))
				}else{
					updateLocalstorage("userItemsList", [])
					if(response.data.orders && response.data.orders.extended_notes){
						// let orders = response.data.orders
						// dispatch(fetchingCartOrderSuccess({
						// 	order: orders,
						// 	availability: [],
						// 	orderId: orders ? orders.id : '',
						// 	items: orders ? orders.items : [],
						// 	extended_notes: orders.extended_notes ? orders.extended_notes : [],
						// 	deliveryLocationId: orders.delivery_location_id ? orders.delivery_location_id : '',
						// 	isRequestCall: !!orders.call_back_user,
						// 	imageList: orders.prescriptions,
						// }))
						return dispatch(setItemList(response.data.orders.extended_notes))
					}
				}
			})
			.catch(error => {
				console.log('error', error)
				alert(error.message)
				return error
			})
		}else{
			updateLocalstorage("userItemsList", itemList)

		}
	}
}

export function handlePromoCode(code) {
	return function (dispatch) {
		dispatch(updatePromoMessage(''))
		return promoCodeHandler(code).then(resp => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400){
				// console.log(resp.data.status.message)
				return dispatch(updatePromoMessage(resp.data.status.message))
			}
		})
		.catch(error => {
			console.log('error', error)
			return dispatch(updatePromoMessage("Invalid promo code!"))
		})
	}
}


export function handleGetDynamicText(argument) {
	return function (dispatch, store) {
		dispatch(fetchDynamicText(true))
		return getDynamicText().then(x => {
			return dispatch(setDynamicText(x.data))
		})
		.catch((error) => {
			dispatch(fetchDynamicText(false))
		})
	}
}

function setAvailability(response, orders, itemObject, messageFlag) {
	return function (dispatch, store) {
		let cart = store().cart
		let order = JSON.parse(JSON.stringify(orders))
		let itemBucketInfo = response.data.availability_info
		let bucketInfo = cart.appDynamicText.delivery_buckets.value
		bucketInfo = bucketInfo.orderable_delivery_buckets.concat(bucketInfo.unorderable_delivery_buckets)
		var newShowFulfilment = {}

		// flaten the main bucket info
		if(bucketInfo && bucketInfo.length){
			for(let i in bucketInfo){
				newShowFulfilment[bucketInfo[i].bucket_name] = {items: [], bucketInfo: bucketInfo[i]}
			}
		}

		// debugger
		let keys = Object.keys(newShowFulfilment), combineList = {}, current_key = ""
		if(itemBucketInfo && itemBucketInfo.length){
			for (let i in itemBucketInfo) {
				let item = itemBucketInfo[i]
				let availInfo = {...newShowFulfilment[item.delivery_bucket_type].bucketInfo, ...item}
				let pushItem = itemObject[item.drug_variation_id]
				pushItem.availInfo = availInfo
				newShowFulfilment[item.delivery_bucket_type].items.push(pushItem)
				order.items[itemObject[item.drug_variation_id].index].availInfo = availInfo//.concat(itemBucketInfo[i])
			}
			// debugger
			for(let i = keys.length - 1; i >= 0; i--){
				let fulfilList = JSON.parse(JSON.stringify(newShowFulfilment))
				if(fulfilList[keys[i]] && fulfilList[keys[i]].items && fulfilList[keys[i]].items.length){
					if (fulfilList[keys[i]].items[0].availInfo.is_clubbing_supported) {
						current_key = keys[i]
						combineList[current_key] = combineList[current_key] ? combineList[current_key] : {}
						combineList[current_key] = fulfilList[keys[i]]
					}else{
						if (current_key) {
							combineList[current_key]['items'] = combineList[current_key] && combineList[current_key]['items'].length ? combineList[current_key]['items'].concat(fulfilList[keys[i]]['items']) : fulfilList[keys[i]]['items']
						}else{
							combineList[keys[i]] = fulfilList[keys[i]]
						}
					}
				}
			}
		}
		// debugger
		if(messageFlag || (newShowFulfilment && newShowFulfilment['out of stock'] && newShowFulfilment['out of stock'].items.length)){
			let header_message = messageFlag ? cart.appDynamicText.checkout_properties.value.dialog_new_availability : cart.appDynamicText.checkout_properties.value.dialog_change_in_quantity
			dispatch(setMedicineNotAvail({avalHeadernew: header_message, availItems: combineList}))
		}else{
			let cartList = cart.cartList
			cartList.items = order.items
			dispatch(handleGetCartCharges(cartList))
			dispatch(callScheduleTime(combineList))
			dispatch(setCartItems(combineList))
			dispatch(fatchingUpdateMedicineCountSuccess(order.items, cartList))
			dispatch(updateBulkAvailability(response.data.availability_info))
		}
	}
}

export function getAvailabilityInfo(ordr, changeLocation ) {
	return function (dispatch, store) {
		let cart = store().cart
		let location = changeLocation ? changeLocation.location : getLocalstorageValue("userLocation")
		let order = ordr
		let itemObject = {}
		// debugger
		if(!order || !order.items || !location.nearest_fc_id) return
		let item = order.items.map((x, index) => {
						x.index = index
						itemObject[x.medicine.id] = x
						return {dv_id: x.medicine.id, quantity: x.quantity}
					})
		let queryJson = {fc_id: location.nearest_fc_id, location_id: location.id, dv_ids: item}
		if (order.query) queryJson = {...queryJson, ...order.query}
		return getBulkMedAvailability(queryJson).then(response => {
			if(changeLocation && changeLocation.queryString){
				dispatch(setPopoverQuery(changeLocation.queryString))
				let new_avail = response.data.availability_info
				let old_avail = cart.bulk_Availability_list
				if (new_avail && old_avail && (new_avail.length == old_avail.length)) {
					let chkAvail = false
					for(let i in new_avail){
						if(new_avail[i].delivery_bucket_type != old_avail[i].delivery_bucket_type){
							chkAvail = true
							dispatch(setAvailability(response, order, itemObject, "onlyMessage"))
							return
						}
					}
					if(!chkAvail){
						dispatch(handleGetActiveLocation(changeLocation))
					}
				} else if (new_avail && old_avail && (new_avail.length != old_avail.length)) {
					dispatch(setAvailability(response, order, itemObject, "onlyMessage"))
				}else{
					dispatch(handleGetActiveLocation(changeLocation))
				}
			}else{
				dispatch(setAvailability(response, order, itemObject))
			}

		})
		.catch(error => {
			console.log('error', error);
		})
	}
}

/**
 * User Order Actions
 */
export function setAndHandleCartOrder(current_order) {
	return function (dispatch, store) {
		let user = store().users
		let cart = store().cart
		if(user.userOnlineStatus == "online"){
			if(cart.isFetching || (cart.cartList && Object.keys(cart.cartList).length && cart.cartList.id && !current_order)){//
				return Promise.resolve("successful")
			}
			dispatch(fetchingCartOrder())
			return getCartOrder().then(response => {
				let orders = response.data.orders
				let order = JSON.parse(JSON.stringify(orders))
				order.query = {context: "home", sub_context: "refresh", source: "web_auto"}
				current_order ? dispatch(handleGetCartRecommendation(orders)) : ""
				dispatch(getAvailabilityInfo(order))
				current_order ? dispatch(handleGetCartCharges(orders)) : ""
				dispatch(fetchingCartOrderSuccess({
						order: orders,
						orderId: orders ? orders.id : '',
						items: orders ? orders.items : [],
						extended_notes: orders && orders.extended_notes ? orders.extended_notes : [],
						deliveryLocationId: orders && orders.delivery_location_id ? orders.delivery_location_id : '',
						isRequestCall: !!(orders && orders.call_back_user),
						imageList: orders ? orders.prescriptions : [],
					}))
				return orders
			})
			.catch(error => {
				console.log('error', error)
				alert("error: "+error.message)
				return dispatch(fetchingCartOrderFailur(error.message))
			})
		}else{
			let items = getLocalstorageValue("userItems")
			let userItemsList = getLocalstorageValue("userItemsList")
			dispatch(setItemList(userItemsList))
			return dispatch(fatchingUpdateMedicineCountSuccess(items, ""))
		}
	}
}

export function setAndHandleUserLocations() {
	return function (dispatch) {
		dispatch(fetchingUserLocations)
		return getUserLocations().then(response => {
			if(response.data.locations && response.data.locations.length){
				return dispatch(fetchingUserLocationsSuccess(response.data.locations))
			}else{
				return dispatch(fetchingUserLocationsSuccess())
			}

		})
		.catch(error => {
			console.log('error', error)
			alert("error: "+error.message)
			return dispatch(fetchingUserLocationsFailur(error.message))
		})
	}
}

export function handleUpdateMedicineCount(medicine, additem) {
	// debugger
	return function (dispatch, store) {
		let cart = store().cart
		let location = cart.userActiveLocation
		let user = store().users
		let orderId = cart.orderId
		let queryJson = {}
		let medDetail = medicine
		if(user.userOnlineStatus == "online"){
			dispatch(fatchingUpdateMedicineCount())

			//when during logout customer added medicine, just after login add items on user's cart
			if(additem){
				let medList = medicine.map(x => {
					return {id: x.drug_variation_id, added_via_search: true, normalized_quantity: x.selected_quantity, quantity_type_selected: x.medicine.allowed_quantity_types[x.quantity_type_selected_index].label}
				})
				queryJson = {orders: {medicines: medList}}
			}else{
				let packing = {}
				if (medicine.medicine) {
					medDetail = medicine.medicine
					packing = medicine.medicine.packings.filter(x => x.is_saleable)[0]
					medDetail.add_quantity = parseInt(medicine.add_quantity) * parseInt(packing.loose_quantity)
					queryJson = {"orders":{"items": [{
														        "uuid": medicine.medicine.uuid,
														        "source": "user",
														        "id": medicine.id,
														        "loose_quantity": parseInt(medicine.add_quantity) * parseInt(packing.loose_quantity),
														        "normalized_quantity": medicine.add_quantity,
														        "dv_packing_level_id": packing.id,
														      }
														    ]
											}}
				}else{
					medDetail.add_quantity = packing.loose_quantity
					packing = medicine.packings.filter(x => x.is_saleable)[0]
					queryJson = {"orders":{"medicines":[{"added_via_search":true,"dv_packing_level_id":packing.id,"loose_quantity":packing.loose_quantity,"id":medicine.id,"normalized_quantity":medicine.add_quantity ? medicine.add_quantity : 1,"source":"user","uuid":medicine.uuid}]}}
				}

			}
			// debugger
			return dispatch(handleGetAvailability({...medicine.query, ...{dv_id: medDetail.id, quantity: medDetail.add_quantity ? medDetail.add_quantity : 1}})).then(avail => {//medDetail.query
				// debugger
				if(avail.delivery_bucket_type && avail.delivery_bucket_type == "out of stock"){
					// alert("medicine is out of stock!!")
					if (cart.searchList && medicine.query.context == "search") {
						cart.searchList.filter(x => x.id == medDetail.id)[0]["out_of_stock"] = true
						dispatch(addSearchListSuccess(cart.searchList))
					}

					let header_message = cart.appDynamicText.checkout_properties.value.dialog_change_in_quantity
					medicine.update_status == "changeQty" ? dispatch(setMedicineNotAvail({avalHeadernew: header_message, availItems: medicine})) : ""
					return false
				}

				return updateMedicineQuantity(queryJson, {location_id: location.id, fc_id: location.nearest_fc_id}).then(response => {
					if(response.data.orders ){
						medicine.recomend ? dispatch(handleGetCartRecommendation(response.data.orders)) : ""
						let orders = response.data.orders
						let order = JSON.parse(JSON.stringify(orders))
						order.query = medicine.query
						dispatch(getAvailabilityInfo(order))
						updateLocalstorage("userItems", [])
						// return dispatch(fatchingUpdateMedicineCountSuccess(orders.items))
					} else if(response.data && response.data.status && response.data.status.code == 400) {
						alert(response.data.status.message)
						return dispatch(fatchingUpdateMedicineCountFailur(response.data.status.message))
					}

				})
				.catch(error => {
					console.log('error', error)
					alert("error: "+error.message)
					return dispatch(fatchingUpdateMedicineCountFailur(error.message))
				})

				// if (avail && avail.similar_items && avail.similar_items.items && avail.similar_items.items.length) {

				// }else{
				// }
			})

		}else{
			let medInfo = data.orders.medicines ? data.orders.medicines[0] : data.orders.items[0]
			let selecteditem = "", items = cart.items.slice(0), item = {}
			if(data.orders.medicines){
				selecteditem = cart.searchList.filter(x => x.id == medInfo.id)[0]

				item["medicine"] = selecteditem
				item["drug_variation_id"] = selecteditem.id
				item["id"] = selecteditem.id
			}else{
				selecteditem = cart.items.filter(x => x.id == medInfo.id)[0]
				item = selecteditem
			}

			item.selected_quantity = medInfo.normalized_quantity ? medInfo.normalized_quantity : medInfo.quantity

			for(var i in item.medicine.allowed_quantity_types){
				if(item.medicine.allowed_quantity_types[i].label == medInfo.quantity_type_selected){
					item.quantity_type_selected_index = i
				}
			}
			if(!items.length || (items.length && !items.filter(x => x.id == medInfo.id).length)){
				items.push(item)
			}
			updateLocalstorage("userItems", items)
			return dispatch(fatchingUpdateMedicineCountSuccess(items, ""))
		}

	}
}

export function handleRemoveOrderItem(itemId) {
	return function (dispatch, store) {
		let cart = store().cart;
		let user = store().users
		let orderId = cart.orderId;

		if(user.userOnlineStatus == "online"){
			dispatch(fatchingUpdateMedicineCount())
			return removeOrderItem(orderId, itemId).then(response => {
				if(response.data.status.code !== 400 && response.data.status.code !== 403 && response.data.status.code !== 499 && response.data.status.code !== 404){
					let items = cart.items;
					let order = JSON.parse(JSON.stringify(cart.cartList))
					items = items.filter(x => { return x.id !== itemId})
					// updateItem ? dispatch(fatchingUpdateMedicineCountSuccess(items, order)) : ""
					order.items = items
					order.query = {action: "removed", context: "cart", source: "web_manual"}
					dispatch(getAvailabilityInfo(order))
					return items
				}else{
					alert(response.data.status.message)
				}

			})
			.catch(error => {
				console.log('error', error)
				alert("error: "+error.message)
				return dispatch(fatchingUpdateMedicineCountFailur(error.message))
			})
		}else{
			let items = cart.items
			items = items.filter(x => { return x.id !== itemId})
			updateLocalstorage("userItems", items)
			return dispatch(fatchingUpdateMedicineCountSuccess(items, ""))
		}
	}

}

export function handleGetCartRecommendation(data) {
	// debugger
	return function (dispatch, store) {
		let cart = store().cart;
		let location = getLocalstorageValue("userLocation")
		let dv_ids = ""
		if(data.items && data.items.length){
			data.items.filter((x, index) => {
				if(data.items.length == index+1){
					dv_ids += `${x.id}`
				}else{
					dv_ids += `${x.id},`
				}
			})
		}
		if(!location.id) return
		return getCartRecommendation({orderId: data.id, dv_ids: dv_ids, fc_id: location.id}).then(response => {
			dispatch(setCartRecommendation(response.data.suggestions))
		})
		.catch(error => {
			// debugger
			console.log('error', error)
		})
	}
}

export function handleGetCartCharges(order) {
	// debugger
	return function (dispatch, store) {
		let cart = store().cart;
		let location = getLocalstorageValue("userLocation")
		// debugger
		return getCartCharges({orderId: order.id, location_id: location.id, fc_id: location.nearest_fc_id}).then(response => {
			dispatch(setCartCharges(response.data.data))
		})
		.catch(error => {
			debugger
			console.log('error', error)
		})
	}
}

export function handleGetActiveLocation(queryString) {
	return function (dispatch, store) {
		let user = JSON.parse(localStorage.user)
		let cart = store().cart;
		dispatch(fetchingActiveLocation())
		if(user && user.user && user.user.id){
			return getUserActiveLocation(user.user.id, queryString && queryString.queryString ? queryString.queryString : null).then(resp => {
				if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400){
					alert("Please resend!!")
				}else{
					if (isLocalStorageSupported()) {
						updateLocalstorage("userLocation", resp.data.body)
					}
					let order = queryString && queryString.order ? queryString.order : cart.cartList
					dispatch(setActiveLocation(resp.data.body, queryString && queryString.queryString ? true : false))
					order.query = {context: "home", sub_context: "address_change", source: "web_manual"}
					queryString && order ? dispatch(getAvailabilityInfo(order, {location: resp.data.body})) : ""
					return resp.data.body
				}
			})
			.catch(error => {
				dispatch(fetchingActiveLocationFailur())
				debugger
				// if (error.data.status.code === 404) {
				// 	let appDynamicText = cart.appDynamicText
				// 	appDynamicText.checkout_properties.value.dialog_new_availability = "can not find any location for your user and we will redirect to first screen to add a new location"
				// 	dispatch(setDynamicText(appDynamicText))
				// }else if (error.data.status.code === 406) {
				// 	let appDynamicText = cart.appDynamicText
				// 	appDynamicText.checkout_properties.value.dialog_new_availability = "server could not determine the location but choose default location for you."
				// 	dispatch(setDynamicText(appDynamicText))
				// }
				console.log('error', error)
				// alert("Please resend!!")
				// return dispatch(fetchingUserLocationsFailur(error.message))
			})
		}else{
			debugger
		}
	}
}

export function callScheduleTime(bucketInfo) {
	return function (dispatch, store) {
		var value = {}
		for (let j in bucketInfo) {
			let queryString = ""
			let query = bucketInfo[j].items[0].availInfo.timeslot_format
			for (let i in query) {
				queryString += `&${i == "number_of_days" ? "no_of_days" : i}=${i == "number_of_days" ? query[i]+1 : query[i]}`
			}
			dispatch(handleGetSchedulingTime(queryString)).then(x => {
				value[j] = x
				let aa = Object.keys(bucketInfo)
				if (aa[aa.length - 1] == j) {
					dispatch(setScheduleTimeUnit(value))
				}
			})
		}
	}
}

export function handleGetSchedulingTime(queryString) {
	return function (dispatch, store) {
		let cart = store().cart;
    queryString += `&vehicle_id=${cart.cartList.vehicle_id}&type=SCHEDULING`

		return getSchedulingTime(queryString).then(response => {
			if (response && response.data && response.data.slot_details && response.data.slot_details.length) {
				let list = {}
				response.data.slot_details.forEach(x => {
				  if(list[x.date]){
						list[x.date].push(x)
				  }else{
						list[x.date] = [x]
				  }
				})
				return list
			}
		})
		.catch(error => {
			debugger
			console.log('error', error)
		})
	}
}


const initialState = {
	isFetching: false,
	isFetchingMedicine: false,
	isFetchingAvailability: false,
	cartList: {},
	orderId: '',
	isRequestCall: false,
	promoError: '',
	imageList: [],
	extended_notes: [],
	items: [],
	deliveryLocationId: '',
	isCallAdword: false,
	searchList: [],
	searchInput: '',
	appDynamicText: "",
	similarItems: {},
	cartRecommendation:"",
	cartBucketItem: "",
	notAvailMedicile: "",
	userActiveLocation: {},
	bulk_Availability_list: "",
	popover_query: "",
	changeLocation: false,
	fetchDynamicLocation: false,
	schedule_time: "",
	isActiveLocation: false,
	schedule_time_unit: "",
}

export default function cart(state = initialState, action) {
	switch (action.type){
		case RESET_STORE:
			return{
				...state,
				cartList: {},
				orderId: '',
				extended_notes: [],
				isRequestCall: false,
				promoError: '',
				isFetching: false,
				items: [],
				imageList: [],
				isCallAdword: false,
				userActiveLocation: {},
				deliveryLocationId: '',
				bulk_Availability_list: "",
				cartBucketItem: "",
				cart_charges: "",
				appDynamicText: "",
				fetchDynamicLocation: false,
			}
		case FETCHING_CART_ORDER:
			return{
				...state,
				isFetching: true,
				cartList: {},
				items: [],
				orderId: '',
				extended_notes: [],
				imageList: [],
				isRequestCall: false,
				promoError: '',
				error: '',
			}
		case FETCHING_CART_ORDER_SUCCESS:
			return{
				...state,
				isFetching: false,
				cartList: action.cartList,
				orderId: action.orderId,
				extended_notes: action.extended_notes,
				items: action.items,
				isRequestCall: action.isRequestCall,
				deliveryLocationId: action.deliveryLocationId,
				imageList: action.imageList,
			}
		case FETCHING_CART_ORDER_FAILUR:
			return{
				...state,
				isFetching: false,
				error: action.error,
			}
		case SET_REQUEST_CALL:
			return{
				...state,
				isRequestCall: action.isRequestCall
			}
		case UPDATE_PROMO_MESSAGE:
			return{
				...state,
				promoError: action.promoError,
			}
		case UPDATE_IMAGE_LIST:
			return{
				...state,
				imageList: action.imageList,
			}
		case SET_ITEM_LIST:
			return {
				...state,
				extended_notes: action.extended_notes,
			}
		case HANDLE_ADDITEM_POPOVER:
			return {
				...state,
				isAddItemPopover: action.isAddItemPopover,
			}
		case GET_AVAILABILITY:
			return {
				...state,
				isFetchingAvailability: true,
			}
		case FETCHING_UPDATE_MEDICINE_COUNT:
			return{
				...state,
				isFetchingMedicine:true,
				error: '',
			}
		case FETCHING_UPDATE_MEDICINE_COUNT_SUCCESS:
			return{
				...state,
				isFetchingMedicine:false,
				items: action.items,
				cartList: action.order,
			}
		case FETCHING_UPDATE_MEDICINE_COUNT_FAILUR:
			return{
				...state,
				isFetchingMedicine:false,
				error: action.error,
			}
		case SET_ADWORD_FLAGE:
			return{
				...state,
				isCallAdword: action.isCallAdword,
			}
		case ADD_SEARCH_LIST:
			return{
				...state,
				isFetching:true,
				searchList: [],
			}
		case ADD_SEARCH_LIST_SUCCESS:
			return{
				...state,
				isFetchingMedicine:false,
				searchList: action.searchList,
				isFetching:false,
			}
		case ADD_SEARCH_LIST_FAILUR:
			return{
				...state,
				isFetching:false,
			}
		case UPDATE_SEARCH_INPUT:
			return{
				...state,
				searchInput: action.searchInput,
			}
		case LOCATION_UPDATE:
			return{
				...state,
				route: action.route,
			}
		case SET_DYNAMIC_TEXT:
			return{
				...state,
				appDynamicText: action.appDynamicText,
				fetchDynamicLocation: false,
			}
		case FETCH_DYNAMIC_TEXT:
			return{
				...state,
				fetchDynamicLocation: action.fetchDynamicLocation,
			}
		case ADD_SIMILAR_ITEMS:
			return{
				...state,
				isFetchingAvailability: false,
				similarItems: action.similarItems,
			}
		case SET_CART_RECOMMENDATION:
			return{
				...state,
				cartRecommendation: action.cartRecommendation
			}
		case SET_CART_ITEMS:
			return{
				...state,
				cartBucketItem: action.cartBucketItem
			}
		case SET_MEDICINE_NOT_AVAIL:
			return{
				...state,
				notAvailMedicile: action.notAvailMedicile,
			}
		case FETCHING_ACTIVE_LOCATION:
			return{
				...state,
				isActiveLocation: true,
			}
		case SET_ACTIVE_LOCATION:
			return{
				...state,
				isActiveLocation: false,
				userActiveLocation: action.userActiveLocation,
				changeLocation: action.changeLocation,
			}
		case ACTIVE_LOCATION_FAILUR:
			return{
				...state,
				isActiveLocation: false,
			}
		case UPDATE_BULK_AVAILABILITY:
			return{
				...state,
				bulk_Availability_list: action.bulk_Availability_list,
			}
		case SET_AVAILPOPOVER_QUERY:
			return{
				...state,
				popover_query: action.popover_query,
			}
		case SET_SCHEDULE_TIME:
			return{
				...state,
				schedule_time: action.schedule_time,
			}
		case SET_CART_CAHRGES:
			return{
				...state,
				cart_charges: action.cart_charges,
			}
		case SET_SCHEDULE_TIME_UNIT:
			return {
				...state,
				schedule_time_unit: action.schedule_time_unit,
			}
		default:
			return state
	}
}