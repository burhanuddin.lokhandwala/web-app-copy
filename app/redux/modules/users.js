import otpRequest, { logout, login, getStorageUserInfo, handleAuthedUser} from '../../helpers/auth'
import {updateOrderHandler, handleCallMeNow, getUserActiveLocation,updateWalletUser,getAllTransactions} from '../../utils/userApi'
import {getUserLocations} from '../../utils/locationApi'
import { clone, isLocalStorageSupported, createCookie, readCookie } from '../../helpers/helperFunction'
import validations from '../../utils/validations'

const AUTH_USER = 'AUTH_USER'
const UNAUTH_USER = 'UNAUTH_USER'
const FETCHING_USER = 'FETCHING_USER'
const FETCHING_USER_FAILURE = 'FETCHING_USER_FAILURE'
const FETCHING_USER_SUCCESS = 'FETCHING_USER_SUCCESS'
const REMOVE_FETCHING_USER = 'REMOVE_FETCHING_USER'
const FETCHING_OTP = 'FETCHING_OTP'
const FETCHING_OTP_SUCCESS = 'FETCHING_OTP_SUCCESS'
const FETCHING_OTP_FAILURE = 'FETCHING_OTP_FAILURE'
const UPDATE_PHONE_NO = 'UPDATE_PHONE_NO'
const UPDATE_OTP_VALUE = 'UPDATE_OTP_VALUE'
const CALLBACK_COUNTER_CHANGE = 'CALLBACK_COUNTER_CHANGE'
const FETCHING_RESET_OTP_FLAG = 'FETCHING_RESET_OTP_FLAG'
const HANDLE_BACK_BUTTON = 'HANDLE_BACK_BUTTON'
const UPDATE_USER_INFO = 'UPDATE_USER_INFO'
const UPDATE_HISTORY_LOCATION = 'UPDATE_HISTORY_LOCATION'
const CHECK_USER_LOCATION = 'CHECK_USER_LOCATION'
const SET_USER_ONLINE_STATUS = 'SET_USER_ONLINE_STATUS'
const HANDLE_SHOW_SEARCH = 'HANDLE_SHOW_SEARCH'

export function authUser () {
	return {
		type: AUTH_USER,
	}
}

export function handleShowSearch(showSearchFlag, searchOrigin=null) {
	return {
		type: HANDLE_SHOW_SEARCH,
		showSearchFlag: showSearchFlag,
		searchOrigin
	}
}

function unauthUser () {
	return {
		type: UNAUTH_USER,
	}
}

export function checkUserLocation(isLocationFlag) {
	return {
		type: CHECK_USER_LOCATION,
		isLocationFlag: isLocationFlag,
	}
}

export function updatePhone(phone){
	return {
		type: UPDATE_PHONE_NO,
		phone: phone,
	}
}

export function updateOTP(otpValue) {
	return {
		type: UPDATE_OTP_VALUE,
		otpValue: otpValue,
	}
}

function fetchingUser () {
	return {
		type: FETCHING_USER,
	}
}

export function fetchingUserFailure (error) {
	return {
		type: FETCHING_USER_FAILURE,
		error: error,
	}
}

export function updateHistoryLocation(loc){
	return {
		type: UPDATE_HISTORY_LOCATION,
		historyLocation: loc,
	}
}

export function fetchingUserSuccess (userInfo) {
	return {
		type: FETCHING_USER_SUCCESS,
		userInfo: userInfo
	}
}

export function fetchingOTP() {
	return {
		type: FETCHING_OTP,
	}
}

export function fetchingOTPSuccess() {
	return {
		type: FETCHING_OTP_SUCCESS,
	}
}

export function fetchingOTPFailure(error) {
	return {
		type: FETCHING_OTP_FAILURE,
		error: error,
	}
}

export function handleBackButton() {
	return {
		type: HANDLE_BACK_BUTTON,
	}
}

export function updateUserInfo(userInfo) {
	return {
		type: UPDATE_USER_INFO,
		userInfo: userInfo,
	}
}

// export function changeHomepageTab(homepageTabType) {
// 	return {
// 		type: CHANGE_HOMEPAGE_TAB,
// 		homepageTabType: homepageTabType,
// 	}
// }

export function setUserOnlineStatus(userOnlineStatus) {
	return {
		type: SET_USER_ONLINE_STATUS,
		userOnlineStatus: userOnlineStatus,
	}
}

export function handleUpdateUserInfo(data) {
	return function (dispatch, store) {
		let userStore = store().users
		return updateOrderHandler(data).then((resp) => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400 || resp.data.status.code === 404){
				alert("Error: " + resp.data.status.message)
				return dispatch(updateUserInfo(userStore.userInfo))
			}
			return dispatch(updateUserInfo(resp.data.user))
		})
		.catch((error) => dispatch(updateUserInfo(userStore.userInfo)))
	}
}

////******** Handle Call back counter *******////

export function counterHandler(){
	let counter = 30;
	return function (dispatch, store) {
		let storee = store
		let userStore = store().users
		if(!userStore.otp){
			clearInterval(userStore.counterTime);
			return
		}
		var counterTime = setInterval(function() {
				counter--;
				if(counter < 0) {
					dispatch(fetchingResetOPTFlag(counter))
					clearInterval(counterTime);
				} else {
					dispatch(counterChange(counter, counterTime))
				}
			}.bind(this), 1000)

	}
}

export function onCallMeNow() {
	return function (dispatch, store) {
		let userStore = store().users
		return handleCallMeNow({otp_request_type: "call", phone_number: userStore.phone}).then(x => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400){
				alert("Please resend!!")
			}
		})
		.catch(error => {
			console.log('error', error)
			alert("Please resend!!")
			// return dispatch(fetchingUserLocationsFailur(error.message))
		})
	}
}

///***** generate OTP for login *********////

export function getOTP() {
	return function (dispatch, store) {
		let userStore = store().users

		if (!validations.minLength(10).reg.test(userStore.phone) || !validations.numeric().reg.test(userStore.phone)){
			return dispatch(fetchingOTPFailure('Invalid phone number'));
		}

		dispatch(fetchingOTP())
		return otpRequest( {phone_number: userStore.phone, 'otp_request_type': 'sms'}).then((resp) => {
			if(resp.data.status.code === 401 || resp.data.status.code === 403 || resp.data.status.code === 400){
				return dispatch(fetchingOTPFailure(resp.data.status.message))
			}
			return dispatch(fetchingOTPSuccess())
		})
		.catch((error) => {
			console.log('error', error)
			return dispatch(fetchingOTPFailure(error.message))
		})
	}
}

///// ************* Request for LogIn ************//////
/**
 * [fetchAndHandleAuthedUser description]
 * @return {[type]} [description]
 */
export function fetchAndHandleAuthedUser () {
	return function (dispatch, store) {
		let userStore = store().users

		if ((userStore.otpValue).length !== 6 || !validations.numeric().reg.test(userStore.otpValue)){
			dispatch(fetchingUserFailure('OTP is invalid!!'));
			return Promise.reject("");
		}

		var isError = false;

		dispatch(fetchingUser())
		return login({phone_number: userStore.phone, 'otp': userStore.otpValue}).then((resp) => {
			if(resp.data.status.code === 200){
			 	dispatch(setUserOnlineStatus("online"))
				// let user = {"status":{"message":"OK","code":200},"access_token":"38f91077b44f4f98b07837735b2b8df2","fc":[{"vehicle_type_id":1,"location":{"latitude":12.964939,"longitude":77.599551,"address":"Richmond Road Godown"},"id":3,"registration_number":null,"label":"Richmond Road"},{"vehicle_type_id":1,"location":{"latitude":12.982813,"longitude":77.614311,"address":"Lingrajpuram godown, 52, Mayura Road"},"id":1,"registration_number":null,"label":"Lingarajpuram"},{"vehicle_type_id":1,"location":{"latitude":12.994986,"longitude":77.575884,"address":"Malleshwaram godown, J 92, 2nd Cross, Anjaneya Block"},"id":2,"registration_number":null,"label":"Malleshwaram"}],"user":{"phone_number":919916435702,"display_id":12838,"is_suspended":null,"is_on_break":true,"channels":["delivery_dashboard"],"current_phone_number":919916435702,"otp_secret_key":"6YQRFAK32VPGGAOT","id":2836,"break":null,"vehicle_id":null,"extra_details":{"imei":null,"sim_number":null},"name":"Anirudh Coontoor","max_amount":null,"hide_if_inactive":false,"role_id":1,"role":{"id":1,"name":"Admin"},"non_performer_reason":null,"is_on_duty":true,"is_available_for_work":false},"channel_uuid":"d2b1bd2c4c6f43acaec461df64d18778","accessToken":"66949e860aab414f94b07383b61b8870"}
			 	if(isLocalStorageSupported()){
			 		localStorage.user = JSON.stringify(resp.data);
			 	}else{
			 		createCookie("user", JSON.stringify(resp.data), 1)
			 	}
			 	return dispatch(fetchingUserSuccess(resp.data.user))
			}
			else if(resp.data.status.code === 401){
			 	isError = true;
				return dispatch(fetchingUserFailure('Incorrect OTP'))
			}
			else{
				isError = true;
				return dispatch(fetchingUserFailure(resp.data.status.message))
			}
		})
		.then(() => {
			if(!isError){
				clearInterval(userStore.counterTime);
				return dispatch(authUser())
			}
		})
		.catch((error) => dispatch(fetchingUserFailure(error)))
	}
}

/**
 * [handleGetUserLocations get uesr location]
 * @return {[type]} [description]
 */
export function handleGetUserLocations(){
	return function (dispatch) {
		return getUserLocations().then(address => {
			(address.data.locations && address.data.locations.length > 0 ) ? '' : dispatch(checkUserLocation(true))
			return address.data
				// let availLocation = address.data.locations.filter(x => { return x.delivery_available === true})
				// let unAvailLocation = address.data.locations.filter(x => { return x.delivery_available === false})
				// console.log('avainlable_location', avainlable_location, unAvainlable_location)
				// dispatch(deliveryFailedLocation({availLocation: availLocation, unAvailLocation: unAvailLocation}))
		})
		.catch(error => {
			console.log('error', error)
			// return dispatch(fetchingUserLocationsFailur(error.message))
		})
	}
}

//// ******* Logout handler *********////
export function logoutAndUnauth () {
	return function (dispatch) {
		dispatch(setUserOnlineStatus("offline"))
		// dispatch(changeHomepageTab("recomended"))
		logout()
		dispatch(unauthUser())
	}
}

export function removeFetchingUser () {
	return {
		type: REMOVE_FETCHING_USER
	}
}

export function userWalletUpdate(queryJson){
	return function (dispatch){
		return updateWalletUser(queryJson).then((resp)=> {
			// console.log("inside the success case for api call",resp)
			return resp.data
		}).catch((err)=>{
			// console.log("inside the error case", err)
			alert("Something Went Wrong! Please Try Again Later " + err)
		})
	}
}

export function getPastTransactions(){
	return function (dispatch){
		return getAllTransactions().then((resp)=>{
			// console.log("isnide the success ccase for api call",resp.data.gift_cards)
			let updated_resp = []
			resp.data.gift_cards.forEach((element) => {
				let month = new Date(element.entry_timestamp*1000).getMonth()
				let year =  new Date(element.entry_timestamp*1000).getFullYear()
				let gift_cards = element
				let someFlag = true
				updated_resp.forEach(item => {
					if(item.month == month && item.year == year){
						someFlag = false
						item.gift_cards.push(element)
					}
				});

				if(someFlag){
					let month_transactions = {
						month : month,
						year : year,
						gift_cards:[element]
					}
					updated_resp.push(month_transactions)
				}
				// console.log(month,year)
			});
			// console.log(updated_resp)
			return updated_resp
		}).catch((error)=>{
			console.log("isnide the error case",error)
		})
	}
}

export function phonepeUserLogin(resp,pathname){
	// alert(pathname);
	resp.channel_uuid = resp.user.channel_uuid
	// alert("inside the fun"+JSON.stringify(resp,getCircularReplacer()));
	return new Promise((resolve,reject)=>{
		// alert(window.localStorage.user);
		if(window.localStorage.user == undefined){ 
			window.localStorage.setItem('user',JSON.stringify(resp))
			// alert(JSON.stringify(window))
			// alert(pathname.split('/')[1] == "track")
			// (pathname.split('/')[1] !== "track") ?
			// 	window.location.reload():null
			resolve();
			
			// setTimeout(()=>{
			// 	// alert(JSON.stringify(window.location))
			// 	window.location.reload()
			// },1000)
			// resolve();
		}else{
			
			// alert(window.localStorage.user)
			reject()
		}
	})
	.then((resp)=>{
		
		(pathname.split('/')[1] !== "track") ?
				window.location.reload():null
		// alert(JSON.stringify(window))
		// if(window) 
		// 	window.location.reload()
		// else{
			// setInterval(()=>{
			// 	window.location.href = window.location.origin+pathname;
			// },2000)
		// }
	}).catch((err)=>{
		// setTimeout(()=>{
		// 	window.location.reload()
		// },3000)
		// alert("promsise rejected"+err)
	})

	
	// return function(dispatch){
	// 	dispatch(setUserOnlineStatus("online"))
	// 	// let user = {"status":{"message":"OK","code":200},"access_token":"38f91077b44f4f98b07837735b2b8df2","fc":[{"vehicle_type_id":1,"location":{"latitude":12.964939,"longitude":77.599551,"address":"Richmond Road Godown"},"id":3,"registration_number":null,"label":"Richmond Road"},{"vehicle_type_id":1,"location":{"latitude":12.982813,"longitude":77.614311,"address":"Lingrajpuram godown, 52, Mayura Road"},"id":1,"registration_number":null,"label":"Lingarajpuram"},{"vehicle_type_id":1,"location":{"latitude":12.994986,"longitude":77.575884,"address":"Malleshwaram godown, J 92, 2nd Cross, Anjaneya Block"},"id":2,"registration_number":null,"label":"Malleshwaram"}],"user":{"phone_number":919916435702,"display_id":12838,"is_suspended":null,"is_on_break":true,"channels":["delivery_dashboard"],"current_phone_number":919916435702,"otp_secret_key":"6YQRFAK32VPGGAOT","id":2836,"break":null,"vehicle_id":null,"extra_details":{"imei":null,"sim_number":null},"name":"Anirudh Coontoor","max_amount":null,"hide_if_inactive":false,"role_id":1,"role":{"id":1,"name":"Admin"},"non_performer_reason":null,"is_on_duty":true,"is_available_for_work":false},"channel_uuid":"d2b1bd2c4c6f43acaec461df64d18778","accessToken":"66949e860aab414f94b07383b61b8870"}
	// 	alert(isLocalStorageSupported())
	// 	if(isLocalStorageSupported()){
	// 	}else{
	// 		createCookie("user", JSON.stringify(resp.data), 1)
	// 	}
	// 	return dispatch(fetchingUserSuccess(resp.data.user))
	// }
}

function counterChange(count, counterTime) {
	return{
		type: CALLBACK_COUNTER_CHANGE,
		callbackCount: count,
		counterTime: counterTime
	}
}

function fetchingResetOPTFlag () {
	return{
		type: FETCHING_RESET_OTP_FLAG,
		isResetOPTFlag: true,
	}
}

const initialUserState = {
	userInfo:{
		name: "",
		access_token: "",
	}
}

function user (state = initialUserState, action) {
	switch (action.type) {
		case FETCHING_USER_SUCCESS :
			return {
				...state,
				userInfo: action.userInfo,
				isFetching: true,
			}
		default :
			return state
	}
}

const initialState = {
	isFetching: false,
	error: '',
	isAuthed: false,
	authedId: '',
	otp: false,
	phone: '',
	otpValue: '',
	callbackCount: 30,
	isResetOPTFlag: false,
	userInfo: {},
	counterTime: '',
	historyLocation: '',
	isLocationFlag: false,
	showSearchFlag: false,
	searchOrigin: '',
	userOnlineStatus: window.localStorage && window.localStorage.user ? 'online' : 'offline',
}

export default function users (state = initialState, action) {
	switch (action.type) {
		case FETCHING_OTP:
			return {
				...state,
				isFetching: true,
				error: '',
			}
		case FETCHING_OTP_SUCCESS:
			return {
				...state,
				otp: true,
				isFetching: false,
				error: '',
			}
		 case FETCHING_OTP_FAILURE:
			return {
				...state,
				otp: false,
				otpValue: "",
				isFetching: false,
				error: action.error,
			}
		case UPDATE_PHONE_NO:
			return{
				...state,
				phone: action.phone,
				error: '',
			}
		case UPDATE_OTP_VALUE:
			return{
				...state,
				otpValue: action.otpValue,
				error: '',
			}
		case AUTH_USER :
			return {
				...state,
				isAuthed: true,
				isFetching: false,
				error: '',
				// authedId: action.uid,
			}
		case UNAUTH_USER :
			return {
				...state,
				isAuthed: false,
				otp: false,
				isFetching: false,
				userInfo: {},
			}
		case FETCHING_USER:
			return {
				...state,
				isFetching: true,
				error: '',
			}
		case FETCHING_USER_FAILURE:
			return {
				...state,
				isFetching: false,
				otpValue: "",
				error: action.error,
			}
		case REMOVE_FETCHING_USER :
			return {
				...state,
				isFetching: false,
			}
		case FETCHING_USER_SUCCESS:

			return action.userInfo === null
				? {
					...state,
					isFetching: false,
					error: '',
				}
				: {
					...state,
					isFetching: true,
					error: '',
					userInfo: action.userInfo,
				}
		case FETCHING_RESET_OTP_FLAG:
			return{
				...state,
				isResetOPTFlag: action.isResetOPTFlag,
			}
		case HANDLE_BACK_BUTTON:
			return{
				...state,
				otp: false,
				callbackCount: 30,
				phone: '',
				error: '',
			}
		case CALLBACK_COUNTER_CHANGE:
			return {
				...state,
				callbackCount: action.callbackCount,
				counterTime: action.counterTime,
			}
		case UPDATE_USER_INFO:
			return {
				...state,
				userInfo: action.userInfo,
			}
		case UPDATE_HISTORY_LOCATION:
			return {
				...state,
				historyLocation: action.historyLocation,
			}
		case CHECK_USER_LOCATION:
			return {
				...state,
				isLocationFlag: action.isLocationFlag,
			}
		// case CHANGE_HOMEPAGE_TAB:
		// 	return {
		// 		...state,
		// 		homepageTabType: action.homepageTabType,
		// 	}
		case SET_USER_ONLINE_STATUS:
			return{
				...state,
				userOnlineStatus: action.userOnlineStatus,
			}
		case HANDLE_SHOW_SEARCH:
			return{
				...state,
				showSearchFlag: action.showSearchFlag,
				searchOrigin: action.searchOrigin
			}
		default :
			return state
	}
}
