import {deleteImageHandler} from '../../utils/ordersApi'
import {createStore} from 'redux'

const ADD_NEW_IMAGE = 'ADD_NEW_IMAGE'
const DELETE_IMAGE = 'DELETE_IMAGE'
const UPDATE_IMAGE_SUCCESS = 'UPDATE_IMAGE_SUCCESS'
const UPDATE_IMAGE_FAILUR = 'UPDATE_IMAGE_FAILUR'

function addNewImage(argument) {

	return {
		type: ADD_NEW_IMAGE,
	}
}

function deleteImage() {
	return{
		type: DELETE_IMAGE,
	}
}

function updateImageSuccess(argument) {
	return{
		type: UPDATE_IMAGE_SUCCESS,
	}
}

function updateImageFailur(error) {
	return{
		type: UPDATE_IMAGE_FAILUR,
	}
}

export function handleDeleteImage(data) {
	return function (dispatch, store) {
		let imageList = store().cart.imageList;
		let updatedImageList = imageList.filter(x => x.id != data.pId )
		return deleteImageHandler(data).then((resp) => {
			// console.log('create order: ', resp)
			return updatedImageList
			// dispatch(updateImageSuccess(resp))
		})
		.catch((error) => {
			console.log('error', error)
			dispatch(updateImageFailur(error.message))
		})
	}
}

const initialState ={
	isUpdating: false,
	error: '',
}

export default function prescriptionImage(state = initialState, action) {
	switch(action.type){
		case ADD_NEW_IMAGE:
			return{
				...state,
				error: '',
			}
		case DELETE_IMAGE:
			return{
				...state,
				error: '',
			}
		case UPDATE_IMAGE_SUCCESS:
			return{
				...state,
			}
		case UPDATE_IMAGE_FAILUR:
			return{
				...state,
				error: action.error,
			}
	}
}