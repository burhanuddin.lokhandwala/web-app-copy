import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import getRoutes from './config/routes'
// import config from './config/config'
import { checkIfAuthed, getStorageUserInfo} from './helpers/auth'
import * as reducers from './redux/modules'
import {getConfigMode} from './config/config'
// import * as phonePe from '$APP/phonepe.js'

// const phonepe = require('./phonepe.js');

import Clevertap from './helpers/clevertap'
import phonePe from './helpers/phonepeFunction';
Clevertap.initialize(getConfigMode().serverMode.mode == ('devServer' || 'debug') ? 'TEST-587-W99-KW5Z' : '487-W99-KW5Z') /*'TEST-587-W99-KW5Z'*/
 // if ('serviceWorker' in navigator) {
 //   window.addEventListener('load', () => {
 //     navigator.serviceWorker.register('/service-worker.js').then(registration => {
 //       console.log('SW registered: ', registration);
 //     }).catch(registrationError => {
 //       console.log('SW registration failed: ', registrationError);
 //     });
 //   });
 // }
//  console.log(phonepe);

// console.log("checking the instance",phonePe, window.PhonePe);
// phonePe.instanceCreate()


// window.PhonePe.PhonePe.build(PhonePe.Constants.Species.web)
// 	.then((sdk)=>{
// 		// alert.log("successfull");
// 		sdk.fetchGrantToken().then((res) => {
//             alert(JSON.stringify(res));
//           }).catch((err) => {
//             console.log("Error occured while fetching the grant token")
//           })
// 	}).catch(()=>{
// 		console.log("error in creating the instance for check");
// 	})


 
  
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk),
  // other store enhancers if any
);
const store = createStore(combineReducers(reducers), enhancer);

// function checkAuth (nextState, replace) {
// 	debugger
// 	if (store.getState().users.isFetching === true || (getStorageUserInfo())) {
// 		return
// 	}

// 	const isAuthed = checkIfAuthed(store)
// 	const nextPathName = nextState.location.pathname
// 	if (nextPathName === '/' || nextPathName === '/login') {
// 		if (isAuthed === true && getStorageUserInfo()) {
// 			history.back()
// 			// replace('/prescription')
// 		}else{
// 			return
// 		}
// 	} else {
// 		if (isAuthed !== true) {
// 			// console.log('context', this)
// 			replace('/login')
// 		}
// 	}
// }

ReactDOM.render(
	<Provider store={store}>
		{getRoutes()}
	</Provider>,
document.getElementById('app'))
